<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnnouncementsAllowWithoutExperienceANDAnnouncementsRequiredDocTypeToAnnouncementsTable extends Migration
{

    public function up()
    {
        Schema::table('Announcements', function (Blueprint $table) {
            $table->boolean('announcementsAllowWithoutExperience')->after('AnnouncementsAgeUntil')->default(0);
            $table->tinyInteger('announcementsRequiredDocType')->after('announcementsAllowWithoutExperience')->comment('1=All Documents, 2=One of the doc, 3=Tow of the doc')->default(2);
        });
    }

    public function down()
    {
        Schema::table('Announcements', function (Blueprint $table) {
            $table->dropColumn('announcementsAllowWithoutExperience');
            $table->dropColumn('announcementsRequiredDocType');
        });
    }

}