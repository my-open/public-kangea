<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkAnnouncementsIDToSharesTbl extends Migration
{

    public function up()
    {
        Schema::table('Shares', function($table)
        {
            $table->integer('fkAnnouncementsID')->after('pkSharesID');
        });
    }

    public function down()
    {
        Schema::table('Shares', function ($table) {
            $table->dropColumn('fkAnnouncementsID');
        });
    }

}
