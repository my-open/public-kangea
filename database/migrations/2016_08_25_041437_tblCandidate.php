<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCandidate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Candidates', function (Blueprint $table) {
            $table->increments('pkCandidatesID');

            $table->integer('fkCompaniesID');
            $table->integer('fkAnnouncementsID');
            $table->integer('fkUsersID');

            $table->string('candidatesDescription', 500);
            $table->boolean('seekerStatus')->default(0);
            $table->boolean('candidatesStatus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Candidates');
    }
}
