<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsIsCityIsDistrictIsCommuneToLocationTable extends Migration
{
    public function up()
    {
        Schema::table('Provinces', function ($table) {
            $table->boolean('Iscity')->default(0)->after('provincesNameTH');
        });

        Schema::table('Districts', function ($table) {
            $table->boolean('IsDistrict')->default(0)->after('districtsNameTH');
        });

        Schema::table('Communes', function ($table) {
            $table->boolean('IsCommune')->default(0)->after('CommunesNameTH');
        });
    }

    public function down()
    {
        Schema::table('Provinces', function ($table) {
            $table->dropColumn('Iscity');
        });

        Schema::table('Districts', function ($table) {
            $table->dropColumn('IsDistrict');
        });

        Schema::table('Communes', function ($table) {
            $table->dropColumn('IsCommune');
        });
    }

}
