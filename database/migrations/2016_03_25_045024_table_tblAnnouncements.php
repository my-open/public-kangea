<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblAnnouncements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Create table Announcements "tblAnnouncements"
        Schema::create('Announcements', function (Blueprint $table) {
          $table->increments('pkAnnouncementsID');
          $table->integer('fkCompaniesID');

          $table->integer('fkSectorsID');
          $table->integer('fkSubsectorsID');
          $table->integer('fkActivitiesID')->nullable();
          $table->integer('fkPositionsID');
          $table->string('fkPositionsCode', 100);

          $table->integer('fkCountriesID');
          $table->integer('fkProvincesID');
          $table->integer('fkDistrictsID');
          $table->integer('fkCommunesID');
          $table->integer('fkVillagesID');
          $table->integer('fkZonesID');

          $table->string('announcementsTitleEN', 150);
          $table->string('announcementsTitleKH', 150);

          $table->string('announcementsHiring', 30);
          $table->string('announcementsSalaryFrom', 100);
          $table->string('announcementsSalaryTo', 100);
          $table->string('announcementsSalaryType', 10);
          $table->string('AnnouncementsSalaryDependsOn', 50);
          $table->string('AnnouncementsFromDay', 3);
          $table->string('AnnouncementsToDay', 3);

          $table->string('AnnouncementsExperiencePositionN', 2);
          $table->string('AnnouncementsPositionType', 10);
          $table->string('AnnouncementsExperienceSectorN', 2);
          $table->string('AnnouncementsSectorType', 10);

          $table->string('AnnouncementsExperienceOrAnd', 5);

          $table->text('announcementsCertificate');
          $table->string('announcementsBenefit', 500);
          $table->string('announcementsLanguage');
          $table->string('announcementsLanguageLevel', 200);

          $table->string('AnnouncementsFromHour', 2);
          $table->string('AnnouncementsFromMinute', 2);
          $table->string('AnnouncementsFromType', 2);

          $table->string('AnnouncementsToHour', 2);
          $table->string('AnnouncementsToMinute', 2);
          $table->string('AnnouncementsToType', 2);

          $table->string('AnnouncementsBreakHoursFromHour', 2);
          $table->string('AnnouncementsBreakHoursFromMinute', 2);
          $table->string('AnnouncementsBreakHoursFromType', 2);

          $table->string('AnnouncementsBreakHoursToHour', 2);
          $table->string('AnnouncementsBreakHoursToMinute', 2);
          $table->string('AnnouncementsBreakHoursToType', 2);


          $table->string('AnnouncementsGender', 6);
          $table->string('AnnouncementsAgeFrom', 2);
          $table->string('AnnouncementsAgeUntil', 2);

          $table->dateTime('announcementsPublishDate');
          $table->dateTime('announcementsClosingDate');

          $table->boolean('announcementsStatus')->default(0);

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('Announcements');
    }
}
