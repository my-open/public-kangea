<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblCountriesTblProvincesTblDistrictsTblCommunesTblVillages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
          //Create table Countries
          Schema::create('Countries', function (Blueprint $table) {
            $table->integer('pkCountriesID')->primary();

            $table->string('countriesNameEN', 100);
            $table->string('countriesNameKH', 100);

            $table->smallInteger('countriesStatus');
            $table->timestamps();
          });

          //Create table tblProvinces
         Schema::create('Provinces', function (Blueprint $table) {
           $table->integer('pkProvincesID')->primary();
           $table->integer('fkCountriesID');

           $table->string('provincesNameEN', 50);
           $table->string('provincesNameKH', 50);

           $table->smallInteger('provincesStatus');
           $table->timestamps();
         });

         //Create table tblDistricts
         Schema::create('Districts', function (Blueprint $table) {
           $table->integer('pkDistrictsID')->primary();
           $table->integer('fkProvincesID');

           $table->string('districtsNameEN', 28);
           $table->string('districtsNameKH', 28);

           $table->smallInteger('districtsStatus');
           $table->timestamps();
         });

         //Create table tblCommunes
         Schema::create('Communes', function (Blueprint $table) {
           $table->integer('pkCommunesID')->primary();
           $table->integer('fkDistrictsID');

           $table->string('CommunesNameEN', 50);
           $table->string('CommunesNameKH', 50);

           $table->smallInteger('communesStatus');
           $table->timestamps();
         });

         //Create table tblVillages
        Schema::create('Villages', function (Blueprint $table) {
          $table->integer('pkVillagesID')->primary();
          $table->integer('fkCommunesID');

          $table->string('villagesNameEN', 50);
          $table->string('villagesNameKH', 50);

          $table->smallInteger('villagesStatus');
          $table->timestamps();
        });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('Countries');
       Schema::drop('Provinces');
       Schema::drop('Districts');
       Schema::drop('Communes');
       Schema::drop('Villages');
     }
 }
