<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompaniesStatusToCompaniesTable extends Migration
{

    public function up()
    {
        Schema::table('Companies', function (Blueprint $table) {
            $table->boolean('companiesStatus')->after('companiesSite');
        });
    }

    public function down()
    {
        Schema::table('Companies', function (Blueprint $table) {
            $table->dropColumn('companiesStatus');
        });
    }

}
