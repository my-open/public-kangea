<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLogsTalbe extends Migration
{

    public function up()
    {
        Schema::create('UserLogs', function (Blueprint $table) {
            $table->increments('pkUserLogsID');
            $table->integer('fkUsersID');

            $table->text('userLogsActivity');
            $table->text('userLogsActivityDescription');
            $table->string('usersLogsIP', 100);
            $table->string('usersLogsBrowser', 50);
            $table->string('usersLogsOS', 50);
            $table->string('usersLogsDeviceType', 30)->comment('phone, computer');
            $table->string('usersLogsDeviceModel', 60);
            $table->tinyInteger('usersLogsStatus')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('UserLogs');
    }
}