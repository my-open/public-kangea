<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Activities', function (Blueprint $table) {
            $table->increments('pkActivitiesID');

            $table->integer('fkSubsectorsID');
            $table->string('activitiesNameEN', 150);
            $table->string('activitiesNameKH', 150);
            $table->integer('activitiesOrder')->default(0);
            $table->boolean('activitiesStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Activities');
    }
}
