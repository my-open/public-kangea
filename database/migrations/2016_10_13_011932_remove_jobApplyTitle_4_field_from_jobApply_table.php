<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveJobApplyTitle4FieldFromJobApplyTable extends Migration
{

    public function up()
    {
        Schema::table('JobApply', function ($table) {
            $table->dropColumn('jobApplyTitleEN');
            $table->dropColumn('jobApplyTitleKH');
            $table->dropColumn('jobApplyTitleZH');
            $table->dropColumn('jobApplyTitleTH');
        });
    }

}
