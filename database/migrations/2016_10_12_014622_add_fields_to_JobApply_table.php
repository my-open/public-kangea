<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToJobApplyTable extends Migration
{
    public function up()
    {
        Schema::table('JobApply', function($table)
        {
            $table->integer('fkSharesID')->after('fkAnnouncementsID');

            $table->integer('jobApplyExperience')->comment('0=None, 1=Yes, 2=No')->after('jobApplyCvFile');
            $table->boolean('jobApplyInteresting')->default(0)->after('jobApplyExperience');
            $table->boolean('jobApplyCallToInterview')->default(0)->after('jobApplyInteresting');
            $table->boolean('jobApplyCallAndReject')->default(0)->after('jobApplyCallToInterview');
            $table->boolean('jobApplyRejected')->default(0)->after('jobApplyCallAndReject');

            $table->boolean('jobApplyHired')->default(0)->after('jobApplyRejected');
            $table->boolean('jobApplyDuplicate')->default(0)->after('jobApplyHired');
            $table->boolean('jobApplyBy')->default(0)->after('jobApplyDuplicate')->comment('1=web, 2=phone, 3=facebook');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('JobApply', function ($table) {
            $table->dropColumn('fkSharesID');

            $table->dropColumn('jobApplyExperience');
            $table->dropColumn('jobApplyInteresting');
            $table->dropColumn('jobApplyCallToInterview');
            $table->dropColumn('jobApplyCallAndReject');
            $table->dropColumn('jobApplyRejected');

            $table->dropColumn('jobApplyHired');
            $table->dropColumn('jobApplyDuplicate');
            $table->dropColumn('jobApplyBy');
        });
    }
}
