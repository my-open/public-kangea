<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTblCountriesTblProvincesTblDistrictsTblCommunesTblVillages extends Migration
{

    public function up()
    {
        // Add 2 Fields to table Countries
        Schema::table('Countries', function($table)
        {
            $table->string('countriesNameZH', 150)->after('countriesNameKH');
            $table->string('countriesNameTH', 150)->after('countriesNameZH');
        });

        // Add 2 Fields to table tblProvinces
        Schema::table('Provinces', function($table)
        {
            $table->string('provincesNameZH', 150)->after('provincesNameKH');
            $table->string('provincesNameTH', 150)->after('provincesNameZH');
        });

        // Add 2 Fields to table tblDistricts
        Schema::table('Districts', function($table)
        {
            $table->string('districtsNameZH', 150)->after('districtsNameKH');
            $table->string('districtsNameTH', 150)->after('districtsNameZH');
        });

        // Add 2 Fields to table tblCommunes
        Schema::table('Communes', function($table)
        {
            $table->string('CommunesNameZH', 150)->after('CommunesNameKH');
            $table->string('CommunesNameTH', 150)->after('CommunesNameZH');
        });

        // Add 2 Fields to table tblVillages
        Schema::table('Villages', function($table)
        {
            $table->string('villagesNameZH', 150)->after('villagesNameKH');
            $table->string('villagesNameTH', 150)->after('villagesNameZH');
        });

    }

    public function down()
    {
        // Remove 2 Fields to table Countries
        Schema::table('Countries', function ($table) {
            $table->dropColumn('countriesNameZH');
            $table->dropColumn('countriesNameTH');
        });

        // Remove 2 Fields to table tblProvinces
        Schema::table('Provinces', function ($table) {
            $table->dropColumn('provincesNameZH');
            $table->dropColumn('provincesNameTH');
        });

        // Remove 2 Fields to table tblDistricts
        Schema::table('Districts', function ($table) {
            $table->dropColumn('districtsNameZH');
            $table->dropColumn('districtsNameTH');
        });

        // Remove 2 Fields to table tblCommunes
        Schema::table('Communes', function ($table) {
            $table->dropColumn('CommunesNameZH');
            $table->dropColumn('CommunesNameTH');
        });

        // Remove 2 Fields to table tblVillages
        Schema::table('Villages', function ($table) {
            $table->dropColumn('villagesNameZH');
            $table->dropColumn('villagesNameTH');
        });

    }
}