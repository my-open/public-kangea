<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('JobLogs', function (Blueprint $table) {
            $table->increments('pkJobLogsID');
            $table->integer('fkUsersID');
            $table->integer('fkAnnouncementsID');
            $table->string('jobLogsType', 30)->comment('view, shareFB, favorite');
            $table->string('jobLogsIP', 100);
            $table->string('jobLogsBrowser', 50);
            $table->string('jobLogsOS', 50);
            $table->string('jobLogsDeviceType', 30)->comment('phone, computer');
            $table->string('jobLogsDeviceModel', 60);
            $table->tinyInteger('jobLogsStatus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('JobLogs');
    }
}
