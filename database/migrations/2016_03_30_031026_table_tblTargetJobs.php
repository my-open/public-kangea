<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblTargetJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
        Schema::create('TargetJobs', function (Blueprint $table) {
            $table->increments('pkTargetJobsID');
            $table->integer('fkUsersID');
            $table->integer('fkCountriesID');
            $table->integer('fkProvincesID');
            $table->integer('fkDistrictsID');
            $table->integer('fkCommunesID');
            $table->integer('fkVillagesID');
            $table->integer('fkZonesID');
            $table->integer('fkSectorsID');
            $table->integer('fkSubsectorsID');
            $table->integer('fkPositionsID');
            $table->timestamps();
        });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('TargetJobs');
     }
  }
