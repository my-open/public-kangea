<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTblCompanies extends Migration
{

    public function up()
    {
        // Add 4 Fields to table Companies
        Schema::table('Companies', function($table)
        {
            $table->string('companiesNameZH', 150)->after('companiesNameKH');
            $table->string('companiesNameTH', 150)->after('companiesNameZH');

            $table->string('companiesDescriptionZH', 150)->after('companiesDescriptionKH');
            $table->string('companiesDescriptionTH', 150)->after('companiesDescriptionZH');
        });
    }

    public function down()
    {
        // Remove 4 Fields to table Companies
        Schema::table('Companies', function ($table) {
            $table->dropColumn('companiesNameZH');
            $table->dropColumn('companiesNameTH');

            $table->dropColumn('companiesDescriptionZH');
            $table->dropColumn('companiesDescriptionTH');
        });
    }
}
