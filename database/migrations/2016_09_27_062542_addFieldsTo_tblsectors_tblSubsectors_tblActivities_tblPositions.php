<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTblsectorsTblSubsectorsTblActivitiesTblPositions extends Migration
{

    public function up()
    {
        // Add 2 Fields to table Sectors
        Schema::table('Sectors', function($table)
        {
            $table->string('sectorsNameZH', 150)->after('sectorsNameKH');
            $table->string('sectorsNameTH', 150)->after('sectorsNameZH');
        });

        // Add 2 Fields to table subsectors
        Schema::table('Subsectors', function($table)
        {
            $table->string('subsectorsNameZH', 150)->after('subsectorsNameKH');
            $table->string('subsectorsNameTH', 150)->after('subsectorsNameZH');
        });

        // Add 2 Fields to table Activities
        Schema::table('Activities', function($table)
        {
            $table->string('activitiesNameZH', 150)->after('activitiesNameKH');
            $table->string('activitiesNameTH', 150)->after('activitiesNameZH');
        });

        // Add 2 Fields to table Positions
        Schema::table('Positions', function($table)
        {
            $table->string('positionsNameZH', 150)->after('positionsNameKH');
            $table->string('positionsNameTH', 150)->after('positionsNameZH');
        });

    }

    public function down()
    {
        // Remove 2 Fields to table Sectors
        Schema::table('Sectors', function ($table) {
            $table->dropColumn('sectorsNameZH');
            $table->dropColumn('sectorsNameTH');
        });

        // Remove 2 Fields to table Subsectors
        Schema::table('Subsectors', function ($table) {
            $table->dropColumn('subsectorsNameZH');
            $table->dropColumn('subsectorsNameTH');
        });

        // Remove 2 Fields to table Activities
        Schema::table('Activities', function ($table) {
            $table->dropColumn('activitiesNameZH');
            $table->dropColumn('activitiesNameTH');
        });

        // Remove 2 Fields to table Positions
        Schema::table('Positions', function ($table) {
            $table->dropColumn('positionsNameZH');
            $table->dropColumn('positionsNameTH');
        });
    }

}
