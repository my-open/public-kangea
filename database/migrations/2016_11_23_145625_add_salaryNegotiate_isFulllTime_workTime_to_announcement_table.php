<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalaryNegotiateIsFulllTimeWorkTimeToAnnouncementTable extends Migration
{

    public function up()
    {
        Schema::table('Announcements', function($table)
        {
            $table->boolean('announcementsSalaryNegotiate')->default(0)->comment('1=Negotiation')->after('announcementsClosingDate');
            $table->boolean('announcementsIsFullTime')->default(0)->comment('0=PartTime, 1=FullTime')->after('announcementsSalaryNegotiate');
            $table->string('announcementsWorkTime', 100)->after('announcementsIsFullTime');
        });
    }

    public function down()
    {
        Schema::table('Announcements', function ($table) {
            $table->dropColumn('announcementsSalaryNegotiate');
            $table->dropColumn('announcementsIsFullTime');
            $table->dropColumn('announcementsWorkTime');
        });
    }

}
