<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTblJobApply extends Migration
{

    public function up()
    {
        // Add 2 Fields to table JobApply
        Schema::table('JobApply', function($table)
        {
            $table->string('jobApplyTitleZH', 150)->after('jobApplyTitleKH');
            $table->string('jobApplyTitleTH', 150)->after('jobApplyTitleZH');
        });
    }

    public function down()
    {
        // Remove 2 Fields to table JobApply
        Schema::table('JobApply', function ($table) {
            $table->dropColumn('jobApplyTitleZH');
            $table->dropColumn('jobApplyTitleTH');
        });
    }
}
