<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionIdToJobApplyTable extends Migration
{

    public function up()
    {
        Schema::table('JobApply', function($table)
        {
            $table->integer('fkPositionsID')->after('fkUsersID');
        });
    }

    public function down()
    {
        Schema::table('JobApply', function ($table) {
            $table->dropColumn('fkPositionsID');
        });
    }

}
