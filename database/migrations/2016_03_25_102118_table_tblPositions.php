<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblPositions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('Positions', function (Blueprint $table) {
             $table->increments('pkPositionsID');
             $table->string('positionsCode', 100);
             $table->integer('fkSectorsID');
             $table->string('positionsNameEN', 150);
             $table->string('positionsNameKH', 150);
             $table->integer('positionsOrder')->default(0);
             $table->boolean('positionsStatus');
             $table->timestamps();

         });
     }

     public function down()
     {
       Schema::drop('Positions');
     }
  }
