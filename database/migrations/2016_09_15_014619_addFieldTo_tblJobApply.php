<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToTblJobApply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('JobApply', function($table)
        {
            $table->string('jobApplyGender', 5)->after('jobApplyTitleKH');
            $table->string('jobApplyCvFile')->after('jobApplyDescription');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('JobApply', function ($table) {
            $table->dropColumn('jobApplyGender');
            $table->dropColumn('jobApplyCvFile');
        });
    }
}
