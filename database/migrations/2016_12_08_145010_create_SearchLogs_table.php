<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchLogsTable extends Migration
{

    public function up()
    {
        Schema::create('SearchLogs', function (Blueprint $table) {
            $table->increments('pkSearchLogsID');
            $table->integer('fkProvincesID');
            $table->integer('fkDistrictsID');
            $table->integer('fkCommunesID');
            $table->integer('fkSectorsID');
            $table->integer('fkSubsectorsID');
            $table->integer('fkActivitiesID');
            $table->integer('fkPositionsID');
            $table->integer('fkUsersID');
            $table->integer('fkAnnouncementsID');

            $table->string('searchLogsIP', 100);
            $table->string('searchLogsBrowser', 50);
            $table->string('searchLogsOS', 50);
            $table->string('searchLogsDeviceType', 30)->comment('phone, computer');
            $table->string('searchLogsDeviceModel', 60);
            $table->tinyInteger('searchLogsStatus')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('SearchLogs');
    }
}
