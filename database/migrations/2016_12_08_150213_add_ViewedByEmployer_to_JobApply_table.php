<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewedByEmployerToJobApplyTable extends Migration
{

    public function up()
    {
        Schema::table('JobApply', function($table)
        {
            $table->boolean('jobApplyViewedByEmployer')->after('jobApplyBy');
        });
    }

    public function down()
    {
        Schema::table('JobApply', function ($table) {
            $table->dropColumn('jobApplyViewedByEmployer');
        });
    }
}