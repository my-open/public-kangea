<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsIsSaleEmpPhoneToUsersTable extends Migration
{

    public function up()
    {
        Schema::table('users', function ($table) {
            $table->boolean('IsSale')->default(0)->after('api_token');
            $table->string('EmpPhone', 11)->after('IsSale')->after('IsSale');
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('IsSale');
            $table->dropColumn('EmpPhone');
        });
    }

}
