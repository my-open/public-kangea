<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCompanyUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CompanyUsers', function (Blueprint $table) {
            $table->integer('fkCompaniesID')->unsigned();
            $table->integer('fkUsersID')->unsigned();

            $table->primary(['fkCompaniesID', 'fkUsersID']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CompanyUsers');
    }
}
