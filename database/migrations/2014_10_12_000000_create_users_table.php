<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fkPositionsID');
            $table->string('name');
            $table->string('phone', 100);
            $table->string('email');
            $table->string('password');
            $table->string('gender', 5);
            $table->string('address');
            $table->boolean('verified')->default(0);
            $table->boolean('experience')->default(0);
            $table->boolean('status')->default(0);
            $table->tinyInteger('companyLevel')->comment('1=Admin, 2=Normal, 3=Inactive');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
