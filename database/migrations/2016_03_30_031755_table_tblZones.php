<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblZones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('Zones', function (Blueprint $table) {
           $table->increments('pkZonesID');
           $table->integer('fkCountriesID');
           $table->integer('fkProvincesID');
           $table->integer('fkDistrictsID');
           $table->integer('fkCommunesID');
           $table->integer('fkVillagesID');
           
           $table->text('zonesNameEN');
           $table->text('zonesNameKH');
           $table->boolean('zonesStatus');
           $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('Zones');
     }
  }
