<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJobApply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('JobApply', function (Blueprint $table) {
            $table->increments('pkJobApplyID');
            $table->integer('fkAnnouncementsID');
            $table->integer('fkCompaniesID');
            $table->integer('fkUsersID');
            $table->string('jobApplyTitleEN');
            $table->string('jobApplyTitleKH');
            $table->string('jobApplyName');
            $table->string('jobApplyEmail');
            $table->string('jobApplyPhone');
            $table->string('jobApplyDescription');
            $table->boolean('jobApplyStatus')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('JobApply');
    }
}
