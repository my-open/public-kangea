<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblSectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('Sectors', function (Blueprint $table) {
           $table->increments('pkSectorsID');

           $table->string('sectorsNameEN', 100);
           $table->string('sectorsNameKH', 100);
           $table->string('sectorsPhoto', 100);
           $table->boolean('sectorsStatus');

           $table->timestamps();

         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('Sectors');
     }
 }
