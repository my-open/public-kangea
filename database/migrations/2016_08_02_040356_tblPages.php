<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pages', function (Blueprint $table) {
            $table->increments('pkPagesID');
            $table->integer('fkPagesSubTitleID')->default(0);

            $table->string('pagesSiteTitle');
            $table->text('pagesSiteKeyword');
            $table->text('pagesSiteDescription');

            $table->string('pagesTitleKH');
            $table->string('pagesTitleEN');
            $table->longText('pagesDescriptionKH');
            $table->longText('pagesDescriptionEN');

            $table->string('pagesUrl');
            $table->boolean('pagesPosition')->comment('1=Top, 0=Botton');
            $table->integer('pagesOrder')->default(0);
            $table->boolean('pagesStatus');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Pages');
    }
}
