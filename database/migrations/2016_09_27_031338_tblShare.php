<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblShare extends Migration
{
    public function up()
    {
        Schema::create('Shares', function (Blueprint $table) {
            $table->increments('pkSharesID');

            $table->string('sharesGender', 10);
            $table->string('sharesName', 150);
            $table->string('sharesPhone', 100);
            $table->integer('sharesWho')->comment('1=Sibling, 2=Relative, 3=Friend, 4=Acquaintance');

            $table->time('sharesRetryTime')->nullable();
            $table->time('sharesTime')->nullable();
            $table->integer('sharesDuration');
            $table->string('sharesStatusCall');
            $table->date('sharesDate');
            $table->boolean('sharesStatus')->default(0);
            $table->tinyInteger('sharesType')->comment('1=phone, 2=facebook');

            $table->string('fbID', 100);
            $table->string('fbGender', 10);
            $table->string('fbName', 100);
            $table->string('fbPhone', 100);
            $table->string('fbEmail', 100);

            $table->boolean('sharesAppliedOrNot')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Shares');
    }
}
