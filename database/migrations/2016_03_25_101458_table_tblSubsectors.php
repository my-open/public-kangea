<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblSubsectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('Subsectors', function (Blueprint $table) {
           $table->increments('pkSubsectorsID');

           $table->integer('fkSectorsID');

           $table->string('subsectorsNameEN', 100);
           $table->string('subsectorsNameKH', 100);
           $table->string('subsectorsPhoto', 100);
           $table->integer('subsectorsOrder')->default(0);
           $table->boolean('subsectorsStatus');

           $table->timestamps();

         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('Subsectors');
     }
 }
