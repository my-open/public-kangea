<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRetryDateToSharesTable extends Migration
{

    public function up()
    {
        Schema::table('Shares', function($table) {
            $table->string('sharesInteraction')->after('sharesStatus');
            $table->date('sharesRetryDate')->after('sharesRetryTime');
            $table->text('sharesApplyRecord')->after('sharesRetryDate')->nullable();
        });

    }

    public function down()
    {
        Schema::table('Shares', function ($table) {
            $table->dropColumn('sharesInteraction');
            $table->dropColumn('sharesRetryDate');
            $table->dropColumn('sharesApplyRecord');
        });
    }
}
