<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTblPages extends Migration
{

    public function up()
    {
        // Add 4 Fields to table Pages
        Schema::table('Pages', function($table)
        {
            $table->string('pagesTitleZH', 150)->after('pagesTitleEN');
            $table->string('pagesTitleTH', 150)->after('pagesTitleZH');

            $table->string('pagesDescriptionZH', 150)->after('pagesDescriptionEN');
            $table->string('pagesDescriptionTH', 150)->after('pagesDescriptionZH');
        });
    }

    public function down()
    {
        // Remove 4 Fields to table Pages
        Schema::table('Pages', function ($table) {
            $table->dropColumn('pagesTitleZH');
            $table->dropColumn('pagesTitleTH');

            $table->dropColumn('pagesDescriptionZH');
            $table->dropColumn('pagesDescriptionTH');
        });
    }
}
