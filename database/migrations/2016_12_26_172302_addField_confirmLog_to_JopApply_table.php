<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldConfirmLogToJopApplyTable extends Migration
{

    public function up()
    {
        Schema::table('JobApply', function ($table) {
            $table->text('jobApplyConfirmLog')->after('jobApplyBy');
            $table->tinyInteger('jobApplyIsConfirm')->default(0)->comment('1=Confirm, 0=Not confirm')->after('jobApplyStatus');
        });
    }

    public function down()
    {
        Schema::table('JobApply', function ($table) {
            $table->dropColumn('jobApplyConfirmLog');
            $table->dropColumn('jobApplyIsConfirm');
        });
    }

}