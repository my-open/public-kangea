<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblJobExperiences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
        Schema::create('JobExperiences', function (Blueprint $table) {
            $table->increments('pkJobExperiencesID');
            $table->integer('fkUsersID');
            $table->integer('fkCountriesID');
            $table->integer('fkProvincesID');
            $table->integer('fkDistrictsID');
            $table->integer('fkCommunesID');
            $table->integer('fkVillagesID');
            $table->integer('fkZonesID');
            $table->integer('fkSectorsID');
            $table->integer('fkSubsectorsID');
            $table->integer('fkPositionsID');
            $table->string('workExperience', 50);
            $table->text('jobExperiencesDescriptionEN');
            $table->text('jobExperiencesDescriptionKH');
            $table->timestamps();
        });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('JobExperiences');
     }
  }
