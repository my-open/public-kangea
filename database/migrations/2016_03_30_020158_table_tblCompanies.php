<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTblCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('Companies', function (Blueprint $table) {
           $table->increments('pkCompaniesID');
          //  $table->integer('fkUsersID');
           $table->integer('fkSectorsID');
           $table->integer('fkSubsectorsID');
           $table->integer('fkActivitiesID');

           $table->string('companiesCode', 200);
           $table->string('companiesNameEN', 200);
           $table->string('companiesNameKH', 200);
           $table->string('companiesNickName', 200);

           $table->integer('companiesNumberOfWorker');
           $table->integer('fkZonesID');

           $table->integer('fkProvincesID');
           $table->integer('fkDistrictsID');
           $table->integer('fkCommunesID');
           $table->integer('fkVillagesID');

           $table->text('companiesAddress');
           $table->float('companiesXCoordinate');
           $table->float('companiesYCoordinate');
           $table->text('companiesDescriptionEN');
           $table->text('companiesDescriptionKH');

           $table->string('companiesPhone', 100);
           $table->string('companiesEmail', 100);
           $table->string('companiesLogo', 100);
           $table->string('companiesSite', 100)->nullable(); //19 Fields

           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('Companies');
     }
 }
