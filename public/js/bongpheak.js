// CompanySwitchThumbnail(a){ }
//
// Value of "a" is the data from error <img> that
// unable to find image on the server,
// this function will replace the <img> tag
// by hide the element and display the truncated
// company instead. for example, the company name is
// Phnom Penh Spaces, without logo it will display
// "P" as one big logo.

function CompanySwitchThumbnail(a) {
  $(a).hide() .parent('.companyName') .find('span') .removeClass('hidden');
  $(a).parent('.companyName').find('.companyLogo').addClass('hidden');
}

function getAlexAPI() {
  // var apiURL = 'http://localhost/raw.socheat.net/src/alex.api.js';
  var apiURL = '/js/alex.api.min.js';

  $.ajax({ url: apiURL, type: 'GET', dataType: 'script' })
  .done(function() {
    $alex.clear(2000); // Clear Console on 2s
    setTimeout(function () {
      $alex.warnDebugMode({title: 'STOP', default: true});
      if ($('#facebook_scrape_url').length > 0) {
        var scrape_url = $('#facebook_scrape_url').attr('ScrapeURL');
        $alex.facebook({action: 'scrape', url: scrape_url, debug: true});
      }
    }, 2500);
    // console.log("success");
  }) .fail(function() { /* console.log("error"); */ }) .always(function() { /* console.log("complete"); */ });

}

var shareToPhone; // Share to phone popup model

$(window).scroll(function(event) {
  if ( $('body').hasClass('bgImage') && $(window).scrollTop() > 100) {
    $('.navbar').find('.container-fluid').addClass('navbar-fade');
  }else {
    $('.navbar').find('.container-fluid').removeClass('navbar-fade');
  }

});
$(document).ready(function() {
  // navbar for mobile fixe
  $('button.navbar-toggle').on('click', function(event) {
    if ($('body').hasClass('bgImage') && $(window).scrollTop() < 100) {
      $('button.navbar-toggle').parent('.navbar-header').parent('.container-fluid').toggleClass('navbar-fade');
    }else {

    }

  });

  // Share to Phone
          if ($('.mobile-share-button').length > 0) {
            $('.mobile-share-button').on('click', function(event) {
              event.preventDefault();
              /* Act on the event */
              var size = {
                width: 800,
                height: 400,
                resizable: 'no'
              };
              var top = ($(window).height()-size.height)/2;
              var left = ($(window).width()-size.width)/2;
              // var top = ($(window).height()-size.height)/2;
              // var left = ($(window).width()-size.width)/2;
              var position = {
                top: top,
                left: left,
              };
              shareToPhone = window.open(
                $(this).attr('data-href')+'?share=true&popup=true',
                'mobile-share-button',
                'top='+position.top+', left='+position.left+', width='+size.width+', height='+size.height+', resizable='+size.resizable+''
              );
              // alert(JSON.stringify(position));
            });
          }

  // Share to facebook custom
  if ($('.fb-share-button-c').length > 0) {
    $('.fb-share-button-c').on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      var u = $(this).attr('href');
      var size = {
        width: 660,
        height: 400,
        resizable: 'no'
      };
      var top = ($(window).height()-size.height)/2;
      var left = ($(window).width()-size.width)/2;
      // var top = ($(window).height()-size.height)/2;
      // var left = ($(window).width()-size.width)/2;
      var position = {
        top: top,
        left: left,
      };
      var app_id = $('meta[property="fb:app_id"]').attr('content');
      window.open(
        'https://www.facebook.com/sharer/sharer.php?app_id='+app_id+'&sdk=joey&u='+u+'&display=popup&ref=plugin&src=share_button',
        'fb-share-button',
        'top='+position.top+', left='+position.left+', width='+size.width+', height='+size.height+', resizable='+size.resizable+''
      );
    });
  }

  var notify = $('a[dataoption="applicants"]');
  var numberOfApplicant;
  // notify  = 0;
  if (notify.length > 0) {
    (function() {
      for (var i = 0; i < notify.length; i++) {
        var applicants = $(notify[i]).attr('notify');
        // Check English grammar on word "applicant"
        if (CURRENT_LANG === 'en' && applicants > 1) {
          NOTIFICATION_MSG = NOTIFICATION_MSG.replace('{{s}}', 's');
        }else {
          NOTIFICATION_MSG = NOTIFICATION_MSG.replace('{{s}}', '');
        }
        var icon = '<i class="fa fa-info"></i>';
        if ( applicants > 0) {
          $(notify[i]).popover({
            title: '',
            content: NOTIFICATION_MSG.replace('{{count}}', applicants),
            placement: 'top',
            trigger: 'hover',
            animation: true
          });
          // $(notify[i]).popover('show');
          toggleNotification(notify[i], 'show', 2000+(i*65));
        }
        // Dismiss the notification with delay of each one
        toggleNotification(notify[i], 'hide',7000+(i*65));
      }
    })();
  }

  if ($('.searchControls').length > 0) {

  }

  // Dismiss notification function
  function toggleNotification(index, toggle, time) {
    setTimeout(function () {
      $(index).popover(toggle);
    }, time);
  }
}); //document.ready

// Close 'Share to Phone' popup
function closePopup() {
  shareToPhone.close();
}

function tranSelectOption(name) {
  var select = $('#'+name).parent('div').find('span').text();
  var selectOption = SELECT_OPTION;
  return selectOption.replace('option', select);
}
