$( document ).ready(function() {

    $( "#announcementsPublishDate" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
    $( "#announcementsClosingDate" ).datepicker({
        dateFormat: "yy-mm-dd"
    });

    //=== Get districts
    if( FKPROVINCEID != '' ){
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getDistricts',
            method: 'GET',
            data: {provincesId:FKPROVINCEID}
        }).done(function(response){
            var selectOption = response['selectOption'];

            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.districtsNameEN;
                else name = value.districtsNameKH;

                if( value.pkDistrictsID == FKDISTRICTID ){
                    $('#fkDistrictsID')
                        .append($("<option selected></option>")
                            .attr("value",value.pkDistrictsID)
                            .text(name));
                }else{
                    $('#fkDistrictsID')
                        .append($("<option></option>")
                            .attr("value",value.pkDistrictsID)
                            .text(name));
                }
            });
        });
    }

    //=== Get Communes
    if( FKDISTRICTID != '' ){
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getCommunes',
            method: 'GET',
            data: {districtId:FKDISTRICTID}
        }).done(function(response){
            var selectOption = response['selectOption'];

            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.communesNameEN;
                else name = value.communesNameKH;

                if( value.pkCommunesID == FKCOMMUNESID ){
                    $('#fkCommunesID')
                        .append($("<option selected></option>")
                            .attr("value",value.pkCommunesID)
                            .text(name));
                }else{
                    $('#fkCommunesID')
                        .append($("<option></option>")
                            .attr("value",value.pkCommunesID)
                            .text(name));
                }
            });
        });
    }

    //=== Get Villages
    if( FKCOMMUNESID != '' ){
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getVillages',
            method: 'GET',
            data: {communeId:FKCOMMUNESID}
        }).done(function(response){
            var selectOption = response['selectOption'];

            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.villagesNameEN;
                else name = value.villagesNameKH;

                if( value.pkVillagesID == FKVILLAGESID ){
                    $('#fkVillagesID')
                        .append($("<option selected></option>")
                            .attr("value",value.pkVillagesID)
                            .text(name));
                }else{
                    $('#fkVillagesID')
                        .append($("<option></option>")
                            .attr("value",value.pkVillagesID)
                            .text(name));
                }
            });
        });
    }
});