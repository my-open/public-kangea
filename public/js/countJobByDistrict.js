$(document).ready(function(){

    //click province
    $('#fkProvincesID').change(function(){

        $('select#fkDistrictsID').empty();
        $('select#fkCommunesID').empty();
        $('select#fkVillagesID').empty();

        var provincesId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getCountJobByDistrict',
            method: 'GET',
            data: {provincesId:provincesId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default district
            $('select#fkDistrictsID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkDistrictsID')));
            //append Any where with empty value
            if(typeof(ANY_WHERE) != "undefined" && ANY_WHERE !== null)
            {
                $('select#fkDistrictsID')
                    .append($("<option></option>")
                        .attr("value","")
                        .text(ANY_WHERE));
            }
            //default commune
            $('select#fkCommunesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkCommunesID')));
            //default village
            $('select#fkVillagesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkVillagesID')));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();

                //View District in Dropdown menu
                if( value.Total_Announcement != 0)
                    $('#fkDistrictsID')
                        .append($("<option></option>")
                            .attr("value",value.pkDistrictsID)
                            .text(value['districtsName'+lang] + ' ('+value.Total_Announcement +')' ) );
                else
                    $('#fkDistrictsID')
                        .append($("<option></option>")
                            .attr("value",value.pkDistrictsID)
                            .attr("disabled", "disabled")
                            .text(value['districtsName'+lang]) );

            });
            // # end each

        });
    });

});
