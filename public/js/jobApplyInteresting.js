$(document).ready(function(){
    $('.jobApplyInteresting').click(function() {
        $(this ).each(function() {
            var checkboxValue = $(this).val();
            var pkJobApplyID = $(this).attr('dataId');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/' + CURRENT_LANG + '/account/postjob/application/interesting/check',
                method: 'PUT',
                data: {checkboxValue:checkboxValue, applyId:pkJobApplyID}
            }).done(function(response){
                location.reload();
            });
        });
    });

});