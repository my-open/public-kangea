$(document).ready(function(){

    //click pkPagesID
    $('#pkPagesID').change(function(){

        $('select#fkPagesSubTitleID').empty();

        var PagesID = $(this).val();
        //alert(PagesID);

        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getPagesSubTitles',
            method: 'GET',
            data: {PagesID:PagesID}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default pageSubTitle
            $('select#fkPagesSubTitleID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.pagesTitleEN;
                else name = value.pagesTitleKH;
                $('#fkPagesSubTitleID')
                    .append($("<option></option>")
                        .attr("value",value.pkPagesID)
                        .text(name));

            });
        });
    });

});