$( document ).ready(function() {

    $('#idBtnShareToPhone').click(function(){
        $.get('/' + CURRENT_LANG + '/logSharePhone', {pkAnnouncementsID:$(this).attr('value-pkAnnouncementsID')}, function(data){
            console.log(data);
        });
    });

    $('#idBtnApplyForSO').click(function(){
        $.get('/' + CURRENT_LANG + '/logApplyForSO', {pkAnnouncementsID:$(this).attr('value-pkAnnouncementsID')}, function(data){
            console.log(data);
        });
    });

    $('#idBtnApplyForMyself').click(function(){
        $.get('/' + CURRENT_LANG + '/logApplyOwn', {pkAnnouncementsID:$(this).attr('value-pkAnnouncementsID')}, function(data){
            console.log(data);
        });
    });

});
