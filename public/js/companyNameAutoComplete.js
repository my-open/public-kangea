$(document).ready(function(){
    var companiesEN = [];
    var companiesKH = [];
    var companies = [];
    var htmlDefaultDiv = '';

    $('.htmlDefault').on('change','#fkSectorsID', function(){
        $('select#fkSubsectorsID').empty();
        $('select#fkActivitiesID').empty();
        $('.subsectorOther').hide();
        $('.activitiesOther').hide();

        var sectorId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getSubsectors',
            method: 'GET',
            data: {sectorId:sectorId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default subsector
            $('select#fkSubsectorsID')
                .append($("<option selected></option>")
                    .attr("value",'')
                    .text(selectOption));
            //default activity
            $('select#fkActivitiesID')
                .append($("<option selected></option>")
                    .attr("value",'')
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkSubsectorsID')
                    .append($("<option></option>")
                        .attr("value",value.pkSubsectorsID)
                        .text(value['subsectorsName'+lang]));

            });

            $('#fkSubsectorsID')
                .append($("<option></option>")
                    .attr("value",'other')
                    .text(OPT_OTHER));
        });
    });

    $('.htmlDefault').on('change','#fkSubsectorsID', function(){
        $('select#fkActivitiesID').empty();
        $('.activitiesOther').hide();

        var subsectorId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getActivities',
            method: 'GET',
            data: {subsectorId:subsectorId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default activity
            $('select#fkActivitiesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkActivitiesID')
                    .append($("<option></option>")
                        .attr("value",value.pkActivitiesID)
                        .text(value['activitiesName'+lang]));

            });

            $('#fkActivitiesID')
                .append($("<option></option>")
                    .attr("value",'other')
                    .text(OPT_OTHER));
        });
    });

    $('.htmlDefault').on('change','#fkProvincesID', function(){
        $('select#fkDistrictsID').empty();
        $('select#fkCommunesID').empty();
        $('select#fkVillagesID').empty();

        var provincesId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getDistricts',
            method: 'GET',
            data: {provincesId:provincesId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default district
            $('select#fkDistrictsID')
                .append($("<option selected></option>")
                    .attr("value",'')
                    .text(selectOption));
            //default commune
            $('select#fkCommunesID')
                .append($("<option selected></option>")
                    .attr("value",'')
                    .text(selectOption));
            //default village
            $('select#fkVillagesID')
                .append($("<option selected></option>")
                    .attr("value",'')
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkDistrictsID')
                    .append($("<option></option>")
                        .attr("value",value.pkDistrictsID)
                        .text(value['districtsName'+lang]));
            });

        });
    });

    $('.htmlDefault').on('change','#fkDistrictsID', function(){
        $('select#fkCommunesID').empty();
        $('select#fkVillagesID').empty();

        var districtId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getCommunes',
            method: 'GET',
            data: {districtId:districtId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default commune
            $('select#fkCommunesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));
            //default village
            $('select#fkVillagesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkCommunesID')
                    .append($("<option></option>")
                        .attr("value",value.pkCommunesID)
                        .text(value['communesName'+lang]));
            });

        });
    });

    //$('.htmlDefault').on('change','#fkCommunesID', function(){
    //    $('select#fkVillagesID').empty();
    //
    //    var communeId = $(this).val();
    //    $.ajax({
    //        url: '/' + CURRENT_LANG + '/account/getVillages',
    //        method: 'GET',
    //        data: {communeId:communeId}
    //    }).done(function(response){
    //        var selectOption = response['selectOption'];
    //
    //        //default village
    //        $('select#fkVillagesID')
    //            .append($("<option selected></option>")
    //                .attr("value","")
    //                .text(selectOption));
    //
    //        $.each(response['data'], function(key, value){
    //            var name ='';
    //            if( CURRENT_LANG == 'en' ) name = value.villagesNameEN;
    //            else name = value.villagesNameKH;
    //            $('#fkVillagesID')
    //                .append($("<option></option>")
    //                    .attr("value",value.pkVillagesID)
    //                    .text(name));
    //
    //        });
    //    });
    //});
});