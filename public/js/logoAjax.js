function issueUpload(inutfile, showtext) {
    var inutfile = '#' + inutfile;
    var showtext = '#' + showtext;
    $(inutfile).click();
    $(inutfile).change(function(){
        // Cut C:/fakepath
        var fileName = getUploadFilename($(inutfile).val());
         //console.log(fileName);
        $(showtext).val(fileName);
        readURL(this);
    });
}

function getUploadFilename(input_file) {
    var path = input_file;
    var filename = path.replace(/^.*\\/, "");
    return filename;
}

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#getImage').css({'background-image':'url(' + e.target.result + ')', 'background-size':'220px 110px', 'background-repeat':'no-repeat'});
        }
        reader.readAsDataURL(input.files[0]);
    }
}