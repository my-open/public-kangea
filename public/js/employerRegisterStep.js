jQuery(function () {
    /*disable non active tabs*/
    $('.nav-tabs li').not('.active').addClass('disabled');
    $('.nav-tabs li').not('.active').find('a').removeAttr("data-toggle");

    //$('.continue').click(function(){
    //    /*enable next tab*/
    //    $('.nav-tabs li.active').next('li').removeClass('disabled');
    //    $('.nav-tabs li.active').next('li').find('a').attr("data-toggle","tab").tab('show');
    //});

    $('.back').click(function(){
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });

    var token = $(this).find('input[name=_token]').val();

    //Company Representative
    $('#btnCompanyRep').click(function(){
        $('.has-error').removeClass('has-error');
        $('.help-block strong').html('');
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/employer/checkValidateCompanyRep',
            method: 'POST',
            data: {
                _token:token,
                companiesID:$("#companiesID").val(),
                companiesNameEN:$("#companiesNameEN").val(),
                companiesNameKH:$("#companiesNameKH").val(),
                fkSectorsID:$("#fkSectorsID").val(),
                fkSubsectorsID:$("#fkSubsectorsID").val(),
                fkActivitiesID:$("#fkActivitiesID").val(),
                gender:$("#gender").val(),
                name:$("#name").val(),
                phone:$("#phone").val(),
                email:$("#email").val(),
                password:$("#password").val(),
                password_confirmation:$("#password_confirmation").val()
            }
        }).done(function(response){
            if(response == 'success'){
                $('.nav-tabs li.active').next('li').removeClass('disabled');
                $('.nav-tabs li.active').next('li').find('a').attr("data-toggle","tab").tab('show');
            }else{
                var valiArray =[ 'companiesNameEN', 'companiesNameKH', 'fkSectorsID', 'fkSubsectorsID', 'fkActivitiesID', 'gender', 'name', 'phone', 'email', 'password', 'password_confirmation' ];
                $.each(response, function (index, message) {
                    if(jQuery.inArray(index, valiArray) != -1) {
                        $('.frm'+index).addClass("has-error");
                        $('.'+index+' strong').html(message);
                    }
                });
            }
        });
    });

    //Company Profile
    $('#btnCompanyPro').click(function(){
        console.log('click Pro!');
        $('.has-error').removeClass('has-error');
        $('.help-block strong').html('');
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/employer/cccccc',
            method: 'POST',
            data: {
                _token:token,
                companiesID:$("#companiesID").val(),
                companiesPhone:$("#companiesPhone").val(),
                companiesEmail:$("#companiesEmail").val(),
                fkProvincesID:$("#fkProvincesID").val(),
                fkDistrictsID:$("#fkDistrictsID").val(),
                companiesSite:$("#companiesSite").val()
            }
        }).done(function(response){
            if(response == 'success'){
                $('.nav-tabs li.active').next('li').removeClass('disabled');
                $('.nav-tabs li.active').next('li').find('a').attr("data-toggle","tab").tab('show');
            }else{
                var valiArray =[ 'companiesPhone', 'companiesEmail', 'companiesSite', 'fkProvincesID', 'fkDistrictsID' ];
                $.each(response, function (index, message) {
                    if(jQuery.inArray(index, valiArray) != -1) {
                        $('.frm'+index).addClass("has-error");
                        $('.'+index+' strong').html(message);
                    }
                });
            }
        });
    });

    //Company Description
    $('#btnCompanyRegister').click(function(){
        console.log('click Reg!');
        var registerData = new FormData();
        $('.has-error').removeClass('has-error');
        $('.help-block strong').html('');
        if($('#companiesLogo').val() != '') {
            registerData.append('companiesLogo', $('#companiesLogo')[0].files[0]);
        }
        registerData.append('_token', token);
        registerData.append('companiesID', $("#companiesID").val());

        //Tap Company Rep
        registerData.append('companiesNameEN', $("#companiesNameEN").val());
        registerData.append('companiesNameKH', $("#companiesNameKH").val());
        registerData.append('fkSectorsID', $("#fkSectorsID").val());
        registerData.append('fkSubsectorsID', $("#fkSubsectorsID").val());
        registerData.append('fkActivitiesID', $("#fkActivitiesID").val());
        registerData.append('gender', $("#gender").val());
        registerData.append('name', $("#name").val());
        registerData.append('phone', $("#phone").val());
        registerData.append('email', $("#email").val());
        registerData.append('password', $("#password").val());
        registerData.append('password_confirmation', $("#password_confirmation").val());

        //Tap Company Pro
        registerData.append('companiesPhone', $("#companiesPhone").val());
        registerData.append('companiesEmail', $("#companiesEmail").val());
        registerData.append('companiesSite', $("#companiesSite").val());
        registerData.append('fkProvincesID', $("#fkProvincesID").val());
        registerData.append('fkDistrictsID', $("#fkDistrictsID").val());
        registerData.append('fkCommunesID', $("#fkCommunesID").val());
        registerData.append('fkVillagesID', $("#fkVillagesID").val());
        registerData.append('fkZonesID', $("#fkZonesID").val());

        //Tap Company Des
        registerData.append('companiesNickName', $("#companiesNickName").val());
        registerData.append('companiesNumberOfWorker', $("#companiesNumberOfWorker").val());
        registerData.append('companiesAddress', $("#companiesAddress").val());
        registerData.append('companiesDescriptionEN', $("#companiesDescriptionEN").val());
        registerData.append('companiesDescriptionKH', $("#companiesDescriptionKH").val());

        $.ajax({
            url: '/' + CURRENT_LANG + '/account/employer/register',
            method: 'POST',
            processData: false,
            contentType: false,
            data: registerData,
            statusCode: {
                /*Validation Errors*/
                203: function (response) {
                    //console.log(JSON.stringify(response));
                    $.each(response, function (index, message) {
                        $('.frm'+index).addClass("has-error");
                        $('.'+index+' strong').html(message);
                    });
                },
                /*Validation success*/
                200: function (response) {
                    window.location.href = response;
                }
            }
        }).complete(function () {
            //$('.loading-img').hide();
            //console.log( $("#companiesID").val() );
        });
    });
});