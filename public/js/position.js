$(document).ready(function(){

    //click sector
    $('#fkSectorsID').change(function(){

        $('select#fkPositionsID').empty();

        var sectorId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getPositions',
            method: 'GET',
            data: {sectorId:sectorId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default position
            $('select#fkPositionsID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkPositionsID')));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkPositionsID')
                    .append($("<option></option>")
                        .attr("value",value.pkPositionsID)
                        .text(value['positionsName'+lang]));

            });
        });

    });

});
