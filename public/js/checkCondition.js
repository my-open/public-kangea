$( document ).ready(function() {

    <!-- Check Number positive and integer only -->
    $(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("Positive integers only"); this.value = ""; this.focus(); });
    $("#remove").click(
        function(e)
        {
            e.preventDefault();
            $(".positive-integer").removeNumeric();
        }
    );

    <!-- Check Salary depends on: Experience, Amount of work -->
    $("#hideShowDependsOn").hide();
    $( "#announcementsSalaryTo, #announcementsSalaryFrom" ).focusout(function() {
        var salaryFrom = parseInt( $("input[name=announcementsSalaryFrom]").val() );
        var salaryTo = parseInt( $("input[name=announcementsSalaryTo]").val() );
        if( salaryTo > salaryFrom ){
            $("#hideShowDependsOn").show();
        }else{
            $("#hideShowDependsOn").hide();
        }
    });
    <!-- Check Salary depends on: Experience, Amount of work ==> In Edit Form -->
    var salaryFrom = parseInt( $("input[name=announcementsSalaryFrom]").val() );
    var salaryTo = parseInt( $("input[name=announcementsSalaryTo]").val() );
    if( salaryTo > salaryFrom ){
        $("#hideShowDependsOn").show();
    }else{
        $("#hideShowDependsOn").hide();
    }


    <!-- Check Age min==15 -->
    $( "#AnnouncementsAgeFrom, #AnnouncementsAgeUntil" ).change(function() {
        var min = parseInt(15);
        if ($(this).val() < min)
        {
            $(this).val(min);
        }
    });

});  //end document ready