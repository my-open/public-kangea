$(document).ready(function () {
    /*disable non active tabs*/
    $('.nav-tabs li').not('.active').addClass('disabled');
    $('.nav-tabs li').not('.active').find('a').removeAttr("data-toggle");

    /*enable next tab*/
    //$('.continue').click(function(){
    //    $('.nav-tabs li.active').next('li').removeClass('disabled');
    //    $('.nav-tabs li.active').next('li').find('a').attr("data-toggle","tab").tab('show');
    //});

    /*back tab*/
    $('.back').click(function(){
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });

    var token = $(this).find('input[name=_token]').val();

    $('#btnUserInfo').click(function(){
        $('.has-error').removeClass('has-error');
        $('.help-block strong').html('');

        $.ajax({
            url: '/' + CURRENT_LANG + '/account/jobseeker/checkValidateUserInfo',
            method: 'POST',
            data: {
                _token:token,
                gender:$("#gender").val(),
                name:$("#name").val(),
                phone:$("#phone").val()
            }
        }).done(function(response){
            if(response == 'success'){
                $('.nav-tabs li.active').next('li').removeClass('disabled');
                $('.nav-tabs li.active').next('li').find('a').attr("data-toggle","tab").tab('show');
            }else{
                var valiArray =[ 'name', 'phone' ];
                $.each(response, function (index, message) {
                    if(jQuery.inArray(index, valiArray) != -1) {
                        $('.frm'+index).addClass("has-error");
                        $('.'+index+' strong').html(message);
                    }
                });
            }
        });
    });

    $('#btnEnteredExperience').click(function(){
        $('.has-error').removeClass('has-error');
        $('.help-block strong').html('');

        var formData = $('#formRegister').serialize();

        $.ajax({
            url: '/' + CURRENT_LANG + '/account/jobseeker/checkValidateExperience',
            method: 'POST',
            data: formData,
        }).done(function(response){
            if(response == 'success'){
                $('.nav-tabs li.active').next('li').removeClass('disabled');
                $('.nav-tabs li.active').next('li').find('a').attr("data-toggle","tab").tab('show');
            }else{
                var valiArray =[ 'fkSectorsID0', 'fkSubsectorsID0', 'fkPositionsID0', 'workExperience0', 'fkSectorsID1', 'fkSubsectorsID1', 'fkPositionsID1', 'workExperience1', 'fkSectorsID2', 'fkSubsectorsID2', 'fkPositionsID2', 'workExperience2', 'fkSectorsID3', 'fkSubsectorsID3', 'fkPositionsID3', 'workExperience3', 'fkSectorsID4', 'fkSubsectorsID4', 'fkPositionsID4', 'workExperience4', 'fkSectorsID5', 'fkSubsectorsID5', 'fkPositionsID5', 'workExperience5' ];
                $.each(response, function (index, message) {
                    if(jQuery.inArray(index, valiArray) != -1) {
                        $('.frm'+index).addClass("has-error");
                        $('.e'+index+' strong').html(message);
                    }
                });
            }
        });
    });

    $('.continue').click(function(){
        $('.has-error').removeClass('has-error');
        $('.help-block strong').html('');

        $('.nav-tabs li.active').next('li').removeClass('disabled');
        $('.nav-tabs li.active').next('li').find('a').attr("data-toggle","tab").tab('show');
    });


    $('#btnRegister').click(function(){
        $('.has-error').removeClass('has-error');
        $('.help-block strong').html('');

        var formData = $('#formRegister').serialize();

        $.ajax({
            url: '/' + CURRENT_LANG + '/account/jobseeker/register',
            method: 'POST',
            data: formData,
            statusCode: {
                /*Validation Errors*/
                203: function (response) {
                    var valiArray =[ 'fkSectorsIDTargetJob', 'fkProvincesID' ];
                    $.each(response, function (index, message) {
                        if(jQuery.inArray(index, valiArray) != -1) {
                            $('.frm'+index).addClass("has-error");
                            $('.'+index+' strong').html(message);
                        }
                    });
                },
                /*Validation success*/
                200: function (response) {
                    window.location.href = response;
                }
            }
        }).complete(function () {
            //$('.loading-img').hide();
            //console.log( $("#companiesID").val() );
        });
    });


    $('#btnExperience').hide();
    $('input:radio[name=experience]').click(function(){
        if($(this).val() == "1"){
            $('.divMoreExperience').show();
            $('.addMoreExperince').show();
            $('#btnEnteredExperience').show();
            $('#btnExperience').hide();
        }else{
            $('.divMoreExperience').hide();
            $('.addMoreExperince').hide();
            $('#btnEnteredExperience').hide();
            $('#btnExperience').show();
            $('.noteNoExperience').hide();
        }
    });


    //Add More Experience
    $('.btnRemoveMoreExperience:first').hide();
    $('#addMoreExperience').click(function (e){
        //Clone Form
        $('.divMoreExperience:first').clone().appendTo('#divAppendTo');

        //remove information last job
        $('#divAppendTo').find('.infoLastjob:last').remove();

        setClassUnique();

        //Clear select in subsector
        $('#divAppendTo').find('select.fkSubsectorsID:last').empty();
        $('#divAppendTo').find('select.fkSubsectorsID:last')
            .append($("<option selected></option>")
                .attr("value", "")
                .text( SELECT_OPTION ));

        //Clear select in position
        $('#divAppendTo').find('select.fkPositionsID:last').empty();
        $('#divAppendTo').find('select.fkPositionsID:last')
            .append($("<option selected></option>")
                .attr("value", "")
                .text( SELECT_OPTION ));

        $('.btnRemoveMoreExperience').show();
        $('.btnRemoveMoreExperience:first').hide();
        if( $('.divMoreExperience').length == 5){
            $('#addMoreExperience').hide();
        }
    });

    //Remove More Experience
    $('#divAppendTo').on('click', '.btnRemoveMoreExperience', function() {
        $(this).parents('.divMoreExperience').remove();
        setClassUnique();
        if( $('.divMoreExperience').length < 5){
            $('#addMoreExperience').show();
        }
    });

    //On change SectorsId to get Subsector
    $('#divAppendTo').on('change','.fkSectorsID', function(){
        $(this).parents('.divMoreExperience:eq(0)').find('select.fkSubsectorsID').empty();
        $(this).parents('.divMoreExperience:eq(0)').find('select.fkPositionsID').empty();
        $(this).parents('.divMoreExperience:eq(0)').find('select.fkPositionsID')
            .append($("<option selected></option>")
                .attr("value", "")
                .text( SELECT_OPTION ));

        var thisObj = this;
        var sectorId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getSubsectors',
            method: 'GET',
            data: {sectorId:sectorId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default subsector
            $(thisObj).parents('.divMoreExperience:eq(0)').find('select.fkSubsectorsID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $(thisObj).parents('.divMoreExperience:eq(0)').find('select.fkSubsectorsID')
                    .append($("<option></option>")
                        .attr("value", value.pkSubsectorsID)
                        .text( value['subsectorsName'+lang] ));
            });
        });
    });

    //On change sectorsID To get position
    $('#divAppendTo').on('change','.fkSectorsID', function(){
        $(this).parents('.divMoreExperience:eq(0)').find('select.fkPositionsID').empty();
        var thisObj = this;
        var sectorId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getPositions',
            method: 'GET',
            data: {sectorId:sectorId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default position
            $(thisObj).parents('.divMoreExperience:eq(0)').find('select.fkPositionsID')
                .append($("<option selected></option>")
                    .attr("value", "")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $(thisObj).parents('.divMoreExperience:eq(0)').find('select.fkPositionsID')
                    .append($("<option></option>")
                        .attr("value", value.pkPositionsID)
                        .text( value['positionsName'+lang] ));
            });
        });
    });
});

function setClassUnique() {
    var num = $('.divMoreExperience').length;

    var frmfkSectorsID = [];
    var fkSectorsID = [];
    var frmfkSubsectorsID = [];
    var fkSubsectorsID = [];
    var frmfkPositionsID = [];
    var fkPositionsID = [];
    var frmworkExperience = [];
    var eworkExperience = [];

    for(i = 0; i < num; i++) {
        frmfkSectorsID[i] = 'frmfkSectorsID'+i;
        fkSectorsID[i] = 'efkSectorsID'+i;

        frmfkSubsectorsID[i] = 'frmfkSubsectorsID'+i;
        fkSubsectorsID[i] = 'efkSubsectorsID'+i;

        frmfkPositionsID[i] = 'frmfkPositionsID'+i;
        fkPositionsID[i] = 'efkPositionsID'+i;

        frmworkExperience[i] = 'frmworkExperience'+i;
        eworkExperience[i] = 'eworkExperience'+i;
    }

    //add class
    $('.frmfkSectorsID').each(function(i) {
        $('.'+frmfkSectorsID[i%frmfkSectorsID.length]).removeClass(frmfkSectorsID[i%frmfkSectorsID.length]);
        $(this).addClass(frmfkSectorsID[i%frmfkSectorsID.length]);
    });
    $('.efkSectorsID').each(function(i) {
        $('.'+fkSectorsID[i%fkSectorsID.length]).removeClass(fkSectorsID[i%fkSectorsID.length]);
        $(this).addClass(fkSectorsID[i%fkSectorsID.length]);
    });

    $('.frmfkSubsectorsID').each(function(i) {
        $('.'+frmfkSubsectorsID[i%frmfkSubsectorsID.length]).removeClass(frmfkSubsectorsID[i%frmfkSubsectorsID.length]);
        $(this).addClass(frmfkSubsectorsID[i%frmfkSubsectorsID.length]);
    });
    $('.efkSubsectorsID').each(function(i) {
        $('.'+fkSubsectorsID[i%fkSubsectorsID.length]).removeClass(fkSubsectorsID[i%fkSubsectorsID.length]);
        $(this).addClass(fkSubsectorsID[i%fkSubsectorsID.length]);
    });

    $('.frmfkPositionsID').each(function(i) {
        $('.'+frmfkPositionsID[i%frmfkPositionsID.length]).removeClass(frmfkPositionsID[i%frmfkPositionsID.length]);
        $(this).addClass(frmfkPositionsID[i%frmfkPositionsID.length]);
    });
    $('.efkPositionsID').each(function(i) {
        $('.'+fkPositionsID[i%fkPositionsID.length]).removeClass(fkPositionsID[i%fkPositionsID.length]);
        $(this).addClass(fkPositionsID[i%fkPositionsID.length]);
    });

    $('.frmworkExperience').each(function(i) {
        $('.'+frmworkExperience[i%frmworkExperience.length]).removeClass(frmworkExperience[i%frmworkExperience.length]);
        $(this).addClass(frmworkExperience[i%frmworkExperience.length]);
    });
    $('.eworkExperience').each(function(i) {
        $('.'+eworkExperience[i%eworkExperience.length]).removeClass(eworkExperience[i%eworkExperience.length]);
        $(this).addClass(eworkExperience[i%eworkExperience.length]);
    });
}
