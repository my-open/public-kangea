$(document).ready(function(){
    $('.sectorOther').hide();
    $('.subsectorOther').hide();

    var optSelected = '';
    if( FKSUBSECTOR_SELECTED == 'other' ){
        optSelected = 'selected="selected"';
        $('.sectorOther').show();
        $('.subsectorOther').show();
    }
    //Subsector Other
    $('#fkSubsectorsID')
        .append($("<option "+ optSelected +"></option>")
            .attr("value",'other')
            .text(OPT_OTHER));

    //Subsector change to Other
    $('#fkSubsectorsID').change(function(){
        $('.sectorOther').hide();
        $('.subsectorOther').hide();
        if( $(this).val() == 'other'){
            $('.sectorOther').show();
            $('.subsectorOther').show();
        }
    });

    /*
    $('.subsectorOther').hide();
    $('.activitiesOther').hide();

    var sectorId = $('#fkSectorsID').val();
    if( sectorId != '' ){
        $('select#fkSubsectorsID').empty();
        $('select#fkActivitiesID').empty();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getSubsectorsBySectorsID',
            method: 'GET',
            data: {sectorId:sectorId}
        }).done(function(response){
            var selectOption = response['selectOption'];
            var optSelected = '';

            //default subsector
            $('select#fkSubsectorsID')
                .append($("<option "+ optSelected +"></option>")
                    .attr("value","")
                    .text(selectOption));

            //default activity
            $('select#fkActivitiesID')
                .append($("<option "+ optSelected +"></option>")
                    .attr("value","")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                if( FKSUBSECTOR_SELECTED == value.pkSubsectorsID ){
                    optSelected = 'selected="selected"';
                }
                $('#fkSubsectorsID')
                    .append($("<option "+ optSelected +"></option>")
                        .attr("value",value.pkSubsectorsID)
                        .text(value.subsectorsName));

                optSelected = '';
            });

            console.log(FKSUBSECTOR_SELECTED);
            if( FKSUBSECTOR_SELECTED == 'other' ){
                optSelected = 'selected="selected"';
                $('.subsectorOther').show();
            }
            $('#fkSubsectorsID')
                .append($("<option "+ optSelected +"></option>")
                    .attr("value",'other')
                    .text(OPT_OTHER));
        });
    }

    if( FKSUBSECTOR_SELECTED != '' ){
        $('select#fkActivitiesID').empty();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getActivitiesBySubsectorsID',
            method: 'GET',
            data: {subsectorId:FKSUBSECTOR_SELECTED}
        }).done(function(response){
            var selectOption = response['selectOption'];
            var optSelected = '';

            $.each(response['data'], function(key, value){
                if( FKACTIVITY_SELECTED == value.pkActivitiesID ){
                    optSelected = 'selected="selected"';
                }
                $('#fkActivitiesID')
                    .append($("<option "+ optSelected +"></option>")
                        .attr("value",value.pkActivitiesID)
                        .text(value.activitiesName));

                optSelected = '';
            });

            if( FKACTIVITY_SELECTED == 'other' ){
                optSelected = 'selected="selected"';
                $('.activitiesOther').show();
            }
            $('#fkActivitiesID')
                .append($("<option "+ optSelected +"></option>")
                    .attr("value",'other')
                    .text(OPT_OTHER));
        });
    }

    //Subsector Other
    $('#fkSubsectorsID').change(function(){
        $('.subsectorOther').hide();
        if( $(this).val() == 'other'){
            $('.subsectorOther').show();
        }
    });

    //Main activity other
    $('#fkActivitiesID').change(function(){
        $('.activitiesOther').hide();
        if( $(this).val() == 'other'){
            $('.activitiesOther').show();
        }
    });*/
});