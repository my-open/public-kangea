$(document).ready(function(){

    //click country
    $('select#fkCountriesID').change(function(){

        $('select#fkProvincesID').empty();
        $('select#fkDistrictsID').empty();
        $('select#fkCommunesID').empty();
        $('select#fkVillagesID').empty();

        var countryId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getProvinces',
            method: 'GET',
            data: {countryId:countryId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default province
            $('select#fkProvincesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));
            //default district
            $('select#fkDistrictsID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));
            //default commune
            $('select#fkCommunesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));
            //default village
            $('select#fkVillagesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.provincesNameEN;
                else name = value.provincesNameKH;
                $('#fkProvincesID')
                    .append($("<option></option>")
                        .attr("value",value.pkProvincesID)
                        .text(name));
            });

        });
    });

});

