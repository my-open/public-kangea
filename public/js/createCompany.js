$(document).ready(function(){

    var sectorId = $('select#fkSectorsID').val();
    var provincesId = $('select#fkProvincesID').val();

    if( sectorId != "" ){
        //get data(subsector) from sector
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getSubsectors',
            method: 'GET',
            data: {sectorId:sectorId}
        }).done(function(response){
            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.subsectorsNameEN;
                else name = value.subsectorsNameKH;

                //get code here
                    if( subsectorId == value.pkSubsectorsID){
                        $('#fkSubsectorsID')
                            .append($("<option selected></option>")
                                .attr("value", value.pkSubsectorsID)
                                .text(name));
                    }else {
                        $('#fkSubsectorsID')
                            .append($("<option></option>")
                                .attr("value", value.pkSubsectorsID)
                                .text(name));
                    }

            });
        });
    }

    if( sectorId !="" && subsectorId !="" ){
        //get data(activity) from subsectorId
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getActivities',
            method: 'GET',
            data: {subsectorId:subsectorId}
        }).done(function(response){
            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.activitiesNameEN;
                else name = value.activitiesNameKH;

                //get code here
                if( activityId == value.pkActivitiesID ){
                    $('#fkActivitiesID')
                        .append($("<option selected></option>")
                            .attr("value", value.pkActivitiesID)
                            .text(name));
                }else {
                    $('#fkActivitiesID')
                        .append($("<option></option>")
                            .attr("value", value.pkActivitiesID)
                            .text(name));
                }

            });
        });

    }

    if( provincesId != ""){
        //get data(district) from province
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getDistricts',
            method: 'GET',
            data: {provincesId:provincesId}
        }).done(function(response){
            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.districtsNameEN;
                else name = value.districtsNameKH;

                //get code here
                if ( districtId == value.pkDistrictsID ){
                    $('#fkDistrictsID')
                        .append($("<option selected></option>")
                            .attr("value",value.pkDistrictsID)
                            .text(name));
                }else{
                    $('#fkDistrictsID')
                        .append($("<option></option>")
                            .attr("value",value.pkDistrictsID)
                            .text(name));
                }
            });
        });
    }

    if( provincesId != "" && districtId != "" ){
        //get data(commune) from district
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getCommunes',
            method: 'GET',
            data: {districtId:districtId}
        }).done(function(response){
            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.communesNameEN;
                else name = value.communesNameKH;

                //get code here
                if ( communeId == value.pkCommunesID ){
                    $('#fkCommunesID')
                        .append($("<option selected></option>")
                            .attr("value",value.pkCommunesID)
                            .text(name));
                }else{
                    $('#fkCommunesID')
                        .append($("<option></option>")
                            .attr("value",value.pkCommunesID)
                            .text(name));
                }
            });
        });
    }

    if( provincesId != "" && districtId != "" && communeId != "" ){
        //get data(village) from commune
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getVillages',
            method: 'GET',
            data: {communeId:communeId}
        }).done(function(response){
            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.villagesNameEN;
                else name = value.villagesNameKH;

                //get code here
                if ( villageId == value.pkVillagesID ){
                    $('#fkVillagesID')
                        .append($("<option selected></option>")
                            .attr("value",value.pkVillagesID)
                            .text(name));
                }else{
                    $('#fkVillagesID')
                        .append($("<option></option>")
                            .attr("value",value.pkVillagesID)
                            .text(name));
                }

            });
        });
    }


});