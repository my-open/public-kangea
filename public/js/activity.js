$(document).ready(function(){

    //click sector
    $('#fkSubsectorsID').change(function(){

        $('select#fkActivitiesID').empty();

        var subsectorId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getActivities',
            method: 'GET',
            data: {subsectorId:subsectorId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default position
            $('select#fkActivitiesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkActivitiesID')));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkActivitiesID')
                    .append($("<option></option>")
                        .attr("value",value.pkActivitiesID)
                        .text(value['activitiesName'+lang]));
            });
        });
    });

});
