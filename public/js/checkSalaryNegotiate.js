$(document).ready(function(e){
    //click announcementsSalaryNegotiate
    $('#announcementsSalaryNegotiate').click(function (){
        if (this.checked) {
            $('#announcementsSalaryFrom').attr('disabled', true);
            $('#announcementsSalaryTo').attr('disabled', true);
            $('#announcementsSalaryFrom').val('');
            $('#announcementsSalaryTo').val('');
            $('#hideShowDependsOn').hide();

            $('#announcementsSalaryFrom').attr('required', false);
            $('#announcementsSalaryTo').attr('required', false);
        } else {
            $('#announcementsSalaryFrom').attr('disabled', false);
            $('#announcementsSalaryTo').attr('disabled', false);

            $('#announcementsSalaryFrom').attr('required', true);
            $('#announcementsSalaryTo').attr('required', true);
        }
    });

    if($("#announcementsSalaryNegotiate").is(':checked')){
        $('#announcementsSalaryFrom').attr('disabled', true);
        $('#announcementsSalaryTo').attr('disabled', true);
        $('#announcementsSalaryFrom').attr('required', false);
        $('#announcementsSalaryTo').attr('required', false);
    }else{
        $('#announcementsSalaryFrom').attr('disabled', false);
        $('#announcementsSalaryTo').attr('disabled', false);
        $('#announcementsSalaryFrom').attr('required', true);
        $('#announcementsSalaryTo').attr('required', true);
    }

});