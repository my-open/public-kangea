$(document).ready(function(){
    $('.activityOther').hide();

    $fkActivityID = $('select#fkActivitiesID').val();
    if( $fkActivityID == 'other' ){
        $('.activityOther').show();
    }

     //Main activity other
     $('#fkActivitiesID').change(function(){
         $('.activityOther').hide();
         if( $(this).val() == 'other'){
            $('.activityOther').show();
         }
     });
});