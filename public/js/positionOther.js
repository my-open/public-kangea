$(document).ready(function(){
    $('.positionOther').hide();

    var fkPositionsID = $('#fkPositionsID').val();
    if( fkPositionsID == 0 && fkPositionsID != ''){
        $('.positionOther').show();
    }

    $('#fkPositionsID').change(function(){
        var fkPositionsID = $(this).val();
        if( fkPositionsID == 0 && fkPositionsID != ''){
            $('.positionOther').show();
        }else{
            $('.positionOther').hide();
        }
    });
});
