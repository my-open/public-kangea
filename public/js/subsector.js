$(document).ready(function(){

    //click sector
    $('#fkSectorsID').change(function(){
        var selectOption = $(this).find('option:first-child').text();
        console.log(selectOption);
        $('select#fkSubsectorsID').empty();
        $('select#fkActivitiesID').empty();

        var sectorId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getSubsectors',
            method: 'GET',
            data: {sectorId:sectorId}
        }).done(function(response){
            // var selectOption = response['selectOption'];
            var selectOption = SELECT_OPTION;

            //default subsector
            $('select#fkSubsectorsID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkSubsectorsID')));

            //default activity
            $('select#fkActivitiesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkActivitiesID')));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkSubsectorsID')
                    .append($("<option></option>")
                        .attr("value",value.pkSubsectorsID)
                        .text(value['subsectorsName'+lang]));

            });
        });
    });

});
