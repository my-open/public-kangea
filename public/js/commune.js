$(document).ready(function(){

    //click District
    $('#fkDistrictsID').change(function(){

        $('select#fkCommunesID').empty();
        $('select#fkVillagesID').empty();

        var districtId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getCommunes',
            method: 'GET',
            data: {districtId:districtId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default commune
            $('select#fkCommunesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkCommunesID')));
            //append Any where with empty value
            if(typeof(ANY_WHERE) != "undefined" && ANY_WHERE !== null)
            {
                $('select#fkDistrictsID')
                    .append($("<option></option>")
                        .attr("value","")
                        .text(ANY_WHERE));
            }
            //default village
            $('select#fkVillagesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(tranSelectOption('fkVillagesID')));

            $.each(response['data'], function(key, value){
                var lang = CURRENT_LANG.toUpperCase();
                $('#fkCommunesID')
                    .append($("<option></option>")
                        .attr("value",value.pkCommunesID)
                        .text(value['communesName'+lang]));
            });

        });
    });

});
