$(document).ready(function() {
  $('#fkProvincesID, #fkDistrictsID, #fkCommunesID').on('change', function(event) {
    loadingAnimation.reset();
    event.preventDefault();

    /* Act on the event */
    var inputArray = $('.bpAjax-input').find('select');
    var selectedID = $(this).val();
    var selectedName = $(this).attr('id');
    var getIDName = $(this).find('option[value="'+selectedID+'"]');

    // console.log([this, selectedID, selectedName, getIDName]);

    if (selectedID !== '' && selectedName === 'fkProvincesID') {
      $('.bpAjax-info').find('.badge.fkDistrictsID, .badge.fkCommunesID').text('');
      $('.'+selectedName).text($(getIDName).text());
      $('.bpAjax-input.bpAjax-input-fkDistrictsID').removeClass('hidden-xs');
    }else if (selectedID !== '' && selectedName === 'fkDistrictsID') {
      $('.bpAjax-info').find('.badge.fkCommunesID').text('');
      $('.'+selectedName).text($(getIDName).text());
      $('.bpAjax-input.bpAjax-input-fkCommunesID').removeClass('hidden-xs');
    }else if (selectedID !== '' && selectedName === 'fkCommunesID') {
      $('.'+selectedName).text($(getIDName).text());
    }

    if (selectedID === '' && selectedName === 'fkProvincesID') {
      $('.bpAjax-info').find('.badge').text('');
      $('.bpAjax-info').addClass('hidden');
      $('.bpAjax-input.bpAjax-input-fkDistrictsID').addClass('hidden-xs');
      $('.bpAjax-input.bpAjax-input-fkCommunesID').addClass('hidden-xs');
    }else if (selectedID === '' && selectedName === 'fkDistrictsID') {
      $('.bpAjax-info').find('.badge.fkDistrictsID, .badge.fkCommunesID').text('');
      $('.bpAjax-input.bpAjax-input-fkCommunesID').addClass('hidden-xs');
    }else if (selectedID === '' && selectedName === 'fkCommunesID') {
      $('.bpAjax-info').find('.badge.fkCommunesID').text('');
    }else {
      $('.bpAjax-info').removeClass('hidden');
    }

    var fkProvincesID = $('#fkProvincesID').val();
    var fkDistrictsID = $('#fkDistrictsID').val();
    var fkCommunesID = $('#fkCommunesID').val();

    loadingAnimation.start();

    $.ajax({
      url: '/'+CURRENT_LANG+'/searchJobLocationAjax',
      type: 'GET',
      dataType: 'html',
      data: {
        lang: CURRENT_LANG.toUpperCase(),
        fkProvincesID: fkProvincesID,
        fkDistrictsID: fkDistrictsID,
        fkCommunesID: fkCommunesID,
      }
    })
    .done(function(successData) {
      var sectorInLocation = {
        manufacturing: [],
        hospitality: [],
        construction: [],
        security: [],
      }; // used for viewing data only
      var jobByLocationData = JSON.parse(successData);

      // Removing existing data
      $('.jobByLocationData .jobByLocationData-content .sectorItem').remove();
      console.log(Object.keys(sectorInLocation));

      // If no data Notify user
      if (jobByLocationData.length === 0) {
        console.log('No data');
        $('.sectorContainer').addClass('hidden');
        $('.sectorContainer.jobByLocationNotFound').removeClass('hidden');

        loadingAnimation.error();
        return false;
      }else {
        $('.sectorContainer').removeClass('hidden');
        $('.sectorContainer.jobByLocationNotFound').addClass('hidden');
      }


      // Pushing array to pre-defined var
      // 1: manufacturing
      // 2: hospitality
      // 3: construction
      // 4: security
      for (var i = 0; i < jobByLocationData.length; i++) {

        switch (jobByLocationData[i].fkSectorsID) {
          case 1:
            sectorInLocation.manufacturing.push(jobByLocationData[i]['positionsName'+CURRENT_LANG.toUpperCase()]);
            break;
          case 2:
            sectorInLocation.hospitality.push(jobByLocationData[i]['positionsName'+CURRENT_LANG.toUpperCase()]);
            break;
          case 3:
            sectorInLocation.construction.push(jobByLocationData[i]['positionsName'+CURRENT_LANG.toUpperCase()]);
            break;
          case 4:
            sectorInLocation.security.push(jobByLocationData[i]['positionsName'+CURRENT_LANG.toUpperCase()]);
            break;
          default:
           console.log('Nothing to Import!');
        }

        var postID = jobByLocationData[i].fkPositionsID;
        var postName = jobByLocationData[i]['positionsName'+CURRENT_LANG.toUpperCase()];

        var sector_name = jobByLocationData[i].sectorsName_EN;
        sector_name = sector_name.replace(/\s+/g, '-').toLowerCase();

        var position_name = jobByLocationData[i].positionsName_EN;
        position_name = position_name.replace(/\s+/g, '-').toLowerCase();

        $('.jobByLocationData .jobByLocationData-content[jobByLocationData="'+jobByLocationData[i].fkSectorsID+'"]').append(
          '<div class="col-xs-12 col-sm-3 sectorItem ">'+
            // '<div class="list"> '+
              '<a href="/'+CURRENT_LANG+'/jobs/'+sector_name+'/'+position_name+'/'+postID+'">'+postName+'</a> '+
            // '</div> '+
          '</div>'
        );


      }

      loadingAnimation.success();
      setTimeout(function () {
        console.log($('.show_all_job').offset().top);
        var snapTo = ($('.show_all_job').offset().top - $('.navbar ').height()) - ($('.navbar ').height()/2) ;
        $('body, html').animate({ scrollTop: snapTo }, "fast");
      }, 3000);

      // Logging
      console.group("searchJobLocationAjax");
      // console.log(JSON.parse(successData));
      console.log(jobByLocationData);
      console.log(sectorInLocation);
      console.groupEnd();
    })
    .fail(function() {
      console.log("error");
    })
    .always(function(data) {

      // Hide Sector if position not exist
      var jobByLocationDataContent = $('.jobByLocationData-content[jobByLocationData]');
      for (var i = 0; i < jobByLocationDataContent.length; i++) {
        if ($(jobByLocationDataContent[i]).has('.sectorItem ').length === 0) {
          $(jobByLocationDataContent[i]).parent('.jobByLocationData').addClass('hidden');
        }else {
          $(jobByLocationDataContent[i]).parent('.jobByLocationData').removeClass('hidden');
        }
      }

      loadingAnimation.reset();

      // console.group("searchJobLocationAjax");
      // console.log(JSON.parse(data));
      // console.groupEnd();
    });


  });
});

function countObjectKeys(keys) {
  var count = 0;
  for (var i in keys) {
    if (keys.hasOwnProperty(i)) {
      // console.count();
      count++;
    }
  }
  return count;
}

var loadingAnimation = (function() {
  var loading = $('.bpAjax-spinner');
  var spin = $(loading).find('.spin');
  var success = $(loading).find('.success');
  var error = $(loading).find('.error');

  var timmer;

  function startLoading() {
    $(loading).removeClass('hidden');
  }

  function successLoading() {
    $(spin).addClass('hidden');
    $(error).addClass('hidden');
    $(success).removeClass('hidden');
  }

  function errorLoading() {
    $(spin).addClass('hidden');
    $(success).addClass('hidden');
    $(error).removeClass('hidden');
  }

  function resetLoading() {
    clearTimeout(timmer);
    timmer = setTimeout(function () {
      $(spin).removeClass('hidden');
      $(loading).addClass('hidden');
      $(success).addClass('hidden');
      $(error).addClass('hidden');
    }, 3000);
  }


  return {
    start: startLoading,
    success: successLoading,
    error: errorLoading,
    reset: resetLoading
  };
})();

function print() {
  // Creating New Window
  var printForm = window.open('', 'Print Preview');

  // Insert HTML into printForm
  var printHTML = $(document).context.documentElement.outerHTML;
  printForm.document.body.innerHTML = printHTML;

  setTimeout(function () {

    // Automatic Open Print Diaglog
    printForm.print();

    // Close Windows after print completed
    printForm.close();
  }, 1000);
}
