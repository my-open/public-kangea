$(document).ready(function(){

    //click commune
    $('#fkCommunesID').change(function(){

        $('select#fkVillagesID').empty();

        var communeId = $(this).val();
        $.ajax({
            url: '/' + CURRENT_LANG + '/account/getVillages',
            method: 'GET',
            data: {communeId:communeId}
        }).done(function(response){
            var selectOption = response['selectOption'];

            //default village
            $('select#fkVillagesID')
                .append($("<option selected></option>")
                    .attr("value","")
                    .text(selectOption));

            $.each(response['data'], function(key, value){
                var name ='';
                if( CURRENT_LANG == 'en' ) name = value.villagesNameEN;
                else name = value.villagesNameKH;
                $('#fkVillagesID')
                    .append($("<option></option>")
                        .attr("value",value.pkVillagesID)
                        .text(name));

            });
        });
    });

});