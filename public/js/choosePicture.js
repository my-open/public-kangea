

function issueUpload(inutfile,showtext) {
    var inutfile = '#' + inutfile;
    var showtext = '#' + showtext;
    // console.log(val1 + val2);
    $(inutfile).click();
    $(inutfile).change(function(){
        // Cut C:/fakepath
        var fileName = getUploadFilename($(inutfile).val());
        // console.log(fileName);
        $(showtext).val(fileName);
    });
}

function getUploadFilename(input_file) {
    var path = input_file;
    var filename = path.replace(/^.*\\/, "");
    // console.log(path + ' ' + filename);
    return filename;
}

