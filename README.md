# វិធីដំឡើង
- Clone this repository
- ដំឡើង Composer (if already install skip this step)
- ប្រើ Composer ដើម្បីដំឡើង component
- កំណត់មូលដា្ឋនទិន្នន័យសម្រាប់ការអភិវឌ្ឍ

# របៀបប្រើប្រាស់ Command

## Clone repository
```sh
git clone {git-url} {directory}
```
* ប្រើដោយគ្មានសញ្ញា {}

## Composer
### ការតំឡើង

ទាញយក Composer ពីទីនេះ :
```url
https://getcomposer.org/
```

### តំឡើង Dependencies
```sh
composer install
```


## កំណត់ Environment សម្រាប់អភិវឌ្ឍ ឬប្រើប្រាស់
* បង្កើតឯកសារមួយទៀតចេញពី '.env.example' និងផ្លាស់ប្តូរឈ្មោះវាទៅជា '.env'
```sh
cp .env.example .env
```
* កែប្រែព័ត៌មានសម្រាប់ការភ្ជាប់ទៅកាន់មូលដ្ឋានទិន្នន័យនៅក្នុងឯកសារ '.env' នេះ
* បង្កើត APP_KEY ថ្មី
```sh
php artisan key:generate
```

# ចប់!!! អ្នកអាចប្រើប្រាស់វាបាន

# អជ្ញាប័ណ្ណ
កម្មវិធីនេះគឺកម្មវិធីឥតគិតថ្លៃ: អ្នកអាចប្រើ, សិក្សាចែករំលែកនិងធ្វើអោយប្រសើរឡើងតាមតែឆន្ទៈរបស់អ្នក។ ជាពិសេសអ្នកអាចចែកចាយឡើងវិញនិង / ឬកែប្រែវានៅក្រោមលក្ខខណ្ឌរបស់
អាជ្ញាប័ណ្ណសាធារណៈទូទៅ GNU ជាបោះពុម្ភផ្សាយ ឥតគិតថ្លៃទាំងកំណែទី 3 នៃអាជ្ញាប័ណ្ណឬ(នៅជម្រើសរបស់អ្នក) កំណែក្រោយៗទៀត។

# License
This program is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the GNU General Public License as published, either version 3 of the License, or (at your option) any later version.
