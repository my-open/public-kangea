<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => '必须被接受',
    'active_url'           => '必须是有效的网址',
    'after'                => '该日期必须在上一项的日期之后',
    'alpha'                => '此项只能为字母',
    'alpha_dash'           => '此项只能为字母、数字或下划线',
    'alpha_num'            => '此项只能为字母和数字',
    'array'                => 'The :attribute must be an array.',
    'before'               => '该日期必须在:date之前',
    'between'              => [
        'numeric' => '该项必须在:min和:max之间',
        'file'    => '该项必须在:min和:max千字节之间',
        'string'  => '该项必须在:min和:max字之间',
        'array'   => '该项必须在:min和:max项之间',
    ],
    'boolean'              => '此项只能填是或不是',
    'confirmed'            => '此项和证明不符',
    'date'                 => '这不是一个有效的日期',
    'date_format'          => '这不符合格式',
    'different'            => '此项必须与:other不同',
    'digits'               => '此项必须为:digits位数',
    'digits_between'       => '此项必须在最大值和最小值之间',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => '这项必须为邮箱的电子邮箱地址',
    'exists'               => '无效',
    'filled'               => '此项必填',
    'image'                => '此项必须为图片',
    'in'                   => '此选择无效',
    'integer'              => '必须为整数',
    'ip'                   => '必须为有效的IP地址',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => '此项不能大于:max',
        'file'    => '该项不能大于:max千字节',
        'string'  => '该项不能大于:max字',
        'array'   => '该项不能大于:max项',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => '此项不能小于:min',
        'file'    => '此项不能小于:min千字节',
        'string'  => '此项不能小于:min字',
        'array'   => '此项不能小于:min项',
    ],
    'not_in'               => '此选项无效',
    'numeric'              => '此项必须为数字',
    'present'              => '此项必填',
    'regex'                => '此格式不正确',
    'required'             => '此项必填',
    'required_if'          => '当:other为:value时，此项必填',
    'required_unless'      => '当:other为:values时，此项不用填，其他情况必填',
    'required_with'        => '当:values出现时，此项必填.',
    'required_with_all'    => '当:values出现时，此项必填',
    'required_without'     => '当:values没有出现时，此项必填',
    'required_without_all' => '当任何:values都没有出现时，此项必填',
    'same'                 => '此项和:other必须相同',
    'size'                 => [
        'numeric' => '此项的大小必须为:size',
        'file'    => '此项的大小必须为:size千字节',
        'string'  => '此项的大小必须为:size字',
        'array'   => '此项的必须含有:size项',
    ],
    'string'               => '必须是字母',
    'timezone'             => '必须是存在的时区',
    'unique'               => '已被使用过',
    'url'                  => '格式错误',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
