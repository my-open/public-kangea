@extends('layouts.account')
@section('title', 'Bongpheak SUDO')
@section('breadcrumbs', Breadcrumbs::render('account'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.sudo') }}</big></div>

        <div class="text-center panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/sudoAsEmp', 'class' => 'form-horizontal', 'company' => 'form')) !!}

                        <div class="form-group">
                            <div class="form-group{{ $errors->has('fkCompaniesID') ? ' has-error' : '' }}">
                                {!! Form::label('fkCompaniesID', trans('text_lang.company'), ['class' => 'col-md-3 control-label']) !!}
                                <div class="col-md-7">
                                    {!! Form::select('fkCompaniesID', (['' => trans('text_lang.selectOption')] + $companies->toArray() ), null, ['class' => 'form-control']) !!}
                                </div>
                                <div class="col-md-2">
                                    @if ($errors->has('fkCompaniesID'))
                                        <span class="help-block text-left">
                                            <strong>{!! $errors->first('fkCompaniesID') !!}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-7 text-right">
                                    <button type="submit" class="btn btn-primary btn-block" name="btnSearch" value="btnSearch">
                                        {{ trans('text_lang.sudo')}}
                                    </button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
