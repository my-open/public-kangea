@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.aboutBongpheak') )
@section('extraCSS')
  <link rel="stylesheet" href="{{ URL::asset('css/registrationScreen.css') }}">
@endsection
@section('content-fluid')
  <div class="row">
      <div class="col-md-12 spaceTopLogo" style="
        background: url('/images/fondo.jpg') no-repeat;
        background-size: cover;
        min-height: 400px;
        margint-top: -10px;
        padding-top: 0;
        margin-bottom: 20px;
      ">
      </div>
  </div>
@endsection
@section('content')
    <div class="row mainPic">
        <div class="col-md-12">
            <div class="row hidden">
                <div class="col-md-12 spaceTopLogo">
                    <img class="img-responsive fondoPic" src="/images/fondo.jpg" alt="fondo">
                </div>
            </div>
            <div class="row hidden">
                <div class="col-md-12 text-center">
                    <a href="{{ url(\Lang::getLocale() . '/') }}"> <img src="/images/logo.png" alt="bongpheak logo"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 box1">
                    @if( Lang::getLocale() == 'kh')
                        <div class="row">
                            <div class="col-md-12 boxKoulen">
                                បងភ័ក្រ​ ជួយ​ស្វែង​រក​ជំនាញ​ដែល​ក្រុម​ហ៊ុន​ត្រូវ​ការ​ !
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12 boxEN">
                                We find the skills you need!
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row aboutus">
        <div class="col-md-12">

            <h2>{{ trans('text_lang.aboutBongpheak') }}</h2>
            @if( Lang::getLocale() == 'en')
            <p><b>Bong Pheak</b> is an Employment Service that helps employers and potential workers to get in touch with each other.</p>

            <p><b>Bong Pheak</b> is specially designed for the construction, manufacturing, hospitality and security sectors, covering both low-skilled and unskilled workers.</p>

            <p>
                <b>For workers, Bong Pheak:</b>
                <ul>
                    <li>Provides information about possible jobs that they (or their friends and family) might be interested in.</li>
                    <li>Allows them to apply for jobs posted by companies, communicating to the companies the contact data of the applicant.</li>
                    <li>Allows them to refer jobs they see in <b>Bong Pheak</b> to friends and family who might be interested.</li>
                </ul>
            </p>

            <p>
                <b>For companies in the above mentioned sectors, Bong Pheak:</b>
                <ul>
                    <li>Allows them to post their job announcements quickly at no cost to the company.</li>
                    <li>Provides contact information of workers who are interested in their job announcements.</li>
                    <li>Allows them to search the applications of workers registered in <b>Bong Pheak</b> for candidates who have the specific skills that they need.</li>
                    <li>Allows them to process application at their own speed and time, not forced by the time at which candidates show up.</li>
                </ul>
            </p>

            <h3><b>What makes Bong Pheak special?</b></h3>

            <p>
                <b>Bong Pheak does not try to change the way potential workers find employment.</b>
                <ul>
                    <li>Often those who seek their first unskilled employment ask their friends and family who are working about available jobs that they know of. These friends and family are usually only able to provide information about available work in the place where they work.</li>
                    <li>Employers often only announce available positions to their current workers, asking them to inform their friends and family about these opportunities.</li>
                    <li><b>Bong Pheak</b> supports this channel of communication by providing information to current employees about jobs in their sector, and helps them send this information to potential workers back at home through automatic phone calls.</li>
                </ul>
            </p>

            <p>
                <b>Bong Pheak uses technology that is adequate for each person</b>
                <ul>
                    <li>At <b>Bong Pheak</b> we know that almost half of those who are presently employed will have smartphones by the end of 2016, but very few of those looking for their first job will have such devices and access to Internet. This is why it only aims to communicate with present workers through Facebook.</li>
                    <li>
                        For those who do not have access to Internet, Bong Pheak is able to communicate the employment offers to them through automatic phone calls, after their friends who use Facebook refer the employment offers to their phone number. They will receive a phone call that will sound more or less like this:
                        <ul>
                            <li><em>Hi, we are calling you from the Bong Pheak employment service. Your older sister has requested that we communicate the following employment offer to you:
                                    The company XXX, which manufactures garments, is located in the Mencheay district of Phnom Penh.
                                    They are looking for three candidates for the position of “linker”, and they offer the following conditions.
                                    Monthly salary of 160 to 210 depending on the ability of the worker, adding also the following benefits: transportation allowance, food allowance, punctuality bonus, and holiday bonus.
                                    The offer is open until 30 / 6 / 2017.
                                    If you would like to hear this announcement again now, please press 0.
                                    If you are interested in this opportunity and you would like the employer to call you, please press 1.
                                    If you are interested in listening to other offers in nearby work places, please press 2.
                                    If you want us to repeat this automatic call to you in one hour, please press 3. If you want as to repeat the call tomorrow, please press 4.</em></li>
                        </ul>
                    <br/>
                    If the person receiving the call hangs up and calls the Bong Pheak number back, Bong Pheak will remember the phone number, and play the same announcement again, or present other jobs near the same location (Bong Pheak assumes that the person wishes to work near their friends and family).<br/><br/>
                    </li>
                    <li>Those who have access to Facebook can use Bong Pheak to search for employment anywhere in the country and in any of the available sectors that they work on. They can also register on Bong Pheak, allowing employers to find them if they have the right expertise.</li>
                </ul>
            </p>

            <h3><b>How can Bong Pheak match workers and employers so well?</b></h3>

            <p><b>Bong Pheak</b> has a deep knowledge of the sectors it works with, knowing all the most common positions available in these sectors, constantly increasing its knowledge. This know-how allows it to help companies prepare announcements that are highly structured, using only pull-down menus. With this information it is able to automatically prepare text or voice-based announcements.</p>
            <p>As the information about location, sector, position, and requirements is well structured, it is possible to automatically match information to the experience of registered workers, or to the preferences of candidates looking for work.</p>
            <p>This structure also allows Bong Pheak to produce announcements that are in a language different from the one of the person who prepared the announcement. For example, an announcement can be prepared in English, but the worker will see it or hear it in Khmer.</p>

            <p>
                <h3><b>What are the advantages for companies of using Bong Pheak?</b></h3>
                <ul>
                    <li>Preparing and posting job announcements in less than two minutes.</li>
                    <li>Receiving lists of candidates that they can manage and call whenever they want.</li>
                    <li>Being able to search Bong Pheak’s list of registered job-seekers looking for work in their sector, and finding immediately those that match their needs.</li>
                    <li>Knowing the skills of workers before they interview them.</li>
                    <li>No cost.</li>
                    <li>A professional, efficient, free, and easy-to-use computer tool that facilitates selection and hiring processes.</li>
                </ul>
            </p>

            <p>
                <h3><b>What are the advantages for workers of using Bong Pheak?</b></h3>
                <ul>
                    <li>Find jobs quickly that match their expertise in the sector and the location where they want to work.</li>
                    <li>Post their profile and experience at Bong Pheak and allow employers to find them and offer them jobs with clear conditions.</li>
                    <li>Refer interesting jobs to their family and friends who are looking for work.</li>
                    <li>Listen to the job offers and signal if they want to be contacted by the employer.</li>
                </ul>
            </p>

            <p>
            <h3><b>How do I use Bong Pheak?</b></h3>
                <h4><b>Companies</b></h4>
                <ul>
                    <li>Register on Bong Pheak. It only takes two minutes.</li>
                    <li>Search for workers or post job announcements.</li>
                    <li>Call the best candidates.</li>
                </ul>
                <h4><b>Workers</b></h4>
                <ul>
                    <li>Search Bong Pheak for the type of job that you are looking for.</li>
                    <li>Register in Bong Pheak and let the employers find you.</li>
                </ul>
            </p>
            @else
                <p><b>បងភ័ក្រ</b> គឺជា​សេវាកម្ម​ការងារ​ដែល​ជួយ​និយោជក និង​និយោជិត/កម្មករ​ឲ្យ​មាន​ទំនាក់ទំនង​ជា​មួយ​គ្នា។</p>
                <p><b>បងភ័ក្រ</b> ត្រូវ​បាន​រចនា​ឡើង​យ៉ាង​​ពិសេស​សម្រាប់​វិស័យ​សំណង់, រោងចក្រ, សន្តិសុខ និង​បដិសណ្ឋារកិច្ច សម្រាប់​និយោជិត/កម្មករ​​គ្មាន​ជំនាញ និង​មាន​ជំនាញ​កម្រិត​ទាប។</p>
                <p>
                    <b>សម្រាប់​និយោជិត/កម្មករ បងភ័ក្រ៖</b>
                    <ul>
                        <li>ផ្ដល់​ព័ត៌មាន​អំពី​ការងារ​ដែល​មាន​ដែល​ពួកគេ (ឬ​មិត្តភ័ក្ដិ និង​ក្រុម​គ្រួសារ​របស់​ពួកគេ)​ អាច​ចាប់អារម្មណ៍</li>
                        <li>អនុញ្ញាត​ឲ្យ​ពួកគេ​ដាក់ពាក្យ​សុំ​ការងារ​​ធ្វើ​​ដែល​បាន​ប្រកាស​ដោយ​ក្រុមហ៊ុន និង​ផ្ដល់​ព័ត៌មាន​ទំនាក់ទំនង​ឲ្យ​​ក្រុមហ៊ុន</li>
                        <li>អនុញ្ញាត​ឲ្យ​ពួកគេ​ប្រាប់​ពី​ការងារ​ដែល​​ឃើញ​នៅ​ក្នុង​​ <b>បងភ័ក្រ</b>​ដល់​មិត្តភ័ក្ដិ និង​ក្រុម​គ្រួសារ​ដែល​​ពួកគេ​អាច​ចាប់អារម្មណ៍</li>
                    </ul>
                </p>

                <p>
                    <b>សម្រាប់​ក្រុមហ៊ុន​ក្នុង​វិស័យ​បាន​​​រៀបរាប់​ខាង​លើ បងភ័ក្រ៖ </b>
                    <ul>
                        <li>អនុញ្ញាត​ឲ្យ​ពួកគេ​ប្រកាស​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​បាន​ភ្លាមៗ​ដោយ​មិន​គិត​ថ្លៃ</li>
                        <li>ផ្ដល់​ព័ត៌មាន​ទំនាក់ទំនង​របស់​និយោជិត​/កម្មករ ដែល​ចាប់អារម្មណ៍​នឹង​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​របស់​ពួក​គេ</li>
                        <li>អនុញ្ញាត​ឲ្យ​ពួកគេ​ស្វែងរក​បញ្ជី​និយោជិត/កម្មករ​​ដែល​បាន​ចុះឈ្មោះ​នៅ​​ក្នុង<b>​បងភ័ក្រ</b>​សម្រាប់​បេក្ខជន​ដែល​មាន​ជំនាញ​​ជាក់លាក់​តាម​តម្រូវការ​របស់​ពួកគេ។</li>
                        <li>អនុញ្ញាត​ឲ្យ​ពួកគេ​ដំណើរការ​ជ្រើសរើស​​បុគ្គលិក​តាម​តម្រូវការ​ និង​ពេលវេលា​របស់​ពួកគេ</li>
                    </ul>
                </p>

                <h3><b>តើ​កត្តា​អ្វី​ធ្វើ​ឲ្យ​បងភ័ក្រ​មាន​លក្ខណៈ​ពិសេស?</b></h3>

                <p>
                    <b>បងភ័ក្រ មិន​ព្យាយាម​ផ្លាស់ប្ដូរ​វិធី​ដែល​និយោជិត/កម្មករ​​រក​ការងារ​ធ្វើ​ទេ។</b>
                <ul>
                    <li>ជា​រឿយៗ​អ្នក​ដែល​ស្វែងរក​ការងារ​គ្មាន​ជំនាញ​ដំបូង​របស់​ពួកគេ​ តែងតែ​សួរ​មិត្តភ័ក្ដិ និង​ក្រុម​គ្រួសារ​ដែល​កំពុង​ធ្វើ​ការ​អំពី​ការងារ​ដែល​មាន​ដែល​ពួកគេ​ដឹង។ តាម​ធម្មតា មិត្តភ័ក្ដិ​ និង​ក្រុម​គ្រួសារ​​អាច​ផ្ដល់​ព័ត៌មាន​អំពី​ការងារ​ដែល​មាន​នៅ​កន្លែង​​ធ្វើការ​របស់​ពួកគេ។</li>
                    <li>ជា​ទូទៅ និយោជក​ប្រកាស​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​ដល់​និយោជិត​/កម្មករ​​ដែល​កំពុង​បម្រើ​ការងារ​ ដោយ​​ឲ្យ​ពួក​គេ​​ប្រាប់​ដំណឹង​នេះ​ដល់​មិត្តភ័ក្ដិ និង​ក្រុម​គ្រួសារ​អំពី​ឱកាស​ការងារ​ទាំ​ង​នេះ។</li>
                    <li><b>បងភ័ក្រ</b> គាំទ្រ​មធ្យោបាយ​ទំនាក់ទំនង​នេះ​ដោយ​ផ្ដល់​ព័ត៌មាន​ដល់​និយោជិត/កម្មករ​​​ដែល​កំពុង​បម្រើ​ការងារ​អំពី​ការងារ​ក្នុង​ផ្នែក​របស់​ពួកគេ ព្រម​ទាំង​ជួយ​ពួកគេ​ផ្ញើ​ព័ត៌មាន​នេះ​ទៅ​និយោជិត/​​កម្មករ​នៅ​ផ្ទះ​តាមរយៈ​ការ​ហៅ​ទូរស័ព្ទ​ដោយ​ស្វ័យ​​ប្រវត្តិ។</li>
                </ul>
                </p>

                <p>
                    <b>បងភ័ក្រ​ ប្រើ​បច្ចេកវិទ្យា​ដែល​សមស្រប​សម្រាប់​មនុស្ស​ម្នាក់ៗ</b>
                <ul>
                    <li>នៅ​ក្នុង ​<b>បងភ័ក្រ</b> យើង​ដឹង​ថា​​មនុស្ស​ជិត​ពាក់កណ្ដាល​​ដែល​បច្ចុប្បន្ន​​កំពុង​បម្រើ​ការងារ​​នឹង​​​មាន​ទូរស័ព្ទ​ទំនើប​​ ប្រើ​ប្រាស់​ត្រឹម​ចុង​ឆ្នាំ ២០១៦ ប៉ុន្តែ​មាន​មនុស្ស​តិចតួច​ណាស់​​ដែល​កំពុង​ស្វែងរក​ការងារ​ដំបូង​មាន​ឧបករណ៍​​ប្រភេទ​នេះ និង​អាច​ប្រើ​អ៊ីនធឺណិត​បាន។ នេះ​​ជា​មូលហេតុ​ដែល​​ <b>បង​ភ័ក្រ</b>​ត្រូវ​​​ទាក់ទង​ជា​មួយ​និយោជិត​/កម្មករ​បច្ចុប្បន្ន​តាមរយៈ​ហ្វេសប៊ុក។</li>
                    <li>
                        សម្រាប់​អ្នក​ដែល​មិន​ប្រើ​អ៊ីនធឺណិត បងភ័ក្រ អាច​​ទាក់ទង​ពួកគេ​​អំពី​ឱកាស​ការងារ​តាមរយៈ​ការ​ហៅ​ទូរស័ព្ទ​ដោយ​ស្វ័យ​ប្រវត្តិ បន្ទាប់​ពី​មិត្តភ័ក្ដិ​របស់​ពួកគេ​ដែល​ប្រើ​ហ្វេសប៊ុក​​​បញ្ជូន​ឱកាស​ការងារ​ទៅ​​លេខ​ទូរស័ព្ទ​​​​ពួកគេ។  ពួកគេ​នឹង​ទទួលការ​ហៅ​ដែល​មាន​សំឡេង​ដូច​ខាង​ក្រោម៖
                        <ul>
                            <li><em>សួស្ដី, យើង​កំពុង​ហៅ​អ្នក​ពី​សេវាកម្ម​ការងារ​បងភ័ក្រ។ បង​ស្រី​របស់​អ្នក​បាន​ស្នើ​សុំ​ឲ្យ​យើង​ទាក់ទង​អ្នក​​អំពី​ការ​ឱកាស​ការងារ​ដូច​ខាង​ក្រោម៖ ក្រុមហ៊ុន កខគ ធ្វើការ​​ផ្នែក​កាត់ដេរ​សំលៀកបំពាក់​ដែល​មាន​ទីតាំង​ស្ថិត​នៅ​ក្នុង​ខណ្ឌ​មាន​ជ័យ រាជធានី​ភ្នំពេញ។ ក្រុមហ៊ុន​កំពុង​ជ្រើសរើស​បុគ្គលិក​បី​នាក់​សម្រាប់​មុខតំណែង "អ្នក​​​តភ្ជាប់"​ ដោយ​ផ្ដល់​ជូន​នូវ៖ ប្រាក់​ខែ​ចាប់​​ពី ១៦០ ដល់ ២១០ ដុល្លារ​អាស្រ័យ​លើ​សមត្ថភាព​របស់​និយោជិត/កម្មករ, ហើយ​ក៏​ផ្ដល់​អត្ថប្រយោជន៍​ផ្សេងៗ​ទៀត​ដូច​ជា៖ ថ្លៃ​ធ្វើ​ដំណើរ, ថ្លៃ​ម្ហូបអាហារ, ប្រាក់​​រង្វាន់​សម្រាប់​ការ​​មក​ធ្វើការ​ទៀងទាត់​ពេលវេលា និង​ប្រាក់​រង្វាន់​សម្រាប់​ការ​ធ្វើ​ការ​នៅ​ថ្ងៃ​ឈប់​សម្រាក។ ឱកាស​ការងារ​នេះ​នឹង​ផុត​កំណត់​នៅ​ថ្ងៃ​ទី ៣០ ខែ​មិថុនា ឆ្នាំ២០១៦។ ឥឡូវ​ ប្រសិនបើ​អ្នក​ចង់​ស្ដាប់​​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​នេះ​​ម្ដង​ទៀត សូម​ចុច​លេខ ០, ប្រសិន​បើ​អ្នក​ចាប់អារម្មណ៍​ឱកាស​ការងារ​នេះ ហើយ​ចង់​ឲ្យ​និយោជក​ហៅ​មក​អ្នក សូម​ចុច​លេខ ១, ប្រសិនបើ​អ្នក​​​​ចាប់អារម្មណ៍​ស្ដាប់​ឱកាស​ការងារ​ផ្សេង​ៗ​ទៀត​នៅ​​​​ក្បែរ​កន្លែង​​អ្នក សូម​ចុច​លេខ ២, ប្រសិនបើ​អ្នក​ចង់​ឲ្យ​យើង​​ហៅ​ទៅ​អ្នក​ដោយ​ស្វ័យ​ប្រវត្តិ​ក្នុង​រយៈពេល​មួយ​ម៉ោង​ក្រោយ សូម​ចុច​លេខ ៣, ប្រសិនបើ​អ្នក​ចង់​ឲ្យ​យើង​​ហៅ​ទៅ​អ្នក​ម្ដង​ទៀត​នៅ​ថ្ងៃ​ស្អែក​សូម​ចុច​លេខ ៤ ។</em></li>
                        </ul>
                        <br/>
                        ប្រសិន​បើ​អ្នក​ជា​អ្នក​​ទទួល​ការ​ហៅ​ដែល​បាន​ដាច់ ហើយ​​ហៅ​​មក​លេខ​បង​ភ័ក្រ​ម្ដង​ទៀត នោះ​បងភ័ក្រ​នឹង​ចងចាំ​លេខ​ទូរស័ព្ទ​ ហើយ​នឹង​​ប្រកាស​​ដំណឹង​ដដែល​ម្ដង​ទៀត ឬ​​ប្រកាស​ការងារ​ផ្សេង​ទៀត​ដែល​នៅ​ជិត​ទីតាំង​ដដែល (បងភ័ក្រ​សន្មត​ថា​មនុស្ស​ចង់​ធ្វើការ​នៅ​ក្បែរ​​មិត្តភ័ក្ដិ និង​ក្រុម​គ្រួសារ​របស់​ពួកគេ)។<br/><br/>
                    </li>
                    <li>សម្រាប់​អ្នក​ដែល​ប្រើ​ហ្វេសប៊ុក​អាច​ប្រើ​បងភ័ក្រ ដើម្បី​ស្វែងរក​ការងារ​នៅ​កន្លែង​ណាមួយ​ក្នុង​ប្រទេស និង​ក្នុង​​ផ្នែក​ណា​មួយ​ក្នុង​ចំណោម​ផ្នែក​ទាំង​បួន​ដែល​ពួកគេ​ធ្វើ​ការ។ ពួកគេ​ក៏​អាច​ចុះឈ្មោះ​ក្នុង​បងភ័ក្រ ដែល​​និយោជក​អាច​រក​បាន ប្រសិនបើ​ពួកគេ​មាន​ជំនាញ​ត្រឹមត្រូវ។</li>
                </ul>
                </p>

                <h3><b>តើ​បងភ័ក្រ​អាច​ផ្គូផ្គង​និយោជិត/កម្មករ និង​និយោជក​បាន​ល្អ​ដោយ​វិធី​ណា?</b></h3>

                <p><b>បងភ័ក្រ</b> មាន​ចំណេះដឹង​​ច្រើន​ពី​ផ្នែក​ដែល​ខ្លួន​ធ្វើការ ដោយ​ដឹង​​​ពី​​មុខតំណែង​ទូទៅ​​បំផុត​ទាំងអស់​ដែល​មាន​ក្នុង​ផ្នែក​ទាំង​នេះ និង​បង្កើន​ចំណេះដឹង​​របស់​ខ្លួន​យ៉ាង​សកម្ម​ភ្លាមៗ។ ជំនាញ​នេះ​​អនុញ្ញាត​ឲ្យ​បង​ភ័ក្រ​ជួយ​ក្រុមហ៊ុន​រៀបចំ​​​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​មាន​រចនាសម្ព័ន្ធ​ល្អ ដោយ​ប្រើ​តែ​ម៉ឺនុយ​ទម្លាក់​ចុះ​ប៉ុណ្ណោះ។ ជា​មួយ​ព័ត៌មាន​នេះ បងភ័ក្រ​អាច​រៀបចំ​ការ​ជូន​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​​តាម​បរិបទ ឬ​សំឡេង​ដោយ​ស្វ័យ​ប្រវត្តិ។</p>
                <p>ដោយ​សារ​តែ​ព័ត៌មាន​អំពី​ទីតាំង, ផ្នែក, មុខតំណែង និង​តម្រូវការ​ត្រូវ​បាន​រៀបចំ​តាម​រចនាសម្ព័ន្ធ​យ៉ាង​ល្អ វា អាច​ផ្គូផ្គង​ដោយ​ស្វ័យប្រវត្តិ​ជា​មួយ​​បទ​ពិសោធន៍​របស់​និយោជិត/កម្មករ​​ដែល​បាន​ចុះឈ្មោះ ឬ​ជា​មួយ​ចំណង់​ចំណូល​ចិត្ត​របស់​របស់​បេក្ខជន​ដែល​ស្វែងរក​ការងារ។</p>
                <p>រចនាសម្ព័ន្ធ​នេះ​ក៏​អនុញ្ញាត​ឲ្យ​បង​ភ័ក្រ​ផលិត​​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​​ជា​ភាសា​ផ្សេង​​ខុស​ពី​​ភាសា​របស់​មនុស្ស​ដែល​រៀបចំ​ដំណឹង​ជ្រើសរើស​បុគ្គលិក។ ឧទាហរណ៍ ដំណឹង​ជ្រើសរើស​បុគ្គលិក​អាច​ត្រូវ​បាន​រៀបចំ​ជា​ភាសា​អង់គ្លេស ប៉ុន្តែ​និយោជិត/កម្មករ​​នឹង​ឃើញ ព្រម​ទាំង​​ស្ដាប់​វា​ជា​ភាសា​ខ្មែរ។</p>

                <p>
                <h3><b>តើ​អ្វី​ជា​គុណសម្បត្តិ​សម្រាប់​ក្រុមហ៊ុន​ដែល​ប្រើ​បងភ័ក្រ?</b></h3>
                <ul>
                    <li>រៀបចំ និង​ប្រកាស​ដំណឹង​ជ្រើសរើស​បុគ្គលិក​​​ដែល​ចំណាយ​ពេល​តិច​ជាង​ពីរ​នាទី</li>
                    <li>ទទួល​បាន​បញ្ជី​បេក្ខជន​ដែល​ពួកគេ​អាច​គ្រប់គ្រង និង​ហៅ​នៅ​ពេល​ណា​ដែល​ពួកគេ​ត្រូវការ</li>
                    <li>អាច​ស្វែងរក​បញ្ជី​អ្នក​ស្វែងរក​ការងារ​ដែល​បាន​ចុះឈ្មោះ​ក្នុង​បងភ័ក្រ ដោយ​រក​មើល​ការងារ​ក្នុង​ផ្នែក​របស់​ពួកគេ និង​រក​ឃើញ​​អ្នក​ដែល​ត្រូវ​នឹង​តម្រូវការ​​របស់​ពួកគេ​បាន​ភ្លាមៗ</li>
                    <li>ដឹង​ពី​ជំនាញ​របស់​​និយោជិត​​​/កម្មករ​មុន​ពេល​សម្ភាសន៍​ពួកគេ</li>
                    <li>មិន​ចាំបាច់​ត្រូវ​ចំណាយ​ថវិកា</li>
                    <li>ជា​កម្មវិធី​​កុំព្យូទ័រ​ដែល​ងាយស្រួល​ប្រើ, មិន​គិត​ថ្លៃ, ប្រកប​ដោយ​សមត្ថភាព និង​មាន​ជំនាញ​​ដែល​សម្រួល​ដល់​ដំណើរការ​ជ្រើសរើស​ និង​ជួល​បុគ្គលិក</li>
                </ul>
                </p>

                <p>
                <h3><b>តើ​អ្វី​ជា​គុណសម្បត្តិ​សម្រាប់​និយោជិត/កម្មករ​​ដែល​ប្រើ​បងភ័ក្រ?</b></h3>
                <ul>
                    <li>ស្វែងរក​​ការងារ​បាន​យ៉ាង​ឆាប់​រហ័ស​ដែល​ត្រូវ​នឹង​ជំនាញ​​​របស់​ពួកគេ​ក្នុង​ផ្នែក​ណា​មួយ និង​ទីកន្លែង​ដែល​ពួក​គេ​ចង់​​ធ្វើ​ការ</li>
                    <li>ប្រកាស​ប្រវត្តិរូប និង​បទ​ពិសោធន៍​របស់​ពួកគេ​ក្នុង​បងភ័ក្រ ដែល​អនុញ្ញាត​ឲ្យ​និយោជក​រក​ឃើញ​​ ព្រម​ទាំង​ផ្ដល់​​​​ការងារ​ឲ្យ​ពួកគេ​ជា​មួយ​នឹង​លក្ខខណ្ឌ​ច្បាស់​លាស់</li>
                    <li>បញ្ជូន​ការងារ​ដែល​ចាប់អារម្មណ៍​ទៅ​ឲ្យ​មិត្តភ័ក្ដិ និង​ក្រុម​គ្រួសារ​របស់​ពួកគេ​ដែល​កំពុង​ស្វែងរក​ការងារ</li>
                    <li>ស្ដាប់​ឱកាស​ការងារ និង​ផ្ដល់​សញ្ញា​ប្រសិន​បើ​ពួកគេ​ចង់​ឲ្យ​និយោជក​ទាក់ទង​មក​​ពួកគេ</li>
                </ul>
                </p>

                <p>
                <h3><b>តើ​ខ្ញុំ​ប្រើ​បងភ័ក្រ​ដោយ​វិធី​ណា?</b></h3>
                <h4><b>ក្រុមហ៊ុន</b></h4>
                <ul>
                    <li>ចុះ​ឈ្មោះ​ក្នុង​បងភ័ក្រ វា​ចំណាយ​ពេល​តែ​ពីរ​នាទី​ប៉ុណ្ណោះ</li>
                    <li>ស្វែងរក​និយោជិត/កម្មករ​ ឬ​ប្រកាស​ដំណឹង​ជ្រើសរើស​បុគ្គលិក</li>
                    <li>ហៅ​បេក្ខជន​ដែល​ឆ្នើម</li>
                </ul>
                <h4><b>និយោជិត/កម្មករ</b></h4>
                <ul>
                    <li>ស្វែងរក​​ប្រភេទ​ការងារ​ដែល​អ្នក​កំពុង​ស្វែងរក​ក្នុង​បងភ័ក្រ</li>
                    <li>ចុះ​ឈ្មោះ​ក្នុង​បងភ័ក្រ និង​អាច​ឲ្យ​និយោជក​​រក​អ្នក​ឃើញ</li>
                </ul>
                </p>
            @endif
            <br/>
        </div>
    </div>
@endsection
