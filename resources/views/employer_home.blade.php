@extends('layouts.bongpheak')
@section('title', 'Bongpheak '. trans('text_lang.employer'))

@section('extraClass', 'bgImage')

@section('extraCSS')
  <style media="screen">
    body {
      background: url('/images/header_bg_2.jpg') fixed no-repeat;
      background-size: cover;
      background-position: top;
    }
    .navbar-default .navbar-nav .dropdown-menu li a,
    .navbar-default .navbar-nav .dropdown-menu {
      background-color: rgba(255, 255, 255, 0.16) !important;
    }
    .navbar-default .navbar-nav .dropdown-menu li a:hover {
      background-color: rgba(255, 255, 255, 0.30) !important;
    }
    .footer-navbar {
      border-top: 5px solid #fff;
    }

    .employerSection {
    }
    .employerSection .greeting {
      text-align: center;
      color: #fff;
      padding-bottom: 60px;
    }
    .employerSection .greeting h1 {
      background-color: rgba(0, 0, 0, 0.25);
      padding: 10px 10px;
      line-height: 60px;
    }
    .employerSection .greeting,
    .employerSection .feature {
      padding-top: 30px;
    }
    .employerSection .feature {
      background-color: #fff;
      border-top: 32px solid #4f97c4;
    }
    .employerSection .feature .feature-item .title {
      font-size: 24px;
    }
    .employerSection .feature .feature-item .desc {
      font-size: 18px;
    }

    .employerSection .divider {
      border-bottom: 2px solid #999;
      width: 250px;
      margin: 60px auto;
    }
    .employerSection .gettingstarted {
      background-color: #fff;
    }
    .employerSection .btn-register {
      color: #fff;
      background-color: #c4414c;
      padding-left: 60px;
      padding-right: 60px;
    }
    .pushUD {
      margin-top: 100px;
      margin-bottom: 100px;
    }
    @media screen and (min-width: 320px) and (max-width: 480px)  {
      body {
        background-position: top;
      }

      .employerSection .greeting h1 {
        background-color: rgba(0, 0, 0, 0.25);
        padding: 10px 10px;
        line-height: 30px;
        font-size: 22px
      }

      .navbar-default .navbar-nav .dropdown-menu li a, .navbar-default .navbar-nav .dropdown-menu {
        background-color: rgba(255, 255, 255, 0) !important;
      }
      .pushUD {
        margin-top: 20px;
        margin-bottom: 20px;
      }
    }

  </style>
@endsection

@section('content')

  <div class="container-fluid employerSection">
    <div class="row greeting">
      <div class="col-xs-12 col-sm-offset-1 col-sm-10 pushUD">
        <h1>

          {{ trans('text_lang.An_innovative_employment_service_for_Cambodia_Manufacturing_Hospitality_Construction_and_Security_sectors') }}
        </h1>
      </div>
    </div>

    <div class="row feature">
      <div class="col-xs-12 col-sm-4 text-center feature-item">
        <h2 class="title">{{ trans('text_lang.clear_target') }}</h2>
        <p class="desc">
          {{ trans('text_lang.bong_pheak_helps_you_connect_with_the_low_skilled_and_unskilled_workers_that_you_need') }}
        </p>
      </div>
      <div class="col-xs-12 col-sm-4 text-center feature-item">
        <h2 class="title">{{ trans('text_lang.innovation') }}</h2>
        <p class="desc">
          {{ trans('text_lang.we_use_the_latest_web_and_voice_technologies_to_reach_out_to_workers_in_your_sector') }}
        </p>
      </div>
      <div class="col-xs-12 col-sm-4 text-center feature-item">
        <h2 class="title">{{ trans('text_lang.our_value') }}</h2>
        <p class="desc">
          {{ trans('text_lang.bong_pheak_is_a_professional_innovative_efficient_free_and_easy_to_use_service_that_will_help_you_find_the_skills_that_you_need_at_the_time_you_need_them') }}
        </p>
      </div>
    </div>

    <div class="row gettingstarted text-center">
      <h2>{{ trans('text_lang.WeFindTheSkillYouNeed') }}</h2>
      <a class="btn btn-default btn-register" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/employer/register') }}">{{ trans('text_lang.register') }}</a>
    </div>

    <div class="row end" style="padding-bottom:100px;background-color:#fff">

    </div>
  </div>

@endsection

@section('extraJS')

@endsection
