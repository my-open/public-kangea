<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('includes._google_crawl')
    <link rel="shortcut icon" href="/favicon.png">
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{ URL::asset('lib/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel='stylesheet' type='text/css'>

    <!-- Fonts -->
    <link href="{{ URL::asset('lib/bower_components/font-awesome/css/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('lib/bower_components/normalize-css/normalize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('lib/bower_components/flag-icon-css/css/flag-icon.min.css') }}" type="text/css">

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrapCustom.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bongpheak.css') }}">
    <style>
        .chartNavIconClose {
            float: right;
            font-size: 34px;
            top: -3px;
        }
        .chartNavIconShow{
            font-size: 30px;
        }
    </style>
@yield('extraCSS')

<!-- Script -->
    <script src="{{ URL::asset('lib/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('lib/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="/js/bongpheak.js" charset="utf-8"></script>

</head>
<body id="app-layout">
<nav class="navbar navbar-fixed-top navbar-default navbar-bg">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
        <!-- <a class="brand-img" href="{{ url(\Lang::getLocale() . '/') }}"><img src="/images/logo.png" alt="bongpheak logo" height="56"></a> -->
            <a class="brand-img" href="{{ url(\Lang::getLocale() . '/') }}">
                @include('includes._logo')
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
        @if (Auth::guest())
            <!-- Left Side Of Navbar -->
            @include('includes._left_navbar')
        @endif
        <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @if(Entrust::hasRole('admin') || Entrust::hasRole('employer'))
                    <?php
                    $isSudoAsEmp = isSudoAsEmp();
                    $isAdmin = isAdmin();
                    ?>
                    @if($isAdmin)
                        @if($isSudoAsEmp)
                            <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/clearSudo') }}">{{ trans('text_lang.clearSudo') }}</a></li>
                        @else
                            <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/sudo') }}">{{ trans('text_lang.sudo') }}</a></li>
                        @endif
                            <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/dashboard') }}">{{ trans('text_lang.dashboard') }}</a></li>
                    @endif
                @endif
                @if ( !Auth::guest())
                    <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account') }}">{{ trans('text_lang.account') }}</a></li>
                @endif
            <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('account/jobseeker/register') }}">{{ trans('text_lang.register') }}</a></li>
                    <li><a href="{{ url('/account/jobseeker/login') }}">{{ trans('text_lang.login') }}</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.logout') }}</a></li>
                        </ul>
                    </li>
                @endif
                @include('includes._lang_switch')
            </ul>
        </div>
    </div>
</nav>
@yield('breadcrumbs')
<div class="container-fluid account-bg">
    @if(Entrust::hasRole('admin'))
    <div class="row">
        <div class="col-md-3 chartNavShow">
            <div class="list-group">
                <a href="#" class="text-center list-group-item active">
                    BONGPHEAK.COM
                    <i class="glyphicon glyphicon-remove chartNavIconClose"></i>
                </a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/dashboard/accumulative" class="list-group-item <?php activeClassIfRequestMatches("accumulative")?>">{{ trans('text_lang.accumulative') }}</a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/dashboard/new" class="list-group-item <?php activeClassIfRequestMatches("new")?>">{{ trans('text_lang.new') }}</a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/dashboard/active" class="list-group-item <?php activeClassIfRequestMatches("active")?>">{{ trans('text_lang.active') }}</a>
                        <!--<a href="/{{ LaravelLocalization::getCurrentLocale() }}/dashboard/accumulativeAllSector" class="list-group-item <?php activeClassIfRequestMatches("accumulativeAllSector")?>">{{ trans('text_lang.accumulative') }}</a>-->
            </div>
        </div>
        <div class="col-md-1 chartNavHide">
            <div class="list-group ">
                <a href="#" class="text-center list-group-item active" style="height: 48px;">
                    <i class="glyphicon glyphicon-menu-hamburger chartNavIconShow"></i>
                </a>
            </div>
        </div>
        <div class="col-md-11 contentChart">
            @yield('content')
        </div>
    </div>
    @endif
</div>
@include('../includes._footer')

<?php
function activeClassIfRequestMatches($requestUri) {
$urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$segments = explode('/', $urlArray);

if (isset( $segments[3] ) && $segments[3] == $requestUri){
echo 'menuActive';
}
}
?>
<script>
    var CURRENT_LANG = '<?php echo Lang::getLocale(); ?>';
    var SELECT_OPTION = '<?php echo trans('text_lang.selectOption'); ?>';

    // For Notification Message
    var NOTIFICATION_MSG = '<?php echo trans('text_lang.notification_msg'); ?>';
    $(document).ready(function(){
        $('.chartNavShow').hide();
        $('.chartNavHide').show();
        $(".chartNavIconClose").click(function(){
            window.dispatchEvent(new Event('resize'));
            $('.chartNavShow').hide();
            $('.chartNavHide').show();
            $('.contentChart').removeClass('col-md-9');
            $('.contentChart').addClass('col-md-11');
        });
        $(".chartNavIconShow").click(function(){
            window.dispatchEvent(new Event('resize'));
            $('.chartNavShow').show();
            $('.chartNavHide').hide();
            $('.contentChart').removeClass('col-md-11');
            $('.contentChart').addClass('col-md-9');
        });
    });
</script>
@yield('extraJS')

</body>
</html>
