<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Bong Pheak, ​បង​ភ័ក្រ, Bong Pheak Job, ​បង​ភ័ក្រ​ការ​ងារ, Job Cambodia, Phnom Penh Job">
    @include('includes._google_crawl')
    @include('includes._opengraph')


    <link rel="shortcut icon" href="/favicon.png">
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ URL::asset('assets/css/all.css') }}">
    @yield('extraCSS')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    {{--<script src="/js/bongpheak.js" charset="utf-8"></script>--}}
</head>

<body id="app-layout">

<!-- <div class="header-1"> -->
<nav class="navbar navbar-fixed-top navbar-default navbar-bg">
    <div class="container-fluid">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Logo Image -->
            <a class="brand-img" href="{{ url(\Lang::getLocale() . '/') }}">
                @include('includes._logo')
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            @include('includes._left_navbar')

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @if ( !Auth::guest())
                    <li><a href="{{ url('/'.Lang::getLocale().'/account') }}">{{ trans('text_lang.account') }}</a></li>
                @endif
                <!-- Authentication Links -->
                @if (Auth::guest())
                    @include('includes._right_navbar')
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/'.Lang::getLocale().'/logout') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.logout') }}</a></li>
                        </ul>
                    </li>
                @endif
                @include('includes.bp._lang_switch')
            </ul>
        </div>
    </div>
</nav>
@yield('breadcrumbs')
@yield('content')
@include('../includes.bp._footer')

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script>
    var CURRENT_LANG = '<?php echo Lang::getLocale(); ?>';
    var SELECT_OPTION = '<?php echo trans('text_lang.selectOptionN'); ?>';
</script>
@yield('extraJS')
</body>
</html>