<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('includes._google_crawl')
    <link rel="shortcut icon" href="/favicon.png">
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{ URL::asset('lib/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel='stylesheet' type='text/css'>

    <!-- Fonts -->
    <link href="{{ URL::asset('lib/bower_components/font-awesome/css/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('lib/bower_components/normalize-css/normalize.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('lib/bower_components/flag-icon-css/css/flag-icon.min.css') }}" type="text/css">

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrapCustom.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bongpheak.css') }}">
    @yield('extraCSS')

    <!-- Script -->
    <script src="{{ URL::asset('lib/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('lib/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="/js/bongpheak.js" charset="utf-8"></script>

</head>
<body id="app-layout">
    <nav class="navbar navbar-fixed-top navbar-default navbar-bg">
        <div class="container-fluid">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <!-- <a class="brand-img" href="{{ url(\Lang::getLocale() . '/') }}"><img src="/images/logo.png" alt="bongpheak logo" height="56"></a> -->
                <a class="brand-img" href="{{ url(\Lang::getLocale() . '/') }}">
                  @include('includes._logo')
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                @if (Auth::guest())
                  <!-- Left Side Of Navbar -->
                  @include('includes._left_navbar')
                @endif
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    @if(Entrust::hasRole('admin') || Entrust::hasRole('employer'))
                        <?php
                            $isSudoAsEmp = isSudoAsEmp();
                            $isAdmin = isAdmin();
                            $isSaleTeam = fnIsSaleTeam();
                        ?>
                        @if($isAdmin)
                            @if($isSudoAsEmp)
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/clearSudo') }}">{{ trans('text_lang.clearSudo') }}</a></li>
                            @else
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/sudo') }}">{{ trans('text_lang.sudo') }}</a></li>
                            @endif
                            @if(!$isSaleTeam)
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/dashboard') }}">{{ trans('text_lang.dashboard') }}</a></li>
                            @endif
                        @endif
                    @endif
                    @if ( !Auth::guest())
                        <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account') }}">{{ trans('text_lang.account') }}</a></li>
                    @endif
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('account/jobseeker/register') }}">{{ trans('text_lang.register') }}</a></li>
                        <li><a href="{{ url('/account/jobseeker/login') }}">{{ trans('text_lang.login') }}</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.logout') }}</a></li>
                            </ul>
                        </li>
                    @endif
                    @include('includes._lang_switch')
                </ul>
            </div>
        </div>
    </nav>
    @yield('breadcrumbs')
    <div class="container-fluid account-bg">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="#" class="text-center list-group-item active">
                        BONGPHEAK.COM
                    </a>
                    @if(Entrust::hasRole('admin'))
                        <?php
                        if ($isSaleTeam) {
                        ?>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/jobseeker" class="list-group-item <?php activeClassIfRequestMatches("jobseeker")?>">{{ trans('text_lang.users') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/company" class="list-group-item <?php activeClassIfRequestMatches("company")?>">{{ trans('text_lang.companies') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/applicants" class="list-group-item <?php activeClassIfRequestMatches("applicants")?>">{{ trans('text_lang.applicants') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/allApplicants" class="list-group-item <?php activeClassIfRequestMatches("allApplicants")?>">{{ trans('text_lang.applicantRegisterAndNotRegister') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/exportJobseeker/view" class="list-group-item <?php activeClassIfRequestMatches("exportJobseeker")?>">{{ trans('text_lang.exportTargetOfJobseeker') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/exportJobseekerExp/view" class="list-group-item <?php activeClassIfRequestMatches("exportJobseekerExp")?>">{{ trans('text_lang.exportExperienceOfJobseeker') }}</a>
                            <?php
                        }else{
                        ?>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/user" class="list-group-item <?php activeClassIfRequestMatches("user")?>">{{ trans('text_lang.users') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/country" class="list-group-item <?php activeClassIfRequestMatches("country")?>">{{ trans('text_lang.locations') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/sector" class="list-group-item <?php activeClassIfRequestMatches("sector")?>">{{ trans('text_lang.sectors') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/position" class="list-group-item <?php activeClassIfRequestMatches("position")?>">{{ trans('text_lang.positions') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/company" class="list-group-item <?php activeClassIfRequestMatches("company")?>">{{ trans('text_lang.companies') }}</a>
                            {{--<a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/zone" class="list-group-item">{{ trans('text_lang.zones') }}</a>--}}
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/page" class="list-group-item <?php activeClassIfRequestMatches("page")?>">{{ trans('text_lang.pageManagement') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/applicants" class="list-group-item <?php activeClassIfRequestMatches("applicants")?>">{{ trans('text_lang.applicants') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/allApplicants" class="list-group-item <?php activeClassIfRequestMatches("allApplicants")?>">{{ trans('text_lang.applicantRegisterAndNotRegister') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/checkmp3/company" class="list-group-item <?php activeClassIfRequestMatches("checkmp3")?>">{{ trans('text_lang.mp3NotYetRecord') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/exportCompany/company" class="list-group-item <?php activeClassIfRequestMatches("exportCompany")?>">{{ trans('text_lang.exportCompany') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/exportAnnouncement/announcement" class="list-group-item <?php activeClassIfRequestMatches("exportAnnouncement")?>">{{ trans('text_lang.exportAnnouncement') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/expShareJob" class="list-group-item <?php activeClassIfRequestMatches("expShareJob")?>">{{ trans('text_lang.exportShareJob') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/exportJobseeker/view" class="list-group-item <?php activeClassIfRequestMatches("exportJobseeker")?>">{{ trans('text_lang.exportTargetOfJobseeker') }}</a>
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/exportJobseekerExp/view" class="list-group-item <?php activeClassIfRequestMatches("exportJobseekerExp")?>">{{ trans('text_lang.exportExperienceOfJobseeker') }}</a>
                            {{--<a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/language" class="list-group-item">{{ trans('text_lang.languages') }}</a>--}}
                        <?php }?>

                    @endif

                    @if(Entrust::hasRole('employer'))
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob" class="list-group-item <?php activeClassIfRequestMatches("postjob")?>">{{ trans('text_lang.myBongPheak') }}</a>
                        <?php /*
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/searchCandidate" class="list-group-item <?php activeClassIfRequestMatches("searchCandidate")?>">{{ trans('text_lang.searchForCandidate') }}</a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/jobApplied" class="list-group-item <?php activeClassIfRequestMatches("jobApplied")?>">{{ trans('text_lang.applicants') }}</a>
                        */?>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/companyProfile" class="list-group-item <?php activeClassIfRequestMatches("companyProfile")?>">{{ trans('text_lang.companyProfile') }}</a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/userProfile" class="list-group-item <?php activeClassIfRequestMatches("userProfile")?>">{{ trans('text_lang.companyRepresentativeProfile') }}</a>
                    @endif

                    @if(Entrust::hasRole(['jobseeker']))
                        <?php /*
                            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/message" class="list-group-item <?php activeClassIfRequestMatches("message")?>">{{ trans('text_lang.messages') }}</a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/seekerApplied" class="list-group-item <?php activeClassIfRequestMatches("seekerApplied")?>">{{ trans('text_lang.jobApply') }}</a>
                        */ ?>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/experience" class="list-group-item <?php activeClassIfRequestMatches("experience")?>">{{ trans('text_lang.experience') }}</a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/targetJob" class="list-group-item <?php activeClassIfRequestMatches("targetJob")?>">{{ trans('text_lang.jobTarget') }}</a>
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/userProfile" class="list-group-item <?php activeClassIfRequestMatches("userProfile")?>">{{ trans('text_lang.userProfile') }}</a>
                    @endif

                </div>
            </div>
            <div class="col-md-9">
                @yield('content')
            </div>
        </div>
    </div>
    @include('../includes._footer')

    <?php
    function activeClassIfRequestMatches($requestUri) {
        $urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $segments = explode('/', $urlArray);

        if (isset( $segments[3] ) && $segments[3] == $requestUri){
            echo 'menuActive';
        }
    }
    ?>


    <script>
        var CURRENT_LANG = '<?php echo Lang::getLocale(); ?>';
        var SELECT_OPTION = '<?php echo trans('text_lang.selectOption'); ?>';

        // For Notification Message
        var NOTIFICATION_MSG = '<?php echo trans('text_lang.notification_msg'); ?>';
    </script>
    <!-- JavaScripts -->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    @yield('extraJS')

</body>
</html>
