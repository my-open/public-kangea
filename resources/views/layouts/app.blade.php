<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('includes._google_crawl')
    @include('includes._opengraph')

    <link rel="shortcut icon" href="/favicon.png">
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{ URL::asset('lib/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel='stylesheet' type='text/css'>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrapCustom.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bongpheak.css') }}">
    @yield('extraCSS')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/js/bongpheak.js" charset="utf-8"></script>
</head>
<body id="app-layout" >
    <nav class="navbar navbar-fixed-top navbar-default navbar-bg">
        <div class="container-fluid">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Logo Image -->
                <!-- <a class="brand-img" href="{{ url(\Lang::getLocale() . '/') }}"><img src="/images/logo.png" alt="bongpheak logo" height="56"></a> -->
                <a class="brand-img" href="{{ url(\Lang::getLocale() . '/') }}">
                  @include('includes._logo')
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @include('includes._left_navbar')

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    @if ( !Auth::guest())
                        <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account') }}">{{ trans('text_lang.account') }}</a></li>
                    @endif
                    <!-- Authentication Links -->
                    @if (Auth::guest())

                        @include('includes._right_navbar')

                        <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ trans('text_lang.register') }}<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/jobseeker/register') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.asJobSeeker') }}</a></li>
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/employer/register') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.asEmployer') }}</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ trans('text_lang.login') }}<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/jobseeker/login') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.asJobSeeker') }}</a></li>
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/employer/login') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.asEmployer') }}</a></li>
                            </ul>
                        </li> -->
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/logout') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp;{{ trans('text_lang.logout') }}</a></li>
                            </ul>
                        </li>
                    @endif
                    @include('includes._lang_switch')
                </ul>
            </div>
        </div>
    </nav>

    @yield('breadcrumbs')
    <div class="container-fluid">
      @yield('content-fluid')
    </div>
    <div class="container">
      @yield('content')
    </div>
    @include('../includes._footer')


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{--{!!Html::script('js/jobseeker_register_step.js')!!}--}}
    <script>
        var CURRENT_LANG = '<?php echo Lang::getLocale(); ?>';
        var SELECT_OPTION = '<?php echo trans('text_lang.selectOption'); ?>';
    </script>
    @yield('extraJS')
    <!-- <script type="text/javascript">
      var toggleFixedNav = true;
      $(document).ready(function() {
        $(window).scroll(function(event) {
          if ($(this).scrollTop() >= 410) {
            $('body').css('margin-top', '110px');
            $('.navbar').addClass('navbar-fixed-top');
          }else if ($(this).scrollTop() < 410) {
            $('body').css('margin-top', '0px');
            $('.navbar').removeClass('navbar-fixed-top');
          }
        });
      });
    </script> -->
</body>
</html>
