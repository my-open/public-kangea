@extends('layouts.app')
@section('title', 'Jobs')
@section('breadcrumbs', Breadcrumbs::render('jobDetail'))
@section('content')
    <div class="row marginTop">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.searchJobs') }}</big></div>

                <div class="text-center panel-body frmLeftSearch">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/jobs', 'class' => 'form-horizontal')) !!}

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.sector') !!}</span>
                        {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), $fkSectorsID, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.subsector') !!}</span>
                        @if( $subsectors )
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @else
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.activities') !!}</span>
                        @if( $activities )
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')] + $activities->toArray()), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @else
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')]), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.position') !!}</span>
                        @if( $positions )
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray()), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @else
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')]), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.province') !!}</span>
                        {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), $fkProvincesID, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.district') !!}</span>
                        @if( $districts )
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts->toArray()), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @else
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.commune') !!}</span>
                        @if( $communes )
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes->toArray()), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @else
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 marginTop">
                        <button type="submit" class="btn btn-primary">
                            {{ trans('text_lang.search')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div id="jobDetail" class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.jobDetail') }}</big></div>
                <div class="text-left panel-body">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" >
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-md-5 col-lg-5">
                                        <div style="float:left;">
                                            {{ trans('text_lang.companyInformation') }}
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-lg-5"> </div>
                                    <div class="col-md-2 col-lg-2"> </div>
                                </div>
                            </h4>
                        </div>
                    </div>

                    <div class="main-job-title">
                        <dl class="dl-horizontal">
                            <ul class="list-inline">
                                <div align="center" style="clear: both;">
                                    <img class="companyLogo" src="{{ URL::asset("images/companyLogos/{$jobDetail->companiesLogo}") }}" width="150" alt="Company logo">
                                    <br>
                                </div>
                                <dl class="dl-horizontal">
                                    <dt> {{ trans('text_lang.companiesName') }}: </dt>
                                    <dd>
                                        <?php $company_name = fnConvertSlug($jobDetail->companiesNameEN); ?>
                                        <a target="_blank" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $jobDetail->pkCompaniesID) }}">
                                            @if( Lang::getLocale() == 'en')
                                                {{ $jobDetail->companiesNameEN }}
                                            @else
                                                {{ $jobDetail->companiesNameKH }}
                                            @endif
                                        </a>
                                    </dd>

                                    <dt> {{ trans('text_lang.sector') }}: </dt>
                                    <dd>
                                        @if( Lang::getLocale() == 'en') {{ $jobDetail->sectorsNameEN }} @else {{ $jobDetail->sectorsNameKH }} @endif
                                    </dd>

                                    <dt> {{ trans('text_lang.subsector') }}: </dt>
                                    <dd>
                                        @if( Lang::getLocale() == 'en') {{ $jobDetail->subsectorsNameEN }} @else {{ $jobDetail->subsectorsNameKH }} @endif
                                    </dd>

                                    <dt> {{ trans('text_lang.mainActivity') }}: </dt>
                                    <dd>
                                        @if( Lang::getLocale() == 'en') {{ $jobDetail->activitiesNameEN }} @else {{ $jobDetail->activitiesNameKH }} @endif
                                    </dd>
                                </dl>
                            </ul>
                        </dl>
                    </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" >
                                    <h4 class="panel-title">
                                        <div class="row">
                                            <div class="col-md-5 col-lg-5">
                                                <div style="float:left;">
                                                    {{ trans('text_lang.jobDescription') }}
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-lg-5"> </div>
                                            <div class="col-md-2 col-lg-2"> </div>
                                        </div>
                                    </h4>
                                </div>
                            </div>
                            <div class="main-job-title">
                                <dl class="dl-horizontal">
                                    <ul class="list-inline">
                                        <dl class="dl-horizontal">
                                            <dt> {{ trans('text_lang.position') }}: </dt>
                                            <dd>
                                                @if( Lang::getLocale() == 'en') {{ $jobDetail->positionsNameEN }} @else {{ $jobDetail->positionsNameKH }} @endif
                                            </dd>

                                            <dt> {{ trans('text_lang.workers') }}: </dt>
                                            <dd>
                                                @if( Lang::getLocale() == 'en' )
                                                    {{ $jobDetail->announcementsHiring }}
                                                @else
                                                    {{ $jobDetail->announcementsHiring }} {{ trans('text_lang.candidate') }}
                                                @endif
                                            </dd>

                                            @if( !empty($jobDetail->AnnouncementsGender) )
                                            <dt> {{ trans('text_lang.gender') }}: </dt>
                                            <dd>
                                                {{ ( $jobDetail->AnnouncementsGender )? config("constants.GENDERREQUIRE")[$jobDetail->AnnouncementsGender][Lang::getLocale()]:'' }}
                                            </dd>
                                            @endif

                                            <dt> {{ trans('text_lang.locationJob') }}: </dt>
                                            <dd>
                                                @if( Lang::getLocale() == 'en')
                                                    @if($jobDetail->provincesNameEN)
                                                        {{ $jobDetail->provincesNameEN }} {{ trans('text_lang.province') }},
                                                    @endif

                                                    @if($jobDetail->districtsNameEN)
                                                        {{ $jobDetail->districtsNameEN }} {{ trans('text_lang.district') }},
                                                    @endif

                                                    @if($jobDetail->CommunesNameEN)
                                                        {{ $jobDetail->CommunesNameEN }} {{ trans('text_lang.commune')  }}
                                                    @endif
                                                @else
                                                    @if($jobDetail->provincesNameKH)
                                                        {{ trans('text_lang.province') }} {{ $jobDetail->provincesNameKH }},
                                                    @endif

                                                    @if($jobDetail->districtsNameKH)
                                                        {{ trans('text_lang.district') }} {{ $jobDetail->districtsNameKH }},
                                                    @endif

                                                    @if($jobDetail->CommunesNameKH)
                                                        {{ trans('text_lang.commune')  }} {{ $jobDetail->CommunesNameKH }}
                                                    @endif
                                                @endif
                                            </dd>

                                            <dt> {{ trans('text_lang.zone') }}: </dt>
                                            <dd>
                                                @if( Lang::getLocale() == 'en') {{ $jobDetail->zonesNameEN }} @else {{ $jobDetail->zonesNameKH }} @endif
                                            </dd>

                                            <dt> {{ trans('text_lang.announcementsPublishDate') }}: </dt>
                                            <dd>
                                                {{ dateConvert( $jobDetail->announcementsPublishDate ) }}
                                            </dd>

                                            <dt> {{ trans('text_lang.announcementsClosingDate') }}: </dt>
                                            <dd>
                                                {{ dateConvert( $jobDetail->announcementsClosingDate ) }}
                                            </dd>
                                        </dl>
                                    </ul>
                                </dl>
                            </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" >
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-md-5 col-lg-5">
                                        <div style="float:left;">
                                            {{ trans('text_lang.salaryAndBenefits') }}
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-lg-5"> </div>
                                    <div class="col-md-2 col-lg-2"> </div>
                                </div>
                            </h4>
                        </div>
                    </div>
                    <div class="main-job-title">
                        <dl class="dl-horizontal">
                            <ul class="list-inline">
                                <dl class="dl-horizontal">
                                    @if( !empty($jobDetail->announcementsSalaryFrom) && $jobDetail->announcementsSalaryTo)
                                        <dt> {{ trans('text_lang.announcementsSalary') }}: </dt>
                                        <dd>
                                            ${{ $jobDetail->announcementsSalaryFrom }} - ${{ $jobDetail->announcementsSalaryTo }}
                                            @if( !empty($jobDetail->announcementsSalaryFrom) && !empty($jobDetail->announcementsSalaryTo) )
                                                /
                                                @if( $jobDetail->announcementsSalaryType == 'month' ) {{ trans('text_lang.month') }}
                                                @elseif($jobDetail->announcementsSalaryType == 'week' ) {{ trans('text_lang.week') }}
                                                @elseif($jobDetail->announcementsSalaryType == '2week' ) {{ trans('text_lang.2weeks') }}
                                                @elseif($jobDetail->announcementsSalaryType == 'day' ) {{ trans('text_lang.day') }} @endif
                                            @endif
                                        </dd>
                                    @else

                                        <dt> {{ trans('text_lang.announcementsSalary') }}: </dt>
                                        <dd> {{ trans('text_lang.negotiable') }} </dd>

                                    @endif

                                    @if( $jobDetail->AnnouncementsSalaryDependsOn )
                                        <dt> {{ trans('text_lang.AnnouncementsSalaryDependsOn') }}: </dt>
                                        <dd>
                                            @if( $jobDetail->AnnouncementsSalaryDependsOn = "experience" )
                                                {{ trans('text_lang.experience') }}
                                            @else
                                                {{ trans('text_lang.amountOfWork') }}
                                            @endif
                                        </dd>
                                    @endif

                                    @if( !empty($jobDetail->AnnouncementsFromHour) || !empty($jobDetail->AnnouncementsFromMinute) || !empty($jobDetail->AnnouncementsFromType) || !empty($jobDetail->AnnouncementsToHour) || !empty($jobDetail->AnnouncementsToMinute) || !empty($jobDetail->AnnouncementsToType) )
                                        <dt>{{ trans('text_lang.announcementsScheduleJob') }}:</dt>
                                        <dd>
                                            @if( Lang::getLocale() == 'en')
                                                {{ $jobDetail->AnnouncementsFromHour }}{{ trans('text_lang.h') }}:{{ $jobDetail->AnnouncementsFromMinute }}{{ trans('text_lang.mn') }} {{ strtoupper($jobDetail->AnnouncementsFromType) }}
                                            @else
                                                AnnouncementsToHour
                                            @endif

                                            @if( Lang::getLocale() == 'en')
                                                <em>To</em>
                                                {{ $jobDetail->AnnouncementsToHour }}{{ trans('text_lang.h') }}:{{ $jobDetail->AnnouncementsToMinute }}{{ trans('text_lang.mn') }} {{ strtoupper($jobDetail->AnnouncementsToType) }}
                                            @else
                                                <em>{{ trans('text_lang.to') }}</em>
                                                {{ trans('text_lang.hour') }}{{ $jobDetail->AnnouncementsToHour }}:{{ $jobDetail->AnnouncementsToMinute }}{{ trans('text_lang.minute') }}
                                                @if($jobDetail->AnnouncementsToType == 'pm') {{ trans('text_lang.evening') }}  @else {{ trans('text_lang.morning') }} @endif
                                            @endif
                                            <em> ( {{ trans('text_lang.from') }} {{ config("constants.DAYS")[$jobDetail->AnnouncementsFromDay][Lang::getLocale()] }}-{{ config("constants.DAYS")[$jobDetail->AnnouncementsToDay][Lang::getLocale()] }} )</em>
                                        </dd>
                                    @endif

                                    @if( !empty($jobDetail->AnnouncementsBreakHoursFromHour) && !empty($jobDetail->AnnouncementsBreakHoursFromMinute) && !empty($jobDetail->AnnouncementsBreakHoursFromType) && !empty($jobDetail->AnnouncementsBreakHoursToHour) &&  !empty($jobDetail->AnnouncementsBreakHoursToMinute) &&  !empty($jobDetail->AnnouncementsBreakHoursToType) )
                                        <dt>{{ trans('text_lang.AnnouncementsBreakHours') }}:</dt>
                                        <dd>
                                            @if( Lang::getLocale() == 'en')
                                                {{ $jobDetail->AnnouncementsBreakHoursFromHour }}{{ trans('text_lang.h') }}:{{ $jobDetail->AnnouncementsBreakHoursFromMinute }}{{ trans('text_lang.mn') }} {{ strtoupper($jobDetail->AnnouncementsBreakHoursFromType) }}
                                            @else
                                                {{ trans('text_lang.hour') }}{{ $jobDetail->AnnouncementsBreakHoursFromHour }}:{{ $jobDetail->AnnouncementsBreakHoursFromMinute }}{{ trans('text_lang.minute') }}
                                                @if($jobDetail->AnnouncementsBreakHoursFromType == 'pm') {{ trans('text_lang.evening') }}  @else {{ trans('text_lang.morning') }}  @endif
                                            @endif

                                            @if( Lang::getLocale() == 'en')
                                                <em>To</em>
                                                {{ $jobDetail->AnnouncementsBreakHoursToHour }}{{ trans('text_lang.h') }}:{{ $jobDetail->AnnouncementsBreakHoursToMinute }} {{ trans('text_lang.mn') }} {{ strtoupper($jobDetail->AnnouncementsBreakHoursToType) }}
                                            @else
                                                <em>ដល់​</em>
                                                {{ trans('text_lang.hour') }}{{ $jobDetail->AnnouncementsBreakHoursToHour }}:{{ $jobDetail->AnnouncementsBreakHoursToMinute }}{{ trans('text_lang.minute') }}
                                                @if($jobDetail->AnnouncementsBreakHoursToType == 'pm') {{ trans('text_lang.evening') }}  @else {{ trans('text_lang.morning') }} @endif
                                            @endif
                                        </dd>
                                    @endif

                                    @if( !empty($benefits) )
                                        <dt> {{ trans('text_lang.benefit') }}: </dt>
                                        <dd>
                                            <?php
                                                if( isset($benefits) ){
                                                    foreach ($benefits as $value) {
                                                        echo "<li> - ". config("constants.BENEFITS")[$value][Lang::getLocale()] ."</li>";
                                                    }
                                                }
                                            ?>
                                        </dd>
                                    @endif
                                </dl>
                            </ul>
                        </dl>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" >
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-md-5 col-lg-5">
                                        <div style="float:left;">
                                            {{ trans('text_lang.jobRequirements') }}
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-lg-5"> </div>
                                    <div class="col-md-2 col-lg-2"> </div>
                                </div>
                            </h4>
                        </div>
                    </div>
                    <div class="main-job-title">
                        <dl class="dl-horizontal">
                            <ul class="list-inline">
                                <dl class="dl-horizontal">
                                    @if( !empty($jobDetail->AnnouncementsExperiencePositionN) )
                                        <dt> {{ trans('text_lang.ExperienceInthisPosition') }}: </dt>
                                        <dd>
                                            {{ $jobDetail->AnnouncementsExperiencePositionN }}
                                            @if( Lang::getLocale() == 'en')
                                                @if( $jobDetail->AnnouncementsExperiencePositionN >1 )
                                                    {{ $jobDetail->AnnouncementsPositionType }}s
                                                @else
                                                    {{ $jobDetail->AnnouncementsPositionType }}
                                                @endif
                                            @else
                                                @if( $jobDetail->AnnouncementsPositionType =='month' ) {{ trans('text_lang.month') }} @else {{ trans('text_lang.year') }}  @endif
                                            @endif


                                            @if( Lang::getLocale() == 'en' )
                                                ({{ $jobDetail->positionsNameEN }})
                                            @else
                                                ({{ $jobDetail->positionsNameKH }})
                                            @endif
                                        </dd>
                                    @endif

                                    @if( !empty($jobDetail->AnnouncementsExperienceSectorN) )
                                        <dt>
                                            @if( !empty($jobDetail->AnnouncementsExperienceOrAnd) && !empty($jobDetail->AnnouncementsExperiencePositionN ) )
                                                @if( $jobDetail->AnnouncementsExperienceOrAnd =="or" )
                                                    {{ trans('text_lang.or') }}
                                                @else
                                                    {{ trans('text_lang.and') }}
                                                @endif
                                            @endif
                                            {{ trans('text_lang.ExperienceInthisSector') }}:
                                        </dt>
                                        <dd>
                                            {{ $jobDetail->AnnouncementsExperienceSectorN }}
                                            @if( Lang::getLocale() == 'en')
                                                @if( $jobDetail->AnnouncementsExperienceSectorN >1 )
                                                    {{ $jobDetail->AnnouncementsSectorType }}s
                                                @else
                                                    {{ $jobDetail->AnnouncementsSectorType }}
                                                @endif
                                            @else
                                                @if( $jobDetail->AnnouncementsSectorType =='month' )  {{ trans('text_lang.month') }} @else  {{ trans('text_lang.year') }} @endif
                                            @endif

                                            @if( Lang::getLocale() == 'en')
                                                ({{ $jobDetail->sectorsNameEN }})
                                            @else
                                                ({{ $jobDetail->sectorsNameKH }})
                                            @endif
                                        </dd>
                                    @endif

                                    @if( !empty($jobDetail->AnnouncementsAgeFrom) )
                                        <dt> {{ trans('text_lang.age') }}: </dt>
                                        <dd>
                                            @if( Lang::getLocale() == 'en' )
                                            {{ $jobDetail->AnnouncementsAgeFrom }} {{ trans('text_lang.years') }} {{ trans('text_lang.to') }} {{ $jobDetail->AnnouncementsAgeUntil }} {{ trans('text_lang.years') }}
                                            @else
                                                {{ $jobDetail->AnnouncementsAgeFrom }} {{ trans('text_lang.years') }}  {{ trans('text_lang.to') }} {{ $jobDetail->AnnouncementsAgeUntil }} {{ trans('text_lang.years') }}
                                            @endif
                                        </dd>
                                    @endif

                                    @if( !empty($jobDetail->announcementsCertificate) )
                                        <dt> {{ trans('text_lang.requiredDocuments') }}: </dt>
                                        <dd>
                                            <?php
                                            $certificates = json_decode($jobDetail->announcementsCertificate);
                                            if( isset($certificates) ){
                                                foreach ($certificates as $key => $value) {
                                                    echo "<li> - ". config("constants.CERTIFICATES")[$key][Lang::getLocale()] ."</li>";
                                                }
                                            }
                                            ?>
                                        </dd>
                                    @endif

                                    @if( !empty($jobDetail->announcementsLanguageLevel) && !empty($jobDetail->announcementsLanguage) )
                                        <dt> {{ trans('text_lang.languages') }}: </dt>
                                        <dd>
                                            <?php
                                            $languageLevel = json_decode($jobDetail->announcementsLanguageLevel);
                                            if( isset($languageLevel) ){
                                                foreach ($languageLevel as $key => $value) {
                                                    echo "<div class='row'>";
                                                        echo "<div class='col-md-7'>";
                                                            echo "<li> - ";
                                                                echo config("constants.LANGUAGES")[$key][Lang::getLocale()];
                                                                echo " <em>(". config("constants.LANGUAGES")[$value][Lang::getLocale()]. ")</em>";
                                                            echo "</li>";
                                                        echo "</div>";
                                                    echo "</div>";
                                                }
                                            }
                                            ?>
                                        </dd>
                                    @endif
                                </dl>
                            </ul>
                        </dl>
                    </div>

                    {{--How To Apply--}}
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" >
                                    <h4 class="panel-title">
                                        <div class="row">
                                            <div class="col-md-5 col-lg-5">
                                                <div style="float:left;">
                                                    {{ trans('text_lang.contactInformation') }}
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-lg-5"> </div>
                                            <div class="col-md-2 col-lg-2"> </div>
                                        </div>
                                    </h4>
                                </div>
                            </div>
                            <div class="main-job-title">
                                <dl class="dl-horizontal">
                                    @if( !empty($jobDetail->companiesAddress) )
                                        <dt> {{ trans('text_lang.companiesAddress') }}: </dt>
                                        <dd> {{ $jobDetail->companiesAddress }} </dd>
                                    @endif
                                    <dt> {{ trans('text_lang.companiesPhone') }}: </dt>
                                    <dd> {{ $jobDetail->companiesPhone }} </dd>

                                    <dt> {{ trans('text_lang.companiesEmail') }}: </dt>
                                    <dd> {{ $jobDetail->companiesEmail }} </dd>

                                    <dt> {{ trans('text_lang.companiesSite') }}: </dt>
                                    <dd> <a target="_blank" href="{{ $jobDetail->companiesSite }}"> {{ $jobDetail->companiesSite }}</a> </dd>
                                </dl>
                            </div>

                            @if( !empty($jobDetail->companiesDescriptionEN) || !empty($jobDetail->companiesDescriptionEN))
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" >
                                            <h4 class="panel-title">
                                                <div class="row">
                                                    <div class="col-md-5 col-lg-5">
                                                        <div style="float:left;">
                                                            {{ trans('text_lang.historyCompany') }}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-lg-5"> </div>
                                                    <div class="col-md-2 col-lg-2"> </div>
                                                </div>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="main-job-title">
                                        <dl class="dl-horizontal">
                                            @if( Lang::getLocale() == 'en') {!! $jobDetail->companiesDescriptionEN  !!} @else {!! $jobDetail->companiesDescriptionKH !!} @endif
                                        </dl>
                                    </div>
                            @endif

                           <div class="row">
                               <div class="col-md-6 col-md-offset-4 md-sm-12 text-center">

                                   {{--check permission who can edit this announcement or this post job?--}}
                                   @if( $jobDetail->fkCompaniesID == $userCompanyID )
                                       @if(Entrust::hasRole('employer'))
                                           <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME").'/postjob/'.$jobDetail->pkAnnouncementsID.'/edit' }}">{{ trans('text_lang.edit')}}</a>
                                           @if($jobDetail->announcementsStatus != 1 ) <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME").'/jobPublish/'.$jobDetail->pkAnnouncementsID }}">{{ trans('text_lang.publish')}}</a> @endif
                                       @endif
                                   @endif
                                   <a class="btn btn-success" href="{{URL::previous()}}">{{ trans('text_lang.back')}}</a>

                                   @if(!Entrust::hasRole('employer') && !Entrust::hasRole('admin'))
                                       <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/jobApply/'.$jobDetail->pkAnnouncementsID }}">{{ trans('text_lang.applyNow')}}</a>
                                   @endif
                               </div>
                           </div>

                            <div class="row">
                                <div class="fb-like col-md-12 text-right" data-href="https://developers.facebook.com/docs/plugins/" data-width="60" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                            </div>
                </div>
                {{--end text-left panel-body--}}
            </div>
        </div>
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
@endsection
