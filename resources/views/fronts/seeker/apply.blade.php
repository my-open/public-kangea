@extends('layouts.bp.bp')
@section('title', 'Bongpheak '. trans('text_lang.jobseeker'))

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('css/apply.css') }}">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row home-padding-top">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Apply for this job</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Laundry Supervisor</h4>
                                    <h4><small>Green Laundry Service</small></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><b>Location:</b> Phnom Penh</div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><b>Salary:</b> $85 to $150</div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><b>Time:</b> Fulltime (48 hours a week)</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            {!! Form::open(array('url' => '/', 'class' => 'form-horizontal', 'files' => true )) !!}
                            <div class="row">
                                <div class="col-md-12">I am applying for:</div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="apply-for  apply-for-me"><span class="glyphicon glyphicon-ok apply-icon-active" aria-hidden="true"></span>&nbsp;For myself</div>
                                    <div class="apply-for apply-for-other"><span class="glyphicon glyphicon-ok apply-icon-active" aria-hidden="true"></span>&nbsp; For someone else</div>

                                    <input type="checkbox" data-off-icon-cls="gluphicon-thumbs-down" data-on-icon-cls="gluphicon-thumbs-up">
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="container-fluid">
    <div class="row home-info">
        <div class="col-md-8 col-md-offset-2 home-padding-top">
            <div class="body-apply">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><big>Apply for this job</big></div>
                    <div class="panel-body">
                        {!! Form::open(array('url' => '/', 'class' => 'form-horizontal', 'files' => true )) !!}
                        @if (Session::has('flash_notification.message'))
                            <div class="row">
                                <div class="text-center col-md-12">
                                    @include('flash::message')
                                </div>
                            </div>
                        @endif
                        <div class="row apply-body">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Laundry Supervisor</h4>
                                        <h4><small>Green Laundry Service</small></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><p class="apply-job-detail"><b>Location:</b> Phnom Penh</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><p class="apply-job-detail"><b>Salary:</b> $85 to $150</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><p class="apply-job-detail"><b>Time:</b> Fulltime (48 hours a week)</p></div>
                                </div>
                                <hr>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="apply-job-detail"><strong>I am applying for:</strong></p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row apply-for-row">
                                            <div class="apply-for  apply-for-me"><span class="glyphicon glyphicon-ok apply-icon-active" aria-hidden="true"></span>&nbsp;For myself</div>
                                            <div class="apply-for apply-for-other"><span class="glyphicon glyphicon-ok apply-icon-active" aria-hidden="true"></span>&nbsp; For someone else</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6"><p class="apply-info-detail"><strong>{{ trans('text_lang.name') }}</strong></p></div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6"><p class="apply-info-detail"><strong>{{ trans('text_lang.phone') }}</strong></p></div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6"><p class="apply-info-detail"><strong>How much experience do you have?</strong></p></div>
                                    <div class="col-md-6">
                                        <div class="radio form-control apply-experience">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                                No experience
                                            </label>
                                        </div>
                                        <div class="radio form-control apply-experience">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                                Less than one year
                                            </label>
                                        </div>
                                        <div class="radio form-control apply-experience">
                                            <label>
                                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                                More than one year
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><p class="apply-info-detail"><strong>Take a picture of your CV</strong></p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><p class="apply-job-detail">If you have a CV, send it with your application</p></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-block apply-cv">Take picture</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn apply-send-application btn-block apply-cv">Send my application</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extraJS')
    <script>
        $(document).ready(function(){
            $(".apply-for-me").addClass('apply-for-active');
            $(".apply-for-me").click(function(){
                    $(this).addClass("apply-for-active");
                    $('.apply-for-other').removeClass("apply-for-active");
            });
            $(".apply-for-other").click(function(){
                $(this).addClass("apply-for-active");
                $('.apply-for-me').removeClass("apply-for-active");
            });
        });
    </script>
@endsection
