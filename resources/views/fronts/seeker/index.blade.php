@extends('layouts.bp.bp')
@section('title', 'Bongpheak '. trans('text_lang.jobseeker'))

@section('extraCSS')
@endsection

@section('content')
<div class="container-fluid">
    <div class="row home-info">
        <div class="col-md-12">
            <h1>Find Your Future Happiness</h1>
            <p>Bongpheak can help you and your friends find the right job fast!</p>

            <button class="btn-info btn-lg">Learn how we can help you</button>
        </div>

        <div class="col-md-12 bp4Emp">
            <p><a href="">Bongpheak For Employer</a></p>
        </div>

        <div class="col-md-12 bp4Emp">
            <h2>I’m looking for jobs in:</h2>
        </div>

        <div class="col-md-4 col-md-offset-4 text-center bpAjax-input">
            <select class="form-control">
                <option>Choose city</option>
                <option>Phnom Penh</option>
                <option>Battambong</option>
                <option>Kondal</option>
                <option>Seam Reap</option>
            </select>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-9 col-md-offset-2 sector">
            <div class="row sector-title">
                <div class="col-md-12"><h3>Manufacturing</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
            </div>
            <div class="row position-title">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#">Chinese interpreter</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-md-offset-2 sector">
            <div class="row sector-title">
                <div class="col-md-12"><h3>Construction</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
            </div>
            <div class="row position-title">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#">Chinese interpreter</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-md-offset-2 sector">
            <div class="row sector-title">
                <div class="col-md-12"><h3>Hotel, Guesthouse and Restaurant</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
            </div>
            <div class="row position-title">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#">Chinese interpreter</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-md-offset-2 sector">
            <div class="row sector-title">
                <div class="col-md-12"><h3>Security</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
            </div>
            <div class="row position-title">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#">Chinese interpreter</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-md-offset-2 sector">
            <div class="row sector-title">
                <div class="col-md-12"><h3>Service</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
            </div>
            <div class="row position-title">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#">Chinese interpreter</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Manual knitting machine</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Sewing Supervisor</a>
                        </div>
                        <div class="col-md-3">
                            <a href="#">Driver</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extraJS')
    <script>
        $(document).ready(function(){
            $( ".position-title:first" ).css( "display", "block" );

            $('.sector-title').click(function(){
                $('.position-title').slideUp();
                $(this).next('div').slideToggle();
                return false;
            });
        });
    </script>
@endsection
