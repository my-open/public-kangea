@extends('layouts.bp.bp')
@section('title', 'Bongpheak '. trans('text_lang.jobDetail'))

@section('extraCSS')
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row home-info">
            <div class="col-md-12 bp4Emp">
                <h2>I’m looking for jobs in:</h2>
            </div>

            <div class="col-md-4 col-md-offset-4 text-center bpAjax-input">
                <select class="form-control">
                    <option>Choose city</option>
                    <option>Phnom Penh</option>
                    <option>Battambong</option>
                    <option>Kondal</option>
                    <option>Seam Reap</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="row">
                    <div class="col-md-12">
                        <h2><b>Laundry Supervisor</b></h2>
                        <h3 class="pale-color">Green Laundry Service</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <dl>
                            <dt><b>Location: </b>Phnom Penh</dt>
                            <dt><b>Salary: </b>$85 to $150</dt>
                            <dt><b>Time:</b> Fulltime (48 hours a week)</dt>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <p class="text-left">Green Laundry Service is looking for 5 interested
                    candidates to work as <b>laundry supervisors</b>.</p>
                <p class="text-left">The position does not require any previous experience. companyHengNew is a Massage​ spa located in Takeo province, Kaoh Andaet district, Prey Khla commune. companyHengNew is looking for 1 interested candidates to work as Cook. Salary can be negotiable Work Full time, morning, Afternoon, Monday to Saturday. companyHengNew offers the following benefits:
                    - Utility fees</p>
                <p class="text-left">This positin is both <b>male</b> and <b>female</b> candidates</p>
                <p class="text-left">
                    This position is both <b>male</b> and <b>female</b> candidates between the ages of <b>18</b> and <b>40</b>. Candidates must apply before the <b>25 of February of 2017</b>.
                </p>
            </div>
        </div>
    </div>

    <div>
        <div class="bg-grey">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-9 col-md-offset-2">
                        <p class="bg-header-detail">Benefits</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <ul>
                    <li>Free Food</li>
                    <li>Health Allowance</li>
                    <li>Utility fees</li>
                    <li>Bonus Khmer New Year</li>
                    <li>Bonus Pchnum Ben</li>
                    <li>Overtime pay</li>
                    <li>Free accoummodation</li>
                </ul>
            </div>
        </div>

        <div class="bg-grey">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-9 col-md-offset-2">
                        <p class="bg-header-detail">Required Documents</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <ul>
                    <li>National id card</li>
                    <li>Birth certificate</li>
                    <li>Family book</li>
                    <li>Residance book</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 col-md-offset-2">
                <div class="form-group">
                    <div class="col-md-4">
                        <button type="submit" class="btn bg-btn-pink" name="">
                            Apply for this job
                        </button>
                    </div>
                    <div class="col-md-4">
                        <button type="submit" class="btn bg-btn-dark-grey" name="">
                            Apply for this job
                        </button>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class=""><b>More Service Jobs in Phnom Penh (36)</b></h2>
                            <div class="">Laundry Supervisor ($150 - $175)</div>
                            <div class="border-b">Laundry Supervisor ($150 - $175)</div>
                            <div class="border-b">Hotel Steward($185)</div>
                            <div class="border-b">Driver ($100)</div>
                            <div class="border-b">Dishwasher ($250</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-md-offset-2 sector">
                <div class="row sector-title">
                    <div class="col-md-12"><h3>Manufacturing</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
                </div>
                <div class="row position-title">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">Chinese interpreter</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-2 sector">
                <div class="row sector-title">
                    <div class="col-md-12"><h3>Construction</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
                </div>
                <div class="row position-title">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">Chinese interpreter</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-2 sector">
                <div class="row sector-title">
                    <div class="col-md-12"><h3>Hotel, Guesthouse and Restaurant</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
                </div>
                <div class="row position-title">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">Chinese interpreter</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-2 sector">
                <div class="row sector-title">
                    <div class="col-md-12"><h3>Security</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
                </div>
                <div class="row position-title">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">Chinese interpreter</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-2 sector">
                <div class="row sector-title">
                    <div class="col-md-12"><h3>Service</h3><span class="glyphicon glyphicon-chevron-right"></span></div>
                </div>
                <div class="row position-title">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">Chinese interpreter</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Manual knitting machine</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Sewing Supervisor</a>
                            </div>
                            <div class="col-md-3">
                                <a href="#">Driver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extraJS')
    <script>
        $(document).ready(function(){
            $( ".position-title:first" ).css( "display", "block" );

            $('.sector-title').click(function(){
                $('.position-title').slideUp();
                $(this).next('div').slideToggle();
                return false;
            });
        });
    </script>
@endsection
