@extends('layouts.account')
@section('title', 'Subsector')
@section('breadcrumbs', Breadcrumbs::render('subsector'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.subsectorInformation') }}</big></div>

        @include('accounts.sectors.tab_sector')

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/account/subsector/search', 'class' => 'form-horizontal')) !!}

                        <div class="form-group">
                            <div class="col-md-3 text-center">
                                <span>{!! trans('text_lang.subsector') !!}</span>
                                <?php $subName = ${'subsectorsName'.$lang}; ?>
                                {!! Form::text('subsectorsName'.$lang, $subName, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-3 text-center">
                                <span>{!! trans('text_lang.sector') !!}</span>
                                {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray() ), $fkSectorsID, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                            </div>

                            <div class="col-md-3 text-center">
                                <span>{!! trans('text_lang.order') !!}</span>
                                {!! Form::select('subsectorsOrder', (['ASC' => trans('text_lang.asc'), 'DESC' => trans('text_lang.desc')]), $subsectorsOrder, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-3">
                                <br/>
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('text_lang.search')}}
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 text-left padding-numberOf">
                    @if( $subsectors->total() > 0 )
                        {{ trans('text_lang.numberOfSubsectors') }}: <b>{{ $subsectors->total() }}</b>
                    @endif
                </div>
                <div class="text-right col-md-8">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/subsector/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addSubsector'), $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.addSubsector') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.id')}}</th>
                            <th>{{ trans('text_lang.photo')}}</th>
                            <th>{{ trans('text_lang.name')}}</th>
                            <th>{{ trans('text_lang.sector')}}</th>
                            <th>{{ trans('text_lang.order')}}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $subsectors as $row )
                            <tr @if( $row->subsectorsStatus == 0 ) class="danger" @endif >
                                <td>{{ $row->pkSubsectorsID }}</td>
                                <td>
                                    <img src="{{ URL::asset("images/subsectors/{$row->subsectorsPhoto}") }}" alt="sectorsPhoto logo" class="photoViewIndex">
                                </td>
                                <td>
                                    {{ object_get($row, "subsectorsName{$lang}" ) }}
                                </td>
                                <td>
                                    {{ object_get($row, "sectorsName{$lang}" ) }}
                                </td>
                                <td>{{ $row->subsectorsOrder }}</td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/subsector/'.$row->pkSubsectorsID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.editSubsector') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/subsector/'.$row->pkSubsectorsID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteSubsector') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $subsectors->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection