@extends('layouts.account')
@section('title', 'Edit Sector')
@section('breadcrumbs', Breadcrumbs::render('subsectorUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editSubsector')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>
            {!! Form::model($subsector, [
                'method' => 'PATCH',
                'action' => array('SubsectorController@update', $subsector->pkSubsectorsID),
                'role'=> 'form', 'class' => 'form-horizontal',
                'files' => true
            ]) !!}

            <div class="form-group{{ $errors->has('subsectorsPhoto') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('subsectorsPhoto', trans('text_lang.photo'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    <input type="file" name="subsectorsPhoto" style="visibility:hidden;height: 0" id="subsectorsPhoto" />
                    <input type="hidden" value="MAX_FILE_SIZE" id="max" />
                    <fieldset class="fieldset_style">
                        <div class="input-group">
                              <span class="input-group-addon" id="basic-addon1">
                                    <a  onclick="issueUpload('subsectorsPhoto','subsectorsPhoto_text')" style="cursor:pointer;"  ><i class="fa fa-upload"></i> {{ trans('text_lang.choosePicture') }}</a>
                                </span>
                            <input type="text" class="form-control"​  placeholder="No File Selected" aria-describedby="basic-addon1"  id="subsectorsPhoto_text" disabled>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkSectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('subsectorsNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('subsectorsNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('subsectorsNameEN', old('subsectorsNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('subsectorsNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('subsectorsNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('subsectorsNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('subsectorsNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('subsectorsNameKH', old('subsectorsNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('subsectorsNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('subsectorsNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('subsectorsNameZH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('subsectorsNameZH', trans('text_lang.nameZH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('subsectorsNameZH', old('subsectorsNameZH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('subsectorsNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('subsectorsNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('subsectorsNameTH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('subsectorsNameTH', trans('text_lang.nameTH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('subsectorsNameTH', old('subsectorsNameTH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('subsectorsNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('subsectorsNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('subsectorsOrder') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('subsectorsOrder', trans('text_lang.order'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('subsectorsOrder', old('subsectorsOrder'), ['class' => 'form-control']) !!}
                    @if ($errors->has('subsectorsOrder'))
                        <span class="help-block">
                            <strong>{!! $errors->first('subsectorsOrder') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('subsectorsStatus') ? ' has-error' : '' }}">
                {!! Form::label('subsectorsStatus', trans('text_lang.status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('subsectorsStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('subsectorsStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('subsectorsStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/subsector' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/choosePicture.js') }}"></script>
@endsection