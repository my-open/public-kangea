@extends('layouts.account')
@section('title', 'CheckMp3')
@section('breadcrumbs', Breadcrumbs::render('sector'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.mp3NotYetRecord') }}</big></div>

        @include('accounts.mp3NotYetRecords.tab_mp3')

        <div class="text-center panel-body" >

            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr width="100%">
                                <th>{{ trans('text_lang.id')}}</th>
                                <th>{{ trans('text_lang.name')}}</th>
                                <th>{{ trans('text_lang.createdAt')}}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $positions as $row )
                                <tr>
                                    <td>{{ $row->pkPositionsID }}</td>
                                    <td>
                                        {{ object_get($row, "positionsName{$lang}") }}
                                    </td>
                                    <td>{{ dateConvertNormal($row->created_at) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        {{--#panel-body--}}

    </div>
@endsection