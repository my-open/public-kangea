<ul class="nav nav-tabs">
    <li  class="@if(str_contains(url()->current(), 'company')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/checkmp3/company/"> {{ ucfirst(trans('text_lang.companies')) }} </a></li>
    <li class="@if(str_contains(url()->current(), 'subsector')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/checkmp3/subsector/" > {{ ucfirst(trans('text_lang.subsectors')) }} </a>
    <li class="@if(str_contains(url()->current(), 'activity')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/checkmp3/activity/"> {{ ucfirst(trans('text_lang.activities')) }} </a>
    <li class="@if(str_contains(url()->current(), 'position')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/checkmp3/position/"> {{ ucfirst(trans('text_lang.positions')) }} </a>

</ul>