@extends('layouts.account')
@section('title', 'Post a new job')
@section('breadcrumbs', Breadcrumbs::render('postjobCreate'))
@section('content')
<div class="panel panel-default">
    <div class="panel-heading"><big>{{ trans('text_lang.addPostjob')}}</big></div>

    <div class="panel-body" >
        @if (Session::has('flash_notification.message'))
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>
        @endif

        @if(Entrust::hasRole('admin'))
                {!! Form::open(array(
                'url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/company/postjob/'.$company->pkCompaniesID,
                'class' => 'form-horizontal',
                'company' => 'form',
                'files' => true
                )) !!}
        @else
            {!! Form::open(array(
            'url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/postjob',
            'class' => 'form-horizontal',
            'company' => 'form',
            'files' => true
            )) !!}
        @endif

        <div class="panel-group" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-10"> <div style="float:left;"> {{ trans('text_lang.companyProfile') }}</div> </div>
                            <div class="col-md-1 col-md-offset-1 text-right">
                                <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" >
                                    <div id="clickPlusOne" style="display:none;" > <span class="glyphicon glyphicon-plus"></span> </div>
                                    <div id="clickMinusOne"> <span class="glyphicon glyphicon-minus"></span> </div>
                                </a>
                            </div>
                        </div>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12 text-center">
                                    <img src="{{ URL::asset("images/companyLogos/thumbnails/{$company->companiesLogo}") }}" alt="Company logo">
                                </div>
                                <dl class="dl-horizontal">
                                    <dt> {{ trans('text_lang.companiesName') }}: </dt>
                                    <dd>
                                        @if( Lang::getLocale() == 'en') {{ $company->companiesNameEN }} @else {{ $company->companiesNameKH }} @endif
                                    </dd>

                                    @if( !empty($company->companiesNumberOfWorker))
                                        <dt> {{ trans('text_lang.numberOfWorker') }}: </dt>
                                        <dd> {{ $company->companiesNumberOfWorker }} </dd>
                                    @endif

                                    <dt> {{ trans('text_lang.sector') }}: </dt>
                                    <dd> {{ $sectorName }} </dd>

                                    <dt> {{ trans('text_lang.subsector') }}: </dt>
                                    <dd> {{ $subsectorName }} </dd>

                                    <dt> {{ trans('text_lang.activity') }}: </dt>
                                    <dd> {{ $activityName }} </dd>

                                    <dt> {{ trans('text_lang.companiesPhone') }}: </dt>
                                    <dd> {{ $company->companiesPhone }} </dd>

                                    <dt> {{ trans('text_lang.companiesEmail') }}: </dt>
                                    <dd> {{ $company->companiesEmail }} </dd>

                                    <dt> {{ trans('text_lang.province') }}: </dt>
                                    <dd> {{ $provincesName }}</dd>

                                    <dt> {{ trans('text_lang.district') }}: </dt>
                                    <dd> {{ $districtName }} </dd>

                                    {{--<dt> {{ trans('text_lang.companiesAddress') }}: </dt>--}}
                                    {{--<dd> {{ $company->companiesAddress }} </dd>--}}

                                    <dt> {{ trans('text_lang.companyDescription') }}: </dt>
                                    <dd>
                                        @if( Lang::getLocale() == 'en') {!!  $company->companiesDescriptionEN  !!} @else {!! $company->companiesDescriptionKH  !!} @endif
                                    </dd>
                                </dl>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-10 text-right">
                                {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/companyProfile/edit/',trans('text_lang.editCompany'), $attributes = array('class' => 'btn btn-sm btn-info' ))) !!}
                            </div>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
            </div>

            <!-- Two -->
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-10"> <div style="float:left;"> {{ trans('text_lang.jobDescription') }}</div> </div>
                            <div class="col-md-1 col-md-offset-1 text-right">
                                <a role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <div id="clickPlusTwo" style="display:none;"> <span class="glyphicon glyphicon-plus"></span> </div>
                                    <div id="clickMinusTwo"> <span class="glyphicon glyphicon-minus"></span> </div>
                                </a>
                            </div>
                        </div>
                    </h4>
                </div>

                <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">

                        <div class="form-group{{ $errors->has('fkPositionsID') ? ' has-error' : '' }} {{ $errors->has('positionOther') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('fkPositionsID', trans('text_lang.position') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray() + ['0' => trans('text_lang.optOther')] ), null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! Form::text('positionOther', old('positionOther'), ['class' => 'form-control positionOther txtOther']) !!}
                                @if ($errors->has('fkPositionsID'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('fkPositionsID') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fkActivitiesID') ? ' has-error' : '' }} {{ $errors->has('activityOther') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('fkActivitiesID', trans('text_lang.mainActivity'), array('class' => 'col-md-4 control-label'))) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')] + $activities->toArray() + ['other' => trans('text_lang.optOther')]), null, ['class' => 'form-control']) !!}
                                {!! Form::text('activityOther', old('activityOther'), ['class' => 'form-control activityOther txtOther']) !!}
                                @if ($errors->has('fkActivitiesID'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('fkActivitiesID') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('announcementsHiring') ? ' has-error' : '' }}">
                            {!! Form::label('announcementsHiring', trans('text_lang.announcementsHiring'), ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::text('announcementsHiring', old('announcementsHiring'), ['class' => 'form-control positive-integer', 'maxlength' => '4']) !!}
                                @if ($errors->has('announcementsHiring'))
                                    <span class="help-block">
                                            <strong>{!! $errors->first('announcementsHiring') !!}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <?php
                        foreach( config("constants.GENDERREQUIRE") as $key => $value ){
                            $genderRequire[$key] = $value[Lang::getLocale()];
                        }
                        ?>
                        <div class="form-group{{ $errors->has('AnnouncementsGender') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('AnnouncementsGender', trans('text_lang.gender'), array('class' => 'col-md-4 control-label'))) !!}
                            <div class="col-md-7">
                                {!! Form::select('AnnouncementsGender', $genderRequire, null, ['class' => 'form-control']) !!}
                                @if ($errors->has('AnnouncementsGender'))
                                    <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsGender') !!}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <h4><b><center>{{ trans('text_lang.locationOfWork') }} </center></b></h4>
                        <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), $company->fkProvincesID, ['class' => 'form-control', 'required' => 'required']) !!}
                                @if ($errors->has('fkProvincesID'))
                                    <span class="help-block">
                                            <strong>{!! $errors->first('fkProvincesID') !!}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fkDistrictsID') ? ' has-error' : '' }}">
                            {!! Form::label('fkDistrictsID', trans('text_lang.district'), ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districtByProvinces->toArray()), $company->fkDistrictsID, ['class' => 'form-control']) !!}
                                @if ($errors->has('fkDistrictsID'))
                                    <span class="help-block">
                                            <strong>{!! $errors->first('fkDistrictsID') !!}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fkCommunesID') ? ' has-error' : '' }}">
                            {!! Form::label('fkCommunesID', trans('text_lang.commune'), ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communeByDistricts->toArray()), $company->fkCommunesID, ['class' => 'form-control']) !!}
                                @if ($errors->has('fkCommunesID'))
                                    <span class="help-block">
                                            <strong>{!! $errors->first('fkCommunesID') !!}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        {{--<div class="form-group{{ $errors->has('fkVillagesID') ? ' has-error' : '' }}">--}}
                            {{--{!! Form::label('fkVillagesID', trans('text_lang.village'), ['class' => 'col-md-4 control-label']) !!}--}}
                            {{--<div class="col-md-7">--}}
                                {{--{!! Form::select('fkVillagesID', (['' => trans('text_lang.selectOption')] + $villageByCommunes->toArray()), $company->fkVillagesID, ['class' => 'form-control']) !!}--}}
                                {{--@if ($errors->has('fkVillagesID'))--}}
                                    {{--<span class="help-block">--}}
                                            {{--<strong>{{ $errors->first('fkVillagesID') }}</strong>--}}
                                        {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('fkZonesID') ? ' has-error' : '' }}">--}}
                            {{--{!! Form::label('fkZonesID', trans('text_lang.zone'), ['class' => 'col-md-4 control-label']) !!}--}}
                            {{--<div class="col-md-7">--}}
                                {{--{!! Form::select('fkZonesID', (['' => trans('text_lang.selectOption')] + $zones->toArray()), $company->fkZonesID, ['class' => 'form-control']) !!}--}}
                                {{--@if ($errors->has('fkZonesID'))--}}
                                    {{--<span class="help-block">--}}
                                            {{--<strong>{{ $errors->first('fkZonesID') }}</strong>--}}
                                        {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('announcementsPublishDate') ? ' has-error' : '' }}" >--}}
                            {{--{!! Html::decode(Form::label('announcementsPublishDate', trans('text_lang.announcementsPublishDate') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}--}}
                            {{--<div class="col-md-7">--}}
                                {{--{!! Form::text('announcementsPublishDate', old('announcementsPublishDate'), ['class' => 'form-control', 'required' => 'required']) !!}--}}
                                {{--@if ($errors->has('announcementsPublishDate'))--}}
                                    {{--<span class="help-block">--}}
                                            {{--<strong>{!! $errors->first('announcementsPublishDate') !!}</strong>--}}
                                        {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group{{ $errors->has('announcementsClosingDate') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('announcementsClosingDate', trans('text_lang.announcementsClosingDate') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                            <div class="col-md-7">
                                {!! Form::text('announcementsClosingDate', old('announcementsClosingDate'), ['class' => 'form-control', 'required' => 'required']) !!}
                                @if ($errors->has('announcementsClosingDate'))
                                    <span class="help-block">
                                            <strong>{!! $errors->first('announcementsClosingDate') !!}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--Four--}}
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-10"> <div style="float:left;">{{ trans('text_lang.salaryAndBenefits') }}</div> </div>
                            <div class="col-md-1 col-md-offset-1 text-right">
                                <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour" >
                                    <div id="clickPlusFour" style="display:none;" > <span class="glyphicon glyphicon-plus"></span> </div>
                                    <div id="clickMinusFour"> <span class="glyphicon glyphicon-minus"></span> </div>
                                </a>
                            </div>
                        </div>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">

                        <div class="form-group{{ $errors->has('announcementsSalary') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('announcementsSalaryFrom', trans('text_lang.salary'), array('class' => 'col-md-3 control-label'))) !!}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="checkbox">
                                            <label>
                                                {{ Form::checkbox('announcementsSalaryNegotiate', 1, false, ['class' => 'field', 'id'=>'announcementsSalaryNegotiate']) }}
                                                {!! trans('text_lang.negotiation') !!}
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-2 form-group{{ $errors->has('announcementsSalaryFrom') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            {!! Form::text('announcementsSalaryFrom', old('announcementsSalaryFrom'), ['class' => 'form-control positive-integer', 'maxlength' => '5', 'id'=>'announcementsSalaryFrom']) !!}
                                        </div>
                                        @if ($errors->has('announcementsSalaryFrom'))
                                            <span class="help-block">
                                                <strong>{!! $errors->first('announcementsSalaryFrom') !!}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    {{--to--}}
                                    {!! Html::decode(Form::label('announcementsSalaryTo', trans('text_lang.to'), array('class' => 'col-md-2 control-label'))) !!}
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            {!! Form::text('announcementsSalaryTo', old('announcementsSalaryTo'), ['class' => 'form-control positive-integer', 'maxlength' => '5', 'id'=>'announcementsSalaryTo' ]) !!}
                                        </div>
                                        @if ($errors->has('announcementsSalaryTo'))
                                            <span class="help-block">
                                                <strong>{!! $errors->first('announcementsSalaryTo') !!}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    {{--per--}}
                                    {!! Form::label('announcementsSalaryType', trans('text_lang.per'), ['class' => 'col-md-1 control-label']) !!}
                                    <div class="col-md-2">
                                        {!! Form::select('announcementsSalaryType', array( 'month' => trans('text_lang.month'), 'week' => trans('text_lang.week'), '2week' => trans('text_lang.2weeks'), 'day' => trans('text_lang.day') ), null, ['class' => 'form-control', 'required'=>'required']) !!}
                                        @if ($errors->has('announcementsSalaryType'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('announcementsSalaryType') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="hideShowDependsOn">
                            <div class="form-group{{ $errors->has('AnnouncementsSalaryDependsOn') ? ' has-error' : '' }}">
                                {!! Html::decode(Form::label('AnnouncementsSalaryDependsOn', trans('text_lang.AnnouncementsSalaryDependsOn'), array('class' => 'col-md-3 control-label'))) !!}
                                <div class="col-md-8">
                                    {!! Form::select('AnnouncementsSalaryDependsOn', array( 'experience' => trans('text_lang.experience'), 'amountOfWork' => trans('text_lang.amountOfWork')), null, ['class' => 'form-control']) !!}
                                    @if ($errors->has('AnnouncementsSalaryDependsOn'))
                                        <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsSalaryDependsOn') !!}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('announcementsSchedule') ? ' has-error' : '' }}">
                            <div class="col-md-3 control-label">{{ trans('text_lang.working') }}</div>
                            <div class="col-md-8 control-label">
                                <div class="row">
                                    <div class="col-md-2 text-left">
                                        {{ Form::radio('announcementsIsFullTime', 0, null, ['class' => 'field']) }} {{trans('text_lang.partTime')}}
                                    </div>
                                    <div class="col-md-2">
                                        {{ Form::radio('announcementsIsFullTime', 1, true, ['class' => 'field']) }} {{trans('text_lang.fullTime')}}
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('announcementsWorkTime') ? ' has-error' : '' }}">
                            <div class="col-md-3"></div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="checkbox">
                                                <label>
                                                    {{ Form::checkbox('announcementsWorkTime[morning]', "morning", true, ['class' => 'field']) }}
                                                    {!! trans('text_lang.morningShift') !!}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="checkbox">
                                                <label>
                                                    {{ Form::checkbox('announcementsWorkTime[afternoon]', "afternoon", true, ['class' => 'field']) }}
                                                    {!! trans('text_lang.afternoonShift') !!}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox">
                                                <label>
                                                    {{ Form::checkbox('announcementsWorkTime[night]', "night", null, ['class' => 'field']) }}
                                                    {!! trans('text_lang.nightShift') !!}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('AnnouncementsFromDay') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('AnnouncementsFromDay', trans('text_lang.fromDay'), array('class' => 'col-md-3 control-label'))) !!}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-5 col-xs-5">
                                        {!! Form::select('AnnouncementsFromDay', $days, 'mon', ['class' => 'form-control padding-zero', 'required' => 'required']) !!}
                                        @if ($errors->has('AnnouncementsFromDay'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsFromDay') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2 col-xs-2">
                                        {!! Html::decode(Form::label('AnnouncementsToDay', trans('text_lang.to'), array('class' => 'control-label padding-top-10'))) !!}
                                    </div>
                                    <div class="col-md-5 col-xs-5">
                                        {!! Form::select('AnnouncementsToDay', $days, 'sat', ['class' => 'form-control padding-zero', 'required' => 'required']) !!}
                                        @if ($errors->has('AnnouncementsToDay'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsToDay') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('announcementsBenefit', trans('text_lang.benefits'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-4 text-left">
                                <div class="checkbox"><label><input name="benefit[serviceCharge]" type="checkbox" value="serviceCharge"> {!! trans('text_lang.serviceCharge') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[serviceAward]" type="checkbox" value="serviceAward"> {!! trans('text_lang.serviceAward') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[freeFood]" type="checkbox" value="freeFood"> {{ trans('text_lang.freeFood') }}</label></div>
                                <div class="checkbox"><label><input name="benefit[foodAllowance]" type="checkbox" value="foodAllowance"> {!! trans('text_lang.foodAllowance') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[travelAllowance]" type="checkbox" value="travelAllowance"> {!! trans('text_lang.travelAllowance') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[insurance]" type="checkbox" value="insurance"> {!! trans('text_lang.insurance') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[healthAllowance]" type="checkbox" value="healthAllowance"> {!! trans('text_lang.healthAllowance') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[feeUtilities]" type="checkbox" value="feeUtilities"> {!! trans('text_lang.feeUtilities') !!}</label></div>
                            </div>
                            <div class="col-md-4 text-left">
                                <div class="checkbox"><label><input name="benefit[bonusChinesNewYear]" type="checkbox" value="bonusChinesNewYear"> {!! trans('text_lang.bonusChinesNewYear') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[bonusNewYearKH]" type="checkbox" value="bonusNewYearKH"> {!! trans('text_lang.bonusNewYearKH') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[bonusPchumBen]" type="checkbox" value="bonusPchumBen"> {!! trans('text_lang.bonusPchumBen') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[overTimePay]" type="checkbox" value="overTimePay"> {!! trans('text_lang.overTimePay') !!}</label></div>
                                <div class="checkbox"><label><input name="benefit[freeAccommodation]" type="checkbox" value="freeAccommodation"> {{ trans('text_lang.freeAccommodation') }}</label></div>
                                <div class="checkbox"><label><input name="benefit[accommodationAllowance]" type="checkbox" value="accommodationAllowance"> {{ trans('text_lang.accommodationAllowance') }}</label></div>
                                <div class="checkbox"><label><input name="benefit[otherBenefit]" type="checkbox" value="otherBenefit"> {!! trans('text_lang.otherBenefit') !!}</label></div>
                            </div>
                        </div>

                    </div>
                    <!-- panel-body -->
                </div>
            </div>

            {{--Three--}}
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-10"> <div style="float:left;"> {{ trans('text_lang.jobRequirements') }}</div> </div>
                            <div class="col-md-1 col-md-offset-1 text-right">
                                <a role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" >
                                    <div id="clickPlusThree" style="display:none;" > <span class="glyphicon glyphicon-plus"></span> </div>
                                    <div id="clickMinusThree"> <span class="glyphicon glyphicon-minus"></span> </div>
                                </a>
                            </div>
                        </div>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('AnnouncementsExperiencePositionN') ? ' has-error' : '' }}">
                            {!! Form::label('AnnouncementsExperiencePositionN', trans('text_lang.ExperienceInthisPosition'), ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4 col-xs-4">
                                        {!! Form::select('AnnouncementsExperiencePositionN', array('0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12' ), null, ['class' => 'form-control padding-zero']) !!}
                                        @if ($errors->has('AnnouncementsExperiencePositionN'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsExperiencePositionN') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        {!! Form::select('AnnouncementsPositionType', array( 'month' => trans('text_lang.months'), 'year' => trans('text_lang.years') ), null, ['class' => 'form-control padding-zero']) !!}
                                        @if ($errors->has('AnnouncementsPositionType'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsPositionType') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2 padding0 col-xs-2">
                                        {!! Form::select('AnnouncementsExperienceOrAnd', array( 'or' => trans('text_lang.or'), 'and' => trans('text_lang.and') ), null, ['class' => 'form-control padding-zero']) !!}
                                        @if ($errors->has('AnnouncementsExperienceOrAnd'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsExperienceOrAnd') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('AnnouncementsExperienceSectorN') ? ' has-error' : '' }}">
                            {!! Form::label('AnnouncementsExperienceSectorN', trans('text_lang.ExperienceInthisSector'), ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4 col-xs-6">
                                        {!! Form::select('AnnouncementsExperienceSectorN', array( '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12' ), null, ['class' => 'form-control padding-zero']) !!}
                                        @if ($errors->has('AnnouncementsExperienceSectorN'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsExperienceSectorN') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-4 col-xs-6">
                                        {!! Form::select('AnnouncementsSectorType', array( 'month' => trans('text_lang.months'), 'year' => trans('text_lang.years') ), null, ['class' => 'form-control padding-zero']) !!}
                                        @if ($errors->has('AnnouncementsSectorType'))
                                            <span class="help-block">
                                            <strong>{!! $errors->first('AnnouncementsSectorType') !!}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-sm-1 col-xs-2 control-label"></div>
                            <div class="col-md-8 col-sm-11 col-xs-10 postjob-paddig-bottom">
                                <label class="checkbox-inline">
                                    <input name="announcementsAllowWithoutExperience" type="checkbox" value="1"> {{ trans('text_lang.I_also_want_to_receive_applications_from_candidate_that_do_not_have_experience') }}
                                </label>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('AnnouncementsAgeFrom') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('AnnouncementsAgeFrom', trans('text_lang.ageFrom') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 col-sm-1 col-xs-2 control-label padding-top-10'))) !!}
                            <div class="col-md-2 col-sm-4 col-xs-4">
                                {!! Form::text('AnnouncementsAgeFrom', old('AnnouncementsAgeFrom'), ['class' => 'form-control positive-integer', 'maxlength' => '2', 'required'=>'required' ]) !!}
                                @if ($errors->has('AnnouncementsAgeFrom'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('AnnouncementsAgeFrom') !!}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 control-label" style="text-align: left;">({{ trans('text_lang.minimum15') }})</div>
                            {!! Html::decode(Form::label('AnnouncementsAgeUntil', trans('text_lang.ageUntil'), array('class' => 'col-md-1 col-sm-1 col-xs-2 control-label padding-top-10'))) !!}
                            <div class="col-md-2 col-sm-4 col-xs-4">
                                {!! Form::text('AnnouncementsAgeUntil', old('AnnouncementsAgeUntil'), ['class' => 'form-control positive-integer' ]) !!}
                                @if ($errors->has('AnnouncementsAgeUntil'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('AnnouncementsAgeUntil') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            {{ Form::label('requiredDocuments', trans('text_lang.requiredDocuments'), array('class' => 'col-md-4 control-label')) }}
                            <div class="col-md-7 text-left  padding-top-10">
                                <label>
                                    <input type="radio" name="announcementsRequiredDocType" value="1" > {{trans('text_lang.All_documents_marked_below')}}<br>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-7 text-left">
                                <label>
                                    <input type="radio" name="announcementsRequiredDocType" value="2" checked> {{trans('text_lang.One_documents_marked_below')}}<br>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-7 text-left">
                                <label>
                                    <input type="radio" name="announcementsRequiredDocType" value="3" > {{trans('text_lang.Two_documents_marked_below')}}<br>
                                </label>
                            </div>
                        </div>

                       <div class="postjob-paddig-left">
                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[nIdcard]" type="checkbox" value="nationalIdCard">  {{ config("constants.CERTIFICATES")['nIdcard'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[birthC]" type="checkbox" value="birthC">  {{ config("constants.CERTIFICATES")['birthC'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[familyB]" type="checkbox" value="familyB">  {{ config("constants.CERTIFICATES")['familyB'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[residenceB]" type="checkbox" value="residenceB">  {{ config("constants.CERTIFICATES")['residenceB'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[cv]" type="checkbox" value="cv">  {{ config("constants.CERTIFICATES")['cv'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[policeCriminal]" type="checkbox" value="policeCriminal">  {{ config("constants.CERTIFICATES")['policeCriminal'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[passport]" type="checkbox" value="passport">  {{ config("constants.CERTIFICATES")['passport'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[visa]" type="checkbox" value="visa">  {{ config("constants.CERTIFICATES")['visa'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[workPermit]" type="checkbox" value="workPermit">  {{ config("constants.CERTIFICATES")['workPermit'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[healthC]" type="checkbox" value="healthC">  {{ config("constants.CERTIFICATES")['healthC'][Lang::getLocale()] }}</label>
                               </div>
                           </div>

                           <div class="form-group">
                               <div class="col-md-4"></div>
                               <div class="col-md-7 text-left">
                                   <label class="checkbox-inline"><input name="certificate[carDL]" type="checkbox" value="carDrivingLicense">  {{ config("constants.CERTIFICATES")['carDL'][Lang::getLocale()] }}</label>
                               </div>
                           </div>
                       </div>

                        {{--language--}}
                        <div class="form-group" role="form">
                            {!! Form::label('announcementsLanguage', trans('text_lang.languages'), ['class' => 'col-md-4 col-xs-12 control-label']) !!}
                            <div class="col-md-2 col-xs-5" align="left">
                                <label class="checkbox-inline"><input name="language[kh]" type="checkbox" value="khmer" id="chKhmer" class="chLanguage"> {{ trans('text_lang.khmer') }}</label>
                            </div>
                            <div class=" col-xs-2 col-md-1 padding-zero">{!! Form::label('announcementsLanguageLevel', trans('text_lang.level'), ['class' => 'col-md-4 control-label padding-top-10']) !!}</div>
                            <div class="col-md-4 col-xs-5">
                                {!! Form::select('announcementsLanguageLevel', array( 'bg' => trans('text_lang.basicGreetings'), 'cl' => trans('text_lang.conversationLevel'), 'ad' => trans('text_lang.advancedReadingOrWriting') ), null, ['class' => 'form-control padding-zero optLevel', 'name'=>'announcementsLanguageLevel[kh]', 'id'=>'Khmer']) !!}
                                @if ($errors->has('announcementsLanguageLevel'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('announcementsLanguageLevel') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group" role="form">
                            <div class="col-md-4 col-xs-12"></div>
                            <div class="col-md-2 col-xs-5 padding-right-zero" align="left">
                                <label class="checkbox-inline"><input name="language[en]" type="checkbox" value="english" id="chEnglish" class="chLanguage"> {{ trans('text_lang.english') }}</label>
                            </div>
                            <div class="col-md-1 col-xs-2 padding-zero">{!! Form::label('announcementsLanguageLevel', trans('text_lang.level'), ['class' => 'col-md-4 control-label padding-top-10']) !!}</div>
                            <div class="col-md-4 col-xs-5">
                                {!! Form::select('announcementsLanguageLevel', array( 'bg' => trans('text_lang.basicGreetings'), 'cl' => trans('text_lang.conversationLevel'), 'ad' => trans('text_lang.advancedReadingOrWriting') ), null, ['class' => 'form-control padding-zero optLevel', 'name'=>'announcementsLanguageLevel[en]', 'id'=>'English']) !!}
                                @if ($errors->has('announcementsLanguageLevel'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('announcementsLanguageLevel') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group" role="form">
                            <div class="col-md-4 col-xs-12"></div>
                            <div class="col-md-2 col-xs-5" align="left">
                                <label class="checkbox-inline"><input name="language[ko]" type="checkbox" value="korean" id="chKorean" class="chLanguage"> {{ trans('text_lang.korean') }}</label>
                            </div>
                            <div class="col-md-1 col-xs-2 padding-zero">{!! Form::label('announcementsLanguageLevel', trans('text_lang.level'), ['class' => 'col-md-4 control-label padding-top-10']) !!}</div>
                            <div class="col-md-4 col-xs-5">
                                {!! Form::select('announcementsLanguageLevel', array( 'bg' => trans('text_lang.basicGreetings'), 'cl' => trans('text_lang.conversationLevel'), 'ad' => trans('text_lang.advancedReadingOrWriting') ), null, ['class' => 'form-control padding-zero optLevel', 'name'=>'announcementsLanguageLevel[ko]', 'id'=>'Korean']) !!}
                                @if ($errors->has('announcementsLanguageLevel'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('announcementsLanguageLevel') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group" role="form">
                            <div class="col-md-4 col-xs-12"></div>
                            <div class="col-md-2 col-xs-5" align="left">
                                <label class="checkbox-inline"><input name="language[zh]" type="checkbox" value="chinese" id="chChinese" class="chLanguage">{{ trans('text_lang.chinese') }}</label>
                            </div>
                            <div class="col-md-1 col-xs-2 padding-zero">{!! Form::label('announcementsLanguageLevel', trans('text_lang.level'), ['class' => 'col-md-4 control-label  padding-top-10']) !!}</div>
                            <div class="col-md-4 col-xs-5">
                                {!! Form::select('announcementsLanguageLevel', array( 'bg' => trans('text_lang.basicGreetings'), 'cl' => trans('text_lang.conversationLevel'), 'ad' => trans('text_lang.advancedReadingOrWriting') ), null, ['class' => 'form-control padding-zero optLevel', 'name'=>'announcementsLanguageLevel[zh]', 'id'=>'Chinese']) !!}
                                @if ($errors->has('announcementsLanguageLevel'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('announcementsLanguageLevel') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group" role="form">
                            <div class="col-md-4 col-xs-12"></div>
                            <div class="col-md-2 col-xs-5" align="left">
                                <label class="checkbox-inline"><input name="language[th]" type="checkbox" value="thai" id="chThai" class="chLanguage">{{ trans('text_lang.thai') }}</label>
                            </div>
                            <div class="col-md-1 col-xs-2 padding-zero">{!! Form::label('announcementsLanguageLevel', trans('text_lang.level'), ['class' => 'col-md-4 control-label padding-top-10']) !!}</div>
                            <div class="col-md-4 col-xs-5">
                                {!! Form::select('announcementsLanguageLevel', array( 'bg' => trans('text_lang.basicGreetings'), 'cl' => trans('text_lang.conversationLevel'), 'ad' => trans('text_lang.advancedReadingOrWriting') ), null, ['class' => 'form-control padding-zero optLevel', 'name'=>'announcementsLanguageLevel[th]', 'id'=>'Thai']) !!}
                                @if ($errors->has('announcementsLanguageLevel'))
                                    <span class="help-block">
                                        <strong>{!! $errors->first('announcementsLanguageLevel') !!}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- end language--}}

                    </div>
                    <!-- panel-body -->
                </div>
            </div>

            <br>
            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-8 col-md-offset-3">
                    <button type="submit" name="submit" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <button href="" name="previewJob" class="btn btn-primary">
                        {{ trans('text_lang.previewJob')}}
                    </button>
                    <button href="" name="publishJob" class="btn btn-primary">
                        {{ trans('text_lang.publish')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/postjob' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--#text-center panel-body--}}
</div>
@endsection

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
@endsection

@section('extraJS')
    <script>
        var FKACTIVITY_SELECTED = '{!! old('fkActivitiesID')  !!}';
        var FKPROVINCEID = '';
        var FKDISTRICTID = '';
        var FKCOMMUNESID = '';
//        var FKVILLAGESID = '';
        <?php if( count( $errors ) > 0 ){ ?>
            FKPROVINCEID = {{ old('fkProvincesID') }}
            FKDISTRICTID = {{ old('fkDistrictsID') }}
            FKCOMMUNESID = {{ old('fkCommunesID') }}
            {{--FKVILLAGESID = {{ old('fkVillagesID') }}--}}
        <?php } ?>
    </script>

    <script src="{{ URL::asset('js/postjobDate.js') }}"></script>

    <script src="{{ URL::asset('js/choosePicture.js') }}"></script>
    <script src="{{ URL::asset('js/collapsePostjob.js') }}"></script>

    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>

    <script src="{{ URL::asset('js/checkLanguageLevel.js') }}"></script>
    <script src="{{ URL::asset('js/checkSalaryNegotiate.js') }}"></script>
    <script src="{{ URL::asset('js/positionOther.js') }}"></script>
    <script src="{{ URL::asset('js/activityOther.js') }}"></script>

    <!-- Check numeric -->
    <script type="text/javascript" src="{{ URL::asset('js/checkCondition.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/inputStartNotZero.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('lib/numeric/jquery/jquery.numeric.js') }}"></script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
@endsection
