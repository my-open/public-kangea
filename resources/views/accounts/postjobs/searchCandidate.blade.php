@extends('layouts.account')
@section('title', 'searchCandidate')
@section('breadcrumbs', Breadcrumbs::render('searchCandidate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.searchForCandidateInformation')}}</big></div>

        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <dl class="dl-horizontal">
                        <ul class="list-inline">
                            <dl class="dl-horizontal">
                                <dt> {{ trans('text_lang.position') }}: </dt>
                                <dd>
                                    {{ object_get($postjob, "positionsName{$lang}" ) }}
                                </dd>

                                <dt> {{ trans('text_lang.sector') }}: </dt>
                                <dd>
                                    {{ object_get($postjob, "sectorsName{$lang}" ) }}
                                </dd>

                                <dt> {{ trans('text_lang.subsector') }}: </dt>
                                <dd>
                                    {{ object_get($postjob, "subsectorsName{$lang}" ) }}
                                </dd>

                                <dt> {{ trans('text_lang.mainActivity') }}: </dt>
                                <dd>
                                    {{ object_get($postjob, "activitiesName{$lang}" ) }}
                                </dd>
                            </dl>
                        </ul>
                    </dl>

                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/postjob/askForApply', 'class' => 'form-horizontal', 'searchCandidate' => 'form', 'files' => true, 'id'=>'form1', 'name'=>'form1')) !!}
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th class="text-center"><input name="chkmain" type="checkbox" id="chkmain" onclick="check( {{ $countSeekers  }} )" /></th>
                                <th>{{ trans('text_lang.name')}}</th>
                                {{--<th>{{ trans('text_lang.phone')}}</th>--}}
                                {{--<th>{{ trans('text_lang.email')}}</th>--}}
                                <th>{{ trans('text_lang.action')}}</th>
                            </tr>
                            </thead>
                            <tbody class="text-left">

                            <input type="hidden" name="pkAnnouncementsID"  value="{{ $postjob->pkAnnouncementsID }}" />
                            <input type="hidden" name="fkCompaniesID"  value="{{ $postjob->fkCompaniesID }}" />
                            <input type="hidden" name="fkPositionsID"  value="{{ $postjob->fkPositionsID }}" />
                            @foreach($seekers as $user)
                                <tr>
                                    <td class="text-center">
                                        <input name="chk_user[]" type="checkbox" value="{{  $user->id }}"/>
                                    </td>
                                    <td>
                                        <a  href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/postjob/viewCandidate/'. $user->id.'/'.$postjob->pkAnnouncementsID ) }}" title="{{ trans('text_lang.viewCandidateDetail') }}">
                                            {{ ( $user->gender ) ? config("constants.GENDER")[$user->gender][Lang::getLocale()]:'' }} {{ $user->name }}
                                        </a>
                                        <?php
                                        $totals_AskToUser = DB::table('Candidates')
                                                ->select(DB::raw('count(*) as askTotals'))
                                                ->where('fkAnnouncementsID', '=', $postjob->pkAnnouncementsID)
                                                ->where('fkUsersID', '=', $user->id)
                                                ->where('fkCompaniesID', '=', $postjob->fkCompaniesID)
                                                ->groupBy('fkAnnouncementsID')
                                                ->count();
                                            if( $totals_AskToUser > 0){
                                                echo "<span class='text-danger'> (". trans('text_lang.alreadyAsk')  ." ". $totals_AskToUser. ")</span>";
                                            }
                                        ?>
                                    </td>
                                    {{--<td>--}}
                                        {{--{{ $user->phone }}--}}
                                    {{--</td>--}}
                                    {{--<td>--}}
                                        {{--{{ $user->email }}--}}
                                    {{--</td>--}}
                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/postjob/viewCandidate/'. $user->id.'/'.$postjob->pkAnnouncementsID,'<span class="glyphicon glyphicon-th"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.viewSeeker') ))) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div>
                        <div class="row text-right noPadding">
                            <div class="col-md-12">
                                @if( $seekers )
                                    {{ $seekers->links() }}
                                @endif
                            </div>
                        </div>

                    <div class="col-md-12" align="center">
                        @if( count($seekers) )
                        <button type="submit" class="btn btn-primary" name="save_rep">
                            {{ trans('text_lang.askForApply')}}
                        </button>
                        @else
                            <h3>{{ trans('text_lang.noJobSeekerThatHaveExperinceInThisPosition') }}</h3>
                        @endif
                    </div>

                    {!! Form::close() !!}


                </div>
                {{--col-md-12--}}
            </div>
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/checkCheckbox.js') }}"></script>
@endsection