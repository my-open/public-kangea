@extends('layouts.account')
@section('title', 'job lists')
@section('breadcrumbs', Breadcrumbs::render('postjob'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.jobAnnouncements') }}</big></div>

        <div class="panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/postjob/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addPostjob'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addPostjob') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead>
                        <tr>
                            <th>{{ trans('text_lang.announcementsTitle')}}</th>
                            {{--<th>{{ trans('text_lang.announcementsPublishDate')}}</th>--}}
                            {{--<th>{{ trans('text_lang.announcementsClosingDate')}}</th>--}}
                            <th>{{ trans('text_lang.sector')}}</th>
                            <th>{{ trans('text_lang.subsector')}}</th>
                            <th width="490" class="text-center">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($postjobs as $postjob)
                            <tr @if( $postjob->announcementsStatus == 0 )
                                    class="danger"
                                @elseif($postjob->announcementsClosingDate < date('Y-m-d 00:00:00') )
                                    class="text-danger"
                                @endif
                            >
                                <td>
                                    <b>
                                        {{ object_get($postjob, "positionsName{$lang}" ) }}
                                    </b>
                                    <br/>
                                    {{ dateConvertNormal($postjob->announcementsPublishDate) }} - {{ dateConvertNormal($postjob->announcementsClosingDate) }}
                                </td>
                                <td>
                                    {{ object_get($postjob, "sectorsName{$lang}" ) }}
                                </td>
                                <td>
                                    {{ object_get($postjob, "subsectorsName{$lang}" ) }}
                                        <?php
                                          $totalApplied = DB::table('JobApply')
                                                ->select(DB::raw('count(*) as totalJobs'))
                                                ->where('fkAnnouncementsID', '=', $postjob->pkAnnouncementsID)
                                                ->where('jobApplyIsConfirm', 1)
                                                ->groupBy('fkAnnouncementsID')
                                                ->count();
                                          $jobApplyStatusNumber = DB::table('JobApply')
                                                ->select(DB::raw('count(*) as totalJobs'))
                                                ->where('fkAnnouncementsID', '=', $postjob->pkAnnouncementsID)
                                                ->where('jobApplyStatus', '=', 0)
                                                ->where('jobApplyIsConfirm', 1)
                                                ->groupBy('fkAnnouncementsID')
                                                ->count();
                                        ?>
                                </td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/postjob/searchCandidate/'.$postjob->pkAnnouncementsID, trans('text_lang.searchCandidate'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.searchCandidate') ))) !!}

                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/postjob/application/new/'.$postjob->pkAnnouncementsID, $totalApplied.' '. trans('text_lang.applications'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.applications'), 'dataoption' => 'applicants', 'notify' => $jobApplyStatusNumber ))) !!}
                                    @if( $postjob->announcementsClosingDate >= date('Y-m-d 00:00:00') )
                                        @if( $postjob->announcementsStatus == 0 )
                                            {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobPublish/'.$postjob->pkAnnouncementsID, trans('text_lang.publish'), $attributes = array('class' => 'btn btn-sm btn-danger postjob-index','title' => trans('text_lang.publish') ))) !!}
                                        @elseif( $postjob->announcementsStatus == 1 )
                                            {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobUnpublish/'.$postjob->pkAnnouncementsID, trans('text_lang.unpublish'), $attributes = array('class' => 'btn btn-sm btn-info postjob-index','title' => trans('text_lang.unpublish') ))) !!}
                                        @endif
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/postjob/'.$postjob->pkAnnouncementsID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.editPostjob') ))) !!}
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/postjob/'.$postjob->pkAnnouncementsID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deletePostjob') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                    @else
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/repost/'.$postjob->pkAnnouncementsID, trans('text_lang.repost'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.repost') ))) !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $postjobs->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection
