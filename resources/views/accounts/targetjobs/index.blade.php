@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.jobSeekerDetail') )
@section('breadcrumbs', Breadcrumbs::render('targetJob'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.jobSeekerDetail')}}</big></div>

        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            @if( $targetJob )
                <dl class="dl-horizontal">
                    <dt> {{ trans('text_lang.sector') }}: </dt>
                    <dd>
                        {{ object_get($targetJob, "sectorsName{$lang}" ) }}
                    </dd>

                    @if( $targetJob->subsectorsNameEN )
                        <dt> {{ trans('text_lang.subsector') }}: </dt>
                        <dd>
                            {{ object_get($targetJob, "subsectorsName{$lang}" ) }}
                        </dd>
                    @endif

                    @if( $targetJob->positionsNameEN )
                        <dt> {{ trans('text_lang.position') }}: </dt>
                        <dd>
                            {{ object_get($targetJob, "positionsName{$lang}" ) }}
                        </dd>
                    @endif

                    @if( $targetJob->provincesNameEN )
                        <dt> {{ trans('text_lang.province') }}: </dt>
                        <dd>
                            {{ object_get($targetJob, "provincesName{$lang}" ) }}
                        </dd>
                    @endif

                    @if( $targetJob->districtsNameEN )
                        <dt> {{ trans('text_lang.district') }}: </dt>
                        <dd>
                            {{ object_get($targetJob, "districtsName{$lang}" ) }}
                        </dd>
                    @endif

                    @if( $targetJob->CommunesNameEN )
                        <dt> {{ trans('text_lang.commune') }}: </dt>
                        <dd>
                            {{ object_get($targetJob, "CommunesName{$lang}" ) }}
                        </dd>
                    @endif

                    <div class="row">
                        <div class="col-sm-12 col-md-12 text-center">
                            {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/targetJob/edit', trans('text_lang.editTargetJob'), $attributes = array('class' => 'btn btn-info' ))) !!}
                        </div>
                    </div>

            @else
                   <div class="row">
                       <div class="col-sm-12 col-md-12 text-center">
                           <h2>{{ trans('text_lang.noSearchJob')}}</h2>
                           {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/targetJob/create', trans('text_lang.addSearchJob'), array('class' => 'btn btn-info' ))) !!}
                       </div>
                   </div>
            @endif

        </div>
    </div>
@endsection
