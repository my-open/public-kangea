@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.addSearchJob') )
@section('breadcrumbs', Breadcrumbs::render('targetJobCreate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.addSearchJob')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/targetJob', 'class' => 'form-horizontal', 'targetjob' => 'form')) !!}

            <div class="form-group{{ $errors->has('fkSectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkSubsectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSubsectorsID', trans('text_lang.subsector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSubsectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSubsectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkPositionsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkPositionsID', trans('text_lang.position') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] ), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkPositionsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkPositionsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkProvincesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkProvincesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkDistrictsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkDistrictsID', trans('text_lang.district') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkDistrictsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkDistrictsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkCommunesID') ? ' has-error' : '' }}">
                {!! Form::label('fkCommunesID', trans('text_lang.commune'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('fkCommunesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkCommunesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{--<div class="form-group{{ $errors->has('fkVillagesID') ? ' has-error' : '' }}">--}}
                {{--{!! Form::label('fkVillagesID', trans('text_lang.village'), ['class' => 'col-md-3 control-label']) !!}--}}
                {{--<div class="col-md-7">--}}
                    {{--{!! Form::select('fkVillagesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}--}}
                    {{--@if ($errors->has('fkVillagesID'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{!! $errors->first('fkVillagesID') !!}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="form-group{{ $errors->has('fkZonesID') ? ' has-error' : '' }}">--}}
                {{--{!! Form::label('fkZonesID', trans('text_lang.zone'), ['class' => 'col-md-3 control-label']) !!}--}}
                {{--<div class="col-md-7">--}}
                    {{--{!! Form::select('fkZonesID', (['' => trans('text_lang.selectOption')] + $zones->toArray()), null, ['class' => 'form-control']) !!}--}}
                    {{--@if ($errors->has('fkZonesID'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{!! $errors->first('fkZonesID') !!}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/targetJob' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/province.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
    <script src="{{ URL::asset('js/village.js') }}"></script>

    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
@endsection
