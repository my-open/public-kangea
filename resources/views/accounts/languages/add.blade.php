@extends('layouts.account')
@section('title', 'Add Language')
@section('breadcrumbs', Breadcrumbs::render('languageCreate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.addLanguage')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/language', 'class' => 'form-horizontal', 'language' => 'form')) !!}

            <div class="form-group{{ $errors->has('languagesName') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('languagesName', trans('text_lang.language') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('languagesName', old('languagesName'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('languagesName'))
                        <span class="help-block">
                            <strong>{!! $errors->first('languagesName') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('languagesStatus') ? ' has-error' : '' }}">
                {!! Form::label('languagesStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('languagesStatus', array( 1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('languagesStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('languagesStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/language' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection
