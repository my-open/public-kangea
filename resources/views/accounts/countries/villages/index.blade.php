@extends('layouts.account')
@section('title', 'Village')
@section('breadcrumbs', Breadcrumbs::render('village'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.villageInformation') }}</big></div>

        @include('accounts.countries.tab_country')

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/village/create','<span class="glyphicon glyphicon-plus"></span> '.trans('text_lang.addVillage'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addVillage') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.id')}}</th>
                            <th>{{ trans('text_lang.name')}}</th>
                            <th>{{ trans('text_lang.communeName')}}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($villages as $village)
                            <tr @if( $village->villagesStatus == 0 ) class="danger" @endif >
                                <td>{{ $village->pkVillagesID }}</td>
                                <td>
                                    @if( Lang::getLocale() == 'en')
                                        {{ $village->villagesNameEN }}
                                    @else
                                        {{ $village->villagesNameKH }}
                                    @endif
                                </td>
                                <td>
                                    @if( Lang::getLocale() == 'en')
                                        {{ $village->CommunesNameEN }}
                                    @else
                                        {{ $village->CommunesNameKH }}
                                    @endif
                                </td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/village/'.$village->pkVillagesID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.editVillage') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/village/'.$village->pkVillagesID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteVillage') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $villages->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection