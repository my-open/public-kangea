@extends('layouts.account')
@section('title', 'Add Village')
@section('breadcrumbs', Breadcrumbs::render('villageCreate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.create_new_village')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-12">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/village', 'class' => 'form-horizontal', 'village' => 'form')) !!}

            <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkProvincesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkProvincesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkDistrictsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkDistrictsID', trans('text_lang.district') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkDistrictsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkDistrictsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkCommunesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkCommunesID', trans('text_lang.commune') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkCommunesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkCommunesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pkVillagesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pkVillagesID', trans('text_lang.id') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pkVillagesID', old('pkVillagesID'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('pkVillagesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pkVillagesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('villagesNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('villagesNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('villagesNameEN', old('villagesNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('villagesNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('villagesNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('villagesNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('villagesNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('villagesNameKH', old('villagesNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('villagesNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('villagesNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('villagesStatus') ? ' has-error' : '' }}">
                {!! Form::label('villagesStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('villagesStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('villagesStatus'))
                        <span class="help-block">
                            <strong>{!!  $errors->first('villagesStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/village' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
@endsection
