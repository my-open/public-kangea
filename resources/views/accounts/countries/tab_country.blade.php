<ul class="nav nav-tabs">
    <li <?php echoActiveClassIfRequestMatches("country")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/country">{{ trans('text_lang.country')}} </a></li>
    <li <?php echoActiveClassIfRequestMatches("province")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/province"> {{ trans('text_lang.province')}}</a></li>
    <li <?php echoActiveClassIfRequestMatches("district")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/district"> {{ trans('text_lang.district')}}</a></li>
    <li <?php echoActiveClassIfRequestMatches("commune")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/commune"> {{ trans('text_lang.commune')}}</a></li>
    <li <?php echoActiveClassIfRequestMatches("village")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/village"> {{ trans('text_lang.village')}}</a></li>
</ul>


<?php
function echoActiveClassIfRequestMatches($requestUri) {
    $urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $segments = explode('/', $urlArray);

    if ($segments[3] == $requestUri){
        echo 'class="active"';
    }
}
?>