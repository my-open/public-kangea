@extends('layouts.account')
@section('title', 'Edit District')
@section('breadcrumbs', Breadcrumbs::render('districtUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editDistrict')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::model($districts, [
                'method' => 'PATCH',
                'action' => array('DistrictController@update', $districts->pkDistrictsID),
                'role'=>'form','class'=>'form-horizontal'
            ]) !!}

            <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkProvincesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkProvincesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pkDistrictsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pkDistrictsID', trans('text_lang.id') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pkDistrictsID', old('pkDistrictsID'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('pkDistrictsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pkDistrictsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('districtsNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('districtsNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('districtsNameEN', old('districtsNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('districtsNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('districtsNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('districtsNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('districtsNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('districtsNameKH', old('districtsNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('districtsNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('districtsNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('districtsNameZH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('districtsNameZH', trans('text_lang.nameZH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('districtsNameZH', old('districtsNameZH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('districtsNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('districtsNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('districtsNameTH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('districtsNameTH', trans('text_lang.nameTH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('districtsNameTH', old('districtsNameTH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('districtsNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('districtsNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('districtsStatus') ? ' has-error' : '' }}">
                {!! Form::label('districtsStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('districtsStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('districtsStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('districtsStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/district' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection
