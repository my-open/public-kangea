@extends('layouts.account')
@section('title', 'District')
@section('breadcrumbs', Breadcrumbs::render('district'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.districtInformation') }}</big></div>

        @include('accounts.countries.tab_country')

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/district/create','<span class="glyphicon glyphicon-plus"></span> '.trans('text_lang.addDistrict'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addDistrict') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.id')}}</th>
                            <th>{{ trans('text_lang.name')}}</th>
                            <th>{{ trans('text_lang.provinceName')}}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($districts as $district)
                            <tr @if( $district->districtsStatus == 0 ) class="danger" @endif >
                                <td>{{ $district->pkDistrictsID }}</td>
                                <td>
                                    {{ object_get($district, "districtsName{$lang}" ) }}
                                </td>
                                <td>
                                    {{ object_get($district, "provincesName{$lang}" ) }}
                                </td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/district/'.$district->pkDistrictsID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.deleteDistrict') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/district/'.$district->pkDistrictsID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteDistrict') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $districts->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection