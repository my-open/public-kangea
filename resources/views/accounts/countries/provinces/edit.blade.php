@extends('layouts.account')
@section('title', 'Edit Province')
@section('breadcrumbs', Breadcrumbs::render('provinceUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editProvince')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::model($provinces, [
                'method' => 'PATCH',
                'action' => array('ProvinceController@update', $provinces->pkProvincesID),
                'role'=>'form','class'=>'form-horizontal'
            ]) !!}

            <div class="form-group{{ $errors->has('fkCountriesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkCountriesID', trans('text_lang.country') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkCountriesID', (['' => trans('text_lang.selectOption')] + $countries->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkCountriesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkCountriesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pkProvincesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pkProvincesID', trans('text_lang.id') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pkProvincesID', old('pkProvincesID'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('pkProvincesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pkProvincesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('provincesNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('provincesNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('provincesNameEN', old('provincesNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('provincesNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('provincesNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('provincesNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('provincesNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('provincesNameKH', old('provincesNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('provincesNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('provincesNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('provincesNameZH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('provincesNameZH', trans('text_lang.nameZH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('provincesNameZH', old('provincesNameZH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('provincesNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('provincesNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('provincesNameTH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('provincesNameTH', trans('text_lang.nameTH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('provincesNameTH', old('provincesNameTH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('provincesNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('provincesNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('provincesStatus') ? ' has-error' : '' }}">
                {!! Form::label('provincesStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('provincesStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('provincesStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('provincesStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/province' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection
