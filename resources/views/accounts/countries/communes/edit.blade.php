@extends('layouts.account')
@section('title', 'Edit Commune')
@section('breadcrumbs', Breadcrumbs::render('communeUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editCommune')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::model($communes, [
                'method' => 'PATCH',
                'action' => array('CommuneController@update', $communes->pkCommunesID),
                'role'=>'form','class'=>'form-horizontal'
            ]) !!}

            <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), $provincesID, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkProvincesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkProvincesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkDistrictsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkDistrictsID', trans('text_lang.district') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $Districts->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkDistrictsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkDistrictsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pkCommunesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pkCommunesID', trans('text_lang.id') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pkCommunesID', old('pkCommunesID'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('pkCommunesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pkCommunesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('CommunesNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('CommunesNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('CommunesNameEN', old('CommunesNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('CommunesNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('CommunesNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('CommunesNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('CommunesNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('CommunesNameKH', old('CommunesNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('CommunesNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('CommunesNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('CommunesNameZH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('CommunesNameZH', trans('text_lang.nameZH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('CommunesNameZH', old('CommunesNameZH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('CommunesNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('CommunesNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('CommunesNameTH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('CommunesNameTH', trans('text_lang.nameTH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('CommunesNameTH', old('CommunesNameTH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('CommunesNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('CommunesNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('communesStatus') ? ' has-error' : '' }}">
                {!! Form::label('communesStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('communesStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('communesStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('communesStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/commune' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/district.js') }}"></script>
@endsection