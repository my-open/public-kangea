@extends('layouts.account')
@section('title', 'Activity')
@section('breadcrumbs', Breadcrumbs::render('activity'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.activityInformation') }}</big></div>

        @include('accounts.sectors.tab_sector')

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/account/activity/searchActivity', 'class' => 'form-horizontal')) !!}

                        <div class="form-group">
                            <div class="col-md-3 text-center">
                                <span>{!! trans('text_lang.activity') !!}</span>
                                <?php $activitiesName = ${'activitiesName'.$lang}; ?>
                                {!! Form::text('activitiesName'.$lang, $activitiesName, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-3 text-center">
                                <span>{!! trans('text_lang.subsector') !!}</span>
                                {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray() ), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                            </div>

                            <div class="col-md-3 text-center">
                                <span>{!! trans('text_lang.order') !!}</span>
                                {!! Form::select('activitiesOrder', (['ASC' => trans('text_lang.asc'), 'DESC' => trans('text_lang.desc')]), $activitiesOrder, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-3">
                                <br/>
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('text_lang.search')}}
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6 text-left padding-numberOf">
                    @if( $activities->total() > 0 )
                        {{ trans('text_lang.numberOfManiactivity') }}: <b>{{ $activities->total() }}</b>
                    @endif
                </div>
                <div class="text-right col-md-6">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/activity/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addActivity'), $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.addActivity') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.id')}}</th>
                            <th>{{ trans('text_lang.name')}}</th>
                            <th>{{ trans('text_lang.subsector')}}</th>
                            <th>{{ trans('text_lang.order')}}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $activities as $row )
                            <tr @if( $row->activitiesStatus == 0 ) class="danger" @endif >
                                <td>{{ $row->pkActivitiesID }}</td>
                                <td>
                                    {{ object_get($row, "activitiesName{$lang}" ) }}
                                </td>
                                <td>
                                    {{ object_get($row, "subsectorsName{$lang}" ) }}
                                </td>
                                <td>{{ $row->activitiesOrder }}</td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/activity/'.$row->pkActivitiesID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.editActivity') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/activity/'.$row->pkActivitiesID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteActivity') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $activities->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection