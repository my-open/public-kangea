@extends('layouts.account')
@section('title', 'Edit Activity')
@section('breadcrumbs', Breadcrumbs::render('activityUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editActivity')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-12">
                    @include('flash::message')
                </div>
            </div>
            {!! Form::model($activity, [
                'method' => 'PATCH',
                'action' => array('ActivityController@update', $activity->pkActivitiesID),
                'role'=> 'form', 'class' => 'form-horizontal',
                'files' => true
            ]) !!}

            <div class="form-group{{ $errors->has('fkSectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), $sectorId, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkSubsectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSubsectorsID', trans('text_lang.subsector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSubsectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSubsectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('activitiesNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('activitiesNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('activitiesNameEN', old('activitiesNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('activitiesNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('activitiesNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('activitiesNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('activitiesNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('activitiesNameKH', old('activitiesNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('activitiesNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('activitiesNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('activitiesNameZH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('activitiesNameZH', trans('text_lang.nameZH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('activitiesNameZH', old('activitiesNameZH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('activitiesNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('activitiesNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('activitiesNameTH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('activitiesNameTH', trans('text_lang.nameTH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('activitiesNameTH', old('activitiesNameTH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('activitiesNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('activitiesNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('activitiesOrder') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('activitiesOrder', trans('text_lang.activitiesOrder'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('activitiesOrder', old('activitiesOrder'), ['class' => 'form-control']) !!}
                    @if ($errors->has('positionsOrder'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsOrder') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('activitiesStatus') ? ' has-error' : '' }}">
                {!! Form::label('activitiesStatus', trans('text_lang.status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('activitiesStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('activitiesStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('activitiesStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/activity' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/choosePicture.js') }}"></script>
@endsection