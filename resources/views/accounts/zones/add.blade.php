@extends('layouts.account')
@section('title', 'Add Zone')
@section('breadcrumbs', Breadcrumbs::render('zoneCreate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.addZone')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-12">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/zone', 'class' => 'form-horizontal', 'zone' => 'form')) !!}

            <div class="form-group{{ $errors->has('fkCountriesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkCountriesID', trans('text_lang.country') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkCountriesID', (['' => trans('text_lang.selectOption')] + $countries->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkCountriesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkCountriesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkProvincesID'))
                        <span class="help-block">
                            <strong>{!!  $errors->first('fkProvincesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkDistrictsID') ? ' has-error' : '' }}">
                {!! Form::label('fkDistrictsID', trans('text_lang.district'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts->toArray()), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('fkDistrictsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkDistrictsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkCommunesID') ? ' has-error' : '' }}">
                {!! Form::label('fkCommunesID', trans('text_lang.commune'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes->toArray()), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('fkCommunesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkCommunesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkVillagesID') ? ' has-error' : '' }}">
                {!! Form::label('fkVillagesID', trans('text_lang.village'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('fkVillagesID', (['' => trans('text_lang.selectOption')] + $villages->toArray()), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('fkVillagesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkVillagesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('zonesNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('zonesNameEN', trans('text_lang.zonesNameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('zonesNameEN', old('zonesNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('zonesNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('zonesNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('zonesNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('zonesNameKH', trans('text_lang.zonesNameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('zonesNameKH', old('zonesNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('zonesNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('zonesNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('zonesStatus') ? ' has-error' : '' }}">
                {!! Form::label('zonesStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('zonesStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('zonesStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('zonesStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/zone' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/province.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
    <script src="{{ URL::asset('js/village.js') }}"></script>
@endsection
