@extends('layouts.account')
@section('title', 'Update '.$row->name. ' profile')
@section('breadcrumbs', Breadcrumbs::render('userUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editUser')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-12">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::model($row, [
                'method' => 'PATCH',
                'action' => array('UserController@update', $row->id),
                'role'=>'form','class'=>'form-horizontal'
            ]) !!}

            <?php
            foreach( config("constants.GENDER") as $key => $value ){
                $gender[$key] = $value[Lang::getLocale()];
            }
            ?>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('name', trans('text_lang.yourName') . ' <span class="requredStar">***</span>', array('class' => 'col-xs-12 col-sm-12 col-md-3 control-label'))) !!}
                <div class="col-xs-4 col-sm-4 col-md-2">
                    {!! Form::select('gender', $gender, null, ['class' => 'form-control', 'id' => 'gender']) !!}
                    @if ($errors->has('name'))
                        <span class="help-block">
                                    <strong>{!! $errors->first('name') !!}</strong>
                                </span>
                    @endif
                </div>
                <div class="col-xs-8 col-sm-8 col-md-5">
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => "70", 'id' => 'name']) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('phone', trans('text_lang.phone') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'required' => 'required', 'maxlength' => '10']) !!}
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{!! $errors->first('phone') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            @if(Entrust::hasRole('jobseeker'))
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('email', trans('text_lang.email'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{!! $errors->first('email') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>
            @endif

            @if(Entrust::hasRole('employer'))
                {{--<div class="form-group{{ $errors->has('fkPositionsID') ? ' has-error' : '' }}">--}}
                    {{--{!! Html::decode(Form::label('fkPositionsID', trans('text_lang.position') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}--}}
                    {{--<div class="col-md-7">--}}
                        {{--{!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions), null, ['class' => 'form-control', 'required' => 'required']) !!}--}}
                        {{--@if ($errors->has('fkPositionsID'))--}}
                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('fkPositionsID') }}</strong>--}}
                                    {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    {!! Html::decode(Form::label('email', trans('text_lang.email') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                    <div class="col-md-7">
                        {!! Form::email('email', old('email'), ['class' => 'form-control', 'required' =>'requried']) !!}
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{!! $errors->first('email') !!}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    {!! Form::label('password', trans('text_lang.password'), ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-7">
                        {!! Form::password('password', ['class' => 'form-control']) !!}
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{!! $errors->first('password') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    {!! Form::label('password_confirmation', trans('text_lang.confirmPassword'), ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-7">
                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{!! $errors->first('password_confirmation') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            @endif

            <div style="display: none;">
                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    {!! Form::label('status', trans('text_lang.status'), ['class' => 'col-md-3 control-label']) !!}
                    <div class="col-md-7">
                        {!! Form::select('status', array( 1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                        @if ($errors->has('status'))
                            <span class="help-block">
                            <strong>{!! $errors->first('status') !!}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary" name="updateUserProfile">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/userProfile' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection
