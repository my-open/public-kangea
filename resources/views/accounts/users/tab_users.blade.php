<ul class="nav nav-tabs">
    <li <?php echoActiveClassIfRequestMatches("user")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/user">{{ trans('text_lang.user')}} </a></li>
    <li <?php echoActiveClassIfRequestMatches("jobseeker")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/jobseeker"> {{ trans('text_lang.jobseeker')}}</a></li>
    <li <?php echoActiveClassIfRequestMatches("employer")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/employer"> {{ trans('text_lang.employer')}}</a></li>
</ul>

<?php
function echoActiveClassIfRequestMatches($requestUri) {
    $urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $segments = explode('/', $urlArray);

    if ($segments[3] == $requestUri){
        echo 'class="active"';
    }
}
?>