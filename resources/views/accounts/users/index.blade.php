@extends('layouts.account')
@section('title', 'Users')
@section('breadcrumbs', Breadcrumbs::render('user'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.userInformation')}}</big></div>

        @include('accounts.users.tab_users')

        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/user/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addUser'), $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.addUser') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.name')}}</th>
                            <th>{{ trans('text_lang.email')}}</th>
                            <th>{{ trans('text_lang.phone')}}</th>
                            <th>{{ trans ('text_lang.create_date') }}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $rows as $row)
                            <tr @if( $row->status == 0 ) class="danger" @endif >
                                <td>{{ fnGetGender($row->gender, $row->name) }}</td>
                                <td>{{ $row->email  }}</td>
                                <td>{{ $row->phone  }}</td>
                                <td>{{ dateConvertNormal($row->created_at) }}</td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/user/'.$row->id.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.editUser') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/user/'.$row->id,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteUser'), 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $rows->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('../includes._modal_dialog')
@endsection
