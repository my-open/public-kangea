@extends('layouts.account')
@section('title', 'Job Applied')
@section('breadcrumbs', Breadcrumbs::render('jobApplied'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.workerAppliedInformation') }}</big></div>

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.position')}}</th>
                                <th>{{ trans('text_lang.companiesName')}}</th>
                                <th>{{ trans('text_lang.email')}}</th>
                                <th>{{ trans('text_lang.phone')}}</th>
                                <th>{{ trans('text_lang.appliedDate')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jobApplied as $jobApply)
                                <tr>
                                    <td>
                                        <?php
                                        $province_name = fnConvertSlug($jobApply->provincesName_EN);
                                        $sector_name = fnConvertSlug($jobApply->sectorsName_EN);
                                        $position_name = fnConvertSlug($jobApply->positionsName_EN);
                                        ?>
                                        @if( $jobApply->announcementsStatus == 1)
                                            <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $jobApply->pkAnnouncementsID ) }}">
                                                {{ object_get($jobApply, "positionsName{$lang}" ) }}
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $company_name = fnConvertSlug($jobApply->companiesNameEN); ?>
                                        <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $jobApply->pkCompaniesID) }}">
                                            @if( Lang::getLocale() == 'en')
                                                {{ $jobApply->companiesNameEN }}
                                            @else
                                                {{ $jobApply->companiesNameKH }}
                                            @endif
                                        </a>
                                    </td>
                                    <td>{{ $jobApply->companiesEmail }}</td>
                                    <td>{{ $jobApply->companiesPhone }}</td>
                                    <td>{{ date('d F, Y', strtotime( $jobApply->created_at)) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $jobApplied->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection
