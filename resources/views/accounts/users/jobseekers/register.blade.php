@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.jobSeekersRegister') )
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>{{ trans('text_lang.jobSeekersRegister') }}</h2>
                    <br/>
                </div>
            </div>

            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a data-target="#tap1" data-toggle="tab"><span class="badge">1</span> {{ trans('text_lang.userInfo') }}</a></li>
                <li><a data-target="#tap2" data-toggle="tab"><span class="badge">2</span> {{ trans('text_lang.workExpericence') }}</a></li>
                <li><a data-target="#tap3" data-toggle="tab"><span class="badge">3</span> {{ trans('text_lang.searchJobIn') }}</a></li>
            </ul>

            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/jobseeker/register', 'class' => 'form-horizontal', 'id' => 'formRegister')) !!}
            <div class="tab-content">
                <div class="tab-pane active" id="tap1">
                    <?php
                    foreach( config("constants.GENDER") as $key => $value ){
                        $gender[$key] = $value[Lang::getLocale()];
                    }
                    ?>
                    <div class="form-group frmname">
                        {!! Html::decode(Form::label('name', trans('text_lang.yourName') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                        <div class="col-md-2">
                            {!! Form::select('gender', $gender, 'f', ['class' => 'form-control', 'id' => 'gender']) !!}
                            @if ($errors->has('gender'))
                                <span class="help-block">
                                            <strong>{!! $errors->first('gender') !!}</strong>
                                        </span>
                            @endif
                            <span class="help-block name">
                                <strong></strong>
                            </span>
                        </div>
                        <div class="col-md-5">
                            {!! Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => "70", 'id' => 'name']) !!}
                        </div>
                    </div>

                    <div class="form-group frmphone">
                        {!! Html::decode(Form::label('phone', trans('text_lang.yourPhone') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                        <div class="col-md-7">
                            <div class="right-inner-addon">
                                <i class="glyphicon glyphicon-phone-alt"></i>
                                {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'required' => 'required', 'maxlength' => "10"]) !!}
                            </div>
                                <span class="help-block phone">
                                    <strong></strong>
                                </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="remember">{{ trans('text_lang.rememberMe') }}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="text-right col-md-7 col-md-offset-3">
                            <a id="btnUserInfo" class="btn btn-primary">{{ trans('text_lang.continue')}}</a>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="tap2">
                    <div id="divAppendTo">
                        <div class="row text-center">
                            <div class="col-md-12"><h2>{{ trans('text_lang.doYouHaveWorkExperience') }}</h2></div>
                        </div>

                        <div class="form-group text-center">
                            <label class="radio-inline control-label"><input type="radio" name="experience" value="1" checked>{{ trans('text_lang.yesIHaveExperience') }}</label>
                            <label class="radio-inline control-label"><input type="radio" name="experience" value="0">{{ trans('text_lang.NoIDontHaveExperience') }}</label>
                        </div>

                        <div class="divMoreExperience">
                            <div class="row infoLastjob text-center">
                                <div class="col-md-12"><h3>{{ trans('text_lang.infoAboutYourLastJob') }}</h3></div>
                            </div>

                            <div class="form-group frmfkSectorsID frmfkSectorsID0">
                                {!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                                <div class="col-md-7">
                                    {!! Form::select('fkSectorsID[]', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control fkSectorsID']) !!}
                                    <span class="help-block efkSectorsID efkSectorsID0">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group frmfkSubsectorsID frmfkSubsectorsID0">
                                {!! Html::decode(Form::label('fkSubsectorsID', trans('text_lang.subsector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                                <div class="col-md-7">
                                    {!! Form::select('fkSubsectorsID[]', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control fkSubsectorsID']) !!}
                                    <span class="help-block efkSubsectorsID efkSubsectorsID0">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group frmfkPositionsID frmfkPositionsID0">
                                {!! Html::decode(Form::label('fkPositionsID', trans('text_lang.position') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                                <div class="col-md-7">
                                    {!! Form::select('fkPositionsID[]', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control fkPositionsID']) !!}
                                    <span class="help-block efkPositionsID efkPositionsID0">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group frmworkExperience frmworkExperience0">
                                {!! Html::decode(Form::label('workExperience', trans('text_lang.workExpericence') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                                <div class="col-md-7">
                                    <select name="workExperience[]" class="form-control monthsOfExperience">
                                        <option value="">{{ trans('text_lang.selectOption') }}</option>
                                        <?php
                                        foreach( config("constants.EXPERIENCES") as $key => $value ){
                                            echo '<option value="'. $key .'">'. $value[Lang::getLocale()] .'</option>';
                                        }
                                        ?>
                                    </select>
                                    <span class="help-block eworkExperience eworkExperience0">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="text-right col-md-7 col-md-offset-3">
                                    <button type="button" class="btn btn-danger btnRemoveMoreExperience">{{ trans('text_lang.remove')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group addMoreExperince">
                        <div class="text-right col-md-7 col-md-offset-3">
                            <button id="addMoreExperience" type="button" class="btn btn-success">{{ trans('text_lang.addMoreExperince')}}</button>
                        </div>
                    </div>

                    <div class="form-group noteNoExperience">
                        <div class="col-md-7 col-md-offset-3">
                            <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="text-right col-md-12">
                            <a class="btn btn-primary back">{{ trans('text_lang.goBack')}}</a>
                            <a id="btnExperience" class="btn btn-primary continue">{{ trans('text_lang.continue')}}</a>
                            <a id="btnEnteredExperience" class="btn btn-primary">{{ trans('text_lang.enteredAllMyWorkExperience')}}</a>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="tap3">
                    <div class="row infoLastjob text-center">
                        <div class="col-md-12"><h3>{{ trans('text_lang.whatTypeOfJobAreYouInterested') }}</h3></div>
                    </div>

                    <div class="form-group frmfkSectorsIDTargetJob">
                        {!! Html::decode(Form::label('fkSectorsIDTargetJob', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkSectorsIDTargetJob', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                            <span class="help-block fkSectorsIDTargetJob">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('fkSubsectorsIDTargetJob', trans('text_lang.subsector'), ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkSubsectorsIDTargetJob', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('fkPositionsIDTargetJob', trans('text_lang.position'), ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkPositionsIDTargetJob', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label(null, trans('text_lang.enterIfYouKnowTheLocationInWhichYouWantToWork'), ['class' => 'col-md-8 text-left control-label']) !!}
                    </div>

                    <div class="form-group frmfkProvincesID">
                        {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control']) !!}
                            <span class="help-block fkProvincesID">
                                <strong></strong>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('fkDistrictsID', trans('text_lang.district'), ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('fkCommunesID', trans('text_lang.commune'), ['class' => 'col-md-3 control-label']) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--{!! Form::label('fkVillagesID', trans('text_lang.village'), ['class' => 'col-md-3 control-label']) !!}--}}
                        {{--<div class="col-md-7">--}}
                            {{--{!! Form::select('fkVillagesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="text-right col-md-7 col-md-offset-3">
                            <a class="btn btn-primary back">{{ trans('text_lang.goBack')}}</a>
                            <a id="btnRegister" class="btn btn-primary">{{ trans('text_lang.register')}}</a>
                        </div>
                    </div>
                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('extraJS')
    <script>
        var ANY_WHERE = '<?php echo trans('text_lang.anyWhere'); ?>';
    </script>
    {!!Html::script('js/seekerRegisterStep.js')!!}
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
    <script src="{{ URL::asset('js/village.js') }}"></script>
@endsection