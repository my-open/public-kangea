@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.login') )
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="col-md-12 text-center">
                <br/>
                <h3><b>{{ trans('text_lang.jobseekerLogin')}}</b></h3>
                <br/>
            </div>
            @if (Session::has('flash_notification.message'))
                <div class="row">
                <div class="text-center col-md-10 col-md-offset-1">
                    @include('flash::message')
                </div>
                </div>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/jobseeker/login') }}">
                {!! csrf_field() !!}
                <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label class="col-md-10 col-md-offset-1">
                        {{ trans('text_lang.phone') }} <span class="requredStar">***</span>
                    </label>

                    <div class="col-md-10 col-md-offset-1">
                        <div class="right-inner-addon">
                            <i class="glyphicon glyphicon-phone-alt"></i>
                            <input type="text"  class="form-control" name="phone" pattern="\d*" maxlength="10" value="{{ old('phone') }}">
                        </div>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                            <strong>{!! $errors->first('phone') !!}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" checked name="remember">{{ trans('text_lang.rememberMe') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10 col-md-offset-1">
                        <button type="submit" class="btn btn-primary  btn-block">
                            {{ trans('text_lang.loginAsSeeker') }}
                        </button>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-10 col-md-offset-1 text-center">
                        {{ trans('text_lang.desSocialLogin') }}
                    </label>

                    <div class="col-md-10 col-md-offset-1 text-center">
                        <a class="btn btn-block btn-social btn-facebook " href="/redirect">
                            <span class="fa fa-facebook-official"></span>
                            {{ trans('text_lang.fbLogin') }}
                        </a>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-7 col-md-offset-1">
                        <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
