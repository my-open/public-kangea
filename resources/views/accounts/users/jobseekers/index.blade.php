@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.jobseeker') )
@section('breadcrumbs', Breadcrumbs::render('jobseeker'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.jobseekerInformation') }}</big></div>

        @include('accounts.users.tab_users')

        <div class="panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="jumbotron">
                            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/jobseeker/search', 'class' => 'form-horizontal', 'company' => 'form')) !!}

                            <div class="form-group">
                                {!! Form::label('name', trans('text_lang.name'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::text("name", $name, ['class' => 'col-md-4 form-control']) !!}
                                </div>

                                {!! Form::label('phone', trans('text_lang.phone'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::text('phone', $phone, ['class' => 'col-md-4 form-control  positive-integer', 'maxlength'=>'11']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('email', trans('text_lang.email'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::text('email', $email, ['class' => 'col-md-4 form-control']) !!}
                                </div>

                                {!! Form::label('createFrom', trans('text_lang.createFrom'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::select('create_from', ( $create_from ),  isset($from)?$from:0, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('fromDate', trans('text_lang.fromDate'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::text('fromDate', isset($fromDate)?$fromDate:'', ['class' => 'col-md-4 form-control', 'id' => 'fromDate']) !!}
                                </div>

                                {!! Form::label('toDate', trans('text_lang.toDate'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::text('toDate', isset($toDate)?$toDate:'', ['class' => 'col-md-4 form-control', 'id' => 'toDate']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-9 text-right">
                                        <button type="submit" class="btn btn-primary btn-block" name="btnSearch" value="btnSearch">
                                            {{ trans('text_lang.search')}}
                                        </button>
                                    </div>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/jobseeker/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addJobseeker'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addJobseeker') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-left padding-numberOf">
                    @if( $rows->total() > 0 )
                        {{ trans('text_lang.numberOfJobseekers') }}: <b>{{ $rows->total() }}</b>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/jobseeker/export', 'class' => 'form-horizontal')) !!}
                    <input type="hidden" name="name" value="{{ $name }}"/>
                    <input type="hidden" name="phone" value="{{ $phone }}"/>
                    <input type="hidden" name="email" value="{{ $email }}"/>
                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-download"></span> {{ trans('text_lang.export') }}
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">

                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.name')}}</th>
                                <th>{{ trans('text_lang.email')}}</th>
                                <th>{{ trans('text_lang.phone')}}</th>
                                <th>{{ trans('text_lang.facebookId')}}</th>
                                <th>{{ trans ('text_lang.create_date') }}</th>

                                <th width="100">{{ trans ('text_lang.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                                <tr @if( $row->status == 0 ) class="danger" @endif >
                                    <td>{{ fnGetGender($row->gender, $row->name) }}</td>
                                    <td>{{ $row->email  }}</td>
                                    <td>{{ $row->phone  }}</td>
                                    <td>{{ $row->provider_user_id  }}</td>
                                    <td>{{ dateConvertNormal($row->created_at)  }}</td>
                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobseeker/'.$row->id.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.editJobseeker') ))) !!}
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobseeker/'.$row->id,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete', 'title' => trans('text_lang.deleteJobseeker'), 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $rows->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
@endsection

@section('extraJS')
    <script>
        $( document ).ready(function() {
            $( "#fromDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
            $( "#toDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>

    <!-- Check numeric -->
    <script type="text/javascript" src="{{ URL::asset('js/checkCondition.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('lib/numeric/jquery/jquery.numeric.js') }}"></script>
@endsection