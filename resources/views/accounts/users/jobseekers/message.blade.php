@extends('layouts.account')
@section('title', 'Experience')
@section('breadcrumbs', Breadcrumbs::render('message'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.allMessages') }}</big></div>

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.position')}}</th>
                                <th>{{ trans('text_lang.company')}}</th>
                                <th>{{ trans('text_lang.closingDate')}}</th>
                                <th width="170">{{ trans ('text_lang.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($messages as $row)
                                <tr @if( $row->seekerStatus == 1 ) class="danger" @endif >
                                    <td>
                                        <?php
                                        $province_name = fnConvertSlug($row->provincesName_EN);
                                        $sector_name = fnConvertSlug($row->sectorsName_EN);
                                        $position_name = fnConvertSlug($row->positionsName_EN);
                                        ?>
                                        <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $row->pkAnnouncementsID ) }}">
                                            @if( Lang::getLocale() == 'en')
                                                {{ $row->positionsNameEN }}
                                            @else
                                                {{ $row->positionsNameKH }}
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <?php $company_name = fnConvertSlug($row->companiesNameEN); ?>
                                        <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $row->pkCompaniesID) }}">
                                            @if( Lang::getLocale() == 'en')
                                                {{ $row->companiesNameEN }}
                                            @else
                                                {{ $row->companiesNameKH }}
                                            @endif
                                        </a>
                                    </td>
                                    <td> {{ dateConvertNormal($row->announcementsClosingDate) }} </td>

                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/jobApply/'. $row->fkAnnouncementsID, trans('text_lang.apply'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.apply') ))) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $messages->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
@endsection