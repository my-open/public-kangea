{{--<div class="form-group{{ $errors->has('fkPositionsID') ? ' has-error' : '' }}">--}}
    {{--{!! Html::decode(Form::label('fkPositionsID', trans('text_lang.position') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}--}}
    {{--<div class="col-md-7">--}}
        {{--{!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray()), null, ['class' => 'form-control fkPositionsID', 'required' => 'required']) !!}--}}
        {{--@if ($errors->has('fkPositionsID'))--}}
            {{--<span class="help-block">--}}
                {{--<strong>{{ $errors->first('fkPositionsID') }}</strong>--}}
            {{--</span>--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}

<div class="fieldset-table">
    @if( ! empty($company->companiesNickName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.companiesNickName') }}: </dt>
        <dd> {{ $company->companiesNickName }} </dd>
    </dl>
    @endif

    @if( ! empty($company->companiesNumberOfWorker))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.numberOfWorker') }}: </dt>
        <dd> {{ $company->companiesNumberOfWorker }} {{ trans('text_lang.person') }}</dd>
    </dl>
    @endif

    @if( ! empty($sectorName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.sector') }}: </dt>
        <dd> {{ $sectorName }} </dd>
    </dl>
    @endif

    @if( ! empty($subsectorName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.subsector') }}: </dt>
        <dd> {{ $subsectorName }} </dd>
    </dl>
    @endif

    @if( ! empty($company->companiesPhone))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.companiesPhone') }}: </dt>
        <dd> {{ $company->companiesPhone }} </dd>
    </dl>
    @endif

    @if( ! empty($company->companiesEmail))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.companiesEmail') }}: </dt>
        <dd> {{ $company->companiesEmail }} </dd>
    </dl>
    @endif

    @if( ! empty($provinceName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.province') }}: </dt>
        <dd> {{ $provinceName }} </dd>
    </dl>
    @endif

    @if( ! empty($districtName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.district') }}: </dt>
        <dd> {{ $districtName }} </dd>
    </dl>
    @endif

    @if( ! empty($communeName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.commune') }}: </dt>
        <dd> {{ $communeName }} </dd>
    </dl>
    @endif

    @if( ! empty($villageName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.village') }}: </dt>
        <dd> {{ $villageName }} </dd>
    </dl>
    @endif

    @if( ! empty($zoneName))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.zone') }}: </dt>
        <dd> {{ $zoneName }} </dd>
    </dl>
    @endif

    @if( ! empty($company->companiesAddress))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.companiesAddress') }}: </dt>
        <dd> {{ $company->companiesAddress }} </dd>
    </dl>
    @endif

    @if( ! empty($company->descriptionEN))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.descriptionEN') }}: </dt>
        <dd> {{ $company->descriptionEN }} </dd>
    </dl>
    @endif

    @if( ! empty($company->descriptionKH))
    <dl class="dl-horizontal">
        <dt> {{ trans('text_lang.descriptionKH') }}: </dt>
        <dd> {{ $company->descriptionKH }} </dd>
    </dl>
    @endif

    @if( ! empty($company->companiesLogo))
        <dl class="dl-horizontal">
            <dt> {{ trans('text_lang.companiesLogo') }}: </dt>
            <dd> <img src="{{ URL::asset("images/companyLogos/thumbnails/{$company->companiesLogo}") }}" alt="Company logo"> </dd>
        </dl>
    @endif
</div>