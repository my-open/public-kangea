@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.viewCandidate') )
@section('breadcrumbs', Breadcrumbs::render('viewCandidate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.jobSeekerDetail')}}</big></div>

        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" >
                                    <h4 class="panel-title">
                                        <div class="row">
                                            <div class="col-md-5 col-lg-5">
                                                <div style="float:left;">
                                                    {{ trans('text_lang.jobseekerProfile') }}
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-lg-5"> </div>
                                            <div class="col-md-2 col-lg-2"> </div>
                                        </div>
                                    </h4>
                                </div>

                                <br>
                                <dl class="dl-horizontal padding-left-dl-horizontal">
                                    <dt> {{ trans('text_lang.name') }}: </dt>
                                    <dd>
                                        {{ ( $user->gender )? config("constants.GENDER")[$user->gender][Lang::getLocale()]:'' }} {{ $user->name }}
                                        <?php
                                        $totals_AskToUser = DB::table('Candidates')
                                                ->select(DB::raw('count(*) as askTotals'))
                                                ->where('fkAnnouncementsID', '=', $jobId)
                                                ->where('fkUsersID', '=', $user->id)
                                                ->where('fkCompaniesID', '=', $companyId)
                                                ->groupBy('fkAnnouncementsID')
                                                ->count();
                                        if( $totals_AskToUser > 0){
                                            echo "<span class='text-danger'> (". trans('text_lang.alreadyAsk')  ." ". $totals_AskToUser. ")</span>";
                                        }
                                        ?>
                                    </dd>

                                    {{--<dt> {{ trans('text_lang.phone') }}: </dt>--}}
                                    {{--<dd> {{ $user->phone }} </dd>--}}

                                    {{--<dt> {{ trans('text_lang.email') }}: </dt>--}}
                                    {{--<dd> {{ $user->email }} </dd>--}}
                                </dl>
                            </div>

                         <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <div class="row">
                                                <div class="col-md-5 col-lg-5">
                                                    <div style="float:left;">
                                                        {{ trans('text_lang.jobseekerExperienceDetail') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-lg-5"> </div>
                                                <div class="col-md-2 col-lg-2"> </div>
                                            </div>
                                        </h4>
                                    </div>

                                 @if( $user->experience == 1 )
                                     <div class="table-responsive">
                                         <table class="table  table-bordered " >
                                             <thead class="text-center">
                                             <tr>
                                                 <th>{{ trans('text_lang.numberAuto')}}</th>
                                                 <th>{{ trans('text_lang.sector')}}</th>
                                                 <th>{{ trans('text_lang.subsector')}}</th>
                                                 <th>{{ trans('text_lang.position')}}</th>
                                                 <th>{{ trans('text_lang.workExpericence')}}</th>
                                             </tr>
                                             </thead>
                                             <tbody>
                                             <?php $i=1; ?>
                                             @foreach($experiences as $experience)
                                                 <tr>
                                                     <td>{{ $i }}</td>
                                                     <td>
                                                         {{ object_get($experience, "sectorsName{$lang}" ) }}
                                                     </td>
                                                     <td>
                                                         {{ object_get($experience, "subsectorsName{$lang}" ) }}
                                                     </td>
                                                     <td>
                                                         {{ object_get($experience, "positionsName{$lang}" ) }}
                                                     </td>
                                                     <td>
                                                         <?php
                                                         echo config("constants.EXPERIENCES")[$experience->workExperience][Lang::getLocale()];
                                                         ?>
                                                     </td>
                                                 </tr>
                                                 <?php $i =$i+1; ?>
                                             @endforeach
                                             </tbody>
                                         </table>
                                     </div>
                                 @endif
                         </div>


                                {{--How To Apply--}}

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <div class="row">
                                                <div class="col-md-5 col-lg-5">
                                                    <div style="float:left;">
                                                        {{ trans('text_lang.targetjobDetail') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-lg-5"> </div>
                                                <div class="col-md-2 col-lg-2"> </div>
                                            </div>
                                        </h4>
                                    </div>

                                    <br>
                                    <dl class="dl-horizontal padding-left-dl-horizontal">
                                        <dt> {{ trans('text_lang.sector') }}: </dt>
                                        <dd>
                                            @if( !empty($targetJob->fkSectorsID) )
                                                {{ object_get($targetJob, "sectorsName{$lang}" ) }}
                                            @endif
                                        </dd>

                                        @if( $targetJob->fkSubsectorsID )
                                            <dt> {{ trans('text_lang.subsector') }}: </dt>
                                            <dd>
                                                {{ object_get($targetJob, "subsectorsName{$lang}" ) }}
                                            </dd>
                                        @endif

                                        @if( $targetJob->fkPositionsID )
                                            <dt> {{ trans('text_lang.position') }}: </dt>
                                            <dd>
                                                {{ object_get($targetJob, "positionsName{$lang}" ) }}
                                            </dd>
                                        @endif

                                        @if( $targetJob->fkProvincesID )
                                            <dt> {{ trans('text_lang.province') }}: </dt>
                                            <dd>
                                                {{ object_get($targetJob, "provincesName{$lang}" ) }}
                                            </dd>
                                        @endif

                                        @if( $targetJob->fkDistrictsID )
                                            <dt> {{ trans('text_lang.district') }}: </dt>
                                            <dd>
                                                {{ object_get($targetJob, "districtsName{$lang}" ) }}
                                            </dd>
                                        @endif

                                        @if( $targetJob->fkCommunesID )
                                            <dt> {{ trans('text_lang.commune') }}: </dt>
                                            <dd>
                                                {{ object_get($targetJob, "CommunesName{$lang}" ) }}
                                            </dd>
                                        @endif

                                    </dl>

                                </div>

                            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/postjob/askForApply', 'class' => 'form-horizontal', 'searchCandidate' => 'form', 'files' => true, 'id'=>'form1', 'name'=>'form1')) !!}
                                <input type="hidden" name="pkAnnouncementsID"  value="{{ $jobId }}" />
                                <input type="hidden" name="fkCompaniesID"  value="{{ $companyId }}" />
                                <input type="hidden" name="fkPositionsID"  value="{{ $positionId }}" />
                                <input type="hidden" name="chk_user[]"  value="{{ $user->id }}" />

                                <div align="center">
                                    <div class="col-md-12 md-sm-12">
                                        <a class="btn btn-success" href="{{URL::previous()}}">{{ trans('text_lang.back')}}</a>
                                        <button type="submit" class="btn btn-primary" name="save_rep">
                                            {{ trans('text_lang.askForApply')}}
                                        </button>
                                    </div>
                                </div>

                            {!! Form::close() !!}
                    </div>
                </div>

        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
@endsection
