@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.employerRegistration') )
@section('content')
    <div class="container">
      <div class="row">
          <div class="col-md-12 text-center">
              <br/>
              <h2>{{ trans('text_lang.employerRegistration') }}</h2><hr/>
          </div>
      </div>
      <div class="row">
          <div class="row">
              <div class="col-md-5 text-right">
                  <div class="row">
                      <div class="col-md-12 text-center">
                          <br/>
                          <h3>{{ trans('text_lang.desSocialLogin') }}</h3>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 text-center">
                          <a class="btn btn-social btn-facebook " href="/redirectEmp">
                              <span class="fa fa-facebook-official"></span>
                              {{ trans('text_lang.fbLogin') }}
                          </a>
                      </div>
                  </div>
              </div>
              <div class="col-md-7">
                  <?php
                  foreach( config("constants.GENDER") as $key => $value ){
                      $gender[$key] = $value[Lang::getLocale()];
                  }
                  ?>
                  {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/employer/register', 'class' => 'form-horizontal', 'files' => true)) !!}
                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                      {!! Html::decode(Form::label('name', trans('text_lang.yourName') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                      <div class="col-md-2">
                          {!! Form::select('gender', $gender, null, ['class' => 'form-control', 'id' => 'gender']) !!}
                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('name')  !!}</strong>
                              </span>
                          @endif
                      </div>
                      <div class="col-md-5">
                          {!! Form::text('name', old('name'), ['class' => 'form-control', 'maxlength' => "70", 'id' => 'name']) !!}
                      </div>
                  </div>

                  <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                      {!! Html::decode(Form::label('phone', trans('text_lang.yourPhone') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                      <div class="col-md-7">
                          <div class="right-inner-addon">
                              <i class="glyphicon glyphicon-phone-alt"></i>
                              {!! Form::text('phone', old('phone'), ['class' => 'form-control positive-integer', 'maxlength' => "10"]) !!}
                          </div>
                          @if ($errors->has('phone'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('phone')  !!}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      {!! Html::decode(Form::label('email', trans('text_lang.yourEmail') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                      <div class="col-md-7">
                          <div class="right-inner-addon">
                              <i class="glyphicon glyphicon-envelope"></i>
                              {!! Form::email('email', old('email'), ['class' => 'form-control', 'maxlength' => "70"]) !!}
                          </div>
                          @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('email')  !!}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                      {!! Html::decode(Form::label('password', trans('text_lang.createAPassword') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                      <div class="col-md-7">
                          <div class="right-inner-addon">
                              <i class="glyphicon glyphicon-lock"></i>
                              {!! Form::password('password', ['class' => 'form-control', 'maxlength' => "70"]) !!}
                          </div>
                          @if ($errors->has('password'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('password')  !!}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group">
                      {!! Html::decode(Form::label('password_confirmation', trans('text_lang.confirmCreateAPassword') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                      <div class="col-md-7">
                          <div class="right-inner-addon">
                              <i class="glyphicon glyphicon-lock"></i>
                              {!! Form::password('password_confirmation', ['class' => 'form-control', 'maxlength' => "70"]) !!}
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-7 col-md-offset-5">
                          <div class="checkbox">
                              <label>
                                  <input type="checkbox" checked name="remember">{{ trans('text_lang.rememberMe') }}
                              </label>
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="col-md-7 col-md-offset-5">
                          <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                      </div>
                  </div>

                  <div class="form-group">
                      <div class="text-right col-md-7 col-md-offset-5 text-right">
                          <button type="submit" class="btn btn-primary btn-block">
                              {{ trans('text_lang.toCompanyDetails')}}
                          </button>
                      </div>
                  </div>
                  {!! Form::close() !!}
              </div>
          </div>
      </div>
    </div>
@endsection

@section('extraCSS')
    {{--<link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">--}}
@endsection

@section('extraJS')
    {{--<script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>--}}
    {{--{!!Html::script('js/companyNameAutoComplete.js')!!}--}}
    {{--{!!Html::script('js/employerRegisterStep.js')!!}--}}
    {{--<script src="{{ URL::asset('js/choosePicture.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('lib/ckeditor/ckeditor.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('lib/ckeditor/adapters/jquery.js') }}"></script>--}}
    {{--<script>--}}
        {{--$('#companiesDescriptionEN').ckeditor();--}}
        {{--$('#companiesDescriptionKH').ckeditor();--}}
    {{--</script>--}}
@endsection
