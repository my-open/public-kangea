@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.user') )
@section('title', 'Users')
@section('breadcrumbs', Breadcrumbs::render('user'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.jobSeekerDetail')}}</big></div>

        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" >
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <div style="float:left;">
                                    {{ trans('text_lang.jobseekerProfile') }}
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-5"> </div>
                            <div class="col-md-2 col-lg-2"> </div>
                        </div>
                    </h4>
                </div>

                <br>
                <dl class="dl-horizontal padding-left-dl-horizontal">
                    <dt> {{ trans('text_lang.name') }}: </dt>
                    <dd> {{ ( $user->gender )? config("constants.GENDER")[$user->gender][Lang::getLocale()]:'' }} {{ $user->name }} </dd>

                    <dt> {{ trans('text_lang.phone') }}: </dt>
                    <dd> {{ $user->phone }} </dd>

                    <dt> {{ trans('text_lang.email') }}: </dt>
                    <dd> {{ $user->email }} </dd>
                </dl>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" >
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <div style="float:left;">
                                    {{ trans('text_lang.jobseekerExperienceDetail') }}
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-5"> </div>
                            <div class="col-md-2 col-lg-2"> </div>
                        </div>
                    </h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.numberAuto')}}</th>
                            <th>{{ trans('text_lang.sector')}}</th>
                            <th>{{ trans('text_lang.subsector')}}</th>
                            <th>{{ trans('text_lang.position')}}</th>
                            <th>{{ trans('text_lang.workExpericence')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($experiences as $experience)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    @if( Lang::getLocale() == 'en')
                                        {{ $experience->sectorsNameEN }}
                                    @else
                                        {{ $experience->sectorsNameKH }}
                                    @endif
                                </td>
                                <td>
                                    @if( Lang::getLocale() == 'en')
                                        {{ $experience->subsectorsNameEN }}
                                    @else
                                        {{ $experience->subsectorsNameKH }}
                                    @endif
                                </td>
                                <td>
                                    @if( Lang::getLocale() == 'en')
                                        {{ $experience->positionsNameEN }}
                                    @else
                                        {{ $experience->positionsNameKH }}
                                    @endif
                                </td>
                                <td>
                                    <?php
                                    echo config("constants.EXPERIENCES")[$experience->workExperience][Lang::getLocale()];
                                    ?>
                                </td>
                            </tr>
                            <?php $i =$i+1; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" role="tab" >
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-md-5 col-lg-5">
                                <div style="float:left;">
                                    {{ trans('text_lang.targetjobDetail') }}
                                </div>
                            </div>
                            <div class="col-md-5 col-lg-5"> </div>
                            <div class="col-md-2 col-lg-2"> </div>
                        </div>
                    </h4>
                </div>

                <br>
                @if( $targetJob )
                <dl class="dl-horizontal padding-left-dl-horizontal">
                    <dt> {{ trans('text_lang.sector') }}: </dt>
                    <dd>
                        @if( !empty($targetJob->fkSectorsID) )
                            @if( Lang::getLocale() == 'en')
                                {{ $targetJob->sectorsNameEN }}
                            @else
                                {{ $targetJob->sectorsNameKH }}
                            @endif
                        @endif
                    </dd>

                    @if( $targetJob->fkSubsectorsID )
                        <dt> {{ trans('text_lang.subsector') }}: </dt>
                        <dd>
                            @if( Lang::getLocale() == 'en')
                                {{ $targetJob->subsectorsNameEN }}
                            @else
                                {{ $targetJob->subsectorsNameKH }}
                            @endif
                        </dd>
                    @endif

                    @if( $targetJob->fkPositionsID )
                        <dt> {{ trans('text_lang.position') }}: </dt>
                        <dd>
                            @if( Lang::getLocale() == 'en')
                                {{ $targetJob->positionsNameEN }}
                            @else
                                {{ $targetJob->positionsNameKH }}
                            @endif
                        </dd>
                    @endif

                    @if( $targetJob->fkProvincesID )
                        <dt> {{ trans('text_lang.province') }}: </dt>
                        <dd>
                            @if( Lang::getLocale() == 'en')
                                {{ $targetJob->provincesNameEN }}
                            @else
                                {{ $targetJob->provincesNameKH }}
                            @endif
                        </dd>
                    @endif

                    @if( $targetJob->fkDistrictsID )
                        <dt> {{ trans('text_lang.district') }}: </dt>
                        <dd>
                            @if( Lang::getLocale() == 'en')
                                {{ $targetJob->districtsNameEN }}
                            @else
                                {{ $targetJob->districtsNameKH }}
                            @endif
                        </dd>
                    @endif

                    @if( $targetJob->fkCommunesID )
                        <dt> {{ trans('text_lang.commune') }}: </dt>
                        <dd>
                            @if( Lang::getLocale() == 'en')
                                {{ $targetJob->CommunesNameEN }}
                            @else
                                {{ $targetJob->sectorsNameKH }}
                            @endif
                        </dd>
                    @endif

                    @if( $targetJob->fkVillagesID )
                        <dt> {{ trans('text_lang.village') }}: </dt>
                        <dd>
                            @if( Lang::getLocale() == 'en')
                                {{ $targetJob->villagesNameEN }}
                            @else
                                {{ $targetJob->villagesNameKH }}
                            @endif
                        </dd>
                    @endif
                </dl>
                @endif
            </div>

            <div align="center">
                <div class="col-md-12 md-sm-12">
                    <a class="btn btn-success" href="{{URL::previous()}}">{{ trans('text_lang.back')}}</a>
                </div>
            </div>

        </div>
        {{--#panel-body--}}

    </div>
@endsection