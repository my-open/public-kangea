@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.addRepCompany') )
@section('breadcrumbs', Breadcrumbs::render('employerCreate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.addRepCompany')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-12">
                    @include('flash::message')
                </div>
            </div>

            <div align="center">
                <img class="companyLogo" src="{{ URL::asset("images/companyLogos/thumbnails/{$company->companiesLogo}") }}" alt="Company logo">
            </div>
            <br>

            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.companiesName') }}: </dt>
                <dd>
                    {{ object_get($company, "companiesName{$lang}" ) }}
                </dd>
            </dl>
            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.sector') }}: </dt>
                <dd>
                    {{ object_get($company, "sectorsName{$lang}" ) }}
                </dd>
            </dl>
            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.subsector') }}: </dt>
                <dd>
                    {{ object_get($company, "subsectorsName{$lang}" ) }}
                </dd>
            </dl>
            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.mainActivity') }}: </dt>
                <dd>
                    {{ object_get($company, "activitiesName{$lang}" ) }}
                </dd>
            </dl>
            @if( !empty($company->companiesNumberOfWorker) )
                <dl class="dl-horizontal">
                    <dt> {{ trans('text_lang.numberOfWorker') }}: </dt>
                    <dd> {{ $company->companiesNumberOfWorker }} </dd>
                </dl>
            @endif
            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.locations') }}: </dt>
                <dd>
                    {!! fnGetLocation( object_get($company, "provincesName{$lang}"), object_get($company, "districtsName{$lang}"), object_get($company, "CommunesName{$lang}"), object_get($company, "villagesName{$lang}") ) !!}
                </dd>
            </dl>

            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.companiesPhone') }}: </dt>
                <dd> {{ $company->companiesPhone }} </dd>
            </dl>
            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.companiesEmail') }}: </dt>
                <dd> {{ $company->companiesEmail }} </dd>
            </dl>
            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.companiesSite') }}: </dt>
                <dd><a href="{{ $company->companiesSite }}" target="_blank">{{ $company->companiesSite }} </a></dd>
            </dl>
            @if($company->companiesAddress)
                <dl class="dl-horizontal">
                    <dt> {{ trans('text_lang.companiesAddress') }}: </dt>
                    <dd> {{ $company->companiesAddress }} </dd>
                </dl>
            @endif

            @if($company->companiesDescriptionEN || $company->companiesDescriptionKH )
                <dl class="dl-horizontal">
                    <dt> {{ trans('text_lang.companyDescription') }}: </dt>
                    <dd> {!! object_get($company, "companiesDescription{$lang}" ) !!} </dd>
                </dl>
            @endif
        </div>

        <div class="row">
            <div class="col-md-12 companyHeader"><h3> Representative Information </h3></div>
        </div>
        @if ($errors->has())
            <div class="row">
                <div class="text-center col-md-12">
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                </div>
            </div>

        @endif
            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/repEmployer/'.$company->pkCompaniesID, 'class' => 'form-horizontal', 'employer' => 'form', 'files' => true)) !!}
            <?php
                foreach( config("constants.GENDER") as $key => $value ){
                    $gender[$key] = $value[Lang::getLocale()];
                }
            ?>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('name', trans('text_lang.employerName') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-2">
                    {!! Form::select('gender', $gender, null, ['class' => 'form-control', 'id' => 'gender']) !!}
                    @if ($errors->has('gender'))
                        <span class="help-block">
                            <strong>{!! $errors->first('gender') !!}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-5">
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required' => 'required', 'maxlength' => "70"]) !!}
                    @if ($errors->has('name'))
                        <span class="help-block">
                                <strong>{!! $errors->first('name') !!}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('phone', trans('text_lang.employerPhone') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'required' => 'required', 'maxlength' => '10']) !!}
                    @if ($errors->has('phone'))
                        <span class="help-block">
                                <strong>{!!  $errors->first('phone') !!}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('employerEmail', trans('text_lang.employerEmail') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::email('email', old('email'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{!! $errors->first('email') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('password', trans('text_lang.createAPassword') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    <input type="password" required class="form-control" name="password" id="password">
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('password_confirmation', trans('text_lang.confirmCreateAPassword') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    <input type="password" required class="form-control" name="password_confirmation">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-7 col-md-offset-4">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-4">
                    <button type="submit" name="btnRepSave" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <a class="btn btn-success" href="{{ URL::previous() }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection