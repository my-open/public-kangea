@extends('layouts.account')
@section('title', 'Job Applied')
@section('breadcrumbs', Breadcrumbs::render('jobApplied'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.workerAppliedInformation') }}</big></div>

        <div class="panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.position')}}</th>
                            <th>{{ trans('text_lang.name')}}</th>
                            <th>{{ trans('text_lang.phone')}}</th>
                            <th>{{ trans('text_lang.appliedDate')}}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($jobApplied as $jobApply)
                                <tr>
                                    <td>
                                        <?php
                                        $province_name = fnConvertSlug($jobApply->provincesName_EN);
                                        $sector_name = fnConvertSlug($jobApply->sectorsName_EN);
                                        $position_name = fnConvertSlug($jobApply->positionsName_EN);
                                        ?>
                                        @if( $jobApply->announcementsStatus == 1)
                                            <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $jobApply->pkAnnouncementsID ) }}">
                                                {{ object_get($jobApply, "positionsName{$lang}" ) }}
                                            </a>
                                        @endif
                                    </td>
                                    <td>{{ ($jobApply->gender )? config("constants.GENDER")[$jobApply->gender][Lang::getLocale()]:'' }}  {{ $jobApply->jobApplyName }}</td>
                                    <td>{{ $jobApply->jobApplyPhone }}</td>

                                    <td> {{ date('d F, Y', strtotime( $jobApply->created_at)) }} </td>


                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobApplied/'.$jobApply->pkJobApplyID,'<span class="glyphicon glyphicon-th"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.viewSeeker') ))) !!}
                                        @if(Entrust::hasRole('employer'))
                                            {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobApplied/'.$jobApply->pkJobApplyID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteJobApplied') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $jobApplied->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection
