@extends('layouts.account')
@section('title', 'searchCandidate')
@section('breadcrumbs', Breadcrumbs::render('searchCandidate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.searchForCandidateInformation')}}</big></div>

        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/searchCandidate', 'class' => 'form-horizontal', 'searchCandidate' => 'form')) !!}

                        <div class="form-group">
                            {!! Form::label('fkSectorsID', trans('text_lang.sector'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), $fkSectorsID, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                            </div>

                            {!! Form::label('fkSubsectorsID', trans('text_lang.subsector'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                @if( $subsectors )
                                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                                @else
                                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fkPositionsID', trans('text_lang.position'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                @if( $positions )
                                    {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray()), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                                @else
                                    {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')]), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                                @endif
                            </div>

                            {!! Form::label('fkProvincesID', trans('text_lang.province'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), $fkProvincesID, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="text-right col-md-4 col-md-offset-8">
                                <button type="submit" class="btn btn-primary btn-block" name="btnSearch">
                                    {{ trans('text_lang.search')}}
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center"><h3>{{ trans('text_lang.yourCandidateList') }}</h3></div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.name')}}</th>
                            {{--<th>{{ trans('text_lang.phone')}}</th>--}}
                            {{--<th>{{ trans('text_lang.email')}}</th>--}}
                            <th>{{ trans('text_lang.create')}}</th>
                            <th>{{ trans('text_lang.action')}}</th>
                        </tr>
                        </thead>
                        <tbody class="text-left">
                        @foreach($seekers as $user)
                            <tr>
                                <td>
                                    <a  href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/searchCandidate/'. $user->id ) }}" title="{{ trans('text_lang.viewCandidateDetail') }}">
                                        {{ ( $user->gender )? config("constants.GENDER")[$user->gender][Lang::getLocale()]:'' }} {{ $user->name }}
                                    </a>
                                </td>
                                {{--<td>--}}
                                    {{--{{ $user->phone }}--}}
                                {{--</td>--}}
                                {{--<td>--}}
                                    {{--{{ $user->email }}--}}
                                {{--</td>--}}
                                <td>
                                    {{ date('d F, Y', strtotime($user->created_at)) }}
                                </td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/searchCandidate/'. $user->id,'<span class="glyphicon glyphicon-th"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.viewSeeker') ))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            @if( $seekers )
                                {{ $seekers->appends($searchCriteria)->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
@endsection
