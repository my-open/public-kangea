@extends('layouts.account')
@section('title', $user->name.' profile')
@section('breadcrumbs', Breadcrumbs::render('user'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.userInformation')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-12">
                    @include('flash::message')
                </div>
            </div>

            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.name') }}: </dt>
                <dd> {{ fnGetGender($user->gender, $user->name) }}</dd>

                <dt> {{ trans('text_lang.phone') }}: </dt>
                <dd> {{ $user->phone }} </dd>

                <dt> {{ trans('text_lang.email') }}: </dt>
                <dd> {{ $user->email }} </dd>

                <dt></dt>
                <dd>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 text-center">
                                 {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/userProfile/edit',trans('text_lang.editUser'), $attributes = array('class' => 'btn btn-sm btn-info' ))) !!}
                        </div>
                    </div>
                </dd>
            </dl>
        </div>
       {{--#panel-body--}}

    </div>

    @if($checkExistsUserID)
        @if( $isCompanyAdmin )
            <div class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.companyRepLists')}}</big></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 text-right">
                            {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/repEmployer/' . $companyID,'<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addCompanyRep'), $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.addUser') ))) !!}
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.userInformation')}}</th>
                                <th width="270">{{ trans ('text_lang.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companyReps as $row)
                                <tr @if( $row->companyLevel == 3 ) class="danger" @endif>
                                    <td class="text-left">
                                        <b>{{ trans('text_lang.id')}}:</b> {{ $row->id }}
                                        <br/><b>{{ trans('text_lang.name')}}:</b> {{ fnGetGender($row->gender, $row->name) }}
                                        @if( $row->companyLevel != 0 )
                                            (<em>{{ config("constants.CompanyRepRole")[$row->companyLevel][Lang::getLocale()]  }}</em>)
                                        @endif
                                        <br/><b>{{ trans('text_lang.phone')}}:</b> {{ $row->phone  }}
                                        <br/><b>{{ trans('text_lang.email')}}:</b> {{ $row->email  }}
                                    </td>
                                    <td class="text-center">
                                        @if( $row->companyLevel == 2 )
                                            {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/approveOrUnapproveCompanyRep/'.$row->id, trans('text_lang.unApproveCompanyRep'), $attributes = array('class' => 'btn btn-sm btn-warning btn-block btnapprove','title' => trans('text_lang.unApproveCompanyRep') ,  'data-title'=> trans('text_lang.title_unapproveCompanyRep'), 'data-content' => trans('text_lang.content_unapprove'), 'onClick'=>'return false;'))) !!}
                                        @elseif( $row->companyLevel == 3 )
                                            {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/approveOrUnapproveCompanyRep/'.$row->id, trans('text_lang.approveCompanyRep'), $attributes = array('class' => 'btn btn-sm btn-info btn-block btnapprove','title' => trans('text_lang.approveCompanyRep') , 'data-title'=> trans('text_lang.title_approveCompanyRep'), 'data-content' => trans('text_lang.content_approve'), 'onClick'=>'return false;'))) !!}
                                        @endif
                                        @if( $row->companyLevel == 0 )
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/deleteCompanyRep/'.$row->id,  trans('text_lang.deleteCompanyRep'), $attributes = array('class' => 'btn btn-sm btn-danger btn-block btndeleteCompanyRep','title' => trans('text_lang.deleteCompanyRep') , 'data-title'=> trans('text_lang.title_deleteCompanyRep'), 'data-content' => trans('text_lang.content_deleteCompanyRep'), 'onClick'=>'return false;'))) !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $companyReps->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif

    @include('includes._modal_dialog_approve_company_rep')
    @include('includes._modal_dialog_delete_company_rep')
@endsection