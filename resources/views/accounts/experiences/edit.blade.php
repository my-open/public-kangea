@extends('layouts.account')
@section('title', 'Edit Experience')
@section('breadcrumbs', Breadcrumbs::render('experienceUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editExperience')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::model($pkJobExperiencesID, [
                'method' => 'PATCH',
                'action' => array('ExperienceController@update', $pkJobExperiencesID->pkJobExperiencesID),
                'role'=>'form','class'=>'form-horizontal'
            ]) !!}

            <div class="form-group{{ $errors->has('fkSectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkSubsectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSubsectorsID', trans('text_lang.subsector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectorsBySectors->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSubsectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSubsectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkPositionsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkPositionsID', trans('text_lang.position') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positionsBysectors->toArray() ), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkPositionsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkPositionsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('workExperience') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('workExperience', trans('text_lang.experience') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('workExperience', (['' => trans('text_lang.selectOption')] + $workExperiences), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('workExperience'))
                        <span class="help-block">
                            <strong>{!!  $errors->first('workExperience') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/experience' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/province.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
    <script src="{{ URL::asset('js/village.js') }}"></script>

    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
@endsection
