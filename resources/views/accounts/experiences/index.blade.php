@extends('layouts.account')
@section('title', 'Experience')
@section('breadcrumbs', Breadcrumbs::render('experience'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.experienceInformation') }}</big></div>

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/experience/create','<span class="glyphicon glyphicon-plus"></span> '. trans("text_lang.addExperience"), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addExperience') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.numberAuto')}}</th>
                            <th>{{ trans('text_lang.sector')}}</th>
                            <th>{{ trans('text_lang.subsector')}}</th>
                            <th>{{ trans('text_lang.position')}}</th>
                            <th>{{ trans('text_lang.workExpericence')}}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($experiences as $experience)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    {{ object_get($experience, "sectorsName{$lang}") }}
                                </td>
                                <td>
                                    {{ object_get($experience, "subsectorsName{$lang}") }}
                                </td>
                                <td>
                                    {{ object_get($experience, "positionsName{$lang}") }}
                                </td>
                                <td>
                                    <?php
                                        echo config("constants.EXPERIENCES")[$experience->workExperience][Lang::getLocale()];
                                    ?>
                                </td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/experience/'.$experience->pkJobExperiencesID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.editExperience') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/experience/'.$experience->pkJobExperiencesID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteExperience') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                            <?php $i =$i+1; ?>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $experiences->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection