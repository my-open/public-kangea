@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.page') )
@section('breadcrumbs', Breadcrumbs::render('page'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.pageInformation') }}</big></div>

        <div class="text-center panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/page/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addPage'), $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.addPage') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.id')}}</th>
                                <th>{{ trans('text_lang.title')}}</th>
                                <th width="100">{{ trans ('text_lang.action') }}</th>
                            </tr>
                            </thead>
                            <tbody class="text-left">
                            @foreach($pages as $page)
                                <tr @if( $page->pagesStatus == 0 ) class="danger" @endif >
                                    <td>{{ $page->pkPagesID }}</td>
                                    <td>
                                        @if( Lang::getLocale() == 'en')
                                            {{ $page->pagesTitleEN }}
                                        @else
                                            {{ $page->pagesTitleKH }}
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/page/'.$page->pkPagesID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.editPage') ))) !!}
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/page/'.$page->pkPagesID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deletePage') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
{{--                            {!! $pages->links() !!}--}}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{--text-center panel-body--}}
    </div>

    @include('includes._modal_dialog')
@endsection