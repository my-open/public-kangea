@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.editPage') )
@section('breadcrumbs', Breadcrumbs::render('pageUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editPage')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::model($page, [
                'method' => 'PATCH',
                'action' => array('PageController@update', $page->pkPagesID),
                'role'=>'form',
                'class'=>'form-horizontal'
            ]) !!}

            <div class="form-group{{ $errors->has('pkPagesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pkPagesID', trans('text_lang.pkPagesID'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('pkPagesID', (['' => trans('text_lang.selectOption')] + $getMainTitles->toArray()), $page->fkPagesSubTitleID, ['class' => 'form-control']) !!}
                    @if ($errors->has('pkPagesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pkPagesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{--<div class="form-group{{ $errors->has('fkPagesSubTitleID') ? ' has-error' : '' }}">--}}
                {{--{!! Html::decode(Form::label('fkPagesSubTitleID', trans('text_lang.fkPagesSubTitleID'), array('class' => 'col-md-3 control-label'))) !!}--}}
                {{--<div class="col-md-7">--}}
                    {{--{!! Form::select('fkPagesSubTitleID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}--}}
                    {{--@if ($errors->has('fkPagesSubTitleID'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('fkPagesSubTitleID') }}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group{{ $errors->has('pagesSiteTitle') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesSiteTitle', trans('text_lang.pagesSiteTitle'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pagesSiteTitle', old('pagesSiteTitle'), ['class' => 'form-control']) !!}
                    @if ($errors->has('pagesSiteTitle'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesSiteTitle') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesSiteKeyword') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesSiteKeyword', trans('text_lang.pagesSiteKeyword'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pagesSiteKeyword', old('pagesSiteKeyword'), ['class' => 'form-control']) !!}
                    @if ($errors->has('pagesSiteKeyword'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesSiteKeyword') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesSiteDescription') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesSiteDescription', trans('text_lang.pagesSiteDescription'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::textarea('pagesSiteDescription', old('pagesSiteDescription'), ['class' => 'form-control','rows' => '3']) !!}
                    @if ($errors->has('pagesSiteDescription'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesSiteDescription') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesTitleEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesTitleEN', trans('text_lang.pagesTitleEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pagesTitleEN', old('pagesTitleEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('pagesTitleEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesTitleEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesTitleKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesTitleKH', trans('text_lang.pagesTitleKH'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pagesTitleKH', old('pagesTitleKH'), ['class' => 'form-control']) !!}
                    @if ($errors->has('pagesTitleKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesTitleKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesDescriptionEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesDescriptionEN', trans('text_lang.pagesDescriptionEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::textarea('pagesDescriptionEN', old('pagesDescriptionEN'), ['class' => 'form-control','rows' => '3', 'required' => 'required']) !!}
                    @if ($errors->has('pagesDescriptionEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesDescriptionEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesDescriptionKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesDescriptionKH', trans('text_lang.pagesDescriptionKH'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::textarea('pagesDescriptionKH', old('companiesDescriptionEN'), ['class' => 'form-control','rows' => '3']) !!}
                    @if ($errors->has('pagesDescriptionKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesDescriptionKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesUrl') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesUrl', trans('text_lang.pagesUrl'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pagesUrl', old('pagesUrl'), ['class' => 'form-control']) !!}
                    @if ($errors->has('pagesUrl'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesUrl') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesOrder') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('pagesOrder', trans('text_lang.pagesOrder'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('pagesOrder', old('pagesOrder'), ['class' => 'form-control']) !!}
                    @if ($errors->has('pagesOrder'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesOrder') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesPosition') ? ' has-error' : '' }}">
                {!! Form::label('pagesPosition', trans('text_lang.pagesPosition'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('pagesPosition', array(0 => trans('text_lang.bottomPageTitle'), 1 => trans('text_lang.topPageTitle')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('pagesPosition'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesOrder') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('pagesStatus') ? ' has-error' : '' }}">
                {!! Form::label('pagesStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('pagesStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('pagesStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('pagesStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/page' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/pageSubTitle.js') }}"></script>
    <script src="{{ URL::asset('lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::asset('lib/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#pagesDescriptionEN').ckeditor();
        $('#pagesDescriptionKH').ckeditor();
    </script>
@endsection