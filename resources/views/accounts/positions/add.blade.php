@extends('layouts.account')
@section('title', 'Add Position')
@section('breadcrumbs', Breadcrumbs::render('positionCreate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.addPosition')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/position', 'class' => 'form-horizontal', 'position' => 'form')) !!}

            <div class="form-group{{ $errors->has('fkSectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('positionsCode') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('positionsCode', trans('text_lang.positionsCode') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('positionsCode', old('positionsCode'), ['class' => 'form-control', 'required' => 'required', 'maxlength' => '5']) !!}
                    @if ($errors->has('positionsCode'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsCode') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('positionsNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('positionsNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('positionsNameEN', old('positionsNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('positionsNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('positionsNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('positionsNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('positionsNameKH', old('positionsNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('positionsNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('positionsNameZH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('positionsNameZH', trans('text_lang.nameZH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('positionsNameZH', old('positionsNameZH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('positionsNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('positionsNameTH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('positionsNameTH', trans('text_lang.nameTH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('positionsNameTH', old('positionsNameTH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('positionsNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('positionsOrder') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('positionsOrder', trans('text_lang.positionsOrder'), array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('positionsOrder', old('positionsOrder'), ['class' => 'form-control']) !!}
                    @if ($errors->has('positionsOrder'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsOrder') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('positionsStatus') ? ' has-error' : '' }}">
                {!! Form::label('positionsStatus', trans('text_lang.Status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('positionsStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('positionsStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('positionsStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.save')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/position' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/choosePicture.js') }}"></script>
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
@endsection
