@extends('layouts.account')
@section('title', 'Position')
@section('breadcrumbs', Breadcrumbs::render('position'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.positionInformation') }}</big></div>

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="jumbotron">
                            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/position/search', 'class' => 'form-horizontal', 'position' => 'form')) !!}

                            <div class="form-group">
                                {!! Form::label('positionsCode', trans('text_lang.positionsCode'), ['class' => 'col-md-3 control-label']) !!}

                                <div class="col-md-3">
                                    {!! Form::text('positionsCode', $positionsCode, ['class' => 'form-control', 'id' => 'positionsCode', 'maxlength' => '5']) !!}
                                </div>

                                {!! Form::label('positionsCode', trans('text_lang.positionName'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {{--{!! Form::text("positionsName{$lang}", object_get($positions, "provincesName{$lang}"), ['class' => 'form-control', 'id' => 'positionsNameEN']) !!}--}}
                                    @if( Lang::getLocale() == 'en')
                                        {!! Form::text('positionsNameEN', $positionsNameEN, ['class' => 'form-control', 'id' => 'positionsNameEN']) !!}
                                    @else
                                        {!! Form::text('positionsNameKH', $positionsNameKH, ['class' => 'form-control', 'id' => 'positionsNameKH']) !!}
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="text-right col-md-4 col-md-offset-8">
                                    <button type="submit" class="btn btn-primary btn-block" name="btnSearch">
                                        {{ trans('text_lang.search')}}
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-md-4 text-left padding-numberOf">
                    @if( $positions->total() > 0 )
                        {{ trans('text_lang.numberOfPositions') }}: <b>{{ $positions->total() }}</b>
                    @endif
                </div>
                <div class="text-right col-md-8">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/position/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addPosition'), $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.addPosition') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.id')}}</th>
                            <th>{{ trans('text_lang.positionsCode')}}</th>
                            <th>{{ trans('text_lang.name')}}</th>
                            <th>{{ trans('text_lang.sector')}}</th>
                            <th width="100">{{ trans ('text_lang.action') }}</th>
                        </tr>
                        </thead>
                        <tbody class="text-left">
                        @foreach($positions as $row)
                            <tr @if( $row->positionsStatus == 0 ) class="danger" @endif >
                                <td>{{ $row->pkPositionsID }}</td>
                                <td>{{ $row->positionsCode }}</td>
                                <td>
                                    {{ object_get($row, "positionsName{$lang}" ) }}
                                </td>
                                <td>
                                    {{ object_get($row, "sectorsName{$lang}" ) }}
                                </td>
                                <td class="text-center">
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/position/'.$row->pkPositionsID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.editSubsector') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/position/'.$row->pkPositionsID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteSubsector') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $positions->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>

    @include('includes._modal_dialog')
@endsection
