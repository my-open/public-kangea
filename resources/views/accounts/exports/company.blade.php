@extends('layouts.account')
@section('title', 'export')
@section('breadcrumbs', Breadcrumbs::render('sector'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.exportCompany') }}</big></div>
        <div class="text-center panel-body" >

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/exportCompany/search', 'class' => 'form-horizontal', 'company' => 'form')) !!}

                        <div class="form-group">
                            {!! Form::label('companiesName', trans('text_lang.companiesName'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text("companiesName{$lang}", $companiesName, ['class' => 'col-md-4 form-control']) !!}
                            </div>

                            {!! Form::label('createFrom', trans('text_lang.createFrom'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::select('create_from', ( $create_from ),  isset($from)?$from:0, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fromDate', trans('text_lang.fromDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('fromDate', $fromDate, ['class' => 'col-md-4 form-control', 'id' => 'fromDate']) !!}
                            </div>

                            {!! Form::label('toDate', trans('text_lang.toDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('toDate', $toDate, ['class' => 'col-md-4 form-control', 'id' => 'toDate']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <?php
                                        $isUsedPost_Hidden = 0;
                                        $isUsedPost = false;
                                            if( isset($isUsedPostJob) && $isUsedPostJob == 1 ) {
                                                $isUsedPost_Hidden = 1;
                                                $isUsedPost = true;
                                            }
                                        ?>
                                        {!! Form::checkbox('IsUsedPostJob', 1, $isUsedPost, ['class' => 'field']) !!}
                                        {{ trans('text_lang.isUsedPostJob') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                           <div class="row">
                               <div class="col-md-3 col-md-offset-9 text-right">
                                   <button type="submit" class="btn btn-primary btn-block" name="btnSearch" value="btnSearch">
                                       {{ trans('text_lang.search')}}
                                   </button>
                               </div>
                           </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-left padding-numberOf">
                    @if( $companies->total() > 0 )
                        {{ trans('text_lang.numberOfCompany') }}: <b>{{ $companies->total() }}</b>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/exportCompany/export', 'class' => 'form-horizontal')) !!}
                        <input type="hidden" name="companiesName{{$lang}}" value="{{ $companiesName }}"/>
                        <input type="hidden" name="fromDate" value="{{ $fromDate }}"/>
                        <input type="hidden" name="isUsedPostJob" value="{{ $isUsedPost_Hidden }}"/>
                        <input type="hidden" name="toDate" value="{{ $toDate }}"/>
                        <input type="hidden" name="create_from" value="{{ $from }}"/>
                        <button type="submit" class="btn btn-primary">
                            <span class="fa fa-download"></span> {{ trans('text_lang.export') }}
                        </button>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead>
                            <tr>
                                <th>{{ trans('text_lang.companyID')}}</th>
                                <th>{{ trans('text_lang.companyName')}}</th>
                                <th>{{ trans('text_lang.companiesEmail')}}</th>
                                <th>{{ trans('text_lang.companiesPhone')}}</th>

                                <th>{{ trans('text_lang.companyRepresentative')}}</th>
                                <th>{{ trans('text_lang.userEmail')}}</th>
                                <th>{{ trans('text_lang.userPhone')}}</th>
                                <th>{{ trans('text_lang.createdAt')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companies as $row)
                                <tr  class="text-left">
                                    <td>{{ $row->pkCompaniesID }}</td>
                                    <td>
                                        <a href="{{ '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/view/'.$row->pkCompaniesID }}" target="_blank">
                                            {{ object_get($row, "companiesName{$lang}" ) }}
                                        </a>
                                    </td>
                                    <td>{{ $row->companiesEmail  }}</td>
                                    <td>{{ $row->companiesPhone  }}</td>

                                    <td><a href="#">{{ $row->name  }}</a></td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->phone }}</td>
                                    <td>{{ dateConvertNormal($row->created_at) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $companies->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        {{--#panel-body--}}

    </div>
@endsection

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
@endsection

@section('extraJS')
    <script>
        $( document ).ready(function() {
            $( "#fromDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
            $( "#toDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
@endsection