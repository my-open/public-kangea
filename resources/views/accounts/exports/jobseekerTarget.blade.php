@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.jobseeker') )
@section('breadcrumbs', Breadcrumbs::render('jobseeker'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.exportTargetOfJobseeker') }}</big></div>

        <div class="panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/exportJobseeker/view', 'class' => 'form-horizontal', 'company' => 'form')) !!}

                        <div class="form-group">
                            {!! Form::label('name', trans('text_lang.name'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text("name", isset($name)? $name: null , ['class' => 'col-md-2 form-control']) !!}
                            </div>

                            {!! Form::label('sector', trans('text_lang.sector'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()),  isset($fkSectorsID)? $fkSectorsID: null, ['class' => 'form-control', 'id'=>'fkSectorsID']) !!}
                            </div>

                            {!! Form::label('subsector', trans('text_lang.subsector'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                @if( $subsectors )
                                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), isset($fkSubsectorsID)? $fkSubsectorsID: null, ['class' => 'form-control', 'id'=>'fkSubsectorsID']) !!}
                                @else
                                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), isset($fkSubsectorsID)? $fkSubsectorsID: null, ['class' => 'form-control', 'id'=>'fkSubsectorsID']) !!}
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('name', trans('text_lang.phone'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text('phone', isset($phone)? $phone: null, ['class' => 'col-md-2 form-control  positive-integer', 'maxlength'=>'11']) !!}
                            </div>

                            {!! Form::label('position', trans('text_lang.position'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                @if( $positions )
                                    {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray()), isset($fkPositionsID)? $fkPositionsID: null, ['class' => 'form-control', 'id'=>'fkPositionsID']) !!}
                                @else
                                    {!! Form::select('fkPositionsID', ['' => trans('text_lang.selectOption')], isset($fkPositionsID)? $fkPositionsID: null, ['class' => 'form-control', 'id'=>'fkPositionsID']) !!}
                                @endif
                            </div>

                            {!! Form::label('province', trans('text_lang.province'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::select('fkProvincesID',  (['' => trans('text_lang.selectOption')] + $provinces->toArray()), isset($fkProvincesID)? $fkProvincesID: null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-9 text-right">
                                    <button type="submit" class="btn btn-primary btn-block" name="btnSearch" value="btnSearch">
                                        {{ trans('text_lang.search')}}
                                    </button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-left padding-numberOf">
                    @if( $targetJobs->total() > 0 )
                        {{ trans('text_lang.numberOfTargetJobOfJobseeker') }}: <b>{{ $targetJobs->total() }}</b>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/exportJobseeker/export', 'class' => 'form-horizontal')) !!}
                    <input type="hidden" name="name" value="{{ isset($name)? $name:'' }}"/>
                    <input type="hidden" name="phone" value="{{ isset($phone)? $phone:'' }}"/>
                    <input type="hidden" name="fkSectorsID" value="{{ isset($fkSectorsID)? $fkSectorsID:'' }}"/>
                    <input type="hidden" name="fkSubsectorsID" value="{{ isset($fkSubsectorsID)? $fkSubsectorsID:'' }}"/>
                    <input type="hidden" name="fkPositionsID" value="{{ isset($fkPositionsID)? $fkPositionsID:'' }}"/>
                    <input type="hidden" name="fkProvincesID" value="{{ isset($fkProvincesID)? $fkProvincesID:'' }}"/>
                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-download"></span> {{ trans('text_lang.export') }}
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.id')}}</th>
                                <th>{{ trans('text_lang.name')}}</th>
                                <th>{{ trans('text_lang.phone')}}</th>

                                <th>{{ trans('text_lang.sector')}}</th>
                                <th>{{ trans('text_lang.subsector')}}</th>
                                <th>{{ trans('text_lang.position')}}</th>
                                <th>{{ trans('text_lang.province')}}</th>

                                <th width="100">{{ trans ('text_lang.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($targetJobs as $row)
                                <tr @if( $row->status == 0 ) class="danger" @endif >
                                    <td>{{ $row->id  }}</td>
                                    <td>{{ fnGetGender($row->gender, $row->name) }}</td>
                                    <td>{{ $row->phone  }}</td>

                                    <td>{{ object_get($row, "sectorsName{$lang}") }}</td>
                                    <td>{{ object_get($row, "subsectorsName{$lang}") }}</td>
                                    <td>{{ object_get($row, "positionsName{$lang}") }}</td>
                                    <td>{{ object_get($row, "provincesName{$lang}") }}</td>
                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobseeker/'.$row->id.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info', 'title' => trans('text_lang.editJobseeker') ))) !!}
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobseeker/'.$row->id,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete', 'title' => trans('text_lang.deleteJobseeker'), 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $targetJobs->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
    @endsection

    @section('extraJS')
        <script>
            var subsectorId = '';
            var positionId = '';
            <?php  if( count( $errors ) > 0 ){ ?>
                subsectorId = {{ old('fkSubsectorsID') }};
                positionId = {{ old('fkPositionsID') }};
            <?php } ?>
        </script>

        <script src="{{ URL::asset('js/subsector.js') }}"></script>
        <script src="{{ URL::asset('js/position.js') }}"></script>
            <!-- Check numeric -->
    <script type="text/javascript" src="{{ URL::asset('js/checkCondition.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('lib/numeric/jquery/jquery.numeric.js') }}"></script>
@endsection