@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.applications') )
@section('breadcrumbs', Breadcrumbs::render('applicants'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.applicantInformation') }}</big></div>

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/expShareJob/search', 'class' => 'form-horizontal', 'Share' => 'form')) !!}

                        <div class="form-group">
                            {!! Form::label('fromDate', trans('text_lang.fromDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('fromDate', isset($fromDate)?$fromDate:'', ['class' => 'col-md-4 form-control', 'id' => 'fromDate']) !!}
                            </div>

                            {!! Form::label('toDate', trans('text_lang.toDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                                {!! Form::text('toDate', isset($toDate)?$toDate:'', ['class' => 'col-md-4 form-control', 'id' => 'toDate']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                                {!! Form::label('jobApplyBy', trans('text_lang.applyBy'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::select('shareBy', array( '' => trans('text_lang.all'), '1'=>trans('text_lang.phone'), '2'=>trans('text_lang.facebook') ), isset($shareBy)?$shareBy:null, ['class' => 'col-md-4 form-control']) !!}
                                </div>

                                {!! Form::label('lbAnn', trans('text_lang.announcementId'), ['class' => 'col-md-2 control-label']) !!}
                                <div class="col-md-4">
                                    {!! Form::number('fkAnnouncementsID', $fkAnnouncementsID, ['class' => 'col-md-4 form-control']) !!}
                                </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-9 text-right">
                                    <button type="submit" class="btn btn-primary btn-block" name="btnSearch">
                                        {{ trans('text_lang.search')}}
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-left padding-numberOf">
                    @if( $shares->total() > 0 )
                        {{ trans('text_lang.numberOfShareJobs') }}: <b>{{ $shares->total() }}</b>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/expShareJob/export', 'class' => 'form-horizontal')) !!}
                    <input type="hidden" name="fromDate" value="{{ $fromDate }}"/>
                    <input type="hidden" name="toDate" value="{{ $toDate }}"/>
                    <input type="hidden" name="shareBy" value="{{ $shareBy }}"/>
                    <input type="hidden" name="fkAnnouncementsID" value="{{ $fkAnnouncementsID }}"/>

                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-download"></span> {{ trans('text_lang.export') }}
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.id')}}</th>
                                <th>{{ trans('text_lang.name')}}</th>
                                <th>{{ trans('text_lang.phone')}}</th>
                                {{--<th>{{ trans('text_lang.shareBy')}}</th>--}}
                                <th>{{ trans('text_lang.position')}}</th>
                                <th>{{ trans('text_lang.date')}}</th>
                            </tr>
                            </thead>
                            <tbody class="text-left">
                            @foreach($shares as $share)
                                <tr>
                                    <td> {{ $share->pkSharesID }} </td>
                                    <td>{{ fnGetGender($share->sharesGender, $share->sharesName) }}</td>

                                    <td> {{ $share->sharesPhone }} </td>
                                    {{--<td> {{ $share->sharesWho }} </td>--}}
                                    <td> {{ object_get($share, "positionsName{$lang}") }} </td>
                                    <td>{{ dateConvertNormal($share->created_at) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $shares->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>

    @include('includes._modal_dialog')
@endsection

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
@endsection

@section('extraJS')
    <script>
        $( document ).ready(function() {
            $( "#fromDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
            $( "#toDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
@endsection