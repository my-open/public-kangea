@extends('layouts.account')
@section('title', 'export')
@section('breadcrumbs', Breadcrumbs::render('sector'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.exportAnnouncement') }}</big></div>
        <div class="text-center panel-body" >

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/exportAnnouncement/search', 'class' => 'form-horizontal', 'company' => 'form')) !!}

                        <div class="form-group">
                            {!! Form::label('companiesName', trans('text_lang.companiesName'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text("companiesName{$lang}", $companiesName, ['class' => 'col-md-2 form-control']) !!}
                            </div>


                            {!! Form::label('fromDate', trans('text_lang.fromDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text('fromDate', $fromDate, ['class' => 'col-md-2 form-control', 'id' => 'fromDate']) !!}
                            </div>

                            {!! Form::label('toDate', trans('text_lang.toDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text('toDate', $toDate, ['class' => 'col-md-2 form-control', 'id' => 'toDate']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-9 text-right">
                                    <button type="submit" class="btn btn-primary btn-block" name="btnSearch" value="btnSearch">
                                        {{ trans('text_lang.search')}}
                                    </button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-left padding-numberOf">
                    @if( $announcements->total() > 0 )
                        {{ trans('text_lang.numberOfAnnouncements') }}: <b>{{ $announcements->total() }}</b>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/exportAnnouncement/export', 'class' => 'form-horizontal')) !!}
                    <input type="hidden" name="companiesName{{$lang}}" value="{{ $companiesName }}"/>
                    <input type="hidden" name="fromDate" value="{{ $fromDate }}"/>
                    <input type="hidden" name="toDate" value="{{ $toDate }}"/>
                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-download"></span> {{ trans('text_lang.export') }}
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>

            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.position')}}</th>
                            <th>{{ trans('text_lang.company')}}</th>
                            <th>{{ trans('text_lang.announcementsClosingDate')}}</th>
                            <th width="190">{{ trans('text_lang.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
                        @foreach($announcements as $job)
                            <?php
                            $province_name = fnConvertSlug($job->provincesName_EN);
                            $sector_name = fnConvertSlug($job->sectorsName_EN);
                            $position_name = fnConvertSlug($job->positionsName_EN);
                            ?>
                            <tr @if( $job->announcementsStatus == 0 )
                                    class="danger text-left"
                                @elseif($job->announcementsClosingDate < date('Y-m-d 00:00:00') )
                                    class="text-danger text-left"
                                @endif
                            >
                                <td>
                                    <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID ) }}">
                                        @if($job->announcementsClosingDate < date('Y-m-d 00:00:00') && $job->announcementsStatus != 0 )
                                            <span class="text-danger">{{ object_get($job, "positionsName{$lang}" ) }}</span>
                                        @else
                                            {{ object_get($job, "positionsName{$lang}" ) }}
                                        @endif
                                    </a>
                                </td>
                                <td>
                                   {{ object_get($job, "companiesName{$lang}") }}
                                </td>
                                <td>
                                    {{ dateConvertNormal($job->announcementsClosingDate) }}
                                </td>
                                <td class="text-right">
                                    @if( $job->announcementsStatus == 0 )
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/jobPublish/'.$job->pkAnnouncementsID, trans('text_lang.publish'), $attributes = array('class' => 'btn btn-sm btn-danger postjob-index','title' => trans('text_lang.publish') ))) !!}
                                    @elseif( $job->announcementsStatus == 1 )
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/jobUnpublish/'.$job->pkAnnouncementsID, trans('text_lang.unpublish'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.unpublish') ))) !!}
                                    @endif
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/job/edit/'.$job->pkAnnouncementsID,'<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.editPostjob') ))) !!}
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/job/delete/'.$job->pkAnnouncementsID,  '<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btnDeletePostjob','title' => trans('text_lang.deletePostjob') , 'data-title'=> trans('text_lang.title_deletePostjob'), 'data-content' => trans('text_lang.content_deletePostjob'), 'onClick'=>'return false;'))) !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row text-right noPadding">
                    <div class="col-md-12">
                        {!! $announcements->appends(Request::input())->links() !!}
                    </div>
                </div>


            </div>
        {{--#panel-body--}}

    </div>
        @include('includes._modal_dialog_delete_job_view_in_company')
@endsection

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
@endsection

@section('extraJS')
    <script>
        $( document ).ready(function() {
            $( "#fromDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
            $( "#toDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
@endsection