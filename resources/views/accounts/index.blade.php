@extends('layouts.account')
@section('title', 'Bongpheak' . trans('text_lang.account') )
@section('breadcrumbs', Breadcrumbs::render('account'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.dashboard') }}</big></div>

        <div class="text-center panel-body">
            <h2>{{ trans('text_lang.welcomeToDashboard') }}</h2> 
            <!-- {{ trans('text_lang.welcome') }} -->
        </div>
    </div>
@endsection
