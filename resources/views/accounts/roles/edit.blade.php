@extends('layouts.account')
@section('title', 'Edit Role')
@section('breadcrumbs', Breadcrumbs::render('roleUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editRole')}}</big></div>
        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>
            {!! Form::model($role, [
                'method' => 'PATCH',
                'action' => array('RoleController@update', $role->id),
                'role'=>'form','class'=>'form-horizontal'
            ]) !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('name', trans('text_lang.name') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{!! $errors->first('name') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('display_name', trans('text_lang.display_name') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('display_name', old('display_name'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('display_name'))
                        <span class="help-block">
                            <strong>{!! $errors->first('display_name') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                {!! Form::label('description', trans('text_lang.description'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control']) !!}
                    @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{!! $errors->first('description') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">

                <div class="text-right col-md-7 col-md-offset-3">
                    <br/>
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/role' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
