@extends('layouts.account')
@section('title', 'Roles')
@section('breadcrumbs', Breadcrumbs::render('role'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.roleInformation')}}</big></div>

        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/role/create','<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addRole'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addRole') ))) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.id')}}</th>
                                <th>{{ trans('text_lang.name')}}</th>
                                <th>{{ trans('text_lang.display_name')}}</th>
                                <th>{{ trans ('text_lang.create_date') }}</th>
                                <th width="100">{{ trans ('text_lang.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->display_name  }}</td>
                                    <td>{{ $role->created_at }}</td>
                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/role/'.$role->id.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.editRole') ))) !!}
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/role/'.$role->id,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete', 'title' => trans('text_lang.deleteRole'), 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes._modal_dialog')
@endsection
