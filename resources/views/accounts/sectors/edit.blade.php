@extends('layouts.account')
@section('title', 'Edit Sector')
@section('breadcrumbs', Breadcrumbs::render('sectorUpdate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.editSector')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>
            {!! Form::model( $sector, [
                'method' => 'PATCH',
                'action' => array('SectorController@update', $sector->pkSectorsID),
                'role' => 'form', 'class' => 'form-horizontal',
                'files' => true
            ]) !!}

            <div class="form-group{{ $errors->has('sectorsPhoto') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('sectorsPhoto', trans('text_lang.photo') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    <input type="file" name="sectorsPhoto" style="visibility:hidden;height: 0" id="sectorsPhoto"/>
                    <input type="hidden" value="MAX_FILE_SIZE" id="max" />
                    <fieldset class="fieldset_style">
                        <div class="input-group">
                              <span class="input-group-addon" id="basic-addon1">
                                    <a  onclick="issueUpload('sectorsPhoto','sectorsPhoto_text')" style="cursor:pointer;"  ><i class="fa fa-upload"></i> {{ trans('text_lang.choosePicture') }}</a>
                                </span>
                            <input type="text" class="form-control"​  placeholder="No File Selected" aria-describedby="basic-addon1"  id="sectorsPhoto_text" disabled>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="form-group{{ $errors->has('sectorsNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('sectorsNameEN', trans('text_lang.nameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('sectorsNameEN', old('sectorsNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('sectorsNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('sectorsNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('sectorsNameKH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('sectorsNameKH', trans('text_lang.nameKH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('sectorsNameKH', old('sectorsNameKH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('sectorsNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('sectorsNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('sectorsNameZH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('sectorsNameZH', trans('text_lang.nameZH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('sectorsNameZH', old('sectorsNameZH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('sectorsNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('sectorsNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('sectorsNameTH') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('sectorsNameTH', trans('text_lang.nameTH') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('sectorsNameTH', old('sectorsNameTH'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('sectorsNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('sectorsNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('sectorsStatus') ? ' has-error' : '' }}">
                {!! Form::label('sectorsStatus', trans('text_lang.status'), ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('sectorsStatus', array(1 => trans('text_lang.active'), 0 => trans('text_lang.inActive')), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('sectorsStatus'))
                        <span class="help-block">
                            <strong>{!! $errors->first('sectorsStatus') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-3">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('text_lang.update')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/sector' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/choosePicture.js') }}"></script>
@endsection