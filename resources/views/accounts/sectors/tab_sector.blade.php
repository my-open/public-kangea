<ul class="nav nav-tabs">
    <li <?=echoActiveClassIfRequestMatches("sector")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/sector">{{ trans('text_lang.sector')}} </a></li>
    <li <?=echoActiveClassIfRequestMatches("subsector")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/subsector"> {{ trans('text_lang.subsector')}}</a></li>
    <li <?=echoActiveClassIfRequestMatches("activity")?> > <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/activity"> {{ trans('text_lang.activity')}}</a></li>
</ul>


<?php
function echoActiveClassIfRequestMatches($requestUri) {
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri){
        echo 'class="active"';
    }

}
?>