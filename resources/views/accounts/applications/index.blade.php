
@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.applications') )
@section('breadcrumbs', Breadcrumbs::render('applicants'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.applicantInformation') }}</big></div>

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/applicants/search', 'class' => 'form-horizontal', 'applicants' => 'form')) !!}

                        <div class="form-group{{ $errors->has('fkCompaniesID') ? ' has-error' : '' }}">
                            {!! Form::label('fkCompaniesID', trans('text_lang.company'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">
                                {!! Form::select('fkCompaniesID', (['' => trans('text_lang.selectOption')] + $companies ), $companyID, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('jobApplyBy') ? ' has-error' : '' }}">
                            {!! Form::label('jobApplyBy', trans('text_lang.applyBy'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-9">
                                {!! Form::select('jobApplyBy', array( '' => trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.phone') ), $applyByValue, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fromDate', trans('text_lang.fromDate'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-3">
                                {!! Form::text('fromDate', $fromDate, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-3 text-center">
                                {{ trans('text_lang.toDate') }}
                            </div>

                            <div class="col-md-3">
                                {!! Form::text('toDate', $toDate, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-9 text-right">
                                    <button type="submit" class="btn btn-primary btn-block" name="btnSearch">
                                        {{ trans('text_lang.search')}}
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 text-left padding-numberOf">
                    @if( $applicants->total() > 0 )
                        {{ trans('text_lang.numberOfApplicants') }}: <b>{{ $applicants->total() }}</b>
                    @endif
                </div>
                <div class="col-md-6 text-right">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/applicants/export', 'class' => 'form-horizontal')) !!}
                    <input type="hidden" name="fkCompaniesID" value="{{ $companyID }}"/>
                    <input type="hidden" name="jobApplyBy" value="{{ $applyByValue }}"/>
                    <input type="hidden" name="fromDate" value="{{ $fromDate }}"/>
                    <input type="hidden" name="toDate" value="{{ $toDate }}"/>
                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-download"></span> {{ trans('text_lang.export') }}
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-condensed" >
                            <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.position')}}</th>
                                <th>{{ trans('text_lang.name')}}</th>
                                <th>{{ trans('text_lang.phone')}}</th>
                                <th>{{ trans('text_lang.company')}}</th>
                                <th>{{ trans('text_lang.date')}}</th>
                            </tr>
                            </thead>
                            <tbody class="text-left">
                            @foreach($applicants as $applicant)
                                <tr>
                                    <td>{{ object_get($applicant, "positionsName{$lang}") }}</td>
                                    <td>{{ fnGetGender($applicant->jobApplyGender, $applicant->jobApplyName) }}</td>
                                    <td>{{ object_get( $applicant, "jobApplyPhone" ) }}</td>
                                    <td>{{ object_get( $applicant, "companiesName{$lang}" ) }}</td>
                                    <td>{{ dateConvertNormal($applicant->created_at) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $applicants->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>

    @include('includes._modal_dialog')
@endsection

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
@endsection

@section('extraJS')
    <script>
        $( document ).ready(function() {
            $( "#fromDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
            $( "#toDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
@endsection