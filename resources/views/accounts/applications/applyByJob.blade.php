@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.jobApply') )
@section('breadcrumbs', Breadcrumbs::render('jobApplied'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.workerAppliedInformation') }}</big></div>

        <div class="panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                        <dl class="dl-horizontal">
                            <ul class="list-inline">
                                <dl class="dl-horizontal">
                                    <dt> {{ trans('text_lang.position') }}: </dt>
                                    <dd>
                                        <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $jobDetail->pkAnnouncementsID ) }}">
                                            {{ object_get($jobDetail, "positionsName{$lang}" ) }}
                                        </a>
                                    </dd>

                                    <dt> {{ trans('text_lang.sector') }}: </dt>
                                    <dd>
                                        {{ object_get($jobDetail, "sectorsName{$lang}" ) }}
                                    </dd>

                                    <dt> {{ trans('text_lang.subsector') }}: </dt>
                                    <dd>
                                        {{ object_get($jobDetail, "subsectorsName{$lang}" ) }}
                                    </dd>

                                    <dt> {{ trans('text_lang.mainActivity') }}: </dt>
                                    <dd>
                                        {{ object_get($jobDetail, "activitiesName{$lang}" ) }}
                                    </dd>
                                </dl>
                            </ul>
                        </dl>

                    <div class="table-responsive">
                        @include('accounts.applications.tab_application')
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <thead class="text-center">
                            <tr>
                                <th>{{ ucfirst(trans('text_lang.name')) }}</th>
                                <th>{{ ucfirst(trans('text_lang.phone')) }}</th>

                                <th>{{ ucfirst(trans('text_lang.experience')) }}</th>
                                <th>{{ ucfirst(trans('text_lang.interesting')) }}</th>
                                <th>{{ ucfirst(trans('text_lang.calledAndInvitedToInterview')) }}</th>
                                <th>{{ ucfirst(trans('text_lang.rejected')) }}</th>
                                <th>{{ ucfirst(trans('text_lang.hired')) }}</th>
                                <th>{{ ucfirst(trans('text_lang.calledAndRejected'))}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jobApplies as $jobApply)
                                <tr>
                                    <td>
                                        @if( $jobApply->fkUsersID != 0 )
                                            <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/postjob/userInfo/'.$jobApply->pkJobApplyID ) }}">
                                                {{ ($jobApply->jobApplyGender )? config("constants.GENDER")[$jobApply->jobApplyGender][Lang::getLocale()]:'' }}  {{ $jobApply->jobApplyName }}
                                            </a>
                                        @else
                                            {{ ($jobApply->jobApplyGender )? config("constants.GENDER")[$jobApply->jobApplyGender][Lang::getLocale()]:'' }}  {{ $jobApply->jobApplyName }}
                                            <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/download/cv/'.$jobApply->jobApplyCvFile ) }}" download="{{ $jobApply->jobApplyCvFile }}">
                                                ({{ trans ('text_lang.downloadCV') }})
                                            </a>
                                        @endif
                                    </td>
                                    <td>{{ $jobApply->jobApplyPhone }}</td>

                                    <td class="text-center">
                                        @if( $jobApply->jobApplyExperience == 0 ) {!! trans('text_lang.noneExperience') !!}
                                        @elseif( $jobApply->jobApplyExperience == 1 ) {!! trans('text_lang.yes') !!}
                                        @elseif( $jobApply->jobApplyExperience == 2 ) {!! trans('text_lang.no') !!}
                                        @endif
                                    </td>

                                    <td class="text-center">
                                        <input type="checkbox" name="jobApplyInteresting" value="">
                                    </td>

                                    <td class="text-center">
                                        <input type="checkbox" name="jobApplyCallToInterview" value="">
                                    </td>

                                    <td class="text-center">
                                        <input type="checkbox" name="jobApplyRejected" value="">
                                    </td>

                                    <td class="text-center">
                                        <input type="checkbox" name="jobApplyHired" value="">
                                    </td>

                                    <td class="text-center">
                                        <input type="checkbox" name="jobApplyCallAndReject" value="">
                                    </td>

                                    {{--<td class="text-center">--}}
                                        {{--@if(Entrust::hasRole('employer'))--}}
                                            {{--{!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/jobApplied/'.$jobApply->pkJobApplyID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteJobApplied') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}--}}
                                        {{--@endif--}}
                                    {{--</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $jobApplies->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection
