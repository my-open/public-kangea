@if( $jobDetail )
<ul class="nav nav-tabs">
    <li  class="@if(str_contains(url()->current(), 'new')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob/application/new/{{$jobDetail->pkAnnouncementsID}}"> {{ ucfirst(trans('text_lang.new')) }} </a></li>
    <li class="@if(str_contains(url()->current(), 'all_')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob/application/all_/{{$jobDetail->pkAnnouncementsID}}" > {{ ucfirst(trans('text_lang.all')) }} </a></li>
    <li class="@if(str_contains(url()->current(), 'interesting')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob/application/interesting/{{$jobDetail->pkAnnouncementsID}}" > {{ ucfirst(trans('text_lang.interesting')) }} </a></li>
    <li  class="@if(str_contains(url()->current(), 'callToInterview')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob/application/callToInterview/{{$jobDetail->pkAnnouncementsID}}" > {{ ucfirst(trans('text_lang.calledAndInvitedToInterview')) }} </a></li>
    <li  class="@if(str_contains(url()->current(), 'callAndRejected')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob/application/callAndRejected/{{$jobDetail->pkAnnouncementsID}}" > {{ ucfirst(trans('text_lang.calledAndRejected')) }} </a></li>
    <li class="@if(str_contains(url()->current(), 'rejected')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob/application/rejected/{{$jobDetail->pkAnnouncementsID}}"> {{ ucfirst(trans('text_lang.rejected')) }} </a></li>
    <li class="@if(str_contains(url()->current(), 'hired')) active @endif"> <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/postjob/application/hired/{{$jobDetail->pkAnnouncementsID}}" > {{ ucfirst(trans('text_lang.hired')) }} </a></li>

</ul>
@endif