@extends('layouts.account')
@section('title', 'Add Company')
@section('breadcrumbs', Breadcrumbs::render('companyCreate'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.addCompany')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-11">
                    @include('flash::message')
                </div>
            </div>

            {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/company', 'class' => 'form-horizontal', 'company' => 'form', 'files' => true)) !!}

            <div class="form-group{{ $errors->has('companiesNameEN') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('companiesNameEN', trans('text_lang.companiesNameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('companiesNameEN', old('companiesNameEN'), ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('companiesNameEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesNameEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesNameKH') ? ' has-error' : '' }}">
                {!! Form::label('companiesNameKH', trans('text_lang.companiesNameKH'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::text('companiesNameKH', old('companiesNameKH'), ['class' => 'form-control']) !!}
                    @if ($errors->has('companiesNameKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesNameKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesNameZH') ? ' has-error' : '' }}">
                {!! Form::label('companiesNameZH', trans('text_lang.companiesNameZH'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::text('companiesNameZH', old('companiesNameZH'), ['class' => 'form-control']) !!}
                    @if ($errors->has('companiesNameZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesNameZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesNameTH') ? ' has-error' : '' }}">
                {!! Form::label('companiesNameTH', trans('text_lang.companiesNameTH'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::text('companiesNameTH', old('companiesNameTH'), ['class' => 'form-control']) !!}
                    @if ($errors->has('companiesNameTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesNameTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesNickName') ? ' has-error' : '' }}">
                {!! Form::label('companiesNickName', trans('text_lang.companiesNickName'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::text('companiesNickName', old('companiesNickName'), ['class' => 'form-control']) !!}
                    @if ($errors->has('companiesNickName'))
                        <span class="help-block">
                            <strong>{!!  $errors->first('companiesNickName') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesNumberOfWorker') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('companiesNumberOfWorker', trans('text_lang.numberOfWorker') , array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('companiesNumberOfWorker', old('companiesNumberOfWorker'), ['class' => 'form-control', 'maxlength' => '4']) !!}
                    @if ($errors->has('companiesNumberOfWorker'))
                        <span class="help-block">
                            <strong>{!!  $errors->first('companiesNumberOfWorker') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkSectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSectorsID'))
                        <span class="help-block">
                            <strong>{!!  $errors->first('fkSectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkSubsectorsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkSubsectorsID', trans('text_lang.subsector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkSubsectorsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkSubsectorsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkActivitiesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkActivitiesID', trans('text_lang.mainActivity') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('fkActivitiesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkActivitiesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesPhone') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('companiesPhone', trans('text_lang.companiesPhone'), array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::text('companiesPhone', old('companiesPhone'), ['class' => 'form-control', 'maxlength' => '10']) !!}
                    @if ($errors->has('companiesPhone'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesPhone') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesEmail') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('companiesEmail', trans('text_lang.companiesEmail'), array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::email('companiesEmail', old('companiesEmail'), ['class' => 'form-control']) !!}
                    @if ($errors->has('companiesEmail'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesEmail') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesSite') ? ' has-error' : '' }}">
                {!! Form::label('companiesSite', trans('text_lang.companiesSite'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">http://example.com</span>
                        {!! Form::text('companiesSite', old('companiesSite'), ['class' => 'form-control']) !!}
                    </div>
                    @if ($errors->has('companiesSite'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesSite') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkProvincesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkProvincesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkDistrictsID') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('fkDistrictsID', trans('text_lang.district') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'required' => 'required']) !!}
                    @if ($errors->has('fkDistrictsID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkDistrictsID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('fkCommunesID') ? ' has-error' : '' }}">
                {!! Form::label('fkCommunesID', trans('text_lang.commune'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                    @if ($errors->has('fkCommunesID'))
                        <span class="help-block">
                            <strong>{!! $errors->first('fkCommunesID') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            {{--<div class="form-group{{ $errors->has('fkVillagesID') ? ' has-error' : '' }}">--}}
                {{--{!! Form::label('fkVillagesID', trans('text_lang.village'), ['class' => 'col-md-4 control-label']) !!}--}}
                {{--<div class="col-md-7">--}}
                    {{--{!! Form::select('fkVillagesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}--}}
                    {{--@if ($errors->has('fkVillagesID'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{!! $errors->first('fkVillagesID') !!}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="form-group{{ $errors->has('fkZonesID') ? ' has-error' : '' }}">--}}
                {{--{!! Html::decode(Form::label('fkZonesID', trans('text_lang.zone'), array('class' => 'col-md-4 control-label'))) !!}--}}
                {{--<div class="col-md-7">--}}
                    {{--{!! Form::select('fkZonesID', (['' => trans('text_lang.selectOption')] + $zones->toArray()), null, ['class' => 'form-control']) !!}--}}
                    {{--@if ($errors->has('fkZonesID'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{!! $errors->first('fkZonesID') !!}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group{{ $errors->has('companiesAddress') ? ' has-error' : '' }}">
                {!! Form::label('companiesAddress', trans('text_lang.companiesAddress'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::textarea('companiesAddress', old('companiesAddress'), ['class' => 'form-control', 'rows'=>'2']) !!}
                    @if ($errors->has('companiesAddress'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesAddress') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesDescriptionEN') ? ' has-error' : '' }}">
                {!! Form::label('companiesDescriptionEN', trans('text_lang.descriptionEN'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::textarea('companiesDescriptionEN', old('companiesDescriptionEN'), ['class' => 'form-control','rows' => '3']) !!}
                    @if ($errors->has('companiesDescriptionEN'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesDescriptionEN') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesDescriptionKH') ? ' has-error' : '' }}">
                {!! Form::label('companiesDescriptionKH', trans('text_lang.descriptionKH'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::textarea('companiesDescriptionKH', old('companiesDescriptionKH'), ['class' => 'form-control','rows' => '3']) !!}
                    @if ($errors->has('companiesDescriptionKH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesDescriptionKH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesDescriptionZH') ? ' has-error' : '' }}">
                {!! Form::label('companiesDescriptionZH', trans('text_lang.descriptionZH'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::textarea('companiesDescriptionZH', old('companiesDescriptionZH'), ['class' => 'form-control','rows' => '3']) !!}
                    @if ($errors->has('companiesDescriptionZH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesDescriptionZH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesDescriptionTH') ? ' has-error' : '' }}">
                {!! Form::label('companiesDescriptionTH', trans('text_lang.descriptionTH'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::textarea('companiesDescriptionTH', old('companiesDescriptionTH'), ['class' => 'form-control','rows' => '3']) !!}
                    @if ($errors->has('companiesDescriptionTH'))
                        <span class="help-block">
                            <strong>{!! $errors->first('companiesDescriptionTH') !!}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-grou">
                <div class="col-md-7 col-md-offset-4">
                    <em>{{ trans('text_lang.companyDescriptionNote') }}</em>.
                </div>
            </div>

            <div class="form-group{{ $errors->has('companiesLogo') ? ' has-error' : '' }}">
                {!! Html::decode(Form::label('companiesLogo', trans('text_lang.companiesLogo') . ' <span class="requredStar">***</span>', array('class' => 'col-md-4 control-label'))) !!}
                <div class="col-md-7">
                    <input type="file" name="companiesLogo" style="visibility:hidden;height: 0" id="companiesLogo" />
                    <input type="hidden" value="MAX_FILE_SIZE" id="max" />
                    <fieldset class="fieldset_style">
                        <div class="input-group">
                              <span class="input-group-addon" id="basic-addon1">
                                    <a  onclick="issueUpload('companiesLogo','companiesLogo_text')" style="cursor:pointer;"  ><i class="fa fa-upload"></i> <span class="choosePicture"> {{ trans('text_lang.choosePicture') }} </span> <em class="noteSize">{{ trans('text_lang.logoSize') }}</em></a>
                                </span>
                            <input type="text" class="form-control"​  placeholder="No File Selected" aria-describedby="basic-addon1"  id="companiesLogo_text" disabled>
                        </div>
                    </fieldset>
                    @if ($errors->has('companiesLogo'))
                        <span class="help-block">
                                    <strong>{!! $errors->first('companiesLogo') !!}</strong>
                                </span>
                    @endif
                </div>
            </div>

            <div class="form-group form-group-lg">
                <div class="col-md-7 col-md-offset-4">
                    <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                </div>
            </div>

            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-4">
                    <button type="submit" class="btn btn-primary" name="save" id="btnsave">
                        {{ trans('text_lang.save')}}
                    </button>
                    <button type="submit" class="btn btn-primary" name="save_rep">
                        {{ trans('text_lang.saveRepresentative')}}
                    </button>
                    <a class="btn btn-success" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME") . '/company' }}">{{ trans('text_lang.cancel')}}</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection

@section('extraJS')
    <script>
        var sectorID = '{{ old('fkSectorsID') }}';
        var subsectorId = '{{ old('fkSubsectorsID') }}';
        var activityId = '{{ old('fkActivitiesID') }}';
        var districtId = '{{ old('fkDistrictsID') }}';
        var communeId = '{{ old('fkCommunesID') }}';
        var villageId = '{{ old('fkVillagesID') }}';
    </script>
    <script src="{{ URL::asset('js/choosePicture.js') }}"></script>

    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
    <script src="{{ URL::asset('js/village.js') }}"></script>

    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
    <script src="{{ URL::asset('js/createCompany.js') }}"></script>
    <script src="{{ URL::asset('lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::asset('lib/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#companiesDescriptionEN').ckeditor();
        $('#companiesDescriptionKH').ckeditor();
        $('#companiesDescriptionZH').ckeditor();
        $('#companiesDescriptionTH').ckeditor();
    </script>

@endsection
