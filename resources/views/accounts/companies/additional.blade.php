@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 text-center">
            <br/>
            <h2>{{ trans('text_lang.additionalInformation') }}</h2><hr/>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/companyProfile/additional/', 'class' => 'form-horizontal', 'files' => true)) !!}

                <div class="form-group{{ $errors->has('companiesDescriptionKH') ? ' has-error' : '' }}">
                    {!! Html::decode(Form::label('companiesDescriptionKH', trans('text_lang.companyDescriptionKH'). '<br/><br/><em class="text-muted">'. trans('text_lang.companyDescriptionNote') .'</em>', array('class' => 'col-md-5 control-label'))) !!}
                    <div class="col-md-7">
                        {!! Form::textarea('companiesDescriptionKH', old('companiesDescriptionKH'), ['class' => 'form-control','rows' => '3']) !!}
                        @if ($errors->has('companiesDescriptionKH'))
                            <span class="help-block">
                            <strong>{!! $errors->first('companiesDescriptionKH') !!}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('companiesDescriptionEN') ? ' has-error' : '' }}">
                    {!! Form::label('companiesDescriptionEN', trans('text_lang.companyDescriptionEN'), ['class' => 'col-md-5 control-label']) !!}
                    <div class="col-md-7">
                        {!! Form::textarea('companiesDescriptionEN', old('companiesDescriptionEN'), ['class' => 'form-control','rows' => '3']) !!}
                        @if ($errors->has('companiesDescriptionEN'))
                            <span class="help-block">
                            <strong>{!! $errors->first('companiesDescriptionEN') !!}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 text-right">
                        <label class="control-label">{{ trans('text_lang.doYourWorkersCallYourCompanyByAdifferentName') }} </label>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('companiesNickName') ? ' has-error' : '' }}">
                    {!! Form::label('companiesNickName', trans('text_lang.companiesNickName'), ['class' => 'col-md-5 control-label']) !!}
                    <div class="col-md-7">
                        {!! Form::text('companiesNickName', old('companiesNickName'), ['class' => 'form-control']) !!}
                        @if ($errors->has('companiesNickName'))
                            <span class="help-block">
                            <strong>{!! $errors->first('companiesNickName') !!}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('companiesNumberOfWorker') ? ' has-error' : '' }}">
                    {!! Form::label('companiesNumberOfWorker', trans('text_lang.numberOfWorker'), ['class' => 'col-md-5 control-label']) !!}
                    <div class="col-md-7">
                        {!! Form::text('companiesNumberOfWorker', old('companiesNumberOfWorker'), ['class' => 'form-control positive-integer', 'maxlength' => '4']) !!}
                        @if ($errors->has('companiesNumberOfWorker'))
                            <span class="help-block">
                                <strong>{!! $errors->first('companiesNumberOfWorker') !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('companiesSite') ? ' has-error' : '' }}">
                    {!! Form::label('companiesSite', trans('text_lang.companiesSite'), ['class' => 'col-md-5 control-label']) !!}
                    <div class="col-md-7">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon3">http://example.com</span>
                            {!! Form::text('companiesSite', old('companiesSite'), ['class' => 'form-control']) !!}
                        </div>
                        @if ($errors->has('companiesSite'))
                            <span class="help-block">
                            <strong>{!! $errors->first('companiesSite') !!}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('companiesLogo') ? ' has-error' : '' }}">
                    {!! Html::decode(Form::label('companiesLogo', trans('text_lang.companiesLogo'), array('class' => 'col-md-5 control-label'))) !!}
                    <div class="col-md-3">
                        <input type="file" name="companiesLogo" style="visibility:hidden;height: 0" id="companiesLogo" />
                        <input type="hidden" value="MAX_FILE_SIZE" id="max" />
                        <fieldset id="getImage" class="fieldset_style" style="width: 180px; height: 180px;background-color: #eeeeee">
                            <span style="margin-top: 60px; padding: 5px; background-color: #7eb289; display: block; text-align: center; opacity: 0.7; color: white">
                                <a  onclick="issueUpload('companiesLogo','companiesLogo_text')" style="cursor:pointer;"  ><i class="fa fa-upload"></i> <span class="choosePicture"> {{ trans('text_lang.choosePicture') }} </span> <br/> <em class="noteSize">{{ trans('text_lang.logoSize') }}</em></a>
                            </span>
                            <input type="text" class="form-control"​  placeholder="No File Selected" aria-describedby="basic-addon1" style="display: none" id="companiesLogo_text" disabled>
                        </fieldset>
                    </div>
                    <div class="col-md-4">
                        @if ($errors->has('companiesLogo'))
                            <span class="help-block">
                                <strong>{!! $errors->first('companiesLogo')  !!}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-7 col-md-offset-5">
                        <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                    </div>
                </div>

                <div class="form-group">
                    <div class="text-right col-md-7 col-md-offset-5 text-right">
                        <button type="submit" class="btn btn-primary btn-block">
                            {{ trans('text_lang.finish')}}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('extraJS')
    {!!Html::script('js/logoAjax.js')!!}

    <script src="{{ URL::asset('lib/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::asset('lib/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#companiesDescriptionEN').ckeditor();
        $('#companiesDescriptionKH').ckeditor();
    </script>

    <!-- Check numeric -->
    <script type="text/javascript" src="{{ URL::asset('js/checkCondition.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('lib/numeric/jquery/jquery.numeric.js') }}"></script>
@endsection