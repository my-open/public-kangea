@extends('layouts.account')
@section('title', 'Bongpheak ' . trans('text_lang.companyProfile') )
@section('breadcrumbs', Breadcrumbs::render('companyDetail'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading text-center"><big>{{ trans('text_lang.companyProfile') }}</big></div>
        <div class="panel-body">
            <div align="center">
                <img class="companyLogo" src="{{ URL::asset("images/companyLogos/thumbnails/{$company->companiesLogo}") }}" alt="Company logo">
            </div>
            <br>

            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.companiesName') }}: </dt>
                <dd>
                    {{ object_get($company, "companiesName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.sector') }}: </dt>
                <dd>
                    {{ object_get($company, "sectorsName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.subsector') }}: </dt>
                <dd>
                    {{ object_get($company, "subsectorsName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.mainActivity') }}: </dt>
                <dd>
                    {{ object_get($company, "activitiesName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.numberOfWorker') }}: </dt>
                <dd> {{ $company->companiesNumberOfWorker }} </dd>

                <dt> {{ trans('text_lang.locations') }}: </dt>
                <dd>
                    {{ fnGetLocation( object_get($company, "provincesName{$lang}"), object_get($company, "districtsName{$lang}"), object_get($company, "CommunesName{$lang}") ) }}
                </dd>

                <dt> {{ trans('text_lang.companiesPhone') }}: </dt>
                <dd> {{ $company->companiesPhone }} </dd>

                <dt> {{ trans('text_lang.companiesEmail') }}: </dt>
                <dd> {{ $company->companiesEmail }} </dd>

                <dt> {{ trans('text_lang.companiesSite') }}: </dt>
                <dd>
                    <a target="_blank" href="{{ $company->companiesSite }}"> {{ $company->companiesSite }}</a>
                </dd>
            @if( !empty($company->companiesAddress)  )
                <dt> {{ trans('text_lang.companiesAddress') }}: </dt>
                <dd> {{ $company->companiesAddress }} </dd>
            @endif

            @if( !empty($company->companiesDescriptionEN)  )
                <dt> {{ trans('text_lang.companyDescription') }}: </dt>
                    {!! object_get($company, "companiesDescription{$lang}" ) !!}
                    {{--<dd> @if( Lang::getLocale() == 'en') {!! $company->companiesDescriptionEN  !!} @else {!! $company->companiesDescriptionKH  !!} @endif </dd>--}}
            @endif
            </dl>

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading text-center"><big>{{ trans('text_lang.jobsList') }}</big></div>
        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="text-right col-md-12">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/company/postjob/'.$company->pkCompaniesID,'<span class="glyphicon glyphicon-plus"></span> '. trans('text_lang.addPostjob'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addPostjob') ))) !!}
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-condensed" >
                <thead class="text-center">
                <tr>
                    <th>{{ trans('text_lang.position')}}</th>
                    <th>{{ trans('text_lang.location')}}</th>
                    <th>{{ trans('text_lang.announcementsClosingDate')}}</th>
                    <th width="190">{{ trans('text_lang.action')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr @if( $job->announcementsStatus == 0 )
                            class="danger"
                        @elseif($job->announcementsClosingDate < date('Y-m-d 00:00:00') )
                            class="text-danger"
                        @endif
                    >
                        <td>
                            <?php
                            $province_name = fnConvertSlug($job->provincesName_EN);
                            $sector_name = fnConvertSlug($job->sectorsName_EN);
                            $position_name = fnConvertSlug($job->positionsName_EN);
                            ?>
                                <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID ) }}">
                                @if($job->announcementsClosingDate < date('Y-m-d 00:00:00') && $job->announcementsStatus != 0 )
                                    <span class="text-danger">{{ object_get($job, "positionsName{$lang}" ) }}</span>
                                @else
                                    {{ object_get($job, "positionsName{$lang}" ) }}
                                @endif
                            </a>
                        </td>
                        <td>
                            {{ fnGetLocation( object_get($job, "provincesName{$lang}"), object_get($job, "districtsName{$lang}"), object_get($job, "CommunesName{$lang}") ) }}
                        </td>
                        <td>
                            {{ dateConvertNormal($job->announcementsClosingDate) }}
                        </td>
                        <td class="text-right">
                            @if( $job->announcementsClosingDate >= date('Y-m-d 00:00:00') )
                                @if( $job->announcementsStatus == 0 )
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/jobPublish/'.$job->pkAnnouncementsID, trans('text_lang.publish'), $attributes = array('class' => 'btn btn-sm btn-danger postjob-index','title' => trans('text_lang.publish') ))) !!}
                                @elseif( $job->announcementsStatus == 1 )
                                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/jobUnpublish/'.$job->pkAnnouncementsID, trans('text_lang.unpublish'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.unpublish') ))) !!}
                                @endif
                                {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/job/edit/'.$job->pkAnnouncementsID,'<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.editPostjob') ))) !!}
                                {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/job/delete/'.$job->pkAnnouncementsID,  '<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btnDeletePostjob','title' => trans('text_lang.deletePostjob') , 'data-title'=> trans('text_lang.title_deletePostjob'), 'data-content' => trans('text_lang.content_deletePostjob'), 'onClick'=>'return false;'))) !!}
                            @else
                                {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/repost/'.$job->pkAnnouncementsID, trans('text_lang.repost'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.repost') ))) !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            <div class="row text-right noPadding">
                <div class="col-md-12">
                    {!! $jobs->links() !!}
                </div>
            </div>
        </div>
    </div>
    @include('includes._modal_dialog_delete_job_view_in_company')
@endsection
