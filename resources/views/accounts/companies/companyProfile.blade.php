@extends('layouts.account')
@section('title', $company->companiesNameEN)

@section('breadcrumbs', Breadcrumbs::render('companyProfile'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans('text_lang.companyInformation')}}</big></div>

        <div class="panel-body">
            <div class="row">
                <div class="text-center col-md-12">
                    @include('flash::message')
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="cover-fieldset-table">

                            <div align="center">

                              <div class="companyName">
                                  <span class="hidden" style="font-size: 72px; color: #566ab0">{{ object_get($company, "companiesName{$lang}" ) }}</span>
                                  <img class="companyLogo" src="{{ URL::asset("images/companyLogos/thumbnails/{$company->companiesLogo}") }}" alt="Company logo" onerror="CompanySwitchThumbnail($(this))">
                              </div>

                            </div>
                            <br>
                            <dl class="dl-horizontal">
                                <dt> {{ trans('text_lang.companiesName') }}: </dt>
                                <dd>
                                    {{ object_get($company, "companiesName{$lang}" ) }}
                                </dd>

                                <dt> {{ trans('text_lang.companiesNickName') }}: </dt>
                                <dd>{{ $company->companiesNickName }}</dd>

                                @if( !empty( $company->companiesNumberOfWorker) )
                                    <dt> {{ trans('text_lang.numberOfWorker') }}: </dt>
                                    <dd> {{ $company->companiesNumberOfWorker }} </dd>
                                @endif

                                <dt> {{ trans('text_lang.sector') }}: </dt>
                                <dd> {{ $sectorName }} </dd>

                                <dt> {{ trans('text_lang.subsector') }}: </dt>
                                <dd> {{ $subsectorName }} </dd>

                                <dt> {{ trans('text_lang.mainActivity') }}: </dt>
                                <dd> {{ $activityName }} </dd>

                                <dt> {{ trans('text_lang.companiesPhone') }}: </dt>
                                <dd> {{ $company->companiesPhone }} </dd>

                                <dt> {{ trans('text_lang.companiesEmail') }}: </dt>
                                <dd><a href="#">{{ $company->companiesEmail }} </a></dd>

                                @if( !empty($company->companiesSite) )
                                    <dt> {{ trans('text_lang.companiesSite') }}: </dt>
                                    <dd>
                                        <a target="_blank" href="{{ $company->companiesSite }}"> {{ $company->companiesSite }}</a>
                                    </dd>
                                @endif

                                <dt> {{ trans('text_lang.province') }}: </dt>
                                <dd> {{ $provincesName }} </dd>

                                <dt> {{ trans('text_lang.district') }}: </dt>
                                <dd> {{ $districtName }} </dd>

                                @if( !empty($company->companiesDescriptionEN) )
                                    <dt> {{ trans('text_lang.companyDescription') }}: </dt>
                                    <dd>
                                        {!! object_get($company, "companiesDescription{$lang}" ) !!}
                                    </dd>
                                @endif

                            </dl>

                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="text-right col-md-7 col-md-offset-3">
                    {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/companyProfile/edit',trans('text_lang.editCompany'), $attributes = array('class' => 'btn btn-sm btn-info' ))) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        {{--#panel-body--}}
    </div>
@endsection
