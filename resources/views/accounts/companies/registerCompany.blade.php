@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.CompanyRegistration') )
@section('content')
    <div class="row">
        <div class="col-md-12 text-center">
            <br/>
            <h2>{{ trans('text_lang.CompanyRegistration') }}</h2><hr/>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/employer/registerCompany', 'class' => 'form-horizontal', 'files' => true)) !!}

                <input type="hidden" name="companiesID" id="companiesID"/>

                <div class="form-group{{ $errors->has('companiesNameEN') ? ' has-error' : '' }}">
                    {!! Html::decode(Form::label('companiesNameEN', trans('text_lang.companiesNameEN') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                    <div class="col-md-7">
                        {!! Form::text('companiesNameEN', old('companiesNameEN'), ['class' => 'form-control']) !!}
                        @if ($errors->has('companiesNameEN'))
                            <span class="help-block">
                                    <strong>{!! $errors->first('companiesNameEN')  !!}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    {!! Html::decode(Form::label('companiesNameKH', trans('text_lang.companiesNameKH'), array('class' => 'col-md-5 control-label'))) !!}
                    <div class="col-md-7">
                        {!! Form::text('companiesNameKH', old('companiesNameKH'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div id="sectorAndSub"></div>
                <div class="htmlDefault">
                    {{--<div class="form-group{{ $errors->has('fkSectorsID') ? ' has-error' : '' }}">--}}
                        {{--{!! Html::decode(Form::label('fkSectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}--}}
                        {{--<div class="col-md-7">--}}
                            {{--{!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control']) !!}--}}
                            {{--@if ($errors->has('fkSectorsID'))--}}
                                {{--<span class="help-block">--}}
                                    {{--<strong>{!! $errors->first('fkSectorsID')  !!}</strong>--}}
                                {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group{{ $errors->has('fkSubsectorsID') ? ' has-error' : '' }}{{ $errors->has('subsectorOther') ? ' has-error' : '' }}">
                        {!! Html::decode(Form::label('fkSubsectorsID', trans('text_lang.sector') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), null, ['class' => 'form-control']) !!}
                            {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control sectorOther txtOther']) !!}
                            {!! Form::text('subsectorOther', old('subsectorOther'), ['class' => 'form-control subsectorOther txtOther']) !!}
                            @if ($errors->has('fkSubsectorsID'))
                                <span class="help-block">
                                    <strong>{!! $errors->first('fkSubsectorsID') !!}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    {{--<div class="form-group{{ $errors->has('fkActivitiesID') ? ' has-error' : '' }} {{ $errors->has('activitiesOther') ? ' has-error' : '' }}">--}}
                        {{--{!! Html::decode(Form::label('fkActivitiesID', trans('text_lang.mainActivity') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}--}}
                        {{--<div class="col-md-7">--}}
                            {{--{!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}--}}
                            {{--{!! Form::text('activitiesOther', old('activitiesOther'), ['class' => 'form-control activitiesOther txtOther']) !!}--}}
                            {{--@if ($errors->has('fkActivitiesID'))--}}
                                {{--<span class="help-block">--}}
                                {{--<strong>{!! $errors->first('fkActivitiesID')  !!}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2 text-right">
                            <h3 class="marginTop10px">{{ trans('text_lang.locationOfTheMainOfficeOfYourCompany') }}</h3>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('fkProvincesID') ? ' has-error' : '' }}">
                        {!! Html::decode(Form::label('fkProvincesID', trans('text_lang.province') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control']) !!}
                            @if ($errors->has('fkProvincesID'))
                                <span class="help-block">
                                <strong>{!! $errors->first('fkProvincesID')  !!}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('fkDistrictsID') ? ' has-error' : '' }}">
                        {!! Html::decode(Form::label('fkDistrictsID', trans('text_lang.district') . ' <span class="requredStar">***</span>', array('class' => 'col-md-5 control-label'))) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                            @if ($errors->has('fkDistrictsID'))
                                <span class="help-block">
                                <strong>{!! $errors->first('fkDistrictsID')  !!}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('fkCommunesID') ? ' has-error' : '' }}">
                        {!! Form::label('fkCommunesID', trans('text_lang.commune'), ['class' => 'col-md-5 control-label']) !!}
                        <div class="col-md-7">
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                            @if ($errors->has('fkCommunesID'))
                            <span class="help-block">
                                <strong>{!! $errors->first('fkCommunesID')  !!}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-7 col-md-offset-5">
                        <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                    </div>
                </div>

                <div class="form-group">
                    <div class="text-right col-md-7 col-md-offset-5 text-right">
                        <button type="submit" class="btn btn-primary btn-block">
                            {{ trans('text_lang.companyRegister')}}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('extraCSS')
{{--    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">--}}
@endsection

@section('extraJS')
    <script>
        var OPT_OTHER = '<?php echo trans('text_lang.optOther'); ?>';
        var FKSUBSECTOR_SELECTED = '{!! old('fkSubsectorsID')  !!}';
        var FKACTIVITY_SELECTED = '{!! old('fkActivitiesID')  !!}';
    </script>
{{--    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>--}}
    {!!Html::script('js/companyNameAutoComplete.js')!!}
    {{--{!!Html::script('js/employerRegisterStep.js')!!}--}}
    {!!Html::script('js/subsectorAndAcitiviyOther.js')!!}
@endsection