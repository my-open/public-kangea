@extends('layouts.account')
@section('title', 'Compnay')
@section('breadcrumbs', Breadcrumbs::render('company'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><big>{{ trans ('text_lang.companyInformation') }}</big></div>

        <div class="text-center panel-body" >
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="jumbotron">
                        {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") .'/company/search', 'class' => 'form-horizontal', 'company' => 'form')) !!}

                        <div class="form-group">
                            {!! Form::label('companiesName', trans('text_lang.companiesName'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text("companiesName{$lang}", isset($companiesName)?$companiesName:null, ['class' => 'col-md-2 form-control']) !!}
                            </div>

                            {!! Form::label('fromDate', trans('text_lang.fromDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text('fromDate', isset($fromDate)?$fromDate:'', ['class' => 'col-md-2 form-control', 'id' => 'fromDate']) !!}
                            </div>

                            {!! Form::label('toDate', trans('text_lang.toDate'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-2">
                                {!! Form::text('toDate', isset($toDate)?$toDate:'', ['class' => 'col-md-2 form-control', 'id' => 'toDate']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3 col-md-offset-9 text-right">
                                    <button type="submit" class="btn btn-primary btn-block" name="btnSearch" value="btnSearch">
                                        {{ trans('text_lang.search')}}
                                    </button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            {{--<div class="row">--}}
                {{--<div class="text-right col-md-12">--}}
                    {{--{!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/company/create','<span class="glyphicon glyphicon-plus"></span> '.trans('text_lang.addCompany'), $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.addCompany') ))) !!}--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="row">
                <div class="col-md-12 text-left padding-numberOf">
                    @if( $companies->total() > 0 )
                        {{ trans('text_lang.numberofCompany') }}: <b>{{ $companies->total() }}</b>
                    @endif
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                            <tr>
                                <th>{{ trans('text_lang.companiesName')}}</th>
                                <th>{{ trans('text_lang.numberOfWorker')}}</th>
                                <th>{{ trans('text_lang.phone')}}</th>
                                <th>{{ trans('text_lang.email')}}</th>
                                <th>{{ trans ('text_lang.create_date') }}</th>

                                <th width="140">{{ trans ('text_lang.action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $row)
                                <tr>
                                    <td class="text-left">
                                        {{ object_get($row, "companiesName{$lang}" ) }}
                                    </td>
                                    <td>{{ $row->companiesNumberOfWorker  }}</td>
                                    <td>{{ $row->companiesPhone  }}</td>
                                    <td>{{ $row->companiesEmail  }}</td>
                                    <td>{{ dateConvertNormal($row->created_at)  }}</td>
                                    <td class="text-center">
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/view/'.$row->pkCompaniesID,'<span class="glyphicon glyphicon-th"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.viewCompany') ))) !!}
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/'.$row->pkCompaniesID.'/edit','<span class="glyphicon glyphicon-pencil"></span>', $attributes = array('class' => 'btn btn-sm btn-info','title' => trans('text_lang.editCompany') ))) !!}
                                        {!! Html::decode(link_to( '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME").'/company/'.$row->pkCompaniesID,'<span class="glyphicon glyphicon-trash"></span>', $attributes = array('class' => 'btn btn-sm btn-danger btndelete','title' => trans('text_lang.deleteCompany') , 'data-title'=> trans('text_lang.title_delete'), 'data-content' => trans('text_lang.content_delete'), 'onClick'=>'return false;'))) !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {!! $companies->appends(Request::input())->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--#text-center panel-body--}}
    </div>
    @include('includes._modal_dialog')
@endsection

@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
@endsection

@section('extraJS')
    <script>
        $( document ).ready(function() {
            $( "#fromDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
            $( "#toDate" ).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    </script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
@endsection