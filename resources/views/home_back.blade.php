@extends('layouts.app')
@section('content')
<div class="row" id="searchHome">
    <div class="col-md-12 text-center">
        <br/><h1> {{ trans('text_lang.startYourSearchNow') }} </h1><br/>
    </div>
    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/searchJob', 'class' => 'form-horizontal')) !!}
        <div class="col-md-2 text-center">
            <span>{!! trans('text_lang.sector') !!}</span>
            {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
        </div>

        <div class="col-md-2 text-center">
            <span>{!! trans('text_lang.subsector') !!}</span>
            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
        </div>

        <div class="col-md-2 text-center">
            <span>{!! trans('text_lang.activities') !!}</span>
            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
        </div>

        <div class="col-md-2 text-center">
            <span>{!! trans('text_lang.province') !!}</span>
            {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
        </div>

        <div class="col-md-2 text-center">
            <span>{!! trans('text_lang.zone') !!}</span>
            {!! Form::select('fkZonesID', (['' => trans('text_lang.selectOption')] + $zones->toArray()), null, ['class' => 'form-control', 'id' => 'fkZonesID']) !!}
        </div>

        <div class="col-md-2">
            <br/>
            <button type="submit" class="btn btn-primary">
                {{ trans('text_lang.search')}}
            </button>
        </div>
    {!! Form::close() !!}
</div>
<div class="row paddingTop bg2">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><big>{{ trans('text_lang.browseJobs')}}</big></div>
            <div class="panel-body">
                <div class="row">
                    @foreach( $positions as $row )
                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobsByPosition/'. $row->pkPositionsID ) }}">
                                @if( Lang::getLocale() == 'en')
                                    {{ $row->positionsNameEN }}
                                @else
                                    {{ $row->positionsNameKH }}
                                @endif
                                    <?php
                                    $totalJobs = DB::table('Announcements')
                                            ->select(DB::raw('count(*) as totalJobs'))
                                            ->where('fkPositionsCode', '=', $row->positionsCode)
                                            ->where('Announcements.announcementsPublishDate', '<=', date('Y-m-d 00:00:00'))
                                            ->where('Announcements.announcementsClosingDate', '>=', date('Y-m-d 00:00:00'))
                                            ->where('Announcements.announcementsStatus', '=', 1)
                                            ->groupBy('fkPositionsID')
                                            ->count();
                                    echo "<em>(".$totalJobs.")</em>";
                                    ?>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            {{--#panel-body--}}
        </div>
    </div>
</div>
<div class="row bg2">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><big>{{ trans('text_lang.companies')}}</big></div>
            <div class="panel-body">
                <div class="row">
                    @foreach( $employers as $row )
                        <div class="col-xs-6 col-sm-2 col-md-2">
                            <div class="thumbnail">
                                <a class="text-center" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'. $row->pkCompaniesID) }}">
                                    <img height="100" width="200" src="/images/companyLogos/thumbnails/{{ $row->companiesLogo }}" class="center-block img-responsive" alt="{{ $row->companiesNameEN }}">
                                </a>
                                <div class="caption companyTitle">
                                    <h4>
                                        @if( Lang::getLocale() == 'en')
                                            {{ $row->companiesNameEN }}
                                        @else
                                            {{ $row->companiesNameKH }}
                                        @endif
                                    </h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            {{--#panel-body--}}
        </div>
    </div>
</div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
@endsection
