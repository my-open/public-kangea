@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.aboutUs') )
@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('css/registrationScreen.css') }}">
@endsection
@section('content-fluid')
    <div class="row">
        <div class="col-md-12 spaceTopLogo" style="
        background: url('/images/fondo.jpg') no-repeat;
        background-size: cover;
        min-height: 400px;
        margint-top: -10px;
        padding-top: 0;
        margin-bottom: 20px;
      ">
        </div>
    </div>
@endsection
@section('content')
    <div class="row mainPic">
        <div class="col-md-12">
            <div class="row hidden">
                <div class="col-md-12 spaceTopLogo">
                    <img class="img-responsive fondoPic" src="/images/fondo.jpg" alt="fondo">
                </div>
            </div>
            <div class="row hidden">
                <div class="col-md-12 text-center">
                    <a href="{{ url(\Lang::getLocale() . '/') }}"> <img src="/images/logo.png" alt="bongpheak logo"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 box1">
                    @if( Lang::getLocale() == 'kh')
                        <div class="row">
                            <div class="col-md-12 boxKoulen">
                                បងភ័ក្រ​ ជួយ​ស្វែង​រក​ជំនាញ​ដែល​ក្រុម​ហ៊ុន​ត្រូវ​ការ​ !
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12 boxEN">
                                We find the skills you need!
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row aboutus">
        <div class="col-md-12">

            <h2>{{ trans('text_lang.contactUs') }}</h2>
            <br><br>
                <p class="text-center">
                    <b>{{ trans('text_lang.address') }}: </b>
                    @if( Lang::getLocale() == 'kh' )
                        ផ្ទះលេខ៨, ផ្លូវ៣៥២, សង្កាត់ បឹងកេងកង១, ខ័ណ្ឌ ចំការមន, ក្រុង ភ្នំពេញ
                    @else
                        #8, Street 352, Sangkat Boeng Keng Kang 1, Khan Chamkarmorn, Phnom Penh.
                    @endif
                </p>
                <p class="text-center"><b>{{ trans('text_lang.emailAddress') }}:</b> info@bongpheak.com </p>
                <p class="text-center"><b>{{ trans('text_lang.phone') }}:</b> 096 696 11 29 | 097 66 46 215 </p>

            <br/>
        </div>
    </div>

@endsection
