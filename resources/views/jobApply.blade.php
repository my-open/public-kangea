@extends('layouts.app')
@section('title', 'Apply job now')
@section('breadcrumbs', Breadcrumbs::render('jobDetail'))
@section('content')
        <div class="panel panel-default">
            <div class="panel-heading text-center"><big>{{ trans('text_lang.applyNow') }}</big></div>
            <div class="panel-body">
                {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale(). '/jobApply/'.$job->pkAnnouncementsID, 'class' => 'form-horizontal', 'files' => true )) !!}
                @if (Session::has('flash_notification.message'))
                    <div class="row">
                        <div class="text-center col-md-12">
                            @include('flash::message')
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12"><u><h3>{{ trans('text_lang.applyForThisJob') }}</h3></u></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">{{ trans('text_lang.position') }}:</div>
                            <div class="col-md-9">
                                <?php
                                $province_name = fnConvertSlug($job->provincesName_EN);
                                $sector_name = fnConvertSlug($job->sectorsName_EN);
                                $position_name = fnConvertSlug($job->positionsName_EN);
                                ?>
                                <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID ) }}">
                                    {{ object_get($job, "positionsName{$lang}" ) }}
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">{{ trans('text_lang.companyName') }}:</div>
                            <div class="col-md-9">
                                <?php $company_name = fnConvertSlug($job->companiesName_EN); ?>
                                <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $job->pkCompaniesID) }}">
                                    {{ object_get($job, "companiesName{$lang}" ) }}
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6" style="margin-top:20px;">
                        <div class="row">
                            <?php
                            $jobMp3 = public_path('files/sounds/kh/confirmJobs/confirmJob_' . $job->pkAnnouncementsID . '.mp3');
                            $jobNoCompanyMp3 = public_path('files/sounds/kh/confirmJobsNoCompany/confirmJob_' . $job->pkAnnouncementsID . '.mp3');
                            $isJobMp3 = isExistMp3($jobMp3);
                            $isJobNoCompanyMp3 = isExistMp3($jobNoCompanyMp3);
                            if( $isJobMp3 || $isJobNoCompanyMp3){
                            ?>
                                <div id="idBtnApplyForMyself" value-pkAnnouncementsID="{{ $job->pkAnnouncementsID }}" >
                                    <div class="col-md-12" id="jobDetail">
                                        <a class="btn btn-success btn-apply lg" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/jobApplyMy/'.$job->pkAnnouncementsID }}">
                                            <i class="fa fa-check"></i>
                                            {{ trans('text_lang.apply_for_myself')}}
                                        </a>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>

                            <div class="col-md-12" style="margin-top: 5px;">
                                <?php
                                $linkFileMp3 = 'files/sounds/kh/jobs/job_'.$job->pkAnnouncementsID.'.mp3';
                                $isHasMp3 = isExistMp3($linkFileMp3);
                                if( $isHasMp3 ){
                                ?>
                                    <div id="idBtnApplyForSO" value-pkAnnouncementsID="{{ $job->pkAnnouncementsID }}" >
                                        <div class="social-share" id="jobDetail">
                                            <a class="btn btn-primary btn-apply lg" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/jobApplySO/'.$job->pkAnnouncementsID }}">
                                                <i class="fa fa-phone"></i>
                                                {{ trans('text_lang.apply_for_somebody_else') }}
                                            </a>
                                        </div>
                                    </div>

                                <?php }else{ ?>
                                    <div class="social-share" id="jobDetail">
                                        <button class="btn btn-primary btn-apply lg" disabled>{{ trans('text_lang.apply_for_somebody_else') }}</button>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/jobLog.js') }}"></script>
@endsection