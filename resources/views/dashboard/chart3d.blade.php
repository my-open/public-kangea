@extends('layouts.dashboard')
@section('title', 'Dashboard')
@section('breadcrumbs', Breadcrumbs::render($breadcrumbs))
@section('extraJS')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" defer="true">
        google.load("visualization", "1", {packages:["columnchart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Year', 'Sales', 'Expenses',"abc"],
                ['2004',  1000,      400 , 400],
                ['2005',  1170,      460 ,0],
                ['2006',  660,       1120,600],
                ['2007',  1030,      540,380]
            ]);

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, {width: 400, height: 240, is3D: true, title: 'Company Performance'});
        }
    </script>
@endsection
@section('extraCSS')
    <link rel="stylesheet" href="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.css') }}">
    <style media="screen">
        .error_select{
            color:red;
        }
        .navbar-fixed-top{
            z-index: 999999;
        }
        .panel-heading{
            z-index: 1;
            position: relative;
        }
        .row-form{
            position: relative;
            z-index: 9999;
            margin-left: 1%;
        }
        #stocks-div{
            z-index: -1;
            margin-top: -80px;
        }
        .checkbox-chart{
            margin-top: 70px;
        }
    </style>
@endsection
@section('content')
    @if(Entrust::hasRole('admin'))
        <div class="panel panel-default">
            <div class="panel-heading"><big>{{ $chartName }}</big></div>
            <div class="panel-body">
                @if (Session::has('flash_notification.message'))
                    <div class="row">
                        <div class="text-center col-md-12">
                            @include('flash::message')
                        </div>
                    </div>
                @endif
                <div class="row">
                    {!! Form::open(['url' => $action , 'method' => 'POST' ,'id'=>'fliter_form', 'class'=>'form-inline']) !!}
                    <div class="col col-12">
                        <div class="row  row-form">
                            <div class="form-group">
                                <label for="start_date">{!! trans('text_lang.from') !!}:</label>
                                <input type="text" placeholder="yyyy/mm/dd" name="from" class="form-control" value="<?php echo isset($_POST['from'])?$_POST['from']:'' ?>" id="start_date">
                            </div>
                            <div class="form-group">
                                <label for="end_date">{!! trans('text_lang.to') !!}:</label>
                                <input type="text" placeholder="yyyy/mm/dd" name='to' class="form-control" value="<?php echo isset($_POST['to'])?$_POST['to']:'' ?>"  id="end_date">
                            </div>
                            <div class="form-group">
                                <label for="location">{!! trans('text_lang.province') !!}:</label>
                                <?php if(empty($_POST['fkProvincesID'])){ ; ?>
                                {!! Form::select('fkProvincesID', (['' => str_replace('option', trans('text_lang.province'), trans('text_lang.selectOptionN')) ] + $provinces),null, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                                <?php }else{?>
                                {!! Form::select('fkProvincesID', (['' => str_replace('option', trans('text_lang.province'), trans('text_lang.selectOptionN')) ] + $provinces),$_POST['fkProvincesID'], ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                                <?php }?>
                            </div>
                            <div class="form-group">
                                <label for="location">{!! trans('text_lang.district') !!}: </label>
                                @if(Session::has('districts') && empty($_POST['fkDistrictsID']))
                                    {!! Form::select('fkDistrictsID', (['' => str_replace('option', trans('text_lang.district'), trans('text_lang.selectOptionN')) ] + Session::get('districts')),null, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                                @elseif (Session::has('districts') && !empty($_POST['fkDistrictsID']))
                                    {!! Form::select('fkDistrictsID', (['' => str_replace('option', trans('text_lang.district'), trans('text_lang.selectOptionN')) ] + Session::get('districts')),$_POST['fkDistrictsID'], ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                                @else
                                    {!! Form::select('fkDistrictsID', (['' => str_replace('option', trans('text_lang.district'), trans('text_lang.selectOptionN')) ] ),null, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="location">{!! trans('text_lang.commune') !!}:</label>
                                <span class="textlabel hidden-xs"></span>
                                @if(Session::has('communes') && empty($_POST['fkCommunesID']))
                                    {!! Form::select('fkCommunesID', (['' => str_replace('option', trans('text_lang.commune'), trans('text_lang.selectOptionN')) ] + Session::get('communes')),null, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                                @elseif (Session::has('communes')&&!empty($_POST['fkCommunesID']))
                                    {!! Form::select('fkCommunesID', (['' => str_replace('option', trans('text_lang.commune'), trans('text_lang.selectOptionN')) ] + Session::get('communes')),$_POST['fkCommunesID'], ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                                @else
                                    {!! Form::select('fkCommunesID', (['' => str_replace('option', trans('text_lang.commune'), trans('text_lang.selectOptionN')) ] ),1, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="location">{!! trans('text_lang.sector') !!}:</label>
                                <span class="textlabel hidden-xs"></span>
                                <?php if(empty($_POST['fkSectorID'])){ ; ?>
                                {!! Form::select('fkSectorID', (['' => str_replace('option', trans('text_lang.sector'), trans('text_lang.selectOptionN')) ] +$sectors),null, ['class' => 'form-control', 'id' => 'pkSectorID']) !!}
                                <?php }else{  ?>
                                {!! Form::select('fkSectorID', (['' => str_replace('option', trans('text_lang.sector'), trans('text_lang.selectOptionN')) ] +$sectors),$_POST['fkSectorID'], ['class' => 'form-control', 'id' => 'pkSectorID']) !!}
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="unit">{!! trans('text_lang.unit') !!}:</label>
                                <?php if(empty($_POST['unit'])){ ; ?>
                                {{ Form::select('unit', [0=>trans('text_lang.daily'),1=>trans('text_lang.monthly')],0,['class'=>'form-control']) }}
                                <?php }else{?>
                                {{ Form::select('unit', [0=>trans('text_lang.daily'),1=>trans('text_lang.monthly')],$_POST['unit'],['class'=>'form-control']) }}
                                <?php }?>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div id="chart_div"></div>
                            </div>
                            <div class="col-md-2 checkbox-chart">

                            </div>
                        </div>
                        <br>
                    </div>
                    {!! form::close() !!}
                </div>
            </div>
        </div>
    @endif
@endsection
@section('extraJS')
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
    <script type="text/javascript" defer="true">
    </script>
@endsection
