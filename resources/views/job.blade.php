@extends('layouts.bongpheak')
@section('title', 'Bongpheak.com - Job Offers')
@section('breadcrumbs', Breadcrumbs::render('jobsList'))
@section('content')
  <div class="container-fluid">
    <div class="row ">
        <div class="col-xs-12 col-md-3 ">
            <div class="panel panel-default">
                <div class="panel-heading text-center expandSearch">
                  <big>{{ trans('text_lang.searchJob') }}</big>
                  <i class="fa fa-chevron-down visible-xs" style="font-size: 18px;"></i>
                </div>

                <div class="text-center panel-body frmLeftSearch">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/jobs', 'class' => 'form-horizontal')) !!}
                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.sector') !!}</span>
                        {!! Form::select('fkSectorsID', (['' => str_replace('option', trans('text_lang.sector'), trans('text_lang.selectOptionN')) ] + $sectors->toArray()), $fkSectorsID, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.subsector') !!}</span>
                        @if( $subsectors )
                            {!! Form::select('fkSubsectorsID', (['' => str_replace('option', trans('text_lang.subsector'), trans('text_lang.selectOptionN')) ] + $subsectors->toArray()), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @else
                            {!! Form::select('fkSubsectorsID', (['' => str_replace('option', trans('text_lang.subsector'), trans('text_lang.selectOptionN')) ]), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.activities') !!}</span>
                        @if( $activities )
                            {!! Form::select('fkActivitiesID', (['' => str_replace('option', trans('text_lang.activities'), trans('text_lang.selectOptionN')) ] + $activities->toArray()), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @else
                            {!! Form::select('fkActivitiesID', (['' => str_replace('option', trans('text_lang.activities'), trans('text_lang.selectOptionN')) ]), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.position') !!}</span>
                        @if( $positions )
                            {!! Form::select('fkPositionsID', (['' => str_replace('option', trans('text_lang.position'), trans('text_lang.selectOptionN')) ] + $positions->toArray()), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @else
                            {!! Form::select('fkPositionsID', (['' => str_replace('option', trans('text_lang.position'), trans('text_lang.selectOptionN')) ]), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.province') !!}</span>
                        {!! Form::select('fkProvincesID', (['' => str_replace('option', trans('text_lang.province'), trans('text_lang.selectOptionN')) ] + $provinces->toArray()), $fkProvincesID, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.district') !!}</span>
                        @if( $districts )
                            {!! Form::select('fkDistrictsID', (['' => str_replace('option', trans('text_lang.district'), trans('text_lang.selectOptionN')) ] + $districts->toArray()), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @else
                            {!! Form::select('fkDistrictsID', (['' => str_replace('option', trans('text_lang.district'), trans('text_lang.selectOptionN')) ] + $districts), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.commune') !!}</span>
                        @if( $communes )
                            {!! Form::select('fkCommunesID', (['' => str_replace('option', trans('text_lang.commune'), trans('text_lang.selectOptionN')) ] + $communes->toArray()), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @else
                            {!! Form::select('fkCommunesID', (['' => str_replace('option', trans('text_lang.commune'), trans('text_lang.selectOptionN')) ] + $communes), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 marginTop">
                        <button type="submit" class="btn btn-primary">
                            {{ trans('text_lang.search')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-9 allofLists" style="margin-top:0;">
            <div class="panel panel-default" style="border:0">
                <div class="panel-heading" style="border: 1px solid #ddd;">
                    <big>
                        @if($positionName != null)
                            {{ $positionName }}
                        @else
                            {{ trans('text_lang.jobsList') }}
                        @endif
                    </big>
                </div>

                <div class="text-left panel-body" style="padding:10px 0 0">
                    <!-- New Style -->
                    <div class="list-group bplist-group">
                      @foreach($jobs as $job)
                        <?php
                            $province_name = fnConvertSlug($job->provincesNameEN);
                            $sector_name = fnConvertSlug($job->sectorsNameEN);
                            $position_name = fnConvertSlug($job->positionsNameEN);
                        ?>
                        <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID ) }}" class="list-group-item">
                          <h4 class="list-group-item-heading">
                            <strong>{{ object_get($job, "companiesName{$lang}" ) }} </strong>
                            <small>
                              <i class="fa fa-clock-o"></i>
                              {{ trans('text_lang.closingDate')}} : {{ dateConvertNormal($job->announcementsClosingDate) }}
                            </small>
                          </h4>
                          <p class="list-group-item-text">
                            <i class="fa fa-suitcase"></i>
                            <span class="label label-info">{{ object_get($job, "positionsName{$lang}" ) }}</span>
                          </p>
                        </a>
                      @endforeach
                    </div>
                    <!-- Old Style -->
                    <div class="table-responsive hidden">
                    <table class="table table-striped table-bordered table-hover table-condensed" >
                        <thead class="text-center">
                        <tr>
                            <th>{{ trans('text_lang.position')}}</th>
                            <th>{{ trans('text_lang.company')}}</th>
                            <th>{{ trans('text_lang.announcementsClosingDate')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jobs as $job)
                            <tr>
                                <td>
                                    <?php
                                    $province_name = fnConvertSlug($job->provincesNameEN);
                                    $sector_name = fnConvertSlug($job->sectorsNameEN);
                                    $position_name = fnConvertSlug($job->positionsNameEN);
                                    ?>
                                    <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID ) }}">
                                        {{ object_get($job, "positionsName{$lang}" ) }}
                                    </a>
                                </td>
                                <td>
                                    <?php $company_name = fnConvertSlug($job->companiesName_EN); ?>
                                    <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $job->pkCompaniesID) }}">
                                        {{ object_get($job, "companiesName{$lang}" ) }}
                                    </a>
                                </td>
                                <td>
                                    {{ dateConvertNormal($job->announcementsClosingDate) }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <div class="row text-right noPadding">
                        <div class="col-md-12">
                            {{ $jobs->appends($searchCriteria)->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
    <script type="text/javascript" defer="true">
      $(document).ready(function() {
        var viewportWidth = $(window).width();
        if (viewportWidth <= 480 ) {
          $('.expandSearch').on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            $('.frmLeftSearch').toggleClass('open');
          });
        }
      });
    </script>
@endsection
