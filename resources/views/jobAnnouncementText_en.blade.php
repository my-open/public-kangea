@extends('layouts.app')
@section('title', object_get($jobDetail, "positionsName{$lang}" ) .' in '. object_get($jobDetail, "companiesName{$lang}" ) )
@section('og-image', $jobDetail->fkSectorsID)
@section('breadcrumbs', Breadcrumbs::render('jobDetail'))
@section('content')
    <div class="row marginTop">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.searchJobs') }}</big></div>

                <div class="text-center panel-body frmLeftSearch">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/jobs', 'class' => 'form-horizontal')) !!}

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.sector') !!}</span>
                        {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), $fkSectorsID, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.subsector') !!}</span>
                        @if( $subsectors )
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @else
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.activities') !!}</span>
                        @if( $activities )
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')] + $activities->toArray()), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @else
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')]), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.position') !!}</span>
                        @if( $positions )
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray()), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @else
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')]), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.province') !!}</span>
                        {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), $fkProvincesID, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.district') !!}</span>
                        @if( $districts )
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts->toArray()), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @else
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.commune') !!}</span>
                        @if( $communes )
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes->toArray()), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @else
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 marginTop">
                        <button type="submit" class="btn btn-primary">
                            {{ trans('text_lang.search')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div id="jobDetail" class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.jobDetail') }}</big></div>
                <div class="text-left panel-body">

                    <br>
                    <div class="main-job-title text-justify">
                        <b>{{ object_get($jobDetail, "companiesName{$lang}") }}</b> is a <b>{{ object_get($jobDetail, "subsectorsName{$lang}") }}</b>
                        @if( $jobDetail->pkSectorsID == 3 || $jobDetail->pkSectorsID == 4 )
                            <em>company</em>
                        @endif

                        @if( $jobDetail->pkSectorsID != 2)
                            specialized in <b>{{  object_get($jobDetail, "activitiesName{$lang}" ) }}</b>
                        @endif


                        located in <b>{{ object_get($jobDetail, "provincesName{$lang}" ) }}</b> province, <b>{{ object_get($jobDetail, "districtsName{$lang}" ) }}</b> district,
                        @if( !empty( object_get($jobDetail, "CommunesName{$lang}" )) )
                            <b>{{ object_get($jobDetail, "CommunesName{$lang}") }}</b> commune.
                        @endif
                        <b>{{ object_get($jobDetail, "companiesName{$lang}" ) }}</b> is looking for <b>{{ $jobDetail->announcementsHiring }}</b> interested candidates to work as <b>{{ object_get($jobDetail, "positionsName{$lang}" ) }}</b>.


                        {{--Check announcementsSalaryNegotiate--}}
                        @if( $jobDetail->announcementsSalaryNegotiate ==1 )
                            Salary can be <b>negotiable</b>
                        @else
                            @if( (int)$jobDetail->announcementsSalaryTo <= (int)$jobDetail->announcementsSalaryFrom  )
                                The salary is <b>{{ $jobDetail->announcementsSalaryFrom }}</b>​ dollars per
                                <b>{{ trans('text_lang.'.$jobDetail->announcementsSalaryType) }}</b>.
                            @else
                                The salary is between <b>{{ $jobDetail->announcementsSalaryFrom }}</b> to <b>{{ $jobDetail->announcementsSalaryTo }}</b> dollars per
                                <b>{{ trans('text_lang.'.$jobDetail->announcementsSalaryType) }}</b>
                                @if( $jobDetail->AnnouncementsSalaryDependsOn )
                                    , depending on
                                    <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsSalaryDependsOn) }}</b>
                                @endif
                            @endif
                        @endif

                        {{ trans('text_lang.working') }}
                        @if($jobDetail->announcementsIsFullTime == 0)
                            <b>{{trans('text_lang.partTime').", " }}</b>
                        @elseif($jobDetail->announcementsIsFullTime == 1)
                            <b>{{trans('text_lang.fullTime').", "}}</b>
                        @endif

                        @if( !empty($workTime) )
                            <span style="list-style: none;">
                                    <?php
                                foreach ($workTime as $value) {
                                    echo "<b>".trans('text_lang.'.$value).", </b>";
                                }
                                ?>
                                </span>
                        @endif
                           <b>{{ config("constants.DAYS")[$jobDetail->AnnouncementsFromDay][Lang::getLocale()] }}</b>
                            to <b>{{ config("constants.DAYS")[$jobDetail->AnnouncementsToDay][Lang::getLocale()]."." }}</b>

                           @if( !empty($benefits) )
                            <b>{{  object_get($jobDetail, "companiesName{$lang}") }}</b> offers the following benefits:
                                <span style="list-style: none;">
                                    <?php
                                        foreach ($benefits as $value) {
                                            echo "<b><li> - ". config("constants.BENEFITS")[$value][Lang::getLocale()] ."</li></b>";
                                        }
                                        ?>
                                </span>
                            @endif

                        <b>{{ object_get($jobDetail, "companiesName{$lang}") }}</b> requires
                            @if( !empty($jobDetail->AnnouncementsExperiencePositionN) || !empty($jobDetail->AnnouncementsExperienceSectorN) )
                                @if( !empty($jobDetail->AnnouncementsExperiencePositionN) )
                                       <b> {{ $jobDetail->AnnouncementsExperiencePositionN }}</b>
                                        @if( $jobDetail->AnnouncementsExperiencePositionN >1 ) <b>{{ $jobDetail->AnnouncementsPositionType }}s</b> @else <b>{{ $jobDetail->AnnouncementsPositionType }}</b> @endif
                                    of experience working as <b>{{ object_get($jobDetail, "positionsName{$lang}" ) }}</b>
                                @endif

                                @if( !empty($jobDetail->AnnouncementsExperienceSectorN) )
                                        @if( !empty($jobDetail->AnnouncementsExperienceOrAnd) && !empty($jobDetail->AnnouncementsExperiencePositionN ) )
                                            <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsExperienceOrAnd) }}</b>
                                        @endif
                                        <b>{{ $jobDetail->AnnouncementsExperienceSectorN }}</b>
                                        @if( $jobDetail->AnnouncementsExperienceSectorN >1 )
                                            <b>{{ $jobDetail->AnnouncementsSectorType }}s</b>
                                        @else
                                            <b>{{ $jobDetail->AnnouncementsSectorType }}</b>
                                        @endif
                                            working in a similar <b>{{ object_get($jobDetail, "sectorsName{$lang}" ) }}</b>
                                            @if( $jobDetail->pkSectorsID == 3 || $jobDetail->pkSectorsID == 4)
                                                <b> company.</b>
                                            @endif

                                @endif
                            @else
                            <b>{{  object_get($jobDetail, "companiesName{$lang}") }} does not require any previous experience in applicants</b>.
                            @endif

                        <?php
                            $certificates = json_decode($jobDetail->announcementsCertificate);
                            if( !empty($certificates) ){
                            ?>
                                <b>{{ object_get($jobDetail, "companiesName{$lang}") }}</b>
                                requires applicants to have the following documents:
                                <div style="list-style: none">
                                    @if($jobDetail->announcementsRequiredDocType == 1)
                                        <b><li> + {{trans('text_lang.The_following_documents_are_required')}}</li></b>
                                    @elseif($jobDetail->announcementsRequiredDocType == 2)
                                        <b><li> + {{trans('text_lang.At_least_one_of_the_following_documents_are_required')}}</li></b>
                                    @elseif($jobDetail->announcementsRequiredDocType == 3)
                                        <b><li> + {{trans('text_lang.At_least_two_of_the_following_documents_are_required')}}</li></b>
                                    @endif
                                </div>

                                <div class="postjob-paddig-left">
                                    <?php
                                        echo "<div style='list-style: none;'>";
                                            foreach ($certificates as $key => $value) {
                                                echo "<b><li> - ". config("constants.CERTIFICATES")[$key][Lang::getLocale()] ."</li></b>";
                                            }
                                        echo "</div>";
                                    ?>
                                </div>
                            <?php
                            }
                        ?>

                            @if( !empty($jobDetail->announcementsLanguageLevel) && !empty($jobDetail->announcementsLanguage) )
                                    <?php
                                    $languageLevel = json_decode($jobDetail->announcementsLanguageLevel);
                                    if( isset($languageLevel) ){
                                        foreach ($languageLevel as $key => $value) {
                                            echo "<div class='row'>";
                                                echo "<div class='col-md-7' style='list-style: none;'>";
                                                    echo "<li> - Speak ";
                                                        echo "<b>".config("constants.LANGUAGES")[$key][Lang::getLocale()]."</b> language level ";
                                                        echo "<b>". config("constants.LANGUAGES")[$value][Lang::getLocale()]. "</b>";
                                                    echo "</li>";
                                                echo "</div>";
                                            echo "</div>";
                                        }
                                    }
                                    ?>
                            @endif

                            @if( !empty($jobDetail->AnnouncementsGender) )
                                This position is
                                @if( $jobDetail->AnnouncementsGender == 'both' )
                                    <b>both male and female</b>
                                @elseif( $jobDetail->AnnouncementsGender == 'm' )
                                    <b>only for male</b> candidates.
                                @elseif( $jobDetail->AnnouncementsGender == 'f' )
                                    <b>only for female</b> candidates.
                                @endif
                            candidates
                            @endif
                        <?php
                            $ageFrom = (int)$jobDetail->AnnouncementsAgeFrom;
                            $ageTo = (int)$jobDetail->AnnouncementsAgeUntil;
                        ?>
                        @if( $ageFrom < $ageTo )
                            between the ages of <b>{{ $jobDetail->AnnouncementsAgeFrom }}</b> and <b>{{ $jobDetail->AnnouncementsAgeUntil }}</b>.
                        @else
                            the ages of <b>{{ $jobDetail->AnnouncementsAgeFrom }}</b>.
                        @endif

                            Candidates must apply before the <b>{{ dateConvert( $jobDetail->announcementsClosingDate ) }}</b>.

                        @if(!empty(!empty($jobDetail->companiesPhone)) || !empty(!empty($jobDetail->companiesEmail)) || !empty(!empty($jobDetail->companiesSite)))
                            <div class="margin-to-bottom-company">
                                <b>{{ trans('text_lang.Please_Contact_to_Company') }}</b>
                            </div>
                        @endif

                        <div class="margin-list-item-company">
                            @if(!empty($jobDetail->companiesPhone)) <div>{{ trans('text_lang.phone') }}: {{ $jobDetail->companiesPhone }}</div> @endif
                            @if(!empty($jobDetail->companiesEmail)) <div>{{ trans('text_lang.email') }}: {{ $jobDetail->companiesEmail }}</div> @endif
                            @if(!empty($jobDetail->companiesSite)) <div>{{ trans('text_lang.companiesSite') }}: {{ $jobDetail->companiesSite }}</div> @endif
                        </div>

                            <?php
                                $linkFileMp3 = 'files/sounds/kh/jobs/job_'.$jobDetail->pkAnnouncementsID.'.mp3';
                                $isHasMp3 = isExistMp3($linkFileMp3);
                                if( $isHasMp3 ){
                                    ?>
                                    <div class="row audio-description">
                                      <div class="col-md-3 audio">
                                          {{ trans('text_lang.Listen_Job_Description') }}
                                      </div>
                                      <div class="col-md-9 playback">
                                        <audio controls>
                                            <source src="{{ URL::asset($linkFileMp3) }}" type="audio/mpeg">
                                        </audio>
                                      </div>
                                    </div>
                                    <?php
                                }
                            ?>

                            <div class="row applyAction">
                                @include('includes._btn_apply_share')
                            </div>

                            <div class="row hidden">
                                <div class="col-md-12 text-right fb-share-button"
                                     data-href="{{ Request::url() }}"
                                     data-layout="button_count">
                                </div>
                            </div>

                    </div>
                    {{--#main-job-title--}}
                </div>
                {{--end text-left panel-body--}}
            </div>
        </div>
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/jobLog.js') }}"></script>
    <script src="{{ URL::asset('js/logShareFB.js') }}"></script>
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
@endsection
