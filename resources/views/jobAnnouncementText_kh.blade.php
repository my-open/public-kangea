@extends('layouts.app')
@section('title', 'Bongpheak '. object_get($jobDetail, "positionsName{$lang}") )
@section('og-image', $jobDetail->fkSectorsID)
@section('breadcrumbs', Breadcrumbs::render('jobDetail'))
@section('content')
    <div class="row marginTop">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.searchJobs') }}</big></div>

                <div class="text-center panel-body frmLeftSearch">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/jobs', 'class' => 'form-horizontal')) !!}

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.sector') !!}</span>
                        {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), $fkSectorsID, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.subsector') !!}</span>
                        @if( $subsectors )
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @else
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.activities') !!}</span>
                        @if( $activities )
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')] + $activities->toArray()), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @else
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')]), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.position') !!}</span>
                        @if( $positions )
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray()), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @else
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')]), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.province') !!}</span>
                        {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), $fkProvincesID, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.district') !!}</span>
                        @if( $districts )
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts->toArray()), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @else
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.commune') !!}</span>
                        @if( $communes )
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes->toArray()), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @else
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 marginTop">
                        <button type="submit" class="btn btn-primary">
                            {{ trans('text_lang.search')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div id="jobDetail" class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.jobDetail') }}</big></div>
                <div class="text-left panel-body">

                    <br>
                    <div class="main-job-title text-justify">
                        <b>{{  object_get($jobDetail, "companiesName{$lang}") }}</b> គឺជា
                        @if( $jobDetail->pkSectorsID == 3 || $jobDetail->pkSectorsID == 4 )
                            <em>ក្រុមហ៊ុន</em>
                        @endif
                        <b>{{ object_get($jobDetail, "subsectorsName{$lang}") }}</b>

                        @if( $jobDetail->pkSectorsID != 2)
                            ខាង <b>{{  object_get($jobDetail, "activitiesName{$lang}" ) }}</b>
                        @endif

                        ទីតាំងស្ថិតនៅ​ ខេត្ត​​ <b>{{ object_get($jobDetail, "provincesName{$lang}") }}</b> ស្រុក <b>{{ object_get($jobDetail, "districtsName{$lang}") }}</b>។
                        <b>{{  object_get($jobDetail, "companiesName{$lang}") }}</b>
                        កំពុងស្វែងរកបេក្ខជនដែលចាប់អារម្មណ៍ធ្វើការជា <b>{{ object_get($jobDetail, "positionsName{$lang}") }}​</b>

                        @if( $jobDetail->announcementsHiring )
                        ចំនួន <b>{{ $jobDetail->announcementsHiring }}</b>​ នាក់
                        @endif

                        {{--Check announcementsSalaryNegotiate--}}
                        @if( $jobDetail->announcementsSalaryNegotiate ==1 )
                            ប្រាក់ខែ <b>អាច​ចរចារបាន</b>
                        @else
                            @if( (int)$jobDetail->announcementsSalaryTo <= (int)$jobDetail->announcementsSalaryFrom  )
                                ក្រុមហ៊ុនផ្តល់ ការងារ​ដែលមាន​ប្រាក់​បៀវត្សចាប់ពី<b>{{ $jobDetail->announcementsSalaryFrom }}​</b>ដុល្លាក្នុង
                                @if( $jobDetail->announcementsSalaryType == 'month' ) <b>១{{ trans('text_lang.month') }}</b>
                                @elseif($jobDetail->announcementsSalaryType == 'week' ) <b>១{{ trans('text_lang.week') }}</b>
                                @elseif($jobDetail->announcementsSalaryType == '2week' ) <b>{{ trans('text_lang.2weeks') }}</b>
                                @elseif($jobDetail->announcementsSalaryType == 'day' ) <b>១{{ trans('text_lang.day') }}</b> @endif
                                ឡើងទៅ។
                            @else
                                ក្រុមហ៊ុនផ្តល់ ការងារ​ដែលមាន​ប្រាក់​បៀវត្សចាប់ពី<b>{{ $jobDetail->announcementsSalaryFrom }}</b> ដល់ <b>{{ $jobDetail->announcementsSalaryTo }}</b>ដុល្លាក្នុង
                                @if( $jobDetail->announcementsSalaryType == 'month' ) <b>១{{ trans('text_lang.month') }}</b>
                                @elseif($jobDetail->announcementsSalaryType == 'week' ) <b>១{{ trans('text_lang.week') }}</b>
                                @elseif($jobDetail->announcementsSalaryType == '2week' ) <b>{{ trans('text_lang.2weeks') }}</b>
                                @elseif($jobDetail->announcementsSalaryType == 'day' ) <b>១{{ trans('text_lang.day') }}</b> @endif
                                @if( $jobDetail->AnnouncementsSalaryDependsOn )
                                    ដោយផ្តល់ទៅ
                                    <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsSalaryDependsOn) }}</b>
                                @endif
                            @endif
                        @endif

                        ម៉ោងធ្វើការ
                        @if($jobDetail->announcementsIsFullTime == 0)
                            <b>ក្រៅ​ម៉ោង</b>
                        @elseif($jobDetail->announcementsIsFullTime == 1)
                            <b>ពេញម៉ោង</b>
                        @endif

                        @if( !empty($workTime) )
                            មាន
                            <span style="list-style: none;">
                                    <?php
                                foreach ($workTime as $value) {
                                    echo "<b>".trans('text_lang.'.$value)." </b>";
                                }
                                ?>
                                </span>
                        @endif

                         ពី<b>{{ config("constants.DAYS")[$jobDetail->AnnouncementsFromDay][Lang::getLocale()] }}</b>
                        ដល់ <b> {{ config("constants.DAYS")[$jobDetail->AnnouncementsToDay][Lang::getLocale()] }}</b>​ ។


                        @if( !empty($benefits) )
                            ក្រុមហ៊ុនផ្ដល់អត្ថប្រយោជន៍ដូចខាងក្រោម:
                            <span style="list-style: none;">
                                <?php
                                foreach ($benefits as $value) {
                                    echo "<b><li> - ". config("constants.BENEFITS")[$value][Lang::getLocale()] ."</li></b>";
                                }
                                ?>
                                </span>
                        @endif

                        @if( (int)$jobDetail->AnnouncementsExperiencePositionN == 0 && (int)$jobDetail->AnnouncementsExperienceSectorN == 0 )
                            @if( $jobDetail->pkSectorsID == 3 || $jobDetail->pkSectorsID == 4 )
                                <em>ក្រុមហ៊ុន</em>
                            @endif
                            <b>{{ object_get($jobDetail, "subsectorsName{$lang}") }}</b>
                            មិន​តម្រូវ​ឲ្យ​បេក្ខជន​មាន​ បទពិសោធន៍ទេ។
                        @elseif( (int)$jobDetail->AnnouncementsExperiencePositionN != 0 && (int)$jobDetail->AnnouncementsExperienceSectorN == 0 )
                            កំពុងស្វែងរកកម្មករដែលមានបទពីសោធន៍  <b>{{ $jobDetail->AnnouncementsExperiencePositionN }}</b>@if( $jobDetail->AnnouncementsPositionType =='month' ) <b>ខែ</b> @else <b>ឆ្នាំ</b>  @endif
                            ជា <b>{{ object_get($jobDetail, "positionsName{$lang}") }}</b> ធ្វើការ​នៅ
                        @elseif( (int)$jobDetail->AnnouncementsExperiencePositionN == 0 && (int)$jobDetail->AnnouncementsExperienceSectorN != 0 )
                            កំពុងស្វែងរកកម្មករដែលមានបទពីសោធន៍ <b>{{ $jobDetail->AnnouncementsExperienceSectorN }}</b>@if( $jobDetail->AnnouncementsPositionType =='month' ) <b>ខែ</b> @else <b>ឆ្នាំ</b>  @endif
                            ជា <b>{{ object_get($jobDetail, "sectorsName{$lang}") }}</b> ធ្វើការ​នៅ
                        @else
                            កំពុងស្វែងរកកម្មករដែលមានបទពីសោធន៍  <b>{{ $jobDetail->AnnouncementsExperiencePositionN }}</b>@if( $jobDetail->AnnouncementsPositionType =='month' ) <b>ខែ</b> @else <b>ឆ្នាំ</b>  @endif
                            ជា <b>{{ object_get($jobDetail, "positionsName{$lang}") }}</b>
                            <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsExperienceOrAnd) }}</b>
                            កំពុងស្វែងរកកម្មករដែលមានបទពីសោធន៍ <b>{{ $jobDetail->AnnouncementsExperienceSectorN }}</b>@if( $jobDetail->AnnouncementsPositionType =='month' ) <b>ខែ</b> @else <b>ឆ្នាំ</b>  @endif
                            ជា <b>{{ object_get($jobDetail, "sectorsName{$lang}") }}</b> ធ្វើការ​នៅ
                        @endif

                        @if( $jobDetail->pkSectorsID == 3 || $jobDetail->pkSectorsID == 4 )
                            <em>ក្រុមហ៊ុន</em>
                        @endif
                        <b>{{ object_get($jobDetail, "subsectorsName{$lang}") }}</b>

                        <?php
                            $certificates = json_decode($jobDetail->announcementsCertificate);
                            if( !empty($certificates) ){
                            ?>

                            ក្រុមហ៊ុនតម្រូវឲ្យមានឯកសារក្នុងការដាក់ពាក្យដូចតទៅៈ
                            <div style="list-style: none">
                                @if($jobDetail->announcementsRequiredDocType == 1)
                                    <b><li> + {{trans('text_lang.The_following_documents_are_required')}}</li></b>
                                @elseif($jobDetail->announcementsRequiredDocType == 2)
                                    <b><li> + {{trans('text_lang.At_least_one_of_the_following_documents_are_required')}}</li></b>
                                @elseif($jobDetail->announcementsRequiredDocType == 3)
                                    <b><li> + {{trans('text_lang.At_least_two_of_the_following_documents_are_required')}}</li></b>
                                @endif
                            </div>

                            <div class="postjob-paddig-left">
                                <?php
                                    echo "<div style='list-style: none;'>";
                                    foreach ($certificates as $key => $value) {
                                        echo "<b><li> - ". config("constants.CERTIFICATES")[$key][Lang::getLocale()] ."</li></b>";
                                    }
                                    echo "</div>";
                                ?>
                            </div>
                            <?php
                            }
                        ?>

                        @if( !empty($jobDetail->announcementsLanguageLevel) && !empty($jobDetail->announcementsLanguage) )
                            <?php
                            $languageLevel = json_decode($jobDetail->announcementsLanguageLevel);
                            if( isset($languageLevel) ){
                                foreach ($languageLevel as $key => $value) {
                                    echo "<div class='row'>";
                                        echo "<div class='col-md-7' style='list-style: none;'>";
                                            echo "<li> - ចេះនិយាយ ";
                                                echo "<b>".config("constants.LANGUAGES")[$key][Lang::getLocale()]."</b>";
                                                echo " <em>(<b>". config("constants.LANGUAGES")[$value][Lang::getLocale()]. "</b>)</em>";
                                            echo "</li>";
                                        echo "</div>";
                                    echo "</div>";
                                }
                            }
                            ?>
                        @endif

                        @if( !empty($jobDetail->AnnouncementsGender) )
                            តួនាទីការងារនេះគឺតម្រូវ
                            @if( $jobDetail->AnnouncementsGender == 'both' )
                                ​ទាំង <b>បុរសនិងនារី</b>
                            @elseif( $jobDetail->AnnouncementsGender == 'm' )
                                តែ <b>ភេទប្រុស</b>
                            @elseif( $jobDetail->AnnouncementsGender == 'f' )
                                តែ <b>ភេទស្រី</b>
                            @endif
                        @endif
                        <?php
                        $ageFrom = (int)$jobDetail->AnnouncementsAgeFrom;
                        $ageTo = (int)$jobDetail->AnnouncementsAgeUntil;
                        ?>
                        ក្នុងការដាក់ពាក្យ​ហើយបេក្ខជនត្រូវមាន​អាយុចាប់​ពី
                        @if( $ageFrom < $ageTo )
                            <b>{{ $jobDetail->AnnouncementsAgeFrom }}</b> ដល់ <b>{{ $jobDetail->AnnouncementsAgeUntil }}</b> ឆ្នាំ។
                        @else
                            <b>{{ $jobDetail->AnnouncementsAgeFrom }}</b> ឆ្នាំឡើងទៅ។
                        @endif
                        បេក្ខជនអាចដាក់ពាក្យបានមុន <em><b>{{ dateConvert( $jobDetail->announcementsClosingDate ) }} </b></em>។

                        @if(!empty(!empty($jobDetail->companiesPhone)) || !empty(!empty($jobDetail->companiesEmail)) || !empty(!empty($jobDetail->companiesSite)))
                            <div class="margin-to-bottom-company">
                                <b>{{ trans('text_lang.Please_Contact_to_Company') }}</b>
                            </div>
                        @endif

                        <div class="margin-list-item-company">
                            @if(!empty($jobDetail->companiesPhone)) <div>{{ trans('text_lang.phone') }}: {{ $jobDetail->companiesPhone }}</div> @endif
                            @if(!empty($jobDetail->companiesEmail)) <div>{{ trans('text_lang.email') }}: {{ $jobDetail->companiesEmail }}</div> @endif
                            @if(!empty($jobDetail->companiesSite)) <div>{{ trans('text_lang.companiesSite') }}: {{ $jobDetail->companiesSite }}</div> @endif
                        </div>

                            <?php
                              $linkFileMp3 = 'files/sounds/kh/jobs/job_'.$jobDetail->pkAnnouncementsID.'.mp3';
                              $isHasMp3 = isExistMp3($linkFileMp3);
                              if( $isHasMp3 ){
                            ?>
                              <div class="row audio-description">
                                <div class="col-md-3 audio">
                                    {{ trans('text_lang.Listen_Job_Description') }}
                                </div>
                                <div class="col-md-9 playback">
                                  <!-- <br> -->
                                  <audio controls>
                                      <source src="{{ URL::asset($linkFileMp3) }}" type="audio/mpeg">
                                  </audio>
                                </div>
                              </div>
                            <?php
                              }
                            ?>

                        <div class="row applyAction">
                            @include('includes._btn_apply_share')
                        </div>

                            <div class="row hidden">
                                <div class="col-md-12 text-right fb-share-button"
                                     data-href="{{ Request::url() }}"
                                     data-layout="button_count">
                                </div>
                            </div>
                    </div>
                    {{--#main-job-title--}}
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/jobLog.js') }}"></script>
    <script src="{{ URL::asset('js/logShareFB.js') }}"></script>
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
@endsection
