@extends('layouts.app')
@section('title', object_get($jobDetail, "positionsName{$lang}" ) .' in '. object_get($jobDetail, "companiesName{$lang}" ) )
@section('og-image', $jobDetail->fkSectorsID)
@section('breadcrumbs', Breadcrumbs::render('jobDetail'))
@section('content')
    <div class="row marginTop">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.searchJobs') }}</big></div>

                <div class="text-center panel-body frmLeftSearch">
                    {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/jobs', 'class' => 'form-horizontal')) !!}

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.sector') !!}</span>
                        {!! Form::select('fkSectorsID', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), $fkSectorsID, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.subsector') !!}</span>
                        @if( $subsectors )
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')] + $subsectors->toArray()), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @else
                            {!! Form::select('fkSubsectorsID', (['' => trans('text_lang.selectOption')]), $fkSubsectorsID, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.activities') !!}</span>
                        @if( $activities )
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')] + $activities->toArray()), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @else
                            {!! Form::select('fkActivitiesID', (['' => trans('text_lang.selectOption')]), $fkActivitiesID, ['class' => 'form-control', 'id' => 'fkActivitiesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.position') !!}</span>
                        @if( $positions )
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')] + $positions->toArray()), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @else
                            {!! Form::select('fkPositionsID', (['' => trans('text_lang.selectOption')]), $fkPositionsID, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.province') !!}</span>
                        {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), $fkProvincesID, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.district') !!}</span>
                        @if( $districts )
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts->toArray()), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @else
                            {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')] + $districts), $fkDistrictsID, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 text-center">
                        <span>{!! trans('text_lang.commune') !!}</span>
                        @if( $communes )
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes->toArray()), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @else
                            {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')] + $communes), $fkCommunesID, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
                        @endif
                    </div>

                    <div class="col-md-12 marginTop">
                        <button type="submit" class="btn btn-primary">
                            {{ trans('text_lang.search')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div id="jobDetail" class="panel panel-default">
                <div class="panel-heading"><big>{{ trans('text_lang.jobDetail') }}</big></div>
                <div class="text-left panel-body">

                    <br>
                    <div class="main-job-title text-justify">
                        <b>{{ object_get($jobDetail, "companiesName{$lang}") }}</b> 是一家 <b>{{ object_get($jobDetail, "subsectorsName{$lang}") }}</b>
                        @if( $jobDetail->pkSectorsID == 3 || $jobDetail->pkSectorsID == 4 )
                            <em>公司</em>
                        @endif
                        @if( $jobDetail->pkSectorsID != 2)
                            专注于 <b>{{  object_get($jobDetail, "activitiesName{$lang}" ) }}</b>
                        @endif

                        工作地点位于 <b>{{ object_get($jobDetail, "provincesName{$lang}" ) }}</b>

                        <b>{{ object_get($jobDetail, "companiesName{$lang}" ) }}</b> 诚聘 <b>{{ $jobDetail->announcementsHiring }}</b> 名 <b>{{ object_get($jobDetail, "positionsName{$lang}" ) }}</b>.

                        @if( (int)$jobDetail->announcementsSalaryTo <= (int)$jobDetail->announcementsSalaryFrom  )
                            薪酬为 <b>{{ $jobDetail->announcementsSalaryFrom }}</b>​ 美元每
                            <b>{{ trans('text_lang.'.$jobDetail->announcementsSalaryType) }}</b>
                        @else
                            薪酬从 <b>{{ $jobDetail->announcementsSalaryFrom }}</b> 到 <b>{{ $jobDetail->announcementsSalaryTo }}</b> 美元每
                            <b>{{ trans('text_lang.'.$jobDetail->announcementsSalaryType) }}</b>
                            @if( $jobDetail->AnnouncementsSalaryDependsOn )
                                薪酬根据
                                <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsSalaryDependsOn) }}</b>
                            @endif
                        @endif

                        工作时间从
                        <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsFromType) }} {{ $jobDetail->AnnouncementsFromHour }}:{{ $jobDetail->AnnouncementsFromMinute }}</b>
                        到
                        <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsFromType) }} {{ $jobDetail->AnnouncementsToHour }}:{{ $jobDetail->AnnouncementsToMinute }}</b>

                        <b>{{ config("constants.DAYS")[$jobDetail->AnnouncementsFromDay][Lang::getLocale()] }}</b>
                        到 <b>{{ config("constants.DAYS")[$jobDetail->AnnouncementsToDay][Lang::getLocale()].", " }}</b>

                        休息时间从
                        <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsBreakHoursFromType) }} {{ $jobDetail->AnnouncementsBreakHoursFromHour }}:{{ $jobDetail->AnnouncementsBreakHoursFromMinute }}</b>
                        到
                        <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsBreakHoursFromType) }} {{ $jobDetail->AnnouncementsBreakHoursToHour }}:{{ $jobDetail->AnnouncementsBreakHoursToMinute }}</b>

                        @if( !empty($benefits) )
                            <b>{{  object_get($jobDetail, "companiesName{$lang}") }}</b> 提供以下福利:
                            <span style="list-style: none;">
                                    <?php
                                foreach ($benefits as $value) {
                                    echo "<b><li> - ". config("constants.BENEFITS")[$value][Lang::getLocale()] ."</li></b>";
                                }
                                ?>
                                </span>
                        @endif

                        <b>{{ object_get($jobDetail, "companiesName{$lang}") }}</b> 要求
                        @if( !empty($jobDetail->AnnouncementsExperiencePositionN) || !empty($jobDetail->AnnouncementsExperienceSectorN) )
                            @if( !empty($jobDetail->AnnouncementsExperiencePositionN) )
                                <b> {{ $jobDetail->AnnouncementsExperiencePositionN }}</b>

                                <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsPositionType) }}</b>

                                作为<b>{{ object_get($jobDetail, "positionsName{$lang}" ) }}</b>的工作经验
                            @endif

                            @if( !empty($jobDetail->AnnouncementsExperienceSectorN) )
                                @if( !empty($jobDetail->AnnouncementsExperienceOrAnd) && !empty($jobDetail->AnnouncementsExperiencePositionN ) )
                                        <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsExperienceOrAnd) }}</b>
                                @endif
                                <b>{{ $jobDetail->AnnouncementsExperienceSectorN }}</b>

                                <b>{{ trans('text_lang.'.$jobDetail->AnnouncementsSectorType) }}</b>

                                在相似 <b>{{ object_get($jobDetail, "sectorsName{$lang}" ) }}</b>的工作经验
                                @if( $jobDetail->pkSectorsID == 3 || $jobDetail->pkSectorsID == 4)
                                        <b> 企业</b>
                                @endif

                            @endif
                        @else
                            <b>{{  object_get($jobDetail, "companiesName{$lang}") }} 不需要任何工作经验</b>.
                        @endif


                        <b>{{ object_get($jobDetail, "companiesName{$lang}") }}</b>
                        要求申请人具有以下证件:
                        <div style="list-style: none">
                            @if($jobDetail->announcementsRequiredDocType == 1)
                                <b><li> + {{trans('text_lang.The_following_documents_are_required')}}</li></b>
                            @elseif($jobDetail->announcementsRequiredDocType == 2)
                                <b><li> + {{trans('text_lang.At_least_one_of_the_following_documents_are_required')}}</li></b>
                            @elseif($jobDetail->announcementsRequiredDocType == 3)
                                <b><li> + {{trans('text_lang.At_least_two_of_the_following_documents_are_required')}}</li></b>
                            @endif
                        </div>
                        <?php
                        $certificates = json_decode($jobDetail->announcementsCertificate);
                        if( !empty($certificates) ){
                            echo "<div style='list-style: none;'>";
                            foreach ($certificates as $key => $value) {
                                echo "<b><li> - ". config("constants.CERTIFICATES")[$key][Lang::getLocale()] ."</li></b>";
                            }
                            echo "</div>";
                        }
                        ?>

                        @if( !empty($jobDetail->announcementsLanguageLevel) && !empty($jobDetail->announcementsLanguage) )
                            <?php
                            $languageLevel = json_decode($jobDetail->announcementsLanguageLevel);
                            if( isset($languageLevel) ){
                                foreach ($languageLevel as $key => $value) {
                                    echo "<div class='row'>";
                                    echo "<div class='col-md-7' style='list-style: none;'>";
                                    echo "<li>";
                                    echo "<b>".config("constants.LANGUAGES")[$key][Lang::getLocale()]."</b> 语言水平：";
                                    echo "<b>". config("constants.LANGUAGES")[$value][Lang::getLocale()]. "</b>";
                                    echo "</li>";
                                    echo "</div>";
                                    echo "</div>";
                                }
                            }
                            ?>
                        @endif

                        @if( !empty($jobDetail->AnnouncementsGender) )
                            @if( $jobDetail->AnnouncementsGender == 'both' )
                                这项职位只招收女性申请人
                            @elseif( $jobDetail->AnnouncementsGender == 'm' )
                                这项职位只招收男性申请人
                            @elseif( $jobDetail->AnnouncementsGender == 'f' )
                                这项职位申请人性别不限
                            @endif
                        @endif
                        申请人需要在 <b>{{ $jobDetail->AnnouncementsAgeFrom }}</b> 到 <b>{{ $jobDetail->AnnouncementsAgeUntil }}</b>岁之间.

                         <b>{{ dateConvert( $jobDetail->announcementsClosingDate ) }}</b>前必须提交申请.

                        @if(!empty(!empty($jobDetail->companiesPhone)) || !empty(!empty($jobDetail->companiesEmail)) || !empty(!empty($jobDetail->companiesSite)))
                            <div class="margin-to-bottom-company">
                                <b>{{ trans('text_lang.Please_Contact_to_Company') }}</b>
                            </div>
                        @endif

                        <div class="margin-list-item-company">
                            @if(!empty($jobDetail->companiesPhone)) <div>{{ trans('text_lang.phone') }}: {{ $jobDetail->companiesPhone }}</div> @endif
                            @if(!empty($jobDetail->companiesEmail)) <div>{{ trans('text_lang.email') }}: {{ $jobDetail->companiesEmail }}</div> @endif
                            @if(!empty($jobDetail->companiesSite)) <div>{{ trans('text_lang.companiesSite') }}: {{ $jobDetail->companiesSite }}</div> @endif
                        </div>

                        <?php
                        $linkFileMp3 = 'files/sounds/kh/jobs/job_'.$jobDetail->pkAnnouncementsID.'.mp3';
                        $isHasMp3 = isExistMp3($linkFileMp3);
                        if( $isHasMp3 ){
                        ?>
                        <div class="row audio-description">
                            <div class="col-md-3 audio">
                                Listen Job Description
                            </div>
                            <div class="col-md-9 playback">
                                <audio controls>
                                    <source src="{{ URL::asset($linkFileMp3) }}" type="audio/mpeg">
                                </audio>
                            </div>
                        </div>
                        <?php
                        }
                        ?>

                        <div class="row applyAction">
                            @include('includes._btn_apply_share')
                        </div>

                        <div class="row hidden">
                            <div class="col-md-12 text-right fb-share-button"
                                 data-href="{{ Request::url() }}"
                                 data-layout="button_count">
                            </div>
                        </div>

                    </div>
                    {{--#main-job-title--}}
                </div>
                {{--end text-left panel-body--}}
            </div>
        </div>
    </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/jobLog.js') }}"></script>
    <script src="{{ URL::asset('js/logShareFB.js') }}"></script>
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
@endsection