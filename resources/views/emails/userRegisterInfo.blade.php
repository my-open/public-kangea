@extends('layouts.email')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <p>Dear <em>Mr./Ms.</em> </p>

            <p>Thank you for registering with Bong Pheak, Cambodia’s first employment service for skilled and unskilled work.</p>

            <p>Please remember your log-in information for the next time:</p>
            <ul>
                <li>Email: {{ $user['email'] }}</li>
                <li>Password: {{ $user['password'] }}</li>
            </ul>

            <p>We recommend to use only one account for each company for easier applicant management.<br/>
                You can now begin to post job announcements and search for candidates on <a href="http://www.bongpheak.com/kh/account/employer/login">Bong Pheak</a>. Watch our tutorial how to post job announcements and manage applicants <a href="#">here</a>.<br/>
                Please don’t hesitate to contact us for questions or assistance regarding the use of Bong Pheak or our services at <a href="mailto:info@bongpheak.com">info@bongpheak.com</a>.
            </p>

            <p>Best regards,<br/>

                Your Bong Pheak Team </p>
        </div>
    </div>
@endsection

