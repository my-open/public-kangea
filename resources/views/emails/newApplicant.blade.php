@extends('layouts.email')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <p>Dear <em>Sir/Madam,</em> </p>
            <p>there is a new applicant for your job announcement for <b><a target="_blank" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $id ) }}">{{$positionName}}</a></b> on Bong Pheak:</p>

            <p>
                Name: {{ $name }}<br/>
                Phone #: {{ $phone }}
            </p>

            <p>The applicant has @if($fileName == '') not @endif attached a CV.</p>

            <p>Please <a href="https://www.bongpheak.com/kh/account/employer/login">login</a> to review @if($title == 'm') his @else her @endif profile in your application management area on Bong Pheak.  </p>

            <p>Best regards,<br/>

                Your Bong Pheak Team </p>
        </div>
    </div>
@endsection