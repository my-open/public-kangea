@extends('layouts.app')
@section('title', 'Completed sign up')
@section('breadcrumbs', Breadcrumbs::render('jobDetail'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading text-center"><big>{{ trans('text_lang.successfullyRegister') }}</big></div>
        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="row">
                    <div class="text-center col-md-12">
                        @include('flash::message')
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>{{ trans('text_lang.successfullyRegister') }}</h2>
                    <p>{{ trans('text_lang.plsWaitingApprovingFromCompanyRepAdministrator') }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
