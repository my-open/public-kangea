@extends('layouts.bongpheak')
@section('title', 'Bongpheak '. trans('text_lang.jobseeker'))

@section('extraClass', 'bgImage bgImage_g')

@section('extraCSS')
 <style media="screen">
    .navbar-default .navbar-nav .dropdown-menu{
      padding-top: 0;
      padding-bottom: 0;
    }
    .navbar-default .navbar-nav .dropdown-menu li a {
      padding-top: 10px;
      padding-bottom: 10px;

    }
   .navbar-default .navbar-nav .dropdown-menu li a,
   .navbar-default .navbar-nav .dropdown-menu {
     background-color: rgba(0, 0, 0, 0.35) !important;
   }
   .navbar-default .navbar-nav .dropdown-menu li a:hover {
     background-color: rgba(255, 255, 255, 0.30) !important;
   }

   @media screen and (min-width: 320px) and (max-width: 480px)  {
     .navbar-default .navbar-nav .dropdown-menu li a,
     .navbar-default .navbar-nav .dropdown-menu,
     .navbar-default .navbar-nav .dropdown-menu li a:hover {
       background-color: rgba(255, 255, 255, 0) !important;
     }
   }
 </style>
@endsection

@section('content')
 <div class="container-fluid">
   <div class="row" id="searchHome">
       <div class="col-md-12 text-center">
           <h1> {{ trans('text_lang.startYourSearchNow') }} </h1><br/>
       </div>
       {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/jobs', 'class' => 'form-horizontal searchControls')) !!}

           <div class="container">
           <?php $optionSize = 5; ?>
             <div class="row">
               <div class="bpAjax-input col-md-4 text-center">
                 <span class="textlabel hidden-xs">{!! trans('text_lang.province') !!}</span>
                   {{ funGetProvincePosition() }}
                   {{--{!! Form::select('fkProvincesID', (['' => str_replace('option', trans('text_lang.province'), trans('text_lang.selectOptionN')) ] + $provinces->toArray()), null, ['class' => 'form-control', 'id' => 'fkProvincesID']) !!}--}}
               </div>

               <div class="bpAjax-input bpAjax-input-fkDistrictsID col-md-4 text-center hidden-xs">
                 <span class="textlabel hidden-xs">{!! trans('text_lang.district') !!}</span>
                 {!! Form::select('fkDistrictsID', (['' => str_replace('option', trans('text_lang.district'), trans('text_lang.selectOptionN')) ] ), null, ['class' => 'form-control', 'id' => 'fkDistrictsID']) !!}
               </div>
               <div class="bpAjax-input bpAjax-input-fkCommunesID col-md-4 text-center hidden-xs">
                 <span class="textlabel hidden-xs">{!! trans('text_lang.commune') !!}</span>
                 {!! Form::select('fkCommunesID', (['' => str_replace('option', trans('text_lang.commune'), trans('text_lang.selectOptionN')) ] ), null, ['class' => 'form-control', 'id' => 'fkCommunesID']) !!}
               </div>

             </div>
           </div>

           <div class="col-md-12 bpAjax-info hidden">
             <h3>
               {{ trans('text_lang.jobsin') }} <br><br>
               <span class="badge fkProvincesID"></span> <span class="badge fkDistrictsID"></span> <span class="badge fkCommunesID"></span>
             </h3>
           </div>
       {!! Form::close() !!}
   </div>
   <!-- </div>  -->
   <!-- Close the Background img -->

 </div>
 <div class="container">
   <div class="row paddingTop bg2">
       <div class="col-md-12">
           <div class="panel panel-default">
               <div class="panel-heading">
                 <i class="fa fa-briefcase"></i><big>{{ trans('text_lang.browseJobs')}}</big>
                 <div class="pull-right">
                   <a class="btn btn-default show_all_job expandJob" style="border-radius: 3px;" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs' ) }}"><span class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;{{ trans('text_lang.showAll')}}</a>
                 </div>
               </div>
               <div class="panel-body">
                   <?php
                        $strWhere = 'tblAnnouncements.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsPublishDate <= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsStatus = 1';
                   ?>
                   <div class="row sectorContainer">
                     @foreach($sectorName as $sectorRow)

                       <?php
                          $jobBySector = DB::table('Positions')
                                          ->join('Announcements', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
                                          ->select('*')
                                          ->where('Positions.fkSectorsID', '=', $sectorRow->pkSectorsID )
                                          ->whereRaw($strWhere)
                                          ->groupby('Positions.pkPositionsID')
                                          ->orderby("Positions.positionsName{$lang}")
                                          ->get();
                       ?>
                       <!-- For Desktop Display -->
                       @if( count($jobBySector) > 0)
                       <div class="col-xs-12 jobByLocationData">
                         <div class="jobByLocationData-content" jobbylocationdata="{{ $sectorRow->pkSectorsID }}">
                           <h3>{{ object_get($sectorRow, "sectorsName{$lang}" ) }}</h3>
                           @foreach($jobBySector as $row)
                           <div class="col-xs-12 col-sm-3 sectorItem truncate">
                               <?php
                                    $sector_name = fnConvertSlug($sectorRow->sectorsNameEN);
                                    $position_name = fnConvertSlug($row->positionsNameEN);
                               ?>
                             <!-- <div class="list"> -->
                                <a class="truncate" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$sector_name.'/'.$position_name.'/'. $row->pkPositionsID ) }}" data-toggle="tooltip" data-placement="bottom" title="{{ object_get($row, "positionsName{$lang}" ) }}">{{ object_get($row, "positionsName{$lang}" ) }}</a>
                             <!-- </div> -->
                           </div>
                           @endforeach
                         </div>
                       </div>
                       @endif
                     @endforeach
                   </div>

                   <div class="row sectorContainer jobByLocationNotFound hidden">
                     <div class="col-xs-12">
                       <h1>
                         {{ trans('text_lang.yourselectedlocationnotavailable')}}
                       </h1>
                     </div>
                   </div>

                   <div class="text-center hidden">
                     <a class="btn btn-default expandJob chevron hidden-xs" href="#">
                       <i class="fa fa-chevron-down"></i>
                       <i class="fa fa-chevron-up"></i>
                     </a>
                   </div>

               </div>
               {{--#panel-body--}}
           </div>
       </div>
   </div>
   <div class="row bg2 companyLists">
       <div class="col-md-12">
           <div class="panel panel-default">
               <div class="panel-heading">
                 <i class="fa fa-building"></i><big>{{ trans('text_lang.companies')}}</big>
                 <div class="pull-right">
                   <a class="btn btn-default show_all_job" style="border-radius: 3px;" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/companies' ) }}"><span class="glyphicon glyphicon-th-list"></span>&nbsp;&nbsp;{{ trans('text_lang.showAll')}}</a>
                 </div>
               </div>
               <div class="panel-body">
                   <div class="row list-company-logo">
                       @foreach($featureM AS $row)
                           <?php $company_name = fnConvertSlug($row->companiesName_EN); ?>
                         <div class="col-xs-12 col-sm-3 col-md-3">
                           <a class="thumbnail" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $row->pkCompaniesID) }}">
                             @if (empty($row->companiesLogo) || file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) === false )
                               <div class="companyName" >
                                   {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 1, '') }}
                               </div>
                             @elseif ( file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) )
                               <div class="companyLogo" style="
                                  background: url('/images/companyLogos/thumbnails/{{ $row->companiesLogo }}') no-repeat;
                               "></div>
                             @endif
                             <div class="caption">
                              {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 30, '...') }}
                            </div>
                           </a>
                         </div>
                       @endforeach
                       @foreach($featureH AS $row)
                         <div class="col-xs-12 col-sm-3 col-md-3">
                             <?php $company_name = fnConvertSlug($row->companiesName_EN); ?>
                           <a class="thumbnail" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $row->pkCompaniesID) }}">
                             @if (empty($row->companiesLogo) || file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) === false )
                               <div class="companyName" >
                                   {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 1, '') }}
                               </div>
                             @elseif ( file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) )
                               <div class="companyLogo" style="
                                  background: url('/images/companyLogos/thumbnails/{{ $row->companiesLogo }}') no-repeat;
                               "></div>
                             @endif
                             <div class="caption">
                              {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 30, '...') }}
                            </div>
                           </a>
                         </div>
                       @endforeach
                       @foreach($featureC AS $row)
                         <div class="col-xs-12 col-sm-3 col-md-3">
                             <?php $company_name = fnConvertSlug($row->companiesName_EN); ?>
                           <a class="thumbnail" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $row->pkCompaniesID) }}">
                             @if (empty($row->companiesLogo) || file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) === false )
                               <div class="companyName" >
                                   {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 1, '') }}
                               </div>
                             @elseif ( file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) )
                               <div class="companyLogo" style="
                                  background: url('/images/companyLogos/thumbnails/{{ $row->companiesLogo }}') no-repeat;
                               "></div>
                             @endif
                             <div class="caption">
                              {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 30, '...') }}
                            </div>
                           </a>
                         </div>
                       @endforeach
                       @foreach($featureS AS $row)
                         <div class="col-xs-12 col-sm-3 col-md-3 featureLimit">
                             <?php $company_name = fnConvertSlug($row->companiesName_EN); ?>
                           <a class="thumbnail" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $row->pkCompaniesID) }}">
                             @if (empty($row->companiesLogo) || file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) === false )
                               <div class="companyName" >
                                   {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 1, '') }}
                               </div>
                             @elseif ( file_exists(public_path('/images/companyLogos/thumbnails/'.$row->companiesLogo)) )
                               <div class="companyLogo" style="
                                  background: url('/images/companyLogos/thumbnails/{{ $row->companiesLogo }}') no-repeat;
                               "></div>
                             @endif
                             <div class="caption">
                              {{ str_limit(trim(object_get($row, "companiesName{$lang}" )), 30, '...') }}
                            </div>
                           </a>
                         </div>
                       @endforeach
                   </div>
               </div>
               {{--#panel-body--}}
           </div>
       </div>
   </div>
 </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/activity.js') }}"></script>
    <script src="{{ URL::asset('js/countJobByDistrict.js') }}"></script>
    <script src="{{ URL::asset('js/countJobByCommune.js') }}"></script>
    <script src="{{ URL::asset('js/searchLog.js') }}"></script>
    <script src="{{ URL::asset('js/searchJobLocationAjax.js') }}"></script>
@endsection
