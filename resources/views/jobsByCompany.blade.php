@extends('layouts.app')
@section('title', object_get($company, "companiesName{$lang}" ) )
@section('extraCSS')
  <style media="screen">
    .companyLogo {
        width: 23%;
    }
    @media screen and (min-width: 320px) and (max-width: 480px)  {
      .companyLogo {
          width: 100%;
      }
    }
  </style>
@endsection
@section('breadcrumbs', Breadcrumbs::render('companyDetail'))
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading text-center"><big>{{ trans('text_lang.companyProfile') }}</big></div>
        <div class="panel-body">
            <div align="center">
                <div class="companyName">
                    <span class="hidden">{{ object_get($company, "companiesName{$lang}") }}</span>
                    <img class="companyLogo" src="{{ URL::asset("images/companyLogos/thumbnails/{$company->companiesLogo}") }}" alt="Company logo" onerror="CompanySwitchThumbnail($(this))">
                </div>
            </div>
            <br>

            <dl class="dl-horizontal">
                <dt> {{ trans('text_lang.companiesName') }}: </dt>
                <dd>
                    {{ object_get($company, "companiesName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.sector') }}: </dt>
                <dd>
                    {{ object_get($company, "sectorsName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.subsector') }}: </dt>
                <dd>
                    {{ object_get($company, "subsectorsName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.mainActivity') }}: </dt>
                <dd>
                    {{ object_get($company, "activitiesName{$lang}" ) }}
                </dd>

                <dt> {{ trans('text_lang.numberOfWorker') }}: </dt>
                <dd> {{ $company->companiesNumberOfWorker }} </dd>

                <dt> {{ trans('text_lang.locations') }}: </dt>
                <dd>
                    {{ fnGetLocation( object_get($company, "provincesName{$lang}"), object_get($company, "districtsName{$lang}"), object_get($company, "CommunesName{$lang}") ) }}
                </dd>

                <dt> {{ trans('text_lang.companiesPhone') }}: </dt>
                <dd> {{ $company->companiesPhone }} </dd>

                <dt> {{ trans('text_lang.companiesEmail') }}: </dt>
                <dd> {{ $company->companiesEmail }} </dd>

                <dt> {{ trans('text_lang.companiesSite') }}: </dt>
                <dd>
                    <a target="_blank" href="{{ $company->companiesSite }}"> {{ $company->companiesSite }}</a>
                </dd>

            @if( !empty($company->companiesDescription)  )
                <dt> {{ trans('text_lang.companyDescription') }}: </dt>
                <dd> {{ $company->companiesDescription }} </dd>
            @endif
            </dl>

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading text-center"><big>{{ trans('text_lang.jobsList') }}</big></div>
        <div class="panel-body">
            <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-condensed jobsByCompanyList" >
                <thead class="text-center">
                <tr>
                    <th>{{ trans('text_lang.position')}}</th>
                    <th>{{ trans('text_lang.location')}}</th>
                    <th>{{ trans('text_lang.announcementsClosingDate')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <?php
                    $province_name = fnConvertSlug($job->provincesNameEN);
                    $sector_name = fnConvertSlug($job->sectorsNameEN);
                    $position_name = fnConvertSlug($job->positionsNameEN);
                    ?>
                    <tr>
                        <td class="truncate">
                            <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID ) }}">
                                {{ object_get($job, "positionsName{$lang}") }}
                            </a>
                        </td>
                        <td class="truncate">
                            {{ fnGetLocation( object_get($job, "provincesName{$lang}"), object_get($job, "districtsName{$lang}"), object_get($job, "CommunesName{$lang}") ) }}
                        </td>
                        <td class="truncate">
                            {{ dateConvertNormal( $job->announcementsClosingDate ) }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
            <div class="row text-right noPadding">
                <div class="col-md-12">
                    {!! $jobs->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
