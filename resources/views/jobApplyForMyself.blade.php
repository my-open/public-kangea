@extends('layouts.app')
@section('title', 'Apply job now')
@section('breadcrumbs', Breadcrumbs::render('jobDetail'))
@section('content')
        <div class="panel panel-default">
            <div class="panel-heading text-center"><big>{{ trans('text_lang.applyNow') }}</big></div>
            <div class="panel-body">
                {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale(). '/jobApplyMy/'.$job->pkAnnouncementsID, 'class' => 'form-horizontal', 'files' => true )) !!}
                @if (Session::has('flash_notification.message'))
                    <div class="row">
                        <div class="text-center col-md-12">
                            @include('flash::message')
                        </div>
                    </div>
                @endif

                <input type="hidden" value="{{ $job->pkCompaniesID }}" name="fkCompaniesID" id="fkCompaniesID" >
                <input type="hidden" value="{{ $job->fkPositionsID }}" name="fkPositionsID" id="fkPositionsID" >
                <input type="hidden" value="{{ $job->positionsNameEN }}" name="positionsNameEN" id="positionsNameEN" >

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12"><u><h3>{{ trans('text_lang.applyForThisJob') }}</h3></u></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">{{ trans('text_lang.position') }}:</div>
                            <div class="col-md-9">
                                <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID ) }}">
                                    {{ object_get($job, "positionsName{$lang}" ) }}
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">{{ trans('text_lang.companyName') }}:</div>
                            <div class="col-md-9">
                                <?php $company_name = fnConvertSlug($job->companiesName_EN); ?>
                                <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $job->pkCompaniesID) }}">
                                    {{ object_get($job, "companiesName{$lang}" ) }}
                                </a>
                            </div>
                        </div>
                    </div>

                    @if ( !Auth::guest() && $user)
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12"><u><h3>{{ trans('text_lang.yourContactInformation') }}</h3></u>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">{{ trans('text_lang.name') }}:</div>
                                <div class="col-md-9">
                                    {{ $user->name }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">{{ trans('text_lang.phone') }}:</div>
                                <div class="col-md-9">
                                    {{ $user->phone }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">{{ trans('text_lang.email') }}:</div>
                                <div class="col-md-9">
                                    {{ $user->email }}
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-6">
                            @if (count($errors) > 0)
                                <div class="row">
                                    <div class="col-md-10 alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{!!  $error !!}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12"><u><h3>{{ trans('text_lang.yourContactInformation') }}</h3></u>
                                </div>
                            </div>

                            <?php
                            foreach (config("constants.GENDER") as $key => $value) {
                                $gender[$key] = $value[Lang::getLocale()];
                            }
                            ?>

                            <div class="row">
                                <div class="col-md-3" style="margin: 4px 0 0 0">
                                    {!! trans('text_lang.name') . ' <span class="requredStar">***</span>'  !!}
                                </div>
                                <div class="col-md-3" style="margin: 3px 0">
                                    {!! Form::select('jobApplyGender', $gender, 'f', ['class' => 'form-control', 'id' => 'gender']) !!}
                                </div>
                                <div class="col-md-4" style="margin: 3px 0">
                                    {!! Form::text('jobApplyName', old('jobApplyName'), ['class' => 'form-control', 'id'=>'jobApplyName']) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3" style="margin: 4px 0 0 0">
                                    {!! trans('text_lang.phone') . ' <span class="requredStar">***</span>'  !!}
                                </div>
                                <div class="col-md-7" style="margin: 3px 0">
                                    {!! Form::text('jobApplyPhone', old('jobApplyPhone'), ['class' => 'form-control positive-integer', 'maxlength' => '10']) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3" style="margin: 4px 0 0 0"></div>
                                <div class="col-md-7" style="margin: 4px 0 0 0">
                                        <label><input type="checkbox" name="jobApplyExperience" value="1" checked> {!! trans('text_lang.experience') !!}</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3" style="margin: 4px 0 0 0">
                                    {!! trans('text_lang.yourCV') !!}
                                </div>
                                <div class="col-md-7" style="margin: 3px 0">
                                    <input type="file" name="jobApplyCvFile" style="visibility:hidden;height: 0"
                                           id="jobApplyCvFile"/>
                                    <input type="hidden" value="MAX_FILE_SIZE" id="max"/>
                                    <fieldset class="fieldset_style">
                                        <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                            <a onclick="issueUpload('jobApplyCvFile','jobApplyCvFile_text')"
                                               style="cursor:pointer;"><i
                                                        class="fa fa-upload"></i> {{ trans('text_lang.chooseCV') }}</a>
                                        </span>
                                            <input type="text" class="form-control" ​ placeholder="No File Selected"
                                                   aria-describedby="basic-addon1" id="jobApplyCvFile_text" disabled>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    @endif
                </div>

                <div class="row marginTop">
                    <div class="col-md-5 text-center col-lg-offset-6">
                        <button type="submit" class="btn btn-success btn-block" name="btnsend">
                            {{ trans('text_lang.send')}}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection

@section('extraJS')
    <script src="{{ URL::asset('js/choosePicture.js') }}"></script>

    <!-- Check numeric -->
    <script type="text/javascript" src="{{ URL::asset('js/checkCondition.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('lib/numeric/jquery/jquery.numeric.js') }}"></script>
    <script src="{{ URL::asset('lib/jquery-ui-1.11.4/jquery-ui.js') }}"></script>
@endsection
