@extends('layouts.regScreen')
@section('content')
    <div class="row mainPic">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12 spaceTopLogo">
                    <img class="img-responsive fondoPic" src="/images/fondo.jpg" alt="fondo">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="{{ url(\Lang::getLocale() . '/') }}"> <img src="/images/logo.png" alt="bongpheak logo"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 box1">
                    @if( Lang::getLocale() == 'kh')
                        <div class="row">
                            <div class="col-md-12 boxKoulen">
                                បងភ័ក្រ​ ជួយ​ស្វែង​រក​ជំនាញ​ដែល​ក្រុម​ហ៊ុន​ត្រូវ​ការ​ !
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12 boxEN">
                                We find the skills you need!
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row box2">
        @if( Lang::getLocale() == 'kh')
            <div class="col-md-12 boxKoulen">
                ការ​បង្កើត​ថ្មីនូវ​សេវា​ការងារ​តែ​មួយ​គត់​ សម្រាប់​វិស័យ​ រោង​ចក្រ​ បដិសណ្ឋារកិច្ច​ សំណង់ និង​សន្តិ​សុខ​នៅ​កម្ពុជា​
            </div>
        @else
            <div class="col-md-12 boxEN">
                An innovative employment service
                for Cambodia’s <br/>Manufacturing,
                Hospitality, Construction, and
                Security sectors.
            </div>
        @endif
    </div>

    <div class="row box3">
        @if( Lang::getLocale() == 'kh')
            <div class="col-md-12 boxbtbBold">
                <span>បងភ័ក្រ</span>គឺ​ជា​សេវា​ដែល​ប្រកប​ដោយ​ វិជ្ជាជីវៈដែល​អនុញ្ញា​តឲ្យ​អ្ន​កស្វែ​ង​រក កម្ម​ក​រ​ដែល​មាន​ជំនាញ​ និង​ គ្មា​ន​ជំនាញ​ បាន​ភ្លាម​ៗ​តាម​រយៈ​ទូរស័ព្ទ​ដៃឬ កំ​ព្យូទ័រ​របស់​អ្នក​ដោយ​ឥត​គិត​ថ្លៃ
            </div>
        @else
            <div class="col-md-12 boxEN">
                <span>Bong Pheak</span> is a professional tool
                that allows you to instantly look
                for low-skilled and unskilled workers from your phone or
                computer at no cost
            </div>
        @endif
    </div>
    @if( Lang::getLocale() == 'kh')
        <div class="row box5kh">
            <div class="col-md-12">
                សេវា<span>បងភ័ក្រ</span>និង​ដំណើរ​ការ​នៅថ្ងៃ​ទី​១២​ ខែ​តុលា​ ​ឆ្នាំ​២០១៦
                <br/>
                <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/employer/register">សូម​ចុះ​ឈ្មោះ</a>​ក្រុ​ម​ហ៊ុន​អ្នក​ឥឡូវ​នេះ​ អ្នក​នឹង​ទទួល​បាន​ព័ត៌មាន​ពីយើង​ នៅពេល​ដែល​សេវា ​បងភ័ក្រ​អាច​ប្រើ​ប្រាស់​ជា​ផ្លូវ​ការ
            </div>
        </div>
    @else
        <div class="row box5">
            <div class="col-md-12">
                <span>Bong Pheak</span> will start its services on <strong>October 12</strong>.<br/>
                <a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/employer/register">Register now</a> and receive reminders from us when it becomes active.

            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 text-center">
            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/aboutUs">{{ trans('text_lang.aboutUs') }}</a>
        </div>
    </div>

@endsection

