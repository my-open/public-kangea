@extends('layouts.bongpheak')
@section('title', 'Bongpheak.com - Companies')
@section('content')
  <div class="container allofLists">
    <div class="col-xs-12">
      <div class="panel panel-default" style="border:0">
        <div class="panel-heading" style="border: 1px solid #ddd;">
          <big>{{ trans('text_lang.companiesList') }}</big>
          <div class="pull-right hidden">
            Filters: &nbsp;
            <button type="button" class="btn btn-default" name="button">
              Sector
              <span class="caret"></span>
            </button>
            <button type="button" class="btn btn-default" name="button">
              Sub Sector
              <span class="caret"></span>
            </button>
            <button type="button" class="btn btn-default" name="button">
              Main Activities
              <span class="caret"></span>
            </button>
          </div>
        </div>
        <div class="text-left panel-body" style="padding:10px 0 0">
          <div class="list-group">
            @foreach($allComp AS $comp)
              <?php $company_name = fnConvertSlug($comp->companiesName_EN); ?>
              @if( !empty(object_get($comp, "companiesName{$lang}" )) && object_get($comp, "companiesName{$lang}" ) !== ' ' )
                <a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/company/'.$company_name.'/'. $comp->pkCompaniesID ) }}" class="list-group-item ">
                  @if ( !empty($comp->companiesLogo) && File::exists(public_path('images/companyLogos/thumbnails/').$comp->companiesLogo) )
                    <img class="pull-right compLogo hidden-xs" src="/images/companyLogos/thumbnails/{{ $comp->companiesLogo }}" alt="" />
                  @endif
                  <h4 class="list-group-item-heading">
                    <strong>{{ object_get($comp, "companiesName{$lang}" ) }}</strong>
                    <small>
                      <i class="fa fa-map-marker"></i>
                      {{ object_get($comp, "provincesName{$lang}" ) }},
                      {{ object_get($comp, "districtsName{$lang}" ) }},
                      {{ object_get($comp, "CommunesName{$lang}" ) }}
                    </small>
                  </h4>
                  <p class="list-group-item-text">
                    @if(!empty(object_get($comp, "sectorsName{$lang}" ))  || !empty(object_get($comp, "subsectorsName{$lang}" )) || !empty(object_get($comp, "activitiesName{$lang}" )) )
                        <i class="fa fa-suitcase"></i>
                        <span class="label label-info @if(empty(object_get($comp, "sectorsName{$lang}" ))) hidden @endif">
                          {{ object_get($comp, "sectorsName{$lang}" ) }}
                        </span>
                        <span class="label label-info @if(empty(object_get($comp, "subsectorsName{$lang}" ))) hidden @endif">
                          {{ object_get($comp, "subsectorsName{$lang}" ) }}
                        </span>
                        <span class="label label-info @if(empty(object_get($comp, "activitiesName{$lang}" ))) hidden @endif">
                          {{ object_get($comp, "activitiesName{$lang}" ) }}
                        </span>
                    @else
                      <br>
                    @endif
                  </p>
                </a>
              @endif
            @endforeach
          </div>
        </div>
      </div>
      <div class="row text-center noPadding">
          <div class="col-md-12">
              {{ $allComp->links() }}
          </div>
      </div>
    </div>
  </div>
@endsection
