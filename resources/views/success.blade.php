@extends('layouts.app')
@section('title', 'Complete')
@section('extraCSS')
  <style media="screen">
    .success {
      margin: 50px 0 80px;
    }
    .success .icon {
      font-size: 128px;
      color: #566ab0;
    }
    .success .msg {
      font-size: 28px;
      color: #616161;
    }
  </style>
@endsection
@section('content')
  <div class="container-fluid success">
    <div class="row">
      <div class="col-xs-12 text-center">
        <div class="icon">
          <i class="fa fa-check"></i>
        </div>
        <div class="msg">
          Check your email! <br>
          We’ve sent a confirmation link to ..email.. <br>
          to confirm your email address.
        </div>
      </div>
    </div>
  </div>
@endsection
