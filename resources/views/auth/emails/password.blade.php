Click here to reset your password: <a href="{{ $link = url('/'.Lang::getLocale().'/password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
