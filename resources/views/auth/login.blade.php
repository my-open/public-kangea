@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.login') )
@section('content')
<div class="container login-register">
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-12">
                <br/>
                <h2><b>{{ trans('text_lang.jobseekerOrEmployerLogin')}}</b></h2>
                <br/>
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/'.LaravelLocalization::getCurrentLocale().'/login') }}">
                {!! csrf_field() !!}
                <div class="form-group form-group-lg {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-md-12">
                        {{ trans('text_lang.email') }}
                    </label>

                    <div class="col-md-10">
                        <input type="email" required class="form-control" name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group form-group-lg {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-md-12">
                        {{ trans('text_lang.password') }}
                    </label>

                    <div class="col-md-10">
                        <input type="password" required class="form-control" name="password">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember">{{ trans('text_lang.rememberMe') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10">
                        <button type="submit" class="btn-lg btn-primary  btn-block">
                            <i class="fa fa-btn fa-sign-in"></i>{{ trans('text_lang.login') }}
                        </button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <a class="btn btn-link" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/password/reset') }}">{{ trans('text_lang.forgetYourPassword') }}</a> |
                        <a class="btn btn-link" href="redirect"><i class="fa fa-btn fa-sign-in"></i>{{ trans('text_lang.fbLogin') }}</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <br/>
            <p>New to Jobsite?</p>
            <p><b>Use us everywhere</b></p>
            <p>At home or at work, on the bus or train, on your mobile, tablet or desktop</p><br/>

            <p><b>It's your data</b></p>
            <p>Make your CV searchable and let recruiters find you, offering jobs that don't get advertised</p><br/>

            <p><b>Get the jobs first</b></p>
            <p>Create personalised jobs alerts</p>

            <a class="btn btn-link" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/register') }}">{{ trans('text_lang.createAsJobSeeker') }}</a>
            <a class="btn btn-link" href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/register') }}">{{ trans('text_lang.createAsEmployer') }}</a>

        </div>
    </div>
</div>
@endsection
