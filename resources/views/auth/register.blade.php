@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.register') )
@section('content')
<div class="container">
<div class="row">
    <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-wizard" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">{{ trans('text_lang.userInfo') }}</a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">{{ trans('text_lang.experience') }}</a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">{{ trans('text_lang.searchJobIn') }}</a>
                    </li>
                </ul>
            </div>

                {!! Form::open(array('url' => '/'.LaravelLocalization::getCurrentLocale().'/'.config("constants.ROUTE_PREFIX_NAME") . '/jobseeker/register', 'class' => 'form-horizontal')) !!}
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('name', trans('text_lang.name') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                            <div class="col-md-7">
                                {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            {!! Html::decode(Form::label('phone', trans('text_lang.phone') . ' <span class="requredStar">***</span>', array('class' => 'col-md-3 control-label'))) !!}
                            <div class="col-md-7">
                                {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'maxlength' => '10']) !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-group-lg">
                            <div class="col-md-7 col-md-offset-3">
                                <em class="requredStar"><b>{{ trans('text_lang.noteStar') }}</b> {{ trans('text_lang.descriptionNotesRequired') }}</em>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-right col-md-7 col-md-offset-3">
                                <button type="button" class="btn btn-primary next-step">
                                    {{ trans('text_lang.continue')}}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <div id="divAppendTo">
                            <div class="divMoreExperience">
                                <div class="form-group">
                                    {!! Form::label('fkSectorsID', trans('text_lang.sector'), ['class' => 'col-md-3 control-label']) !!}
                                    <div class="col-md-7">
                                        {!! Form::select('fkSectorsID[]', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control fkSectorsID']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('fkSubsectorsID', trans('text_lang.subsector'), ['class' => 'col-md-3 control-label']) !!}
                                    <div class="col-md-7">
                                        {!! Form::select('fkSubsectorsID[]', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control fkSubsectorsID']) !!}
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('fkPositionsID') ? ' has-error' : '' }}">
                                    {!! Form::label('fkPositionsID', trans('text_lang.position'), ['class' => 'col-md-3 control-label']) !!}
                                    <div class="col-md-7">
                                        {!! Form::select('fkPositionsID[]', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control fkPositionsID']) !!}
                                        @if ($errors->has('fkPositionsID'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('fkPositionsID') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('monthsOfExperience') ? ' has-error' : '' }}">
                                    {!! Form::label('monthsOfExperience', trans('text_lang.monthsOfexpericence'), ['class' => 'col-md-3 control-label']) !!}
                                    <div class="col-md-7">
                                        <select id="monthsOfExperience" name="monthsOfExperience[]" class="form-control">
                                            <option value="">{{ trans('text_lang.selectOption') }}</option>
                                            <option value="none">{{ trans('text_lang.noExperience') }}</option>
                                            <option value="3months">{{ trans('text_lang.moreThan3Months') }}</option>
                                            <option value="6months">{{ trans('text_lang.moreThan6Months') }}</option>
                                            <option value="1year">{{ trans('text_lang.moreThan1Year') }}</option>
                                            <option value="3years">{{ trans('text_lang.moreThan3Years') }}</option>
                                            <option value="5Years">{{ trans('text_lang.moreThan5Years') }}</option>
                                        </select>
                                        @if ($errors->has('monthsOfExperience'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('monthsOfExperience') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="text-right col-md-7 col-md-offset-3">
                                        <button type="button" class="btn btn-default btnRemoveMoreExperience">{{ trans('text_lang.remove')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="text-right col-md-7 col-md-offset-3">
                                <button id="addMoreExperience" type="button" class="btn btn-default">{{ trans('text_lang.addMoreExperince')}}</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="text-right col-md-7 col-md-offset-3">
                                <button type="button" class="btn btn-default prev-step">{{ trans('text_lang.previous')}}</button>
                                <button type="button" class="btn btn-primary next-step">
                                    {{ trans('text_lang.continue')}}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="complete">
                        <div class="form-group">
                            {!! Form::label('fkSectorsIDTargetJob', trans('text_lang.sector'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkSectorsIDTargetJob', (['' => trans('text_lang.selectOption')] + $sectors->toArray()), null, ['class' => 'form-control', 'id' => 'fkSectorsID']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fkSubsectorsIDTargetJob', trans('text_lang.subsector'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkSubsectorsIDTargetJob', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'id' => 'fkSubsectorsID']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fkPositionsIDTargetJob', trans('text_lang.position'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkPositionsIDTargetJob', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control', 'id' => 'fkPositionsID']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fkProvincesID', trans('text_lang.province'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkProvincesID', (['' => trans('text_lang.selectOption')] + $provinces->toArray()), null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fkDistrictsID', trans('text_lang.district'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkDistrictsID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fkCommunesID', trans('text_lang.commune'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkCommunesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('fkVillagesID', trans('text_lang.village'), ['class' => 'col-md-3 control-label']) !!}
                            <div class="col-md-7">
                                {!! Form::select('fkVillagesID', (['' => trans('text_lang.selectOption')]), null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-right col-md-7 col-md-offset-3">
                                <button type="button" class="btn btn-default prev-step">{{ trans('text_lang.previous')}}</button>
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('text_lang.register')}}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            {!! Form::close() !!}
        </div>
    </section>
</div>
</div>
@endsection
@section('extraJS')
    {!!Html::script('js/seekerRegisterStep.js')!!}
    <script src="{{ URL::asset('js/subsector.js') }}"></script>
    <script src="{{ URL::asset('js/position.js') }}"></script>
    <script src="{{ URL::asset('js/district.js') }}"></script>
    <script src="{{ URL::asset('js/commune.js') }}"></script>
    <script src="{{ URL::asset('js/village.js') }}"></script>
@endsection
