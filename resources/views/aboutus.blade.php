@extends('layouts.app')
@section('title', 'Bongpheak ' . trans('text_lang.aboutUs') )
@section('extraCSS')
  <link rel="stylesheet" href="{{ URL::asset('css/registrationScreen.css') }}">
@endsection
@section('content-fluid')
  <div class="row">
      <div class="col-md-12 spaceTopLogo" style="
        background: url('/images/fondo.jpg') no-repeat;
        background-size: cover;
        min-height: 400px;
        margint-top: -10px;
        padding-top: 0;
        margin-bottom: 20px;
      ">
      </div>
  </div>
@endsection
@section('content')
    <div class="row mainPic">
        <div class="col-md-12">
            <div class="row hidden">
                <div class="col-md-12 spaceTopLogo">
                    <img class="img-responsive fondoPic" src="/images/fondo.jpg" alt="fondo">
                </div>
            </div>
            <div class="row hidden">
                <div class="col-md-12 text-center">
                    <a href="{{ url(\Lang::getLocale() . '/') }}"> <img src="/images/logo.png" alt="bongpheak logo"></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 box1">
                    @if( Lang::getLocale() == 'kh')
                        <div class="row">
                            <div class="col-md-12 boxKoulen">
                                បងភ័ក្រ​ ជួយ​ស្វែង​រក​ជំនាញ​ដែល​ក្រុម​ហ៊ុន​ត្រូវ​ការ​ !
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12 boxEN">
                                We find the skills you need!
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row aboutus">
        <div class="col-md-12">

            <h2>{{ trans('text_lang.mission') }}</h2>

            @if( Lang::getLocale() == 'en')
            <p><b>Bong Pheak</b> is an employment service for Cambodian unskilled and low-skilled workers.</p>

            <p>While there are many employment services in Cambodia, none of them addresses unskilled or low-skilled workers in the sectors of construction, manufacturing, hospitality or security. This is mostly due to the fact that existing services use only Internet as a medium, and most of these workers do not have direct access to this network.</p>

            <p>Cambodian companies are not always able to communicate their employment opportunities to prospective workers, often not filling all the available positions. A complete workforce leads to an increase in efficiency and profit.</p>

            <p>Meanwhile, Cambodian youth searching for their first job are unable to find employment in Cambodia, in spite of work being available. Workers end up emigrating to other countries to find work, with all the risks of trafficking in person that this entails.</p>

            <p>To reach out to potential workers, the Bong Pheak employment service provides information to their family and friends who are already working, and who have access to Internet. When these find an employment opportunity that they want to communicate to the potential worker back in their village, they only need to click and enter the phone number of the worker. The worker will receive an automatic phone call describing the job, and giving them the opportunity of having the employer call them if they are interested on the job.</p>

            <p><b>Bong Pheak</b> has been created by the NGO Open Institute as part of the USAID Countering trafficking-in-Persons program, led by the NGO Winrock. While receiving funding from a donor in order to start the service, Bong Pheak is expected to become an independent sustainable service after it is able to demonstrate its value to employers and workers.</p>
            @else
                <p><b>បង​ភ័ក្រ</b>​ ជា​សេវា​កម្ម​ការងារ​សម្រាប់​និយោជិត/កម្មករ​ខ្មែរ​ដែល​មាន​គ្មាន​ជំនាញ​ និង​មាន​ជំនាញ​កម្រិត​ទាប</p>

                <p>ខណៈ​ពេល​ដែល​ក្នុង​ប្រទេស​កម្ពុជា​មាន​សេវាកម្ម​ការងារ​ជាច្រើន​ តែ​​សេវា​កម្ម​ការងារ​​​ទាំង​នោះ​​មិន​បាន​ធ្វើ​ការ​ផ្សព្វផ្សាយ​ព័ត៌មាន​ការងារ​សម្រាប់​និយោជិត​/កម្មករ​គ្មាន​ជំនាញ​ ឬ​មាន​ជំនាញ​កម្រិត​ទាប​ដែល​ស្ថិត​ក្នុង​វិស័យ​ សំណង់​ រោងចក្រ​ បដិសណ្ឋារកិច្ច​ ឬក៏​សន្តិសុខ​នោះ​ទេ​​។ ​ដោយសារ​តែ​សេវា​កម្ម​ការងារ​ដែល​មានស្រាប់​ស្ទើរ​តែ​ទាំង​អស់​នោះ​តម្រូវ​ឲ្យ​ប្រើ​អ៊ីនធឺណិត​ជា​មធ្យោបាយ ហើយ​និយោជិត​/កម្មករ​ភាគ​ច្រើន​មិន​បាន​ប្រើប្រាស់​បណ្ដាញ​នេះ​ទេ។</p>

                <p>ក្រុមហ៊ុន​របស់​ខ្មែរ​មិន​តែងតែ​​​ទាក់ទង​ពី​ឱកាស​ការងារ​​របស់​ពួកគេ​ជា​មួយ​និយោជិត/កម្មករ​​​សម្រាប់​ត្រៀម​ទុក​ទេ ដែល​ជា​រឿយៗ​​មិន​បំពេញ​មុខតំណែង​ដែល​មាន​ទាំងអស់។ កម្លាំង​ពលកម្ម​ពេញលេញ​នាំ​ឲ្យ​ក្រុមហ៊ុន​អាច​បង្កើន​​ប្រាក់​ចំណូល និង​ប្រសិទ្ធភាព​ការងារ។</p>

                <p>ទន្ទឹម​​នឹង​នេះ​​ដែរ​​ យុវជន​ខ្មែរ​​​ស្វែងរក​ការងារ​ដំបូង​របស់​ពួក​គេ​​ តែ​មិន​អាច​រក​បាន​នៅ​ក្នុង​ស្រុក​ខ្មែរ​ សូម្បី​តែ​មាន​ការងារ​ក៏ដោយ។  កម្មករ​នឹង​លែង​ធ្វើ​ចំណាក​ស្រុក​ទៅ​ប្រទេស​ផ្សេង​ ដែល​មាន​ភាព​ប្រថុយប្រថាន​ក្នុង​ការ​ជួញដូរ​មនុស្ស​ដែល​តែងតែ​កើត​មាន​ឡើង។</p>

                <p>​​​ដើម្បី​ធ្វើ​ការ​​ផ្សព្វផ្សាយ​ទៅដល់​​និយោជិត/កម្មករ​ដែល​មាន​សក្ដានុពល​ សេវាកម្ម​​ការងារ​បង​ ភ័ក្រ​ ផ្ដល់​ព័ត៌មាន​ការងារ​ដល់​ក្រុម​​គ្រួសារ ​​និង​មិត្តភ័ក្ដិ​របស់​ពួក​គេ​ដែល​មាន​ការងារ​ធ្វើ​រួច​ហើយ ​ដែល​ពួក​គេ​អាច​ប្រើ​ប្រាស់​អ៊ីនធឺណិត​​​បាន។ នៅ​ពេល​​​ស្វែងរក​​​ឱកាស​ការងារ​ដែល​ពួកគេ​ចង់​ទាក់ទង​ជា​មួយ​និយោជិត/កម្មករ​ក្នុង​ភូមិ ពួកគេ​​គ្រាន់តែ​ចុច​ ហើយ​បញ្ចូល​លេខ​ទូរស័ព្ទ​របស់​និយោជិត/កម្មករ។ និយោជិត/កម្មករ​នឹង​ទទួល​បាន​ការ​ហៅ​ទូរស័ព្ទ​ដោយ​ស្វ័យ​ប្រវត្តិ​​ដែល​​ពិពណ៌នា​អំពី​ការងារ និង​ផ្ដល់​​​​ឱកាស​ឲ្យ​និយោជក​ហៅ​​ពួកគេ ​ប្រសិន​បើ​គេ​ចាប់អារម្មណ៍​នឹង​ការងារ​នោះ។</p>

                <p><b>បង​ភ័ក្រ</b>​ត្រូវ​បាន​បង្កើត​ឡើង​ដោយ​អង្គការ​វិទ្យាស្ថាន​បើក​ទូលាយ​ដែល​ជា​ផ្នែក​មួយ​នៃ​កម្មវិធី​​ប្រយុទ្ធ​​ប្រឆាំង​​ការ​ជួញ​ដូរ​មនុស្ស​នៅ​កម្ពុជា​របស់​ទីភ្នាក់ងារ​សហរដ្ឋអាមេរិក​សម្រាប់​ការ​​អភិវឌ្ឍ​អន្តរជាតិ​   ដែល​​ដឹក​នាំ​ដោយ​អង្គការ​វីនរ៉ក់​​អន្តរជាតិ​ ក្នុង​ពេល​ដែល​ទទួល​មូលនិធិ​ពី​ម្ចាស់​ជំនួយ​ក្នុង​ការ​ដាក់​ឲ្យ​ប្រើ​ប្រាស់​សេវាកម្ម​​​ការ​ងារ​ ​បង​ភ័ក្រ​ត្រូវ​បាន​រំពឹង​ទុក​ថា​នឹង​ក្លាយ​ជា​សេវា​កម្ម​ឯករាជ្យ ​និង​មាន​និរន្តរភាព​ បន្ទាប់​ពី​សេវា​កម្ម​នេះ​​បង្ហាញ​ពី​តម្លៃ​របស់​ខ្លួន​ដល់​និយោជក និង​និយោជិត/កម្មករ។</p>
            @endif
            <br/>
        </div>
    </div>
    <div class="row advLogo">
        <div class="col-md-4">
            <a href="#" class="thumbnail">
                <img src="/images/usaid.png" alt="usaid" style="height:65px">
            </a>
        </div>
        <div class="col-md-4">
            <a href="#" class="thumbnail">
                <img src="/images/oi.png" alt="oi" style="height:65px">
            </a>
        </div>
        <div class="col-md-4">
            <a href="#" class="thumbnail">
                <img src="/images/winrock.png" alt="winrock" style="height:65px">
            </a>
        </div>
    </div>
@endsection
