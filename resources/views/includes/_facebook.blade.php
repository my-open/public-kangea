<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1094203813935482";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Facebook Scrab URL -->
<div class="hidden" id="facebook_scrape_url" ScrapeURL="{{ url()->full() }}" debug="true">
  Use for Facebook scrape url
  <script type="text/javascript">
    $(document).ready(function() {
      console.log('Facebook Scrape API');
      getAlexAPI();
    });
  </script>
</div>
