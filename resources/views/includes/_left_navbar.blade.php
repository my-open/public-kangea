<ul class="nav navbar-nav navbar-left">

  @if ( Auth::guest())
    @if ( str_contains(Request::path(), 'employer') )
      <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/#">{{ trans('text_lang.bongpheakForJobSeeker') }}</a></li>
    @else {{-- if ( str_contains(Request::path(), 'jobseeker') || Route::current()->getUri() == '/' || Route::current()->getUri() == LaravelLocalization::getCurrentLocale()) --}}
      <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/employer">{{ trans('text_lang.bongpheakForEmployer') }}</a></li>
    @endif
  @endif
    <!-- <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/aboutBongpheak">{{ trans('text_lang.aboutBongpheak') }}</a></li> -->
</ul>
