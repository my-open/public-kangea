<div class="footer-navbar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-1 col-md-11 footer-logo-{{ Lang::getLocale() }}">
                <!-- Logo -->
                <img src="{{ URL::to('/') }}/images/logo_{{ Lang::getLocale() }}_white.png" alt="" />
            </div>
        </div>

        <div class="row">
            <!-- Navigation -->
            <div class="col-md-offset-1 col-md-2">
                <h4>{{trans('text_lang.jobseeker')}}</h4>
                <ul>
                    <li><a href="/{{ Lang::getLocale() }}/account/jobseeker/register">{{trans('text_lang.registerAsJobSeeker')}}</a></li>
                    <li><a href="#">{{trans('text_lang.informationForJobSeeker')}}</a></li>
                    <li><a href="#">{{trans('text_lang.termsOfServices')}}</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h4>{{trans('text_lang.employer')}}</h4>
                <ul>
                    <li><a href="/{{ Lang::getLocale() }}/account/employer/register">{{trans('text_lang.registerAsEmployer')}}</a></li>
                    <li><a href="#">{{trans('text_lang.informationForEmployer')}}</a></li>
                    <li><a href="#">{{trans('text_lang.termsOfServices')}}</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h4>{{trans('text_lang.about')}}</h4>
                <ul>
                    <li><a href="/{{ Lang::getLocale() }}/aboutBongpheak">{{trans('text_lang.aboutBongpheak')}}</a></li>
                    <li><a href="/{{ Lang::getLocale() }}/aboutUs">{{trans('text_lang.mission')}}</a></li>
                    <li><a href="/{{ Lang::getLocale() }}/contactUs">{{trans('text_lang.contactUs')}}</a></li>
                </ul>
            </div>
        </div>

        <div class="row wrap-copy-right">
            <!-- Copyright Text -->
            <div class="col-md-offset-1 col-md-5">
                {{trans('text_lang.copyRightBongpheakAllRightsReserved')}}
            </div>

            <!-- EZECOM Power by -->
            <div class="col-md-5 text-right">
                <a href="http://www.ezecom.com.kh/" target="_blank">{{ trans('text_lang.Powered_by_EZECOM') }}</a>
            </div>
        </div>
    </div>
</div>
{{--@include('includes._facebook')--}}

<!-- GoogleAnalytics Code for www.bongpheak.com -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-86295795-1', 'auto');
    ga('send', 'pageview');
</script>

<!-- Hotjar Tracking Code for www.bongpheak.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:398192,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>