<li class="dropdown">
  <a href="#"class="dropdown-toggle hidden-xs" data-toggle="dropdown" role="button" aria-expanded="false">
    <i class="fa fa-globe"></i>
    <span class="caret"></span>
  </a>
  <a href="#"class="dropdown-toggle visible-xs" data-toggle="dropdown" role="button" aria-expanded="false">
    {{ trans('text_lang.language') }}
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
      @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
          <li>
            <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                <img src="/images/{{{ $properties['flag'] }}}" alt="{{{ $properties['name'] }}}" width="20" height="12">&nbsp;&nbsp;{{{ $properties['native'] }}}
            </a>
          </li>
      @endforeach
  </ul>
</li>
