<div id="modalDeleteComRep" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-titleDeleteComRep">Modal title</h4>
            </div>
            <div class="modal-body"><p id="modalDeleteComRepMessage"></p></div>
            <div class="modal-footer">
                {!! Form::open([
                    'method' => 'POST',
                    'id' => 'confirmDeleteComRep',
                    'style'=>'display:inline-block',
                ]) !!}
                <input type="hidden" name="_method" value="GET">
                {!! Form::submit( trans('text_lang.yes'), ['class' => 'btn btn-success'] ) !!}
                {!! Form::close() !!}
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{ trans('text_lang.no')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.btndeleteCompanyRep').click(function(e) {
            var href = jQuery(this).attr('href');
            var message = jQuery(this).attr('data-content');
            var title = jQuery(this).attr('data-title');

            jQuery('.modal-titleDeleteComRep').text(title);
            jQuery('#modalDeleteComRepMessage').text(message);
            jQuery('#confirmDeleteComRep').attr("action", href);
            jQuery('#modalDeleteComRep').modal();
        });
    });
</script>