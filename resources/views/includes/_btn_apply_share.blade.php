<div class="col-sm-2 hidden-xs">
    <a class="btn btn-warning btn-apply lg" href="{{URL::previous()}}">
        <i class="fa fa-chevron-left"></i>
        {{ trans('text_lang.back')}}
    </a>
</div>
<div class="col-xs-12 col-sm-4">
    {{--check permission who can edit this announcement or this post job?--}}
    @if( $jobDetail->fkCompaniesID == $userCompanyID )
        @if(Entrust::hasRole('employer') || Entrust::hasRole('admin'))
            <a class="btn btn-success btn-apply lg" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME").'/postjob/'.$jobDetail->pkAnnouncementsID.'/edit' }}">
                <i class="fa fa-edit"></i>
                {{ trans('text_lang.edit')}}
            </a>
            <a class="btn btn-success btn-apply lg" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME").'/jobPublish/'.$jobDetail->pkAnnouncementsID }}">{{ trans('text_lang.publish')}}</a>
        @endif
    @endif

    @if(Entrust::hasRole('admin'))
        <a class="btn btn-success btn-apply lg" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/'. config("constants.ROUTE_PREFIX_NAME").'/company/job/edit/'.$jobDetail->pkAnnouncementsID }}">
            <i class="fa fa-edit"></i>
            {{ trans('text_lang.edit')}}
        </a>
    @endif

    @if(!Entrust::hasRole('employer') && !Entrust::hasRole('admin'))
        <?php
        $linkFileMp3 = 'files/sounds/kh/confirmJobs/confirmJob_'.$jobDetail->pkAnnouncementsID.'.mp3';
        $jobNoCompanyMp3 = public_path('files/sounds/kh/confirmJobsNoCompany/confirmJob_' . $jobDetail->pkAnnouncementsID . '.mp3');
        $isHasMp3 = isExistMp3($linkFileMp3);
        $isJobNoCompanyMp3 = isExistMp3($jobNoCompanyMp3);
        if( $isHasMp3 || $isJobNoCompanyMp3 ){
        ?>
            <a class="btn btn-success btn-apply lg" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/jobApply/'.$jobDetail->pkAnnouncementsID }}">
                <i class="fa fa-check"></i>
                {{ trans('text_lang.applyNow')}}
            </a>
        <?php
        }
        ?>
    @endif
</div>

<?php
$linkFileMp3 = 'files/sounds/kh/jobs/job_'.$jobDetail->pkAnnouncementsID.'.mp3';
$isHasMp3 = isExistMp3($linkFileMp3);
if( $isHasMp3 ){
?>
@if( $jobDetail->announcementsStatus ==1 )
    <div class="col-xs-12 col-sm-4">
        <div class="social-share" id="idBtnShareToPhone" value-pkAnnouncementsID="{{ $jobDetail->pkAnnouncementsID }}">
            <a class="btn btn-primary btn-apply lg" href="{{ '/'. LaravelLocalization::getCurrentLocale() .'/jobApplySO/'.$jobDetail->pkAnnouncementsID }}">
                <i class="fa fa-phone"></i>
                {{ trans('text_lang.shareToPhone') }}
            </a>
        </div>
    </div>
@endif
<?php
}
?>
<div class="col-xs-12 col-sm-2" id="ibBtnShare" value-pkAnnouncementsID="{{ $jobDetail->pkAnnouncementsID }}">
    <a href="{{ Request::url() }}" class="btn btn-apply fb-share-button-c">
        <i class="fa fa-facebook-official"></i>
        {{ trans('text_lang.share') }}
    </a>
    <div class="text-right fb-share-button hidden"
         data-href="{{ Request::url() }}"
         data-layout="button_count"
         data-size="large">
    </div>
</div>
<div class="col-xs-12 visible-xs">
    <a class="btn btn-warning btn-apply" href="{{URL::previous()}}">
        <i class="fa fa-chevron-left"></i>
        {{ trans('text_lang.back')}}
    </a>
</div>