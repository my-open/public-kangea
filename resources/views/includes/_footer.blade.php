<div class="container-fluid hidden">
  <div class="row footer-bg">
      <div class="col-md-12">
          All Rights Reserved by BONGPHEAK.COM
      </div>
  </div>
</div>
<div class="container-fluid footer-navbar ">
  <div class="row">
    <!-- Logo -->
    <div class="col-md-offset-1 col-md-11 footer-navbar-logo">
      @if( Lang::getLocale() == 'en')
        <img src="/images/logo_en_white.png" alt="" style="height: 36px; width: auto;" />
      @else
        <img src="/images/logo_kh_white.png" alt="" style="height: 68px; width: auto;" />
      @endif
    </div>
    <!-- Navigation -->
    <div class="col-md-offset-1 col-md-2">
      <h4>{{trans('text_lang.jobseeker')}}</h4>
      <ul>
        <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/jobseeker/register">{{trans('text_lang.registerAsJobSeeker')}}</a></li>
        <li><a href="#">{{trans('text_lang.informationForJobSeeker')}}</a></li>
        <li><a href="#">{{trans('text_lang.termsOfServices')}}</a></li>
      </ul>
    </div>
    <div class="col-md-2">
      <h4>{{trans('text_lang.employer')}}</h4>
      <ul style="list-style: none; padding-left: 0">
        <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/account/employer/register">{{trans('text_lang.registerAsEmployer')}}</a></li>
        <li><a href="#">{{trans('text_lang.informationForEmployer')}}</a></li>
        <li><a href="#">{{trans('text_lang.termsOfServices')}}</a></li>
      </ul>
    </div>
    <div class="col-md-2">
      <h4>{{trans('text_lang.about')}}</h4>
      <ul style="list-style: none; padding-left: 0">
        <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/aboutBongpheak">{{trans('text_lang.aboutBongpheak')}}</a></li>
        <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/aboutUs">{{trans('text_lang.mission')}}</a></li>
        {{--<li><a href="#">{{trans('text_lang.pressRelease')}}</a></li>--}}
        <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/contactUs">{{trans('text_lang.contactUs')}}</a></li>
      </ul>
    </div>
    <div class="col-md-2 hidden">
      <h4>Dummy</h4>
      <ul style="list-style: none; padding-left: 0">
        <li><a href="#">{{trans('text_lang.abc')}}</a></li>
        <li><a href="#">{{trans('text_lang.cde')}}</a></li>
        <li><a href="#">{{trans('text_lang.efg')}}</a></li>
      </ul>
    </div>
    <div class="col-md-4 hidden">
      <h4>{{trans('text_lang.contactUs')}}</h4>
      <form class="" action="index.html" method="post" class="">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
    </div>
    <!-- Copyright Text -->
    <div class="col-md-offset-1 col-md-5 footer-navbar-logo" style="margin-top: 20px;">
      <p>
        {{trans('text_lang.copyRightBongpheakAllRightsReserved')}}
      </p>
    </div>
    <div class="col-md-5 footer-navbar-logo ezecom_footer">
      <p><a href="http://www.ezecom.com.kh/" target="_blank">{{ trans('text_lang.Powered_by_EZECOM') }}</a></p>
    </div>
    <div class="col-md-1 footer-navbar-logo"></div>
  </div>
</div>
@include('includes._facebook')
{{--<!-- Your share button code -->--}}


<div class="bpAjax-spinner hidden">
  <div class="bpAjax-spinning">
    <div class="spin">
      <div class="loading"></div>
      <div class="msg">
        {{ trans('text_lang.looking_for_jobs_in_your_selected_location')}}
      </div>
    </div>
    <div class="error hidden">
      <div class="msg">
        <i class="fa fa-times"></i> {{ trans('text_lang.yourselectedlocationnotavailable')}}
      </div>
    </div>
    <div class="success hidden">
      <div class="msg">
        <i class="fa fa-check"></i> {{ trans('text_lang.search_completed')}} 
      </div>
    </div>
  </div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86295795-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Hotjar Tracking Code for www.bongpheak.com -->
<script>
  (function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
    h._hjSettings={hjid:398192,hjsv:5};
    a=o.getElementsByTagName('head')[0];
    r=o.createElement('script');r.async=1;
    r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
    a.appendChild(r);
  })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>