@if ( str_contains(Request::path(), 'employer') )
  <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/employer/login') }}">{{ trans('text_lang.login') }}</a></li>
  <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/employer/register') }}">{{ trans('text_lang.register') }}</a></li>
@else {{--( str_contains(Request::path(), 'jobseeker') || Route::current()->getUri() == '/' || Route::current()->getUri() == LaravelLocalization::getCurrentLocale())--}}
  <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/jobseeker/login') }}">{{ trans('text_lang.login') }}</a></li>
  <li><a href="{{ url('/'.LaravelLocalization::getCurrentLocale().'/account/jobseeker/register') }}">{{ trans('text_lang.register') }}</a></li>
@endif
