<meta name="google-site-verification" content="otqr64keFgC5oEFuEjwMJxhVajZWA90pJJ0VquDRS24" />
<link rel="alternate" hreflang="en" href="http://{{ env('HTTP_HOST') }}" />
<link rel="alternate" hreflang="km" href="http://{{ env('HTTP_HOST') }}/kh" />
<link rel="alternate" hreflang="zh" href="http://{{ env('HTTP_HOST') }}/zh" />
<script type="application/ld+json">
  // {
  //   "@context": "http://schema.org",
  //   "@type": "Organization",
  //   "url": "http://www.your-company-site.com",
  //   "contactPoint": [{
  //     "@type": "ContactPoint",
  //     "telephone": "+1-401-555-1212",
  //     "contactType": "customer service"
  //   }]
  // }
</script>
