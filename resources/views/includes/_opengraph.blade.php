

<meta property="fb:app_id"        content="1094203813935482" />
<meta property="og:url"           content="{{ Request::url() }}" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="Bongpheak.com @yield('title')"/>
<meta property="og:description"   content="បង​ភ័ក្រ​គឺ​ជា​សេ​វាកម្ម​ការងារ​ដែល​ជួយ​និយោជក​​ និង​និយោជិត​/កម្មករ​ដែល​មាន​សក្ដានុពល​មាន​ទំនាក់ទំនង​ជា​មួយ​គ្នា​ និង​រចនា​យ៉ាង​ពិសេស​សម្រាប់​វិស័យ​ សំណង់​ រោងចក្រ​​បដិសណ្ឋារកិច្ច​ និង​សន្តិសុខ​​ ដែល​រួម​បញ្ចូល​ទាំង​មាន​ជំនាញ​កម្រិតទាប​និង​ ការងារ​គ្មាន​ជំនាញ" />

@if (trim($__env->yieldContent('og-image')))
  <meta property="og:image"         content= "http://{{ env('HTTP_HOST')}}/images/og/@yield('og-image').jpg" />
@else
  <meta property="og:image"         content= "http://{{ env('HTTP_HOST')}}/images/og_image.png" />
@endif
