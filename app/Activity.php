<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'Activities';
    protected $primaryKey = 'pkActivitiesID';
    protected $fillable = ['fkSubsectorsID', 'activitiesNameEN', 'activitiesNameKH', 'activitiesNameZH', 'activitiesNameTH', 'activitiesOrder', 'activitiesStatus'];
}
