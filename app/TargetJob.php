<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TargetJob extends Model
{
    protected $table = 'TargetJobs';
    protected $primaryKey = 'pkTargetJobsID';
    protected $fillable = ['fkUsersID', 'fkCountriesID', 'fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'fkVillagesID', 'fkZonesID', 'fkSectorsID', 'fkSubsectorsID', 'fkPositionsID'];
}
