<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'Pages';
    protected $primaryKey = 'pkPagesID';
    protected $fillable = [
        'fkPagesSubTitleID', 'pagesSiteTitle', 'pagesSiteKeyword', 'pagesSiteDescription',
        'pagesTitleKH', 'pagesTitleEN', 'pagesDescriptionKH', 'pagesDescriptionEN',
        'pagesUrl', 'pagesPosition', 'pagesOrder', 'pagesStatus'
    ];
}
