<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $table = 'JobExperiences';
    protected $primaryKey = 'pkJobExperiencesID';
    protected $fillable = ['fkUsersID', 'fkCountriesID', 'fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'fkVillagesID', 'fkZonesID', 'fkSectorsID', 'fkSubsectorsID', 'fkPositionsID', 'workExperience', 'jobExperiencesDescriptionEN', 'jobExperiencesDescriptionKH'];
}
