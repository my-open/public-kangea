<?php

function dateConvertNormal($strDate){
    $str_DAYnum=date("d",strtotime($strDate));
    $str_YEARname=date("Y",strtotime($strDate));
    $str_MONname = trans('text_lang.'.strtolower( date( "F",strtotime($strDate) ) ) ) ;

    return ($str_DAYnum."-".$str_MONname."-".$str_YEARname);
}

function dateConvert($strDate){
    $str_DAYnum=date("d",strtotime($strDate));
    $str_MONname = trans('text_lang.'.strtolower( date( "F",strtotime($strDate) ) ) ) ;
    $str_YEARname=date("Y",strtotime($strDate));

    if( Lang::getLocale() == 'en'){
        return ($str_DAYnum." ".trans('text_lang.of')." ".$str_MONname." ".trans('text_lang.of')." ".$str_YEARname);
    }elseif( Lang::getLocale() == 'kh'){
        return ( trans('text_lang.dateTe')." ".$str_DAYnum." ".trans('text_lang.month').$str_MONname." ".trans('text_lang.year')." ".$str_YEARname);
    }

}

function fnGetLocation($province, $district, $commune='', $village=''){
    $locations = [
        'province' => $province,
        'district' => $district,
        'commune' => $commune,
        'village' => $village,
    ];

    $strLocation = '';
    foreach($locations as $key => $value){
        if( $value == '' ){
            $$key = null;
            continue;
        }

        $comma = ( $strLocation == '' ) ? '':', ';
        if( Lang::getLocale() == 'en'){
            $strLocation .= $comma.$value. ' '.trans('text_lang.'.$key);
        }elseif( Lang::getLocale() == 'kh'){
            $comma = ( $strLocation == '' ) ? '':', ';
            $strLocation .= $comma.trans('text_lang.'.$key ). ' '.$value;
        }else{
            $strLocation .= $comma.$value. ' '.trans('text_lang.'.$key);
        }
    }
    return $strLocation;
}

function provinceFn($province, $district, $commune, $village){
    if( Lang::getLocale() == 'en'){
        return ($province." ".trans('text_lang.province').", ".$district." ".trans('text_lang.district').", ".$commune." ".trans('text_lang.commune').", ".$village." ".trans('text_lang.village') );
    }elseif( Lang::getLocale() == 'kh'){
        return ( trans('text_lang.province')." ".$province." ".trans('text_lang.district')." ".$district.trans('text_lang.commune')." ".$commune." ".trans('text_lang.village').$village );
    }
}

function isExistMp3( $linkFileMp3 ){
    $file = false;
    if ( is_file($linkFileMp3) === true){
        $file = true;
    }
    return $file;
}

function fnGetGender($sex, $name){
    if( $sex != '' ){
        $sex = config("constants.GENDER")[$sex][Lang::getLocale()];
    }
    $fName = $sex.' '.$name;
    if( Lang::getLocale() == 'zh' ){
        $fName = $name.' '.$sex;
    }
    return $fName;
}

function isSudoAsEmp(){
    $rel = false;
    $user = DB::table('users')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->select('users.*', 'role_user.role_id')
        ->where('users.id', '=', Auth::id() )
        ->whereIn('role_user.role_id', [1, 2])
        ->where('users.status', 1)
        ->first();

    if( $user->companyLevel == 0 && $user->role_id == 2){
        $rel = true;
    }
    return $rel;
}

function isAdmin(){
    $rel = false;
    $user = DB::table('users')
        ->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->select('users.*', 'role_user.role_id')
        ->where('users.id', '=', Auth::id() )
        ->whereIn('role_user.role_id', [1, 2])
        ->where('users.status', 1)
        ->first();

    if( isset($user) && $user->companyLevel == 0 ){
        $rel = true;
    }
    return $rel;
}

function fnIsSale(){
    $rel = false;
    $user = Auth::user();
    if( isset($user) && $user->IsSale == true ){
        $rel = true;
    }
    return $rel;
}

function fnUserLog($data){
    if( \BrowserDetect::isMobile() ){
        $deviceType = 'phone';
    }elseif( \BrowserDetect::isTablet() ){
        $deviceType = 'tablet';
    }elseif( \BrowserDetect::isDesktop() ){
        $deviceType = 'computer';
    }else{
        $deviceType = 'other';
    }

    $data['usersLogsIP'] = \Request::ip();
    $data['usersLogsBrowser'] = \BrowserDetect::browserFamily();
    $data['usersLogsOS'] = \BrowserDetect::osFamily();
    $data['usersLogsDeviceType'] = $deviceType;
    $data['usersLogsDeviceModel'] = \BrowserDetect::deviceModel();
    $userLog = App\UserLog::create($data);
    return $userLog;
}

function fnSearchLog($data){
    $user = Auth::user();

    if( isset( $user ) && $user->hasRole('jobseeker') ){
        $data['fkUsersID'] = Auth::id();
    }

    if( \BrowserDetect::isMobile() ){
        $deviceType = 'phone';
    }elseif( \BrowserDetect::isTablet() ){
        $deviceType = 'tablet';
    }elseif( \BrowserDetect::isDesktop() ){
        $deviceType = 'computer';
    }else{
        $deviceType = 'other';
    }

    $data['searchLogsIP'] = \Request::ip();
    $data['searchLogsBrowser'] = \BrowserDetect::browserFamily();
    $data['searchLogsOS'] = \BrowserDetect::osFamily();
    $data['searchLogsDeviceType'] = $deviceType;
    $data['searchLogsDeviceModel'] = \BrowserDetect::deviceModel();
    $searchLog = App\SearchLog::create($data);
    return $searchLog;
}

function fnJobLog($data){
    $user = Auth::user();

    if( isset( $user ) && $user->hasRole('jobseeker') ){
        $data['fkUsersID'] = Auth::id();
    }

    if( \BrowserDetect::isMobile() ){
        $deviceType = 'phone';
    }elseif( \BrowserDetect::isTablet() ){
        $deviceType = 'tablet';
    }elseif( \BrowserDetect::isDesktop() ){
        $deviceType = 'computer';
    }else{
        $deviceType = 'other';
    }

    $data['jobLogsIP'] = \Request::ip();
    $data['jobLogsBrowser'] = \BrowserDetect::browserFamily();
    $data['jobLogsOS'] = \BrowserDetect::osFamily();
    $data['jobLogsDeviceType'] = $deviceType;
    $data['jobLogsDeviceModel'] = \BrowserDetect::deviceModel();
    $searchLog = App\JobLog::create($data);
    return $searchLog;
}

function funGetProvincePosition(){
    $lang = strtoupper(Lang::getLocale());
    $strWhere = 'tblAnnouncements.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsPublishDate <= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsStatus = 1';

    $countJobByProvinces = DB::select(
        DB::raw("SELECT pkProvincesID, provincesName{$lang}, count(pkAnnouncementsID) as Total_Announcement
                    FROM `tblProvinces`
                    LEFT JOIN (SELECT * FROM tblAnnouncements WHERE $strWhere) b
                    on b.`fkProvincesID` = `pkProvincesID`
                    group by `tblProvinces`.`pkProvincesID`
                    order BY Total_Announcement  DESC ")
    );

    echo "<select name='fkProvincesID' class='form-control' id='fkProvincesID'>";
        echo "<option value='0'>".trans('text_lang.selectOptionProvince')."</option>";
        foreach($countJobByProvinces as $key => $val ){
            if($val->Total_Announcement != 0)
                echo "<option value='$val->pkProvincesID'>".object_get($val, "provincesName{$lang}")." (".$val->Total_Announcement.")</option>";
            else
                echo "<option value='$val->pkProvincesID' disabled='disabled'>".object_get($val, "provincesName{$lang}")."</option>";
        }
    echo "</select>";
}

function fnIsSaleTeam(){
    $user = \Illuminate\Support\Facades\Auth::user();
    $saleTeam = false;
    if( $user->IsSale == true ){
        $saleTeam = true;
    }
    return $saleTeam;
}

function fnConvertSlug( $string){
    return str_slug($string);
}