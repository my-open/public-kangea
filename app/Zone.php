<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'Zones';
    protected $primaryKey = 'pkZonesID';
    protected $fillable = ['fkCountriesID', 'fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'fkVillagesID', 'zonesNameEN', 'zonesNameKH', 'zonesStatus'];
}
