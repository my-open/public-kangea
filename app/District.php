<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'Districts';
    protected $primaryKey = 'pkDistrictsID';
    protected $fillable = ['pkDistrictsID','fkProvincesID', 'districtsNameEN', 'districtsNameKH', 'districtsNameZH', 'districtsNameTH', 'IsDistrict', 'districtsStatus'];
}
