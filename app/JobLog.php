<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobLog extends Model
{
    protected $table = 'JobLogs';
    protected $primaryKey = 'pkJobLogsID';
    protected $fillable = ['fkUsersID', 'fkAnnouncementsID', 'jobLogsType', 'jobLogsIP', 'jobLogsBrowser', 'jobLogsOS', 'jobLogsDeviceType', 'jobLogsDeviceModel', 'jobLogsStatus'];
}
