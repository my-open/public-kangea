<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;
use DB;

class Postjob extends Model
{
    protected $table = 'Announcements';
    protected $primaryKey = 'pkAnnouncementsID';
    protected $fillable = [
        'fkCompaniesID', 'fkSectorsID', 'fkSubsectorsID', 'fkActivitiesID', 'fkPositionsID', 'fkPositionsCode',
        'fkCountriesID', 'fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'fkVillagesID', 'fkZonesID',
        'announcementsTitleEN', 'announcementsTitleKH', 'announcementsHiring',
        'announcementsSalaryFrom', 'announcementsSalaryTo', 'announcementsSalaryType', 'AnnouncementsSalaryDependsOn',
        'AnnouncementsFromDay', 'AnnouncementsToDay',
        'announcementsCertificate', 'announcementsBenefit', 'announcementsLanguage', 'announcementsLanguageLevel',
        'AnnouncementsFromHour', 'AnnouncementsFromMinute', 'AnnouncementsFromType',
        'AnnouncementsToHour', 'AnnouncementsToMinute', 'AnnouncementsToType',
        'AnnouncementsBreakHoursFromHour', 'AnnouncementsBreakHoursFromMinute', 'AnnouncementsBreakHoursFromType',
        'AnnouncementsBreakHoursToHour', 'AnnouncementsBreakHoursToMinute', 'AnnouncementsBreakHoursToType',
        'AnnouncementsExperiencePositionN', 'AnnouncementsPositionType',
        'AnnouncementsExperienceSectorN', 'AnnouncementsExperienceOrAnd', 'AnnouncementsSectorType',
        'AnnouncementsGender', 'AnnouncementsAgeFrom', 'AnnouncementsAgeUntil', 'announcementsAllowWithoutExperience', 'announcementsRequiredDocType',
        'announcementsPublishDate', 'announcementsClosingDate','announcementsSalaryNegotiate', 'announcementsIsFullTime', 'announcementsWorkTime',
        'announcementsStatus'
    ];

    public function convertConfirmJobApplyMp3($job){
        $companyNameMp3 = public_path('files/sounds/kh/companies-name/company_n_' . $job->fkCompaniesID . '_kh.mp3');
        $positionNameMp3 = public_path('files/sounds/kh/positions/' . $job->fkPositionsID . '_kh.mp3');
        $isHasCompanyNameMp3 = isExistMp3($companyNameMp3);
        $isHasPositionNameMp3 = isExistMp3($positionNameMp3);
        if ($isHasCompanyNameMp3 && $isHasPositionNameMp3) {
            if( $job->fkSectorsID != 2 ){
                $get_Contents = file_get_contents(public_path('files/sounds/kh/others/') . 'company.mp3');
                $get_Contents .= file_get_contents(public_path('files/sounds/kh/companies-name/') . 'company_n_' . $job->fkCompaniesID . '_kh.mp3');
            }else{
                $get_Contents = file_get_contents(public_path('files/sounds/kh/companies-name/') . 'company_n_' . $job->fkCompaniesID . '_kh.mp3');
            }

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'position.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/positions/') . $job->fkPositionsID . '_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'in-some-where.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/provinces/') . $job->fkProvincesID . '_kh.mp3');

            file_put_contents(public_path('files/sounds/kh/confirmJobs/') . 'confirmJob_' . $job->pkAnnouncementsID . '.mp3',
                $get_Contents);
        }else{
            $jobMp3 = public_path('files/sounds/kh/confirmJobs/confirmJob_'.$job->pkAnnouncementsID.'.mp3');
            $isJobMp3 = isExistMp3($jobMp3);
            if( $isJobMp3 ){
                unlink( public_path('files/sounds/kh/confirmJobs/confirmJob_' . $job->pkAnnouncementsID . '.mp3') );
            }
        }
    }

    public function convertConfirmJobApplyNoCompanyMp3($job){
        $positionNameMp3 = public_path('files/sounds/kh/positions/' . $job->fkPositionsID . '_kh.mp3');
        $isHasPositionNameMp3 = isExistMp3($positionNameMp3);
        if ($isHasPositionNameMp3) {
            $get_Contents = file_get_contents(public_path('files/sounds/kh/others/') . 'position.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/positions/') . $job->fkPositionsID . '_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'in-some-where.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/provinces/') . $job->fkProvincesID . '_kh.mp3');

            file_put_contents(public_path('files/sounds/kh/confirmJobsNoCompany/') . 'confirmJob_' . $job->pkAnnouncementsID . '.mp3',
                $get_Contents);
        }else{
            $jobMp3 = public_path('files/sounds/kh/confirmJobsNoCompany/confirmJob_'.$job->pkAnnouncementsID.'.mp3');
            $isJobMp3 = isExistMp3($jobMp3);
            if( $isJobMp3 ){
                unlink( public_path('files/sounds/kh/confirmJobsNoCompany/confirmJob_' . $job->pkAnnouncementsID . '.mp3') );
            }
        }
    }

    public function convertJobMp3($job)
    {
        $companyNameMp3 = public_path('files/sounds/kh/companies-name/company_n_' . $job->fkCompaniesID . '_kh.mp3');
        $positionNameMp3 = public_path('files/sounds/kh/positions/' . $job->fkPositionsID . '_kh.mp3');
        $subsectorNameMp3 = public_path('files/sounds/kh/subsectors/' . 'sub' . $job->fkSubsectorsID . '_kh.mp3');
        $activityNameMp3 = public_path('files/sounds/kh/activities/' . $job->fkActivitiesID . '_kh.mp3');
        $isHasCompanyNameMp3 = isExistMp3($companyNameMp3);
        $isHasPositionNameMp3 = isExistMp3($positionNameMp3);
        $isHasSubsectorMp3 = isExistMp3($subsectorNameMp3);
        $isHasActivityMp3 = isExistMp3($activityNameMp3);
        if ($isHasCompanyNameMp3 && $isHasPositionNameMp3 && $isHasSubsectorMp3) {
            $get_Contents = file_get_contents(public_path('files/sounds/kh/companies-name/') . 'company_n_' . $job->fkCompaniesID . '_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_1_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/subsectors/') . 'sub' . $job->fkSubsectorsID . '_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_2_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/sectors/') . 'sector'.$job->fkSectorsID.'_kh.mp3');
            if( $job->fkActivitiesID != 0 && $isHasActivityMp3 ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_3_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/activities/') . $job->fkActivitiesID . '_kh.mp3');
            }
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_4_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/provinces/') . $job->fkProvincesID . '_kh.mp3');

            if ($job->fkDistrictsID > 0) $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/districts/') . $job->fkDistrictsID . '_kh.mp3');
            if (!empty($job->fkCommunesID)) {
                //if( $job->fkCommunesID > 0 ) $get_Contents .= ".". file_get_contents(public_path('files/sounds/kh/communes/').$job->fkCommunesID.'_kh.mp3');
            }

            $get_Contents .= file_get_contents(public_path('files/sounds/kh/companies-name/') . 'company_n_' . $job->fkCompaniesID . '_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_5_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/positions/') . $job->fkPositionsID . '_kh.mp3');
            if ($job->announcementsHiring != '' || $job->announcementsHiring != 0) {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'amount_kh.mp3');
                if (strlen($job->announcementsHiring) <= 2) {
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/number-of-people/') . $job->announcementsHiring . '_people_kh.mp3');
                }else{
                    if (substr($job->announcementsHiring, 1, 3) == '00') {
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/number-of-people/') . substr($job->announcementsHiring, 0, 1) . '00_people_kh.mp3');
                    } else {
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/number-of-people/continue/') . substr($job->announcementsHiring, 0, 1) . '00_kh.mp3');
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/number-of-people/') . substr($job->announcementsHiring, 1, 3) . '_people_kh.mp3');
                    }
                }
            }

            if( isset($job->announcementsSalaryNegotiate) && $job->announcementsSalaryNegotiate == 1 ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'salaryNegotiate.mp3');
            }else{
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_6_kh.mp3');
                if( substr($job->announcementsSalaryFrom, 0, 1) != 0 || substr($job->announcementsSalaryTo, 0, 1) != 0){
                    if (strlen($job->announcementsSalaryFrom) == 2) {
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . $job->announcementsSalaryFrom . '_dolla_kh.mp3');
                    }if(strlen($job->announcementsSalaryFrom) == 1){
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . $job->announcementsSalaryFrom . '_dolla_kh.mp3');
                    } else {
                        if (substr($job->announcementsSalaryFrom, 1, 3) == '00') {
                            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . substr($job->announcementsSalaryFrom, 0, 1) . '00_dolla_kh.mp3');
                        } else {
                            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . substr($job->announcementsSalaryFrom, 0, 1) . '00_kh.mp3');
                            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . substr($job->announcementsSalaryFrom, 1, 3) . '_dolla_kh.mp3');
                        }
                    }

                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_7_kh.mp3');
                    if (strlen($job->announcementsSalaryTo) == 2) {
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . $job->announcementsSalaryTo . '_dolla_kh.mp3');
                    }if( strlen($job->announcementsSalaryTo) == 1 ){
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . $job->announcementsSalaryTo . '_dolla_kh.mp3');
                    } else {
                        if (substr($job->announcementsSalaryTo, 1, 3) == '00') {
                            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . substr($job->announcementsSalaryTo, 0, 1) . '00_dolla_kh.mp3');
                        } else {
                            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . substr($job->announcementsSalaryTo, 0, 1) . '00_kh.mp3');
                            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salaries/') . substr($job->announcementsSalaryTo, 1, 3) . '_dolla_kh.mp3');
                        }
                    }
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_8_kh.mp3');
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/salary-type/') . $job->announcementsSalaryType . '_salary_kh.mp3');

                    if( $job->announcementsSalaryTo != $job->announcementsSalaryFrom ){
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_9_kh.mp3');
                        $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $job->AnnouncementsSalaryDependsOn . '_kh.mp3');
                    }
                }
            }

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_10_kh.mp3');
            if( $job->announcementsIsFullTime == 0 ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'part-time.mp3');
            }else{
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'full-time.mp3');
            }

            $workTimes = json_decode($job->announcementsWorkTime);
            if (count($workTimes) > 0) {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'have-time.mp3');
                foreach ($workTimes as $value) {
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $value . '_kh.mp3');
                }
            }

            /*if( $job->AnnouncementsFromMinute == '00' ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsFromHour . '-00_kh.mp3');
            }else{
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsFromHour . '-00_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsFromMinute . '_minutes_kh.mp3');
            }

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $job->AnnouncementsFromType . '_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_12_kh.mp3');

            if( $job->AnnouncementsToMinute == '00' ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsToHour . '-00_kh.mp3');
            }else{
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsToHour . '-00_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsToMinute . '_minutes_kh.mp3');
            }
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $job->AnnouncementsToType . '_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_14_kh.mp3');

            if( $job->AnnouncementsBreakHoursFromMinute == '00' ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsBreakHoursFromHour . '-00_kh.mp3');
            }else{
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsBreakHoursFromHour . '-00_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsBreakHoursFromMinute . '_minutes_kh.mp3');
            }
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $job->AnnouncementsBreakHoursFromType . '_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_12_kh.mp3');
            if( $job->AnnouncementsBreakHoursToMinute == '00' ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsBreakHoursToHour . '-00_kh.mp3');
            }else{
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsBreakHoursToHour . '-00_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/times/') . $job->AnnouncementsBreakHoursToMinute . '_minutes_kh.mp3');
            }
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $job->AnnouncementsBreakHoursToType . '_kh.mp3');*/

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_18_kh.mp3');

            if ($job->AnnouncementsFromDay == 'mon' && $job->AnnouncementsToDay == 'mon') {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/numbers/') . '1_kh.mp3');
            } elseif ($job->AnnouncementsFromDay == 'mon' && $job->AnnouncementsToDay == 'tue') {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/numbers/') . '2_kh.mp3');
            } elseif ($job->AnnouncementsFromDay == 'mon' && $job->AnnouncementsToDay == 'wed') {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/numbers/') . '3_kh.mp3');
            } elseif ($job->AnnouncementsFromDay == 'mon' && $job->AnnouncementsToDay == 'thu') {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/numbers/') . '4_kh.mp3');
            } elseif ($job->AnnouncementsFromDay == 'mon' && $job->AnnouncementsToDay == 'fri') {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/numbers/') . '5_kh.mp3');
            } elseif ($job->AnnouncementsFromDay == 'mon' && $job->AnnouncementsToDay == 'sat') {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/numbers/') . '6_kh.mp3');
            } elseif ($job->AnnouncementsFromDay == 'mon' && $job->AnnouncementsToDay == 'sun') {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/numbers/') . '7_kh.mp3');
            }
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_19_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_20_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_21_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/dates/days/') . $job->AnnouncementsFromDay . '_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_22_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/dates/days/') . $job->AnnouncementsToDay . '_kh.mp3');

            $benefits = json_decode($job->announcementsBenefit);
            if (count($benefits) > 0) {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_23_kh.mp3');
                foreach ($benefits as $value) {
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/benefits/') . $value . '_kh.mp3');
                }
            }

            if ($job->AnnouncementsExperiencePositionN == 0 && $job->AnnouncementsExperienceSectorN == 0) {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_24a_kh.mp3');
            } elseif ($job->AnnouncementsExperiencePositionN != 0 && $job->AnnouncementsExperienceSectorN == 0) {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_24_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/positions/') . $job->fkPositionsID . '_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_25a_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/experiences/') . $job->AnnouncementsExperiencePositionN . $job->AnnouncementsPositionType . '_kh.mp3');
            } elseif ($job->AnnouncementsExperiencePositionN == 0 && $job->AnnouncementsExperienceSectorN != 0) {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_26_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/sectors/') . 'sector' . $job->fkSectorsID . '_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_25a_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/experiences/') . $job->AnnouncementsExperienceSectorN . $job->AnnouncementsSectorType . '_kh.mp3');
            } else {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_24_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/positions/') . $job->fkPositionsID . '_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_25a_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/experiences/') . $job->AnnouncementsExperiencePositionN . $job->AnnouncementsPositionType . '_kh.mp3');

                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $job->AnnouncementsExperienceOrAnd . '_experience_in_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/sectors/') . 'sector' . $job->fkSectorsID . '_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_25a_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/experiences/') . $job->AnnouncementsExperienceSectorN . $job->AnnouncementsSectorType . '_kh.mp3');
            }

            //$get_Contents .= ".". file_get_contents(public_path('files/sounds/kh/others/').'job_a_28_kh.mp3');

            $doc = json_decode($job->announcementsCertificate);
            if (count($doc) > 0) {
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_28a_kh.mp3');
                foreach ($doc as $value) {
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/documents/') . $value . '_kh.mp3');
                }
            }

            $languages = json_decode($job->announcementsLanguage);
            $languageLevels = json_decode($job->announcementsLanguageLevel);
            if (count($languages) > 0) {
                foreach ($languages as $key => $value) {
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . $value . '.mp3');
                    $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'lang_' . object_get($languageLevels, $key) . '_kh.mp3');
                }
            }

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'gender_' . $job->AnnouncementsGender . '_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_29_a_kh.mp3');

            if( $job->AnnouncementsAgeUntil == $job->AnnouncementsAgeFrom ){
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/ages/') . 'age_' . $job->AnnouncementsAgeUntil . '_kh.mp3');
            }else{
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/ages/') . 'age_' . $job->AnnouncementsAgeFrom . '_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_29_b_kh.mp3');
                $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/ages/') . 'age_' . $job->AnnouncementsAgeUntil . '_kh.mp3');
            }

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/others/') . 'job_a_30_kh.mp3');

            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/dates/dates/') . 'd_' . date("d", strtotime($job->announcementsClosingDate)) . '_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/dates/months/') . 'm_' . date("m", strtotime($job->announcementsClosingDate)) . '_kh.mp3');
            $get_Contents .= "." . file_get_contents(public_path('files/sounds/kh/dates/years/') . 'y_' . date("Y", strtotime($job->announcementsClosingDate)) . '_kh.mp3');

            file_put_contents(public_path('files/sounds/kh/jobs/') . 'job_' . $job->pkAnnouncementsID . '.mp3',
                $get_Contents);
        }else{
            $jobMp3 = public_path('files/sounds/kh/jobs/job_'.$job->pkAnnouncementsID.'.mp3');
            $isJobMp3 = isExistMp3($jobMp3);
            if( $isJobMp3 ){
                unlink( public_path('files/sounds/kh/jobs/job_' . $job->pkAnnouncementsID . '.mp3') );
            }
        }
    }

    //BP New version
    public static function repost($id){
        $date = date("Y-m-d");
        $date_add_1month = strtotime(date("Y-m-d", strtotime($date)) . " +1 month");
        $date_add_1month = date("Y-m-d 00:00:00", $date_add_1month);

        $post = Postjob::findOrFail($id)->replicate();
        $post->save();

        DB::table('Announcements')
            ->where('pkAnnouncementsID', $post->pkAnnouncementsID)
            ->update(['announcementsPublishDate' => date("Y-m-d"), 'announcementsClosingDate' => $date_add_1month]);
    }
}
