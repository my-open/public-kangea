<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'Provinces';
    protected $primaryKey = 'pkProvincesID';
    protected $fillable = ['pkProvincesID', 'fkCountriesID', 'provincesNameEN', 'provincesNameKH', 'provincesNameZH', 'provincesNameTH', 'Iscity', 'provincesStatus'];
}
