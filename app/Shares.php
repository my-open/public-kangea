<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shares extends Model
{
    protected $table = 'Shares';
    protected $primaryKey = 'pkSharesID';
    protected $fillable = [
        "fkAnnouncementsID", "sharesGender", "sharesName", "sharesPhone", "sharesWho",
        "sharesRetryTime", "sharesRetryDate", "sharesApplyRecord", "sharesTime", "sharesDuration", "sharesStatusCall", "sharesDate", "sharesStatus", "sharesInteraction", "sharesType",
        "fbID", "fbGender", "fbName", "fbPhone", "fbEmail", "sharesAppliedOrNot"
    ];
}