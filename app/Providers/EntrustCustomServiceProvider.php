<?php
namespace App\Providers;

class EntrustCustomServiceProvider extends \Zizaco\Entrust\EntrustServiceProvider
{

    public function boot()
    {
        parent::boot();
        $this->bladeDirectives();
    }

    public function register()
    {
        parent::register();
    }

    private function bladeDirectives()
    {
        \Blade::directive('role', function($expression){
            return "<?php if (\\Entrust::hasRole{$expression}): ?>";
        });

        \Blade::directive('endrole', function($expression){
            return "<?php endif; // Entrust::hasRole ?>";
        });

        \Blade::directive('permission', function($expression){
            return "<?php if (\\Entrust::can{$expression}) : ?>";
        });

        \Blade::directive('endpermission', function($expression){
            return "<?php endif; //Entrust::can ?>";
        });

        \Blade::directive('ability', function($expression){
            return "<?php if (\\Entrust::ability{$expression}) : ?>";
        });

        \Blade::directive('endability', function($expression){
            return "<?php endif; //Entrust::ability ?>";
        });
    }
}