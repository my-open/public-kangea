<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'Companies';
    protected $primaryKey = 'pkCompaniesID';
    protected $fillable = [
        'fkSectorsID', 'fkSubsectorsID', 'fkActivitiesID', 'companiesCode',
        'companiesNameEN', 'companiesNameKH', 'companiesNameZH', 'companiesNameTH',
        'companiesNickName', 'companiesNumberOfWorker',
        'fkZonesID', 'fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'fkVillagesID', 'companiesAddress', 'companiesXCoordinate',
        'companiesYCoordinate', 'companiesDescriptionEN', 'companiesDescriptionKH', 'companiesDescriptionZH', 'companiesDescriptionTH',
        'companiesPhone', 'companiesEmail', 'companiesLogo', 'companiesSite', 'companiesStatus'
    ];
}

