<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'Sectors';
    protected $primaryKey = 'pkSectorsID';
    protected $fillable = ['sectorsNameEN', 'sectorsNameKH', 'sectorsNameZH', 'sectorsNameTH', 'sectorsPhoto', 'sectorsStatus'];
}
