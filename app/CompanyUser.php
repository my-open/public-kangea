<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyUser extends Model
{
    protected $table = 'CompanyUsers';
    protected $primaryKey = 'fkUsersID';
    protected $fillable = ['fkCompaniesID', 'fkUsersID'];
}
