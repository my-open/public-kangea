<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserLog extends Model
{
    protected $table = 'UserLogs';
    protected $primaryKey = 'pkUserLogsID';
    protected $fillable = ['fkUsersID', 'userLogsActivity', 'userLogsActivityDescription', 'usersLogsIP', 'usersLogsBrowser', 'usersLogsOS', 'usersLogsDeviceType', 'usersLogsDeviceModel', 'usersLogsStatus'];

    public static function IsLoggedLogin(){
        if (Auth::check()) {
            if (!UserLog::whereDate('created_at', '=', date('Y-m-d'))->where('fkUsersID', Auth::id())->where('userLogsActivity', 'access')->exists()) {
                $comID = DB::table('CompanyUsers')->where('fkUsersID', Auth::id())->value('fkCompaniesID');
                $data['fkUsersID'] = Auth::id();
                $data['userLogsActivity'] = 'access';
                $data['userLogsActivityDescription'] = 'access';
                if( $comID ){
                    $activityDes['fkUsersID'] = Auth::id();
                    $activityDes['fkCompaniesID'] = $comID;
                    $data['userLogsActivityDescription'] = json_encode( $activityDes );
                }
                fnUserLog($data);
            }
        }
    }
}
