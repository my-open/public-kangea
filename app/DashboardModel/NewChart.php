<?php

namespace App\DashboardModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class NewChart extends Model
{
    public static function getAllNew($from,$to,$fkProvincesID,$fkDistrictsID,$fkCommunesID,$fkSectorID,$unit)
    {
        //if user filter start day
        if($from == null || $from <= '2016-10-19' ){
            $startFrom = '2016-10-19';
            $operator = '<=';
        }else if($from > '2016-10-19'){
            $startFrom = $from;
            $operator = '=';
        }
        //unit 0:day;1:month
        if($unit == 0){
            $whereUnit = 'day';
            $whereUnitAppPhone = 'day';
            $whereUnitJobView = 'day';
            $whereUnitShares = 'day';
            $whereUnitJobLogs = 'day';
        }else{
            $whereUnit = DB::raw("MONTH(created_at)");
            $whereUnitAppPhone = DB::raw("MONTH(tblJobApply.created_at)");
            $whereUnitJobView = DB::raw("MONTH(tblJobLogs.created_at)");
            $whereUnitShares = DB::raw("MONTH(tblShares.created_at)");
            $whereUnitJobLogs = DB::raw("MONTH(tblJobLogs.created_at)");
        }
        if($unit == 0){
        }else{
        }
        //New companies
        $data = array();
        $data['FromStartDate'] = $startFrom;
        if($fkProvincesID != null) {
        //New company
            $companiesFromStart =  $companies = DB::table('Companies')
                ->select(DB::raw('pkCompaniesID as totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('created_at',$operator,$startFrom)
                ->count();
            $companies = DB::table('Companies')
                ->select(DB::raw('COUNT(`pkCompaniesID`) as totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('created_at','>',$from)
                ->whereDate('created_at','<=',$to)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        //New announcement
            $announcementsFromStart = DB::table('Announcements')
                ->select(DB::raw('pkAnnouncementsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkProvincesID', $fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('created_at',$operator,$startFrom)
                ->count();
            $announcements = DB::table('Announcements')
                ->select(DB::raw('COUNT(`pkAnnouncementsID`)as totlePerDay,DATE(created_at) as day'))
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('created_at','>',$from)
                ->whereDate('created_at','<=',$to)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            //applicant Web
            $applicantsWebFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('JobApply.created_at',$operator,$startFrom)
                ->where('jobApplyBy', 1)
                ->count();
            $applicantsWeb = DB::table('JobApply')
                ->select(DB::raw('count(pkJobApplyID) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID', $fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('JobApply.created_at','>',$from)
                ->whereDate('JobApply.created_at','<=',$to)
                ->where('jobApplyBy', 1)
                ->groupBy($whereUnitAppPhone)
                ->orderBy('day')
                ->get();
            // Applicant Phone
            $applicantsPhoneFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('JobApply.created_at',$operator,$startFrom)
                ->where('jobApplyBy', 2)
                ->count();
            $applicantsPhone = DB::table('JobApply')
                ->select(DB::raw('COUNT(`pkJobApplyID`) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('JobApply.created_at','>',$from)
                ->whereDate('JobApply.created_at','<=',$to)
                ->where('jobApplyBy', 2)
                ->orderBy('day')
                ->get();
            //new Other Search
            if($fkProvincesID == 0 ){
                $otherSearchesFromStart = DB::table('SearchLogs')
                    ->select(DB::raw(' pkSearchLogsID as totlePerDay,DATE(created_at) as day'))
                    ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                    ->where('fkProvincesID','<>',0)
                    ->where('fkSectorsID','<>',0)
                    ->where('created_at',$operator,$startFrom)
                    ->count();
                $otherSearches = DB::table('SearchLogs')
                    ->select(DB::raw(' COUNT(`pkSearchLogsID`) as totlePerDay,DATE(created_at) as day'))
                    ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                    ->where('fkProvincesID','<>',0)
                    ->where('fkSectorsID','<>',0)
                    ->whereBetween('created_at',array($from,$to))
                    ->groupBy($whereUnit)
                    ->orderBy('day')
                    ->get();
            }else{
                $otherSearchesFromStart = DB::table('SearchLogs')
                    ->select(DB::raw(' pkSearchLogsID as totlePerDay,DATE(created_at) as day'))
                    ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                    ->where('fkProvincesID',$fkProvincesID)
                    ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                    ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                    ->where('fkSectorsID','<>',0)
                    ->where('created_at',$operator,$startFrom)
                    ->count();
                $otherSearches = DB::table('SearchLogs')
                    ->select(DB::raw(' COUNT(`pkSearchLogsID`) as totlePerDay,DATE(created_at) as day'))
                    ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                    ->where('fkProvincesID',$fkProvincesID)
                    ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                    ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                    ->where('fkSectorsID','<>',0)
                    ->whereBetween('created_at',array($from,$to))
                    ->groupBy($whereUnit)
                    ->orderBy('day')
                    ->get();
            }
           //New job view
            $jobViewsFromStart = DB::table('JobLogs')
                ->select(DB::raw('pkJobLogsID AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID', $fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->where('JobLogs.created_at',$operator,$startFrom)
                ->count();
            $jobViews = DB::table('JobLogs')
                ->select(DB::raw('COUNT(`pkJobLogsID`) AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('JobLogs.created_at', array($from, $to))
                ->groupBy($whereUnitJobView)
                ->orderBy('day')
                ->get();
            //New Phone Shares
            $phoneSharesFromStart = DB::table('Shares')
                ->select(DB::raw('pkSharesID as totlePerDay,DATE(tblShares.created_at) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'Shares.fkAnnouncementsID')
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('Shares.created_at',$operator,$startFrom)
                ->count();
            $phoneShares = DB::table('Shares')
                ->select(DB::raw('COUNT(`pkSharesID`)as totlePerDay,DATE(tblShares.created_at) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'Shares.fkAnnouncementsID')
                ->where('fkProvincesID','like',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('Shares.created_at','>',$from)
                ->whereDate('Shares.created_at','<=',$to)
                ->groupBy($whereUnitShares)
                ->orderBy('day')
                ->get();
            //New fb Shares
            $fbSharesFromStart = DB::table('JobLogs')
                ->select(DB::raw('pkJobLogsID as totlePerDay,DATE(tblJobLogs.created_at) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('fkProvincesID','like',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('jobLogsType','=', 'shareFB')
                ->where('announcementsStatus', 1)
                ->whereDate('JobLogs.created_at',$operator,$startFrom)
                ->count();
            $fbShares = DB::table('JobLogs')
                ->select(DB::raw('COUNT(`pkJobLogsID`)as totlePerDay,DATE(tblJobLogs.created_at) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('fkProvincesID','like',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('jobLogsType','=', 'shareFB')
                ->where('announcementsStatus', 1)
                ->whereDate('JobLogs.created_at','>',$from)
                ->whereDate('JobLogs.created_at','<=',$to)
                ->groupBy($whereUnitJobLogs)
                ->orderBy('day')
                ->get();
        }else{
            //New company
            $companiesFromStart =  $companies = DB::table('Companies')
                ->select(DB::raw('pkCompaniesID as totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('created_at',$operator,$startFrom)
                ->count();
            $companies = DB::table('Companies')
                ->select(DB::raw('COUNT(`pkCompaniesID`) as totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('created_at','>',$from)
                ->whereDate('created_at','<=',$to)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            //New announcement
            $announcementsFromStart = DB::table('Announcements')
                ->select(DB::raw('pkAnnouncementsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkProvincesID', $fkProvincesID)
                ->where('announcementsStatus', 1)
                ->whereDate('created_at',$operator,$startFrom)
                ->count();
            $announcements = DB::table('Announcements')
                ->select(DB::raw('COUNT(`pkAnnouncementsID`)as totlePerDay,DATE(created_at) as day'))
                ->where('announcementsStatus', 1)
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('created_at','>',$from)
                ->whereDate('created_at','<=',$to)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            //applicant web
            $applicantsWeb = DB::table('JobApply')
                ->select(DB::raw('count(pkJobApplyID) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus',1)
                ->whereDate('JobApply.created_at','>',$from)
                ->whereDate('JobApply.created_at','<=',$to)
                ->where('jobApplyBy', 1)
                ->groupBy($whereUnitAppPhone)
                ->orderBy('day')
                ->get();
            $applicantsWebFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus',1)
                ->whereDate('JobApply.created_at',$operator,$startFrom)
                ->where('jobApplyBy', 1)
                ->count();
            //applicant phone
            $applicantsPhoneFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->where('announcementsStatus', 1)
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('JobApply.created_at',$operator,$startFrom)
                ->where('jobApplyBy', 2)
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->count();
            $applicantsPhone = DB::table('JobApply')
                ->select(DB::raw('COUNT(`pkJobApplyID`) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->where('announcementsStatus', 1)
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereDate('JobApply.created_at','>',$from)
                ->whereDate('JobApply.created_at','<=',$to)
                ->where('jobApplyBy', 2)
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            //new Other Search
            $otherSearchesFromStart = DB::table('SearchLogs')
                ->select(DB::raw(' pkSearchLogsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('fkProvincesID','<>',0)
                ->where('fkSectorsID','<>',0)
                ->where('created_at',$operator,$startFrom)
                ->count();
            $otherSearches = DB::table('SearchLogs')
                ->select(DB::raw(' COUNT(`pkSearchLogsID`) as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('fkProvincesID','<>',0)
                ->where('fkSectorsID','<>',0)
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            $mainSearchesFromStart = DB::table('SearchLogs')
                ->select(DB::raw(' pkSearchLogsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('fkProvincesID',0)
                ->where('fkSectorsID',0)
                ->where('created_at',$operator,$startFrom)
                ->count();
            $mainSearches = DB::table('SearchLogs')
                ->select(DB::raw(' COUNT(`pkSearchLogsID`) as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('fkProvincesID',0)
                ->where('fkSectorsID',0)
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            $data['mainSearches'] = $mainSearches;
            $data['mainSearchesFromStart'] = $mainSearchesFromStart;
            //New Job Seeker
            $jobSeekersFromStart = DB::table('users')
                ->select(DB::raw('*'))
                ->join('role_user','users.id','=','role_user.user_id')
                ->where('role_id',3)
                ->whereDate('created_at',$operator,$startFrom)
                ->count();
            $jobSeekers = DB::table('users')
                ->select(DB::raw('COUNT(tblusers.id) AS totlePerDay,DATE(created_at) as day '))
                ->join('role_user','users.id','=','role_user.user_id')
                ->where('role_id',3)
                ->whereDate('created_at','>',$from)
                ->whereDate('created_at','<=',$to)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            $data['jobSeekersFromStart'] = $jobSeekersFromStart;
            $data['jobSeekers'] = $jobSeekers;
            //new job view
            $jobViewsFromStart = DB::table('JobLogs')
                ->select(DB::raw('pkJobLogsID AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('JobLogs.created_at',$operator,$startFrom)
                ->where('announcementsStatus', 1)
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->count();
            $jobViews = DB::table('JobLogs')
                ->select(DB::raw('COUNT(`pkJobLogsID`) AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('JobLogs.created_at', array($from, $to))
                ->where('announcementsStatus', 1)
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->groupBy($whereUnitJobView)
                ->orderBy('day')
                ->get();
            //New Phone Shares
            $phoneSharesFromStart = DB::table('Shares')
                ->select(DB::raw('pkSharesID as totlePerDay,DATE(tblShares.created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('Shares.created_at',$operator,$startFrom)
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'Shares.fkAnnouncementsID')
                ->count();
            $phoneShares = DB::table('Shares')
                ->select(DB::raw('COUNT(`pkSharesID`)as totlePerDay,DATE(tblShares.created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereDate('Shares.created_at','>',$from)
                ->whereDate('Shares.created_at','<=',$to)
                ->groupBy($whereUnitShares)
                ->orderBy('day')
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'Shares.fkAnnouncementsID')
                ->get();
            //New Phone Shares
            $fbSharesFromStart = DB::table('JobLogs')
                ->select(DB::raw('pkJobLogsID as totlePerDay,DATE(tblJobLogs.created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('jobLogsType','=', 'shareFB')
                ->where('announcementsStatus', 1)
                ->whereDate('JobLogs.created_at',$operator,$startFrom)
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->count();
            $fbShares = DB::table('JobLogs')
                ->select(DB::raw('COUNT(`pkJobLogsID`)as totlePerDay,DATE(tblJobLogs.created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('jobLogsType','=', 'shareFB')
                ->where('announcementsStatus', 1)
                ->whereDate('JobLogs.created_at','>',$from)
                ->whereDate('JobLogs.created_at','<=',$to)
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->groupBy($whereUnitJobLogs)
                ->orderBy('day')
                ->get();
        }
        $data['companies'] = $companies;
        $data['companiesFromStart'] = $companiesFromStart;
        $data['announcements'] = $announcements;
        $data['announcementsFromStart'] = $announcementsFromStart;
        $data['applicantsWebFromStart'] = $applicantsWebFromStart;
        $data['applicantsWeb'] = $applicantsWeb;
        $data['applicantsPhoneFromStart'] = $applicantsPhoneFromStart;
        $data['applicantsPhone'] = $applicantsPhone;
        $data['otherSearches'] = $otherSearches;
        $data['otherSearchesFromStart'] = $otherSearchesFromStart;
        $data['jobViewsFromStart'] = $jobViewsFromStart;
        $data['jobViews'] = $jobViews;
        $data['phoneShares'] = $phoneShares;
        $data['phoneSharesFromStart'] = $phoneSharesFromStart;
        $data['fbShares'] = $fbShares;
        $data['fbSharesFromStart'] = $fbSharesFromStart;
        return $data;
    }
}
