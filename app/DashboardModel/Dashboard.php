<?php

namespace App\DashboardModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Dashboard extends Model
{
    public static function getAllAccumulative($from,$to,$fkProvincesID,$fkDistrictsID,$fkCommunesID,$fkSectorID,$unit)
    {
        //if user filter start day
        $startFrom = '2015-01-01';
        //unit 0:day;1:month
        if($unit == 0){
            $whereUnit = 'day';
        }else{
            $whereUnit = DB::raw("MONTH(created_at)");
        }
        $data = array();
        if($fkProvincesID != null) {
        //companies Accumulative
            $companiesFromStart =  $companies = DB::table('Companies')
                ->select(DB::raw('pkCompaniesID as  totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($startFrom,$from))
                ->count();
            $companies = DB::table('Companies')
                ->select(DB::raw('COUNT(`pkCompaniesID`) as totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            //Announcement Accumulative
            $announcementsFromStart = DB::table('Announcements')
                ->select(DB::raw('pkAnnouncementsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('created_at',array($startFrom,$from))
                ->count();
            $announcements = DB::table('Announcements')
                ->select(DB::raw('COUNT(`pkAnnouncementsID`)as totlePerDay,DATE(created_at) as day'))
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        //Searches Accumulative
            $searchesFromStart = DB::table('SearchLogs')
                ->select(DB::raw(' pkSearchLogsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkProvincesID','like',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($startFrom,$from))
                ->count();
            $searches = DB::table('SearchLogs')
                ->select(DB::raw(' COUNT(`pkSearchLogsID`) as totlePerDay,DATE(created_at) as day'))
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        //applicants web Accumulative
            $applicantsWeb = DB::table('JobApply')
                ->select(DB::raw('COUNT(`pkJobApplyID`) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('JobApply.created_at', array($from, $to))
                ->where('announcementsStatus', 1)
                ->where('jobApplyBy', 1)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            $applicantsWebFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('JobApply.created_at', array($startFrom,$from))
                ->where('jobApplyBy', 1)
                ->count();
        //applicants Phone Accumulative
            $applicantsPhoneFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('JobApply.created_at', array($startFrom, $from))
                ->where('jobApplyBy', 2)
                ->count();
            $applicantsPhone = DB::table('JobApply')
                ->select(DB::raw('COUNT(`pkJobApplyID`) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('JobApply.created_at', array($from, $to))
                ->where('jobApplyBy', 2)
                ->orderBy('day')
                ->get();
            $jobViewsFromStart = DB::table('JobLogs')
                ->select(DB::raw('pkJobLogsID AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('JobLogs.created_at', array($startFrom, $from))
                ->count();
            $jobViews = DB::table('JobLogs')
                ->select(DB::raw('COUNT(`pkJobLogsID`) AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('JobLogs.jobLogsType','view')
                ->where('Announcements.fkProvincesID',$fkProvincesID)
                ->where('Announcements.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Announcements.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('JobLogs.created_at', array($from, $to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        }else{
        //companies Accumulative
            $companiesFromStart =  $companies = DB::table('Companies')
                ->select(DB::raw('pkCompaniesID as  totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($startFrom,$from))
                ->count();
            $companies = DB::table('Companies')
                ->select(DB::raw('COUNT(`pkCompaniesID`) as totlePerDay,DATE(created_at) as day'))
                ->where('companiesStatus','<>',3)
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        //Announcement Accumulative
            $announcementsFromStart = DB::table('Announcements')
                ->select(DB::raw('pkAnnouncementsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('created_at',array($startFrom,$from))
                ->count();
            $announcements = DB::table('Announcements')
                ->select(DB::raw('COUNT(`pkAnnouncementsID`)as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            //Searches Accumulative
            $searchesFromStart = DB::table('SearchLogs')
                ->select(DB::raw(' pkSearchLogsID as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($startFrom,$from))
                ->count();
            $searches = DB::table('SearchLogs')
                ->select(DB::raw(' COUNT(`pkSearchLogsID`) as totlePerDay,DATE(created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        //applicants web accomulative
            $applicantsWeb = DB::table('JobApply')
                ->select(DB::raw('COUNT(`pkJobApplyID`) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('JobApply.created_at', array($from, $to))
                ->where('announcementsStatus', 1)
                ->where('jobApplyBy', 1)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            $applicantsWebFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->whereBetween('JobApply.created_at', array($startFrom,$from))
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->where('jobApplyBy', 1)
                ->count();
        //applicants phone accomulative
            $applicantsPhoneFromStart = DB::table('JobApply')
                ->select(DB::raw('pkJobApplyID as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('JobApply.created_at', array($startFrom, $from))
                ->where('jobApplyBy', 2)
                ->count();
            $applicantsPhone = DB::table('JobApply')
                ->select(DB::raw('COUNT(`pkJobApplyID`) as totlePerDay, DATE(tblJobApply.`created_at`) as day '))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('JobApply.created_at', array($from, $to))
                ->where('announcementsStatus', 1)
                ->where('jobApplyBy', 2)
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        //Job Seeker Accumulative
            $jobSeekersFromStart = DB::table('role_user')
                ->select(DB::raw('tblusers.id AS totlePerDay,DATE(tblusers.created_at) as day '))
                ->join('users','users.id','=','role_user.user_id')
                ->where('role_id',3)
                ->whereBetween('created_at',array($startFrom,$from))
                ->count();
            $jobSeekers = DB::table('role_user')
                ->select(DB::raw('COUNT(tblusers.id) AS totlePerDay,DATE(tblusers.created_at) as day '))
                ->join('users','users.id','=','role_user.user_id')
                ->where('role_id',3)
                ->whereBetween('created_at',array($from,$to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
            $data['jobSeekersFromStart'] = $jobSeekersFromStart;
            $data['jobSeekers'] = $jobSeekers;

            $jobViewsFromStart = DB::table('JobLogs')
                ->select(DB::raw('pkJobLogsID AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('JobLogs.jobLogsType','view')
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->where('announcementsStatus', 1)
                ->whereBetween('JobLogs.created_at', array($startFrom, $from))
                ->count();
            $jobViews = DB::table('JobLogs')
                ->select(DB::raw('COUNT(`pkJobLogsID`) AS totlePerDay,DATE(tblJobLogs.`created_at`) as day'))
                ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobLogs.fkAnnouncementsID')
                ->where('JobLogs.jobLogsType','view')
                ->where('announcementsStatus', 1)
                ->where('Announcements.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('JobLogs.created_at', array($from, $to))
                ->groupBy($whereUnit)
                ->orderBy('day')
                ->get();
        }

        $data['companies'] = $companies;
        $data['companiesFromStart'] = $companiesFromStart;
        $data['announcements'] = $announcements;
        $data['announcementsFromStart'] = $announcementsFromStart;
        $data['searches'] = $searches;
        $data['searchesFromStart'] = $searchesFromStart;
        $data['applicantsWebFromStart'] = $applicantsWebFromStart;
        $data['applicantsWeb'] = $applicantsWeb;
        $data['applicantsPhoneFromStart'] = $applicantsPhoneFromStart;
        $data['applicantsPhone'] = $applicantsPhone;
        $data['jobViewsFromStart'] = $jobViewsFromStart;
        $data['jobViews'] = $jobViews;
        return $data;
    }
    public static function  getAllProvinces(){
        $lang = strtoupper(Lang::getLocale());
        return $provinces = DB::table('Provinces')
            ->where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
    }
    public static function  getAllDistricts($fkProvincesID){
        $lang = strtoupper(\Session::get('lang'));
        return $districts = DB::table('Districts')->where('districtsStatus', '=', 1)->where('fkProvincesID', '=', $fkProvincesID)->orderBy('districtsName' . $lang)->pluck('districtsName' . $lang, 'pkDistrictsID');
    }
    public static function  getAllCommunes($fkDistrictsID){
        $lang = strtoupper(\Session::get('lang'));
        return $communes = DB::table('Communes')->where('communesStatus', '=', 1)->where('fkDistrictsID', '=', $fkDistrictsID)->orderBy('communesName' . $lang)->pluck('communesName' . $lang, 'pkCommunesID');
    }
    public static  function getAllSector(){
        $lang = strtoupper(Lang::getLocale());
        return $sectors = DB::table('Sectors')
            ->select('pkSectorsID', 'sectorsPhoto', 'sectorsNameEN', 'sectorsNameKH',  'sectorsNameZH', 'sectorsNameTH', 'sectorsStatus', 'created_at')
            ->where('sectorsStatus', '<>', 3)->pluck('sectorsName' . $lang, 'pkSectorsID');
    }
}
