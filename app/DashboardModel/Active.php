<?php

namespace App\DashboardModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;;


class Active extends Model
{
    public static function getAllActive($from,$to,$fkProvincesID,$fkDistrictsID,$fkCommunesID,$fkSectorID,$unit,$arrayChart)
    {
        //unit 0:day;1:month
        if ($unit == 0) {
            $whereUnit = 'day';
            $by = " week";
            $loopUnit = 8;
        } else {
            $whereUnit = DB::raw("MONTH(created_at)");
            $by = " month";
            $loopUnit = 2;
        }
        //New companies
        $data = array();
        //loggedInComapany
        $weekDay =  Carbon::now()->format('Y-m-d');
        $twoMonthBefore = date('Y-m-d', strtotime("-2 months", strtotime($weekDay)));
        if($fkProvincesID != null){
            $loggedComapany = DB::select('SELECT count(DISTINCT com.pkCompaniesID)  as totlePerDay,DATE("' . $weekDay . '") as day FROM tblCompanies com JOIN tblCompanyUsers user ON com.pkCompaniesID = user.fkCompaniesID JOIN tblUserLogs log ON log.fkUsersID = user.fkUsersID WHERE com.fkSectorsID LIKE "%' . $fkSectorID . '%"  AND com.fkProvincesID =  "' . $fkProvincesID . '" AND com.fkDistrictsID LIKE "%' . $fkDistrictsID . '%" AND com.fkCommunesID LIKE "%' . $fkCommunesID . '%" AND ( (com.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR log.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '" )');
            $data['loggedComapany'][] = $loggedComapany[0];
            if ($arrayChart['activeAccount']['value']) {
                $activeAccount = DB::select('SELECT count(DISTINCT user.id)  as totlePerDay,DATE("' . $weekDay . '") as day FROM tblusers user JOIN tblrole_user role ON role.user_id = user.id LEFT JOIN tblUserLogs log ON log.fkUsersID = user.id LEFT JOIN tblSearchLogs search ON search.fkUsersID = user.id LEFT JOIN tblJobApply apply ON apply.fkUsersID = user.id WHERE role.role_id = 3  AND (( user.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (log.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (search.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (apply.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") )');
                $data['activeAccount'][] = $activeAccount[0];
            }
            for($i=1;$i<=$loopUnit;$i++){
                $weekDay = date('Y-m-d', strtotime('-'.$i.$by));
                $twoMonthBefore = date('Y-m-d', strtotime("-2 months", strtotime($weekDay)));
                $loggedComapany = DB::select('SELECT count(DISTINCT com.pkCompaniesID)  as totlePerDay,DATE("'.$weekDay.'") as day FROM tblCompanies com JOIN tblCompanyUsers user ON com.pkCompaniesID = user.fkCompaniesID JOIN tblUserLogs log ON log.fkUsersID = user.fkUsersID WHERE com.fkSectorsID LIKE "%'.$fkSectorID.'%"  AND com.fkProvincesID = "'.$fkProvincesID.'" AND com.fkDistrictsID LIKE "%'.$fkDistrictsID.'%" AND com.fkCommunesID LIKE "%'.$fkCommunesID.'%" AND ( (com.created_at BETWEEN "'.$twoMonthBefore.'" AND "'.$weekDay.'") OR log.created_at BETWEEN "'.$twoMonthBefore.'" AND "'.$weekDay.'" )');
                $data['loggedComapany'][] = $loggedComapany[0];
            }
            //postingCompany
            $postingCompany = DB::table('Companies')
                ->select(DB::raw('count( DISTINCT pkCompaniesID)  as totlePerDay,DATE(tblAnnouncements.created_at) as day '))
                ->join('Announcements', 'Announcements.fkCompaniesID', '=', 'Companies.pkCompaniesID')
                ->where('Companies.fkProvincesID', $fkProvincesID)
                ->where('Companies.fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('Companies.fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('Companies.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('Announcements.created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy('day')
                ->get();
            $data['postingCompany'] = $postingCompany;
            //active announcement
            $announcements = DB::table('Announcements AS ann')
                ->select(DB::raw('(SELECT COUNT(ann2.`pkAnnouncementsID`) FROM tblAnnouncements AS ann2 WHERE ann2.fkSectorsID like "%'.$fkSectorID.'%" AND ann2.announcementsStatus = 1 AND tblann.announcementsPublishDate >= ann2.announcementsPublishDate AND tblann.announcementsPublishDate<= ann2.`announcementsClosingDate` ) AS totlePerDay ,DATE(tblann.created_at) as day'))
                ->where('fkProvincesID',$fkProvincesID)
                ->where('announcementsStatus', 1)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy($whereUnit)
                ->get();
            $data['announcements'] = $announcements;
            //active announcement that have received application
            $announcementReceivedApplication = DB::table('Announcements AS ann')
                ->select(DB::raw('( SELECT COUNT(ann2.`pkAnnouncementsID`) FROM tblAnnouncements AS ann2 JOIN tblJobApply apply ON ann2.pkAnnouncementsID = apply.pkJobApplyID WHERE ann2.fkSectorsID like "%'.$fkSectorID.'%" AND apply.created_at <= ann2.announcementsClosingDate AND ann2.announcementsStatus = 1 AND tblann.announcementsPublishDate >= ann2.announcementsPublishDate AND tblann.announcementsPublishDate<= ann2.`announcementsClosingDate`  ) AS totlePerDay ,DATE(tblann.created_at) as day'))
                ->where('announcementsStatus', 1)
                ->where('fkProvincesID',$fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy($whereUnit)
                ->get();
            $data['announcementReceivedApplication'] = $announcementReceivedApplication;
            //job announcementsHiring
            $job = DB::table('Announcements AS ann')
                ->select(DB::raw('(SELECT sum(ann2.`announcementsHiring`) FROM tblAnnouncements AS ann2 WHERE ann2.fkSectorsID like "%'.$fkSectorID.'%" AND ann2.announcementsStatus = 1 AND tblann.announcementsPublishDate >= ann2.announcementsPublishDate AND tblann.announcementsPublishDate<= ann2.`announcementsClosingDate` ) AS totlePerDay ,DATE(tblann.created_at) as day'))
                ->where('announcementsStatus', 1)
                ->where('fkProvincesID', $fkProvincesID)
                ->where('fkDistrictsID','like', '%' . $fkDistrictsID. '%')
                ->where('fkCommunesID','like', '%' . $fkCommunesID. '%')
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy($whereUnit)
                ->get();
        }
        else{
            $loggedComapany = DB::select('SELECT count(DISTINCT com.pkCompaniesID)  as totlePerDay,DATE("' . $weekDay . '") as day FROM tblCompanies com JOIN tblCompanyUsers user ON com.pkCompaniesID = user.fkCompaniesID JOIN tblUserLogs log ON log.fkUsersID = user.fkUsersID WHERE com.fkSectorsID LIKE "%' . $fkSectorID . '%"  AND ( (com.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR log.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '" )');
            $data['loggedComapany'][] = $loggedComapany[0];
            if ($arrayChart['activeAccount']['value']) {
                $activeAccount = DB::select('SELECT count(DISTINCT user.id)  as totlePerDay,DATE("' . $weekDay . '") as day FROM tblusers user JOIN tblrole_user role ON role.user_id = user.id LEFT JOIN tblUserLogs log ON log.fkUsersID = user.id LEFT JOIN tblSearchLogs search ON search.fkUsersID = user.id LEFT JOIN tblJobApply apply ON apply.fkUsersID = user.id WHERE role.role_id = 3  AND (( user.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (log.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (search.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (apply.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") )');
                $data['activeAccount'][] = $activeAccount[0];
            }
            for($i=1;$i<=$loopUnit;$i++){
                $weekDay = date('Y-m-d', strtotime('-'.$i.$by));
                $twoMonthBefore = date('Y-m-d', strtotime("-2 months", strtotime($weekDay)));
                $loggedComapany = DB::select('SELECT count(DISTINCT com.pkCompaniesID)  as totlePerDay,DATE("'.$weekDay.'") as day FROM tblCompanies com JOIN tblCompanyUsers user ON com.pkCompaniesID = user.fkCompaniesID JOIN tblUserLogs log ON log.fkUsersID = user.fkUsersID WHERE com.fkSectorsID LIKE "%'.$fkSectorID.'%" AND ( (com.created_at BETWEEN "'.$twoMonthBefore.'" AND "'.$weekDay.'") OR log.created_at BETWEEN "'.$twoMonthBefore.'" AND "'.$weekDay.'" )');
                $data['loggedComapany'][] = $loggedComapany[0];

//            //Active Account User
                if ($arrayChart['activeAccount']['value']) {
                    $activeAccount = DB::select('SELECT count(DISTINCT user.id)  as totlePerDay,DATE("' . $weekDay . '") as day FROM tblusers user JOIN tblrole_user role ON role.user_id = user.id LEFT JOIN tblUserLogs log ON log.fkUsersID = user.id LEFT JOIN tblSearchLogs search ON search.fkUsersID = user.id LEFT JOIN tblJobApply apply ON apply.fkUsersID = user.id WHERE role.role_id = 3  AND (( user.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (log.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (search.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") OR (apply.created_at BETWEEN "' . $twoMonthBefore . '" AND "' . $weekDay . '") )');
                    $data['activeAccount'][] = $activeAccount[0];
                }
            }
            //postingCompany
            $postingCompany = DB::table('Companies')
                ->select(DB::raw('count( DISTINCT pkCompaniesID)  as totlePerDay,DATE(tblAnnouncements.created_at) as day '))
                ->join('Announcements', 'Announcements.fkCompaniesID', '=', 'Companies.pkCompaniesID')
                ->where('Companies.fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('Announcements.created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy('day')
                ->get();
            $data['postingCompany'] = $postingCompany;
            //active announcement
            $announcements = DB::table('Announcements AS ann')
                ->select(DB::raw('(SELECT COUNT(ann2.`pkAnnouncementsID`) FROM tblAnnouncements AS ann2 WHERE ann2.fkSectorsID like "%'.$fkSectorID.'%" AND ann2.announcementsStatus = 1 AND tblann.announcementsPublishDate >= ann2.announcementsPublishDate AND tblann.announcementsPublishDate<= ann2.`announcementsClosingDate` ) AS totlePerDay ,DATE(tblann.created_at) as day'))

                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy($whereUnit)
                ->get();
            $data['announcements'] = $announcements;
            //active announcement that have received application
            $announcementReceivedApplication = DB::table('Announcements AS ann')
                ->select(DB::raw('( SELECT COUNT(ann2.`pkAnnouncementsID`) FROM tblAnnouncements AS ann2 JOIN tblJobApply apply ON ann2.pkAnnouncementsID = apply.pkJobApplyID WHERE ann2.fkSectorsID like "%'.$fkSectorID.'%" AND apply.created_at <= ann2.announcementsClosingDate AND ann2.announcementsStatus = 1 AND tblann.announcementsPublishDate >= ann2.announcementsPublishDate AND tblann.announcementsPublishDate<= ann2.`announcementsClosingDate`  ) AS totlePerDay ,DATE(tblann.created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy($whereUnit)
                ->get();
            $data['announcementReceivedApplication'] = $announcementReceivedApplication;
            //job announcementsHiring
            $job = DB::table('Announcements AS ann')
                ->select(DB::raw('(SELECT sum(ann2.`announcementsHiring`) FROM tblAnnouncements AS ann2 WHERE ann2.fkSectorsID like "%'.$fkSectorID.'%" AND ann2.announcementsStatus = 1 AND tblann.announcementsPublishDate >= ann2.announcementsPublishDate AND tblann.announcementsPublishDate<= ann2.`announcementsClosingDate` ) AS totlePerDay ,DATE(tblann.created_at) as day'))
                ->where('fkSectorsID','like', '%' . $fkSectorID. '%')
                ->whereBetween('created_at',array($from,$to))
                ->orderBy("day",'desc')
                ->groupBy($whereUnit)
                ->get();
        }

        $data['job'] = $job;
        return $data;
    }
}
