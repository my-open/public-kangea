<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'Candidates';
    protected $primaryKey = 'pkCandidatesID';
    protected $fillable = ['fkCompaniesID', 'fkAnnouncementsID', 'fkUsersID', 'candidatesDescription', 'candidatesStatus'];
}
