<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class JobApply extends Model
{
    protected $table = 'JobApply';
    protected $primaryKey = 'pkJobApplyID';
    protected $fillable = [
        "pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
        "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
        "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy", "jobApplyConfirmLog", "jobApplyIsConfirm",
        "jobApplyStatus"
    ];

    public static function retryCall(){
        `
SELECT c.totalApply, a.created_at, a.*

 FROM tblJobApply a

INNER JOIN (SELECT COUNT(b.jobApplyPhone) as totalApply, b.pkJobApplyID FROM tblJobApply b

WHERE  DATE(b.created_at) = DATE('2017-03-28') AND b.pkJobApplyID IN (SELECT MAX(pkJobApplyID) FROM tblJobApply GROUP BY pkJobApplyID)

GROUP BY b.jobApplyPhone ) c ON c.pkJobApplyID = a.pkJobApplyID`;

        //return $data;
    }
}


