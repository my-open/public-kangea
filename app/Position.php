<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'Positions';
    protected $primaryKey = 'pkPositionsID';
    protected $fillable = ['positionsCode' ,'fkSectorsID', 'positionsNameEN', 'positionsNameKH', 'positionsNameZH', 'positionsNameTH', 'positionsOrder', 'positionsStatus'];
}