<?php

namespace App;

use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Contracts\User as ProviderUser;
use DB;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            DB::beginTransaction();
                $account = new SocialAccount([
                    'provider_user_id' => $providerUser->getId(),
                    'provider' => 'facebook'
                ]);

                $user = User::whereEmail($providerUser->getEmail())->first();

                if (!$user) {

                    $user = User::create([
                        'name' => $providerUser->getName(),
                        'status' => 1,
                    ]);

                    $companyLevel = 0;
                    if( session('companyRep') ){
                        $companyLevel = 1;
                    }

                    DB::table('users')
                        ->where('id', $user->id)
                        ->update(['email' => $providerUser->getEmail(), 'companyLevel' => $companyLevel]);

                    $roleID = config("constants.JOBSEEKER");
                    if( session('companyRep') ){
                        $roleID = config("constants.EMPLOYER");
                    }

                    $role_user = DB::table('role_user')->insert(
                        ['user_id' => $user->id, 'role_id' => $roleID]
                    );

                    //Add user log
                    $data['fkUsersID'] = $user->id;
                    $data['userLogsActivity'] = 'register';
                    $data['userLogsActivityDescription'] = 'register';
                    fnUserLog($data);
                }

                $account->user()->associate($user);
                $account->save();


            if( !$user || !$role_user )
            {
                DB::rollBack();
            } else {
                DB::commit();
            }

            return $user;
        }
    }
}