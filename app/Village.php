<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
  protected $table = 'Villages';
  protected $primaryKey = 'pkVillagesID';
  protected $fillable = ['pkVillagesID', 'fkCommunesID', 'villagesNameEN', 'villagesNameKH', 'villagesStatus'];
}
