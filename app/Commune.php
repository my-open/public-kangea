<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $table = 'Communes';
    protected $primaryKey = 'pkCommunesID';
    protected $fillable = ['pkCommunesID', 'fkDistrictsID', 'CommunesNameEN', 'CommunesNameKH', 'CommunesNameZH', 'CommunesNameTH', 'IsCommune', 'communesStatus'];
}
