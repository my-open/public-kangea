<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsector extends Model
{
    protected $table = 'Subsectors';
    protected $primaryKey = 'pkSubsectorsID';
    protected $fillable = ['fkSectorsID', 'subsectorsNameEN', 'subsectorsNameKH', 'subsectorsNameZH', 'subsectorsNameTH', 'subsectorsPhoto', 'subsectorsOrder', 'subsectorsStatus'];
}
