<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use App\RoleUser;
use Illuminate\Http\Request;
use App\User;
use Flash;
use Lang;
use DB;
use Auth;

use App\Http\Requests;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['viewProfile', 'editUserProfile', 'update']]);
    }

    public function index()
    {
        $rows = DB::table('users')
            ->Join('role_user', 'role_user.user_id', '=', 'users.id')
            ->select('*')
            ->where('role_user.role_id', '=', config("constants.ADMIN") )
            ->orderBy('users.id', 'DESC')
            ->where('status', '<>', 3)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );
        return view('accounts.users.index')->withRows( $rows );
    }

    public function create()
    {
        return view('accounts.users.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:users',
            'gender' => 'required',
            'phone' => 'required|digits_between:9,10|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'status' => 'required|integer|max:1',
        ]);

        DB::beginTransaction();
        $user = User::create([
            'name' => $request['name'],
            'gender' => $request['gender'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'status' => $request['status'],
        ]);

        // Create role user
        $role_user = DB::table('role_user')->insert(
            ['user_id' => $user->id, 'role_id' => config("constants.ADMIN")]
        );

        if( !$user || !$role_user )
        {
            DB::rollBack();
        } else {
            DB::commit();
        }

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/user');
    }

    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $rows = User::findOrFail($id);

        if($rows->IsSale == '1' ){
            $isSaleTrue = 1;
        }

        return view('accounts.users.edit', compact('rows', 'isSaleTrue') );
    }

    public function update( Request $request, $id)
    {
        $user = \Auth::user();

        $rows = User::findOrFail($id);

        $this->validate($request, [
            //'name' => 'required|unique:users,name,'.$id.',id',
            'name' => 'required',
            'gender' => 'required',
            'phone' => 'required|digits_between:9,10|unique:users,phone,'.$id.',id',
            'email' => 'email|max:255|unique:users,email,'.$id.',id',
            'status' => 'required|integer|max:1',
        ]);

        if( $user -> hasRole('admin') || $user -> hasRole('employer') ) {
            $this->validate($request, [
                'password' => 'confirmed|min:6',
                'email' => 'required|email|max:255|unique:users,email,'.$id.',id',
            ]);
        }

        $input = $request->all();

        if( $request->IsSale == null){
            $input['IsSale'] = 0;
        }

        if( $user -> hasRole('admin') || $user -> hasRole('employer') ) {
            if ( !empty( $request['password'] ) ) {
                $input['password'] = bcrypt($request['password']);
            } else {
                $input['password'] = $rows['password'];
            }
        }else{
            $input['password'] = bcrypt($request['phone']);
        }


        $rows->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        if( isset($request['updateUserProfile']) ){
            return redirect(\Lang::getLocale().'/account/userProfile');
        }else{
            return redirect(\Lang::getLocale().'/account/user');
        }

    }

    public function destroy($id)
    {
        User::where('id', '=', $id)
            ->update(['status' => '3']);
//        $user = User::findOrFail($id)->delete();
//        $roleUser = RoleUser::findOrFail($id)->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/user');
    }

    public function viewProfile(){
        $userId =  Auth::id();
        $user = User::findOrFail($userId);

        $companyID = DB::table('CompanyUsers')->where('fkUsersID', $user->id)->value('fkCompaniesID');

        $companyReps = DB::table('users')
            ->Join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->Join('Companies', 'Companies.pkCompaniesID', '=', 'CompanyUsers.fkCompaniesID')
            ->where('CompanyUsers.fkCompaniesID', '=', $companyID )
            ->where('CompanyUsers.fkUsersID', '!=', $user->id )
            ->select('users.id', 'users.gender', 'users.name', 'users.email' , 'users.phone', 'users.status', 'users.companyLevel', 'users.created_at')
            ->orderBy('users.id', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        $isCompanyAdmin = false;
        if( $user->companyLevel == 1 || $user->companyLevel == 0 ){
            $isCompanyAdmin = true;
        }

        //check user has company or not
        $checkExistsUserID = DB::table('CompanyUsers')->where('fkUsersID', '=', Auth::id())->count();

        return view('accounts.users.profile', compact('user', 'companyReps', 'isCompanyAdmin', 'companyID', 'checkExistsUserID'));
    }

    public function editUserProfile(){
        $userId =  Auth::id();
        $row = User::findOrFail($userId);
        return view('accounts.users.editProfile', compact('row'));
    }

}
