<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Friday 09, Dec 2016
Modified Date = Friday 09, Dec 2016
*/

namespace App\Http\Controllers;

use App\Commune;
use App\District;
use App\Http\Requests;
use App\Province;
use App\Sector;
use App\Zone;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Lang;


class LogController extends Controller
{
    public function searchLogLocationAjax( Request $request ){
        $data['fkProvincesID'] = $request->get('fkProvincesID');
        $data['fkDistrictsID'] = $request->get('fkDistrictsID');
        $data['fkCommunesID'] = $request->get('fkCommunesID');
        $result = fnSearchLog($data);
        return $result;
    }

    public function logShareFBAjax( Request $request ){
        $jobLogs['fkAnnouncementsID'] = $request->get('pkAnnouncementsID');
        $jobLogs['jobLogsType'] = 'shareFB';
        $result = fnJobLog($jobLogs);
        return $result;
    }

    //New Data
    public function logShareToPhoneAjax( Request $request ){
        $jobLogs['fkAnnouncementsID'] = $request->get('pkAnnouncementsID');
        $jobLogs['jobLogsType'] = 'sharePhone';
        $result = fnJobLog($jobLogs);
        return $result;
    }

    public function logApplyForMyselfAjax( Request $request ){
        $jobLogs['fkAnnouncementsID'] = $request->get('pkAnnouncementsID');
        $jobLogs['jobLogsType'] = 'applyOwn';
        $result = fnJobLog($jobLogs);
        return $result;
    }

    public function logApplyForSOAjax( Request $request ){
        $jobLogs['fkAnnouncementsID'] = $request->get('pkAnnouncementsID');
        $jobLogs['jobLogsType'] = 'applySO';
        $result = fnJobLog($jobLogs);
        return $result;
    }

}
