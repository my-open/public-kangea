<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Page;
use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Lang;
use Laracasts\Flash\Flash;

class PageController extends Controller
{
    public function index()
    {
        $pages = DB::table('Pages')
            ->select(
                'pkPagesID', 'fkPagesSubTitleID', 'pagesSiteTitle', 'pagesSiteKeyword', 'pagesSiteDescription',
                'pagesTitleKH', 'pagesTitleEN', 'pagesDescriptionKH', 'pagesDescriptionEN',
                'pagesUrl', 'pagesPosition', 'pagesOrder', 'pagesStatus'
            )
            ->where('pagesStatus', true)
            ->orderBy('pagesOrder')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.pages.index', compact('pages') );
    }

    public function create()
    {
        //get page name
        if( Lang::getLocale() == 'en' ){
            $pagesMainTitle = Page::where('pagesStatus', true)->where('fkPagesSubTitleID', 0)->orderBy('pagesOrder')->pluck('pagesTitleEN', 'pkPagesID');
        }
        else{
            $pagesMainTitle = Page::where('pagesStatus', true)->where('fkPagesSubTitleID', 0)->orderBy('pagesOrder')->pluck('pagesTitleKH', 'pkPagesID');
        }

        return view('accounts.pages.add', compact('pagesTitle', 'pagesMainTitle') );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'pagesTitleEN' => 'required',
            'pagesDescriptionEN' => 'required',
        ]);

        $input = $request->all();

        $fkPagesSubTitleID = 0 ;
        if( $request->input('pkPagesID') != ''){
            $fkPagesSubTitleID = $request->input('pkPagesID');
        }
        if( $request->input('fkPagesSubTitleID') != ''){
            $fkPagesSubTitleID = $request->input('fkPagesSubTitleID');
        }
        $input['fkPagesSubTitleID'] = $fkPagesSubTitleID;

        if( $request->input('pagesTitleKH') == '' ){
            $input['pagesTitleKH'] = $request->input('pagesTitleEN');
        }
        if( $request->input('pagesDescriptionEN') == '' ){
            $input['pagesDescriptionKH'] = $request->input('pagesDescriptionEN');
        }

        Page::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/page');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $page = Page::findOrFail($id);

        if( Lang::getLocale() == 'en' ){
            $getMainTitles = Page::where('pagesStatus', true)->where('fkPagesSubTitleID', $page->fkPagesSubTitleID)->orderBy('pagesOrder')->pluck('pagesTitleEN', 'pkPagesID');
        }
        else{
            $getMainTitles = Page::where('pagesStatus', true)->where('fkPagesSubTitleID', $page->fkPagesSubTitleID)->orderBy('pagesOrder')->pluck('pagesTitleKH', 'pkPagesID');
        }

        return view('accounts.pages.edit', compact('page', 'getMainTitles') );
    }

    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);

        $this->validate($request, [
            'pagesTitleEN' => 'required',
            'pagesDescriptionEN' => 'required',
        ]);

        $input = $request->all();

        $fkPagesSubTitleID = 0 ;
        if( $request->input('pkPagesID') != ''){
            $fkPagesSubTitleID = $request->input('pkPagesID');
        }
        if( $request->input('fkPagesSubTitleID') != ''){
            $fkPagesSubTitleID = $request->input('fkPagesSubTitleID');
        }
        $input['fkPagesSubTitleID'] = $fkPagesSubTitleID;

        if( $request->input('pagesTitleKH') == '' ){
            $input['pagesTitleKH'] = $request->input('pagesTitleEN');
        }
        if( $request->input('pagesDescriptionEN') == '' ){
            $input['pagesDescriptionKH'] = $request->input('pagesDescriptionEN');
        }

        $page->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/page');
    }

    public function destroy($id)
    {
        $page = Page::findOrFail( $id );
        $page->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/page');
    }

    public function aboutUs()
    {
        return view('aboutus');
    }

    public function contactUs()
    {
        return view('contactus');
    }

    public function aboutBongpheak(){
        return view('aboutbongpheak');
    }

    public function getPagesSubTitlesAjax(Request $request){
        $pageSubtitles = Page::select('pagesTitleEN', 'pagesTitleKH', 'pkPagesID')
            ->where('fkPagesSubTitleID', '=', $request->input('PagesID'))
            ->where('pagesStatus', '=', 1)
            ->orderBy('pagesOrder')
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'), 'data' => $pageSubtitles];
    }

}
