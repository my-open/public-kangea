<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Flash;
use Lang;
use App\Sector;
use App\Subsector;

class SubsectorController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getSubsectorsAjax']]);
    }

    public function index()
    {
        $subsectorsNameEN = null;
        $subsectorsNameKH = null;
        $subsectorsNameZH = null;
        $subsectorsNameTH = null;
        $fkSectorsID = null;
        $subsectorsOrder = null;

        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        $subsectors = DB::table('Subsectors')
            ->join('Sectors', 'Subsectors.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->select('Subsectors.pkSubsectorsID', 'Subsectors.fkSectorsID', 'Subsectors.subsectorsPhoto',
                'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH', 'Subsectors.subsectorsNameZH', 'Subsectors.subsectorsNameTH',
                'Subsectors.subsectorsStatus', 'Subsectors.created_at as createdAt', 'Subsectors.subsectorsOrder',
                'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH', 'Sectors.sectorsNameZH', 'Sectors.sectorsNameTH'
                )
            ->where('subsectorsStatus', '<>', 3)
            ->orderBy('pkSubsectorsID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.subsectors.index', compact('subsectorsNameEN', 'subsectorsNameKH', 'subsectorsNameZH', 'subsectorsNameTH', 'fkSectorsID', 'subsectorsOrder', 'sectors', 'subsectors', 'lang' ) );
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        return view('accounts.subsectors.add', ['sectors' => $sectors]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'subsectorsPhoto' => 'image|mimes:jpeg,jpg,png',
            'fkSectorsID' => 'required|max:11',
            'subsectorsNameEN' => 'required|max:100',
            'subsectorsNameKH' => 'required|max:100',
            'subsectorsNameZH' => 'required|max:100',
            'subsectorsNameTH' => 'required|max:100',
            'subsectorsOrder' => 'digits_between:1,5',
            'subsectorsStatus' => 'required|max:1'
        ]);

        $input = $request->all();
        $input['subsectorsPhoto'] = '';
        if ($request->hasFile('subsectorsPhoto')) {
            if ($request->file('subsectorsPhoto')->isValid()) {
                $file = $request->file('subsectorsPhoto');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if ($file->move('images/subsectors', $fileName)) {
                    $input['subsectorsPhoto'] = $fileName;
                }
            }
        }
        Subsector::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/subsector');
    }

    public function show($id)
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        $subsector = Sector::findOrFail($id);
        return view('accounts.sectors.edit', compact('sectors', 'subsector'));
    }

    public function edit($id)
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        $subsector = Subsector::findOrFail($id);
        return view('accounts.subsectors.edit', compact('sectors', 'subsector'));
    }

    public function update(Request $request, $id)
    {
        $subsector = Subsector::findOrFail($id);

        $this->validate($request, [
            'subsectorsPhoto' => 'image|mimes:jpeg,jpg,png',
            'subsectorsNameEN' => 'required|max:100',
            'subsectorsNameKH' => 'required|max:100',
            'subsectorsNameZH' => 'required|max:100',
            'subsectorsNameTH' => 'required|max:100',
            'subsectorsOrder' => 'digits_between:1,5',
            'subsectorsStatus' => 'required|max:1'
        ]);

        $input = $request->all();
        if( !empty( $input['subsectorsPhoto'] )){
            if ( $request->file('subsectorsPhoto')->isValid() ) {
                $file = $request->file('subsectorsPhoto');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if( $file->move('images/subsectors', $fileName))
                {
                    $input['subsectorsPhoto'] = $fileName;
                }
            }
        }else{
            $input['subsectorsPhoto'] = $subsector->subsectorsPhoto;
        }
        $subsector->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect( \Lang::getLocale().'/account/subsector');
    }

    public function destroy($id)
    {
        Subsector::where('pkSubsectorsID', '=', $id)
            ->update([ 'subsectorsStatus' => '3' ]);

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/subsector');
    }

    public function getSubsectorsAjax(Request $request){
        $subsectors = Subsector::select('subsectorsNameEN', 'subsectorsNameKH', 'subsectorsNameZH', 'subsectorsNameTH', 'pkSubsectorsID')
            ->where('fkSectorsID', '=', $request->input('sectorId'))
            ->where('subsectorsStatus', '=', 1)
            ->orderBy('subsectorsOrder')
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'), 'data' => $subsectors];
    }

    public function searchSubsector( Request $request )
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        $subsectorsNameEN = $request->get('subsectorsNameEN');
        $subsectorsNameKH = $request->get('subsectorsNameKH');
        $subsectorsNameZH = $request->get('subsectorsNameZH');
        $subsectorsNameTH = $request->get('subsectorsNameTH');
        $fkSectorsID = $request->get('fkSectorsID');
        $subsectorsOrder = $request->get('subsectorsOrder');

        $searchCriteria = [
            'subsectorsNameEN' => trim( $request->get('subsectorsNameEN') ),
            'subsectorsNameKH' => trim( $request->get('subsectorsNameKH') ),
            'fkSectorsID' => $request->get('fkSectorsID'),
        ];

        $strWhere = '';
        foreach($searchCriteria as $column => $value){
            if( $value == '' ){
                $$column = null;
                continue;
            }

            $$column = trim($value);

            if( $strWhere == '' ){
                if( $column == 'subsectorsNameEN' ||  $column == 'subsectorsNameKH' ||  $column == 'subsectorsNameZH' ||  $column == 'subsectorsNameTH' ){
                    $strWhere = "( tblSubsectors.subsectorsNameEN LIKE '%". $value . "%' OR tblSubsectors.subsectorsNameKH LIKE '%". $value . "%' OR tblSubsectors.subsectorsNameZH LIKE '%". $value . "%' OR tblSubsectors.subsectorsNameTH LIKE '%". $value . "%' )";
                }else{
                    $strWhere = 'tblSubsectors.' . $column. " = '". $value."' ";
                }
            }else{
                if( $column == 'subsectorsNameEN' ||  $column == 'subsectorsNameKH' ||  $column == 'subsectorsNameZH' ||  $column == 'subsectorsNameTH'){
                    $strWhere .= " AND ( tblSubsectors.subsectorsNameEN LIKE '%". $value . "%' OR tblSubsectors.subsectorsNameKH LIKE '%". $value . "%' OR tblSubsectors.subsectorsNameZH LIKE '%". $value . "%' OR tblSubsectors.subsectorsNameTH LIKE '%". $value . "%' )";
                }else{
                    $strWhere .= ' AND tblSubsectors.' . $column. " = '". $value."' ";
                }
            }
        }

        if( $strWhere != ''  ){
            $subsectors = DB::table('Subsectors')
                ->join('Sectors', 'Subsectors.fkSectorsID', '=', 'Sectors.pkSectorsID')
                ->select('Subsectors.pkSubsectorsID', 'Subsectors.fkSectorsID', 'Subsectors.subsectorsPhoto', 'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH', 'Subsectors.subsectorsNameZH', 'Subsectors.subsectorsNameTH', 'Subsectors.subsectorsStatus', 'Subsectors.created_at as createdAt', 'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH', 'Subsectors.subsectorsOrder')
                ->whereRaw($strWhere)
                ->orderBy('Subsectors.subsectorsOrder', $request->get('subsectorsOrder'))
                ->paginate( config("constants.PAGINATION_NUM_MAX") );
        }else{
            $subsectors = DB::table('Subsectors')
                ->join('Sectors', 'Subsectors.fkSectorsID', '=', 'Sectors.pkSectorsID')
                ->select('Subsectors.pkSubsectorsID', 'Subsectors.fkSectorsID', 'Subsectors.subsectorsPhoto', 'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH', 'Subsectors.subsectorsNameZH', 'Subsectors.subsectorsNameTH', 'Subsectors.subsectorsStatus', 'Subsectors.created_at as createdAt', 'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH', 'Subsectors.subsectorsOrder')
                ->orderBy('Subsectors.subsectorsOrder', $request->get('subsectorsOrder'))
                ->paginate( config("constants.PAGINATION_NUM_MAX") );
        }

        return view('accounts.subsectors.index', compact( 'lang', 'subsectorsNameEN', 'subsectorsNameKH', 'subsectorsNameZH', 'subsectorsNameTH', 'fkSectorsID', 'subsectorsOrder', 'sectors', 'subsectors' ) );
    }

}
