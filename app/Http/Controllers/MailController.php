<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;

class MailController extends Controller
{
    public function sentMail(){
        $user = array('title'=>'m', 'name'=>"Sorn Sea", 'id'=>1, 'positionName'=>"Cleaner", 'phone'=>'011256345');
        $emails = ['sorn.sea@gmail.com','sea@open.org.kh'];

        Mail::send('emails.newApplicant', ['user' => $user], function ($m) use ($emails) {
            $m->to($emails, 'Sorn Sea')->subject('Registration confirmation E-mail');
        });
    }
}
