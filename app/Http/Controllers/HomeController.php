<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use App\Commune;
use App\District;
use App\Http\Requests;
use App\Province;
use App\Sector;
use App\Zone;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Lang;


class HomeController extends Controller
{
    public function home(){
        return view('fronts.seeker.index');
    }

    public function jobApply(){
        return view('fronts.seeker.apply');
    }

    public function noPermission(){
        return view('errors.403');
    }

   public function index()
   {
       $lang = strtoupper(Lang::getLocale());


       //Log for home page
       $data=[];
       fnSearchLog($data);

       $sectorName = DB::table('Sectors')
           ->select('*')
           ->get();

       $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

       $employers = DB::table('Companies')
           ->where('Announcements.announcementsStatus', '=', 1)
           ->where('Announcements.announcementsClosingDate', '>=', date('Y-m-d 00:00:00'))
           ->select(array('Companies.*', DB::raw('COUNT(tblAnnouncements.fkCompaniesID) as total_jobs')))
           ->leftjoin('Announcements', 'Announcements.fkCompaniesID', '=', 'Companies.pkCompaniesID')
           ->groupBy('Companies.pkCompaniesID')
           ->orderBy('total_jobs', 'desc')
           ->limit(12)
           ->get();

       $featureLimit = 4;
       $featureSelect = [ 'Companies.fkSectorsID', 'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesLogo' ];
       $featureWhere = 'tblAnnouncements.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsStatus = 1 AND tblCompanies.companiesStatus = 1 AND tblCompanies.companiesLogo != ""';
       $featureCase = ['Companies.pkCompaniesID', 'NOT IN', 'Announcements.fkCompaniesID'];

       $featureM = self::homepageFeatureCompany(1, $featureLimit);

       if (count($featureM) < 4) { $featureLimit = 4+(4-count($featureM)); }
       $featureH = self::homepageFeatureCompany(2, $featureLimit);

       if (count($featureH) < 4) { $featureLimit = 4+(4-count($featureH)); }
       $featureC = self::homepageFeatureCompany(3, $featureLimit);

       if (count($featureC) < 4) { $featureLimit = 4+(4-count($featureC)); }
       $featureS = self::homepageFeatureCompany(4, $featureLimit);

       return view('jobseeker_home', compact('sectorName', 'provinces', 'employers', 'sectors', 'districts', 'communes', 'lang', 'featureM', 'featureH', 'featureC', 'featureS') );
   }

    public function locationInsert( $param ){
        if( $param == 'district' ){
            $districts = District::select('districtsNameEN', 'pkDistrictsID')->get();
            foreach($districts as $key => $value){
                District::where('pkDistrictsID', $value->pkDistrictsID)->update(array('districtsNameZH' => $value->districtsNameEN, 'districtsNameTH' => $value->districtsNameEN));
            }
        }elseif( $param == 'commune' ){
            $communes = Commune::select('communesNameEN', 'pkCommunesID')->get();
            foreach($communes as $key => $value){
                Commune::where('pkCommunesID', $value->pkCommunesID)->update(array('CommunesNameZH' => $value->communesNameEN, 'CommunesNameTH' => $value->communesNameEN));
            }
        }
    }

    public function homepageFeatureCompany($sectorID, $limit) {
      $lang = strtoupper(Lang::getLocale());
      $featureLimit = $limit;
      $featureSelect = [ 'Companies.fkSectorsID', 'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNameEN AS companiesName_EN', 'Companies.companiesLogo' ];
      $featureWhere = 'tblAnnouncements.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsStatus = 1 AND tblCompanies.companiesStatus = 1 AND tblCompanies.companiesLogo != ""';
      $featureCase = ['Companies.pkCompaniesID', 'NOT IN', 'Announcements.fkCompaniesID'];
      return DB::table('Companies')
                            ->leftjoin('Announcements', 'Announcements.fkCompaniesID', '=', 'Companies.pkCompaniesID')
                            ->select($featureSelect)
                            ->selectRaw('(SELECT count(tblAnnouncements.pkAnnouncementsID) FROM tblAnnouncements WHERE tblAnnouncements.fkCompaniesID = tblCompanies.pkCompaniesID AND  tblAnnouncements.announcementsStatus = 1 AND tblAnnouncements.announcementsClosingDate >= '.date('"Y-m-d 00:00:00"').') AS `NumberOfAnnouncements`')
                            ->orderby('NumberOfAnnouncements', 'DESC')
                            ->where('Companies.fkSectorsID', '=', $sectorID)
                            ->whereRaw($featureWhere)
                            ->selectRaw('(SELECT count(tblAnnouncements.pkAnnouncementsID) FROM tblAnnouncements WHERE tblAnnouncements.fkCompaniesID = tblCompanies.pkCompaniesID AND  tblAnnouncements.announcementsStatus = 1 AND tblAnnouncements.announcementsClosingDate >= '.date('"Y-m-d 00:00:00"').') AS `NumberOfAnnouncements`')
                            ->orderby('NumberOfAnnouncements', 'DESC')
                            ->limit($featureLimit)
                            ->groupby('Companies.companiesNameEN')
                            ->get();
    }
}
