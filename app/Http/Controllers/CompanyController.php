<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Country;
use App\Position;
use App\Province;
use App\District;
use App\Commune;
use App\Village;

use Illuminate\Http\Request;

use App\Http\Requests;
use Flash;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Lang;
use DB;
use App\Sector;
use App\Subsector;
use App\Zone;
use App\Company;
use Auth;

class CompanyController extends Controller
{
    public function __construct()
    {
        $excepts = ['getAutocompleteCompaniesAjax', 'getFrmCompanyAjax'];
        $this->middleware('role:admin', ['except' => $excepts]);

        $user = \Auth::user();
        if( isset( $user ) && $user -> hasRole('employer') ){
            array_push($excepts, 'companyProfile', 'editCompanyProfile', 'update');
            $this->middleware('role:admin', ['except' => $excepts]);
        }
    }

    public function index()
    {
        $lang =  strtoupper(Lang::getLocale());

        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        $strWhere = ' tblCompanies.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;

        $companies = DB::table('Companies')
            ->select('pkCompaniesID', 'companiesName'.$lang, 'companiesNickName', 'companiesNumberOfWorker', 'companiesPhone', 'companiesEmail', 'created_at')
            ->whereRaw( $strWhere )
            ->where('companiesStatus', '<>', 3)
            ->orderby('pkCompaniesID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.companies.index', compact('companies', 'lang', 'fromDate', 'toDate'));
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districts = District::where('districtsStatus', true)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communes = Commune::where('communesStatus', true)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
        //$villages = Village::where('villagesStatus', true)->orderBy('villagesName'.$lang)->pluck('villagesName'.$lang, 'pkVillagesID');
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        //$zones = Zone::where('zonesStatus', true)->orderBy('zonesName'.$lang)->pluck('zonesName'.$lang, 'pkZonesID');

        return view('accounts.companies.add', compact('sectors','zones', 'provinces', 'districts', 'communes', 'villages')  );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fkSectorsID' => 'required|max:11',
            'fkSubsectorsID' => 'required|max:11',
            'fkActivitiesID' => 'required|max:11',
            'companiesNameEN' => 'required',
            'fkProvincesID'=> 'required|exists:Provinces,pkProvincesID',
            'companiesPhone'=> 'digits_between:9,10',
            'companiesEmail' => 'email|max:70',
            'companiesNumberOfWorker' => 'digits_between:1,4',
            'companiesLogo' => 'max:'.config("constants.IMAGE_SIZE_MAX").'|image|mimes:jpeg,jpg,png',
            'companiesSite' => 'url',
        ]);

        if ( $request->file('companiesLogo')->isValid() ) {
            $file = $request->file('companiesLogo');
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            if( $file->move('images/companyLogos', $fileName))
            {
                $img = Image::make('images/companyLogos/'.$fileName);
                $img->resize(config("constants.COMPANY_LOGO_W"), config("constants.COMPANY_LOGO_H"), function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save('images/companyLogos/thumbnails/'.$fileName);
                $companiesLogoName = $fileName;
            }
        } else {
            $companiesLogoName = 'defaultLogo.png';
        }

        $input = $request->all();
        $input['companiesLogo'] = $companiesLogoName;

        //check companiesNameKH
        if( empty( $request['companiesNameKH'] ) || empty( $request['companiesNameZH'] ) || empty( $request['companiesNameTH'] ) ){
            $input['companiesNameKH'] = $request['companiesNameEN'];
            $input['companiesNameZH'] = $request['companiesNameEN'];
            $input['companiesNameTH'] = $request['companiesNameEN'];
        }
        if( empty( $request['companiesDescriptionKH'] ) || empty( $request['companiesDescriptionZH'] ) || empty( $request['companiesDescriptionTH'] ) ){
            $input['companiesDescriptionKH'] = $request['companiesDescriptionEN'];
            $input['companiesDescriptionZH'] = $request['companiesDescriptionEN'];
            $input['companiesDescriptionTH'] = $request['companiesDescriptionEN'];
        }

        $company = Company::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        if( isset($_POST['save']) ){
            return redirect( \Lang::getLocale().'/account/company');
        }elseif( isset($_POST['save_rep']) ){
            return redirect( \Lang::getLocale().'/account/repEmployer/'. $company->pkCompaniesID );
        }

    }

    public function edit($id)
    {
        $rows = Company::findOrFail($id);

        $lang =  strtoupper(Lang::getLocale());
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $rows->fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $rows->fkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $subsectorBySectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $rows->fkSectorsID)->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');
        $activitiesBySubsector = Activity::where('ActivitiesStatus', true)->where('fkSubsectorsID', $rows->fkSubsectorsID)->orderBy('ActivitiesOrder')->pluck('ActivitiesName'.$lang, 'pkActivitiesID');

        return view('accounts.companies.edit', compact('rows', 'sectors', 'subsectorBySectors', 'activitiesBySubsector','zones', 'provinces', 'districtByProvinces', 'communeByDistricts', 'villageByCommunes') );
    }

    public function update( Request $request, $id)
    {
        $rows = Company::findOrFail($id);

        $this->validate($request, [
            'fkSectorsID' => 'required|max:11',
            'fkSubsectorsID' => 'required|max:11',
            ////'fkActivitiesID' => 'required|max:11',
            'companiesNameEN' => 'required',

            'fkProvincesID'=> 'required|exists:Provinces,pkProvincesID',
            'fkDistrictsID'=> 'required',
            'companiesPhone' => 'digits_between:9,10',
            'companiesEmail' => 'email|max:70',

            'companiesNumberOfWorker' => 'digits_between:1,4',
            'companiesLogo' => 'max:'.config("constants.IMAGE_SIZE_MAX").'|image|mimes:jpeg,jpg,png',
            'companiesSite' => 'url',
        ]);

        $input = $request->all();
        if( !empty( $input['companiesLogo'] )){
            if ( $request->file('companiesLogo')->isValid() ) {
                $file = $request->file('companiesLogo');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if( $file->move('images/companyLogos', $fileName))
                {
                    $img = Image::make('images/companyLogos/'.$fileName);
                    $img->resize(config("constants.COMPANY_LOGO_W"), config("constants.COMPANY_LOGO_H"), function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save('images/companyLogos/thumbnails/'.$fileName);
                    $input['companiesLogo'] = $fileName;
                }
            }
        }else{
            $input['companiesLogo'] = $rows->companiesLogo;
        }

        //check companiesNameKH
        if( empty( $request['companiesNameKH'] ) || empty( $request['companiesNameZH'] ) || empty( $request['companiesNameTH'] ) ){
            $input['companiesNameKH'] = $request['companiesNameEN'];
            $input['companiesNameZH'] = $request['companiesNameEN'];
            $input['companiesNameTH'] = $request['companiesNameEN'];
        }
        if( empty( $request['companiesDescriptionKH'] ) || empty( $request['companiesDescriptionZH'] ) || empty( $request['companiesDescriptionTH'] ) ){
            $input['companiesDescriptionKH'] = $request['companiesDescriptionEN'];
            $input['companiesDescriptionZH'] = $request['companiesDescriptionEN'];
            $input['companiesDescriptionTH'] = $request['companiesDescriptionEN'];
        }

        $rows->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        if( isset($_POST['save']) ){
            return redirect( \Lang::getLocale().'/account/company');
        }elseif( isset($_POST['save_rep']) ){
            return redirect( \Lang::getLocale().'/account/repEmployer/'. $id );
        }elseif( isset($_POST['update']) ){
            return redirect( \Lang::getLocale().'/account/companyProfile');
        }
    }

    public function destroy($id)
    {
        Company::where('pkCompaniesID', '=', $id)
            ->update(['companiesStatus' => '3']);

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/company');
    }

    public function companyProfile()
    {
        $employerId =  Auth::id();

        //check user has company or not
        $checkExistsUserID = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->where('Companies.companiesStatus', 1)
            ->count();
        if( $checkExistsUserID <= 0 ) {
            echo  $checkExistsUserID;
            return redirect()->to(\Lang::getLocale() . '/account/employer/registerCompany');
        }

        $company = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->where('Companies.companiesStatus', 1)
            ->first();

        if( isset( $company ) ) {
            $lang =  strtoupper(Lang::getLocale());
            $sectorName = DB::table('Sectors')->where('pkSectorsID', $company->fkSectorsID)->value('sectorsName'.$lang);
            $subsectorName = DB::table('Subsectors')->where('pkSubsectorsID', $company->fkSubsectorsID)->value('subsectorsName'.$lang);
            $activityName = DB::table('Activities')->where('pkActivitiesID', $company->fkActivitiesID)->value('ActivitiesName'.$lang);
            $provincesName = DB::table('Provinces')->where('pkProvincesID', $company->fkProvincesID)->value('provincesName'.$lang);
            $districtName = DB::table('Districts')->where('pkDistrictsID', $company->fkDistrictsID)->value('districtsName'.$lang);
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

            $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $company->fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
            $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $company->fkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
            //$villageByCommunes = Village::where('villagesStatus', true)->where('fkCommunesID', $company->fkCommunesID)->orderBy('villagesName'.$lang)->pluck('villagesName'.$lang, 'pkVillagesID');
            //$zoneName = DB::table('Zones')->where('pkZonesID', $company->fkZonesID)->value('zonesName'.$lang);

            return view('accounts.companies.companyProfile', compact('lang', 'company', 'sectorName', 'subsectorName', 'activityName', 'zoneName', 'provinces', 'provincesName', 'districtName', 'districtByProvinces', 'communeByDistricts', 'villageByCommunes')  );
        }
    }


    public function editCompanyProfile(){
        $employerId =  Auth::id();

        $company = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->where('Companies.companiesStatus', 1)
            ->first();

        $lang =  strtoupper(Lang::getLocale());
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $company->fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $company->fkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $subsectorBySectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $company->fkSectorsID)->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');
        $activitiesBySubsector = Activity::where('ActivitiesStatus', true)->where('fkSubsectorsID', $company->fkSubsectorsID)->orderBy('ActivitiesOrder')->pluck('ActivitiesName'.$lang, 'pkActivitiesID');
        //$villageByCommunes = Village::where('villagesStatus', true)->where('fkCommunesID', $company->fkCommunesID)->orderBy('villagesName'.$lang)->pluck('villagesName'.$lang, 'pkVillagesID');
        //$zones = Zone::where('zonesStatus', true)->orderBy('zonesName'.$lang)->pluck('zonesName'.$lang, 'pkZonesID');

        if( isset($_POST['update']) ){
            Flash::message( trans('text_lang.updateSuccessful') );
            return redirect( \Lang::getLocale().'/account/companyProfile');
        }

        return view('accounts.companies.editCompanyProfile', compact('company', 'sectors', 'subsectorBySectors', 'activitiesBySubsector','zones', 'provinces', 'districtByProvinces', 'communeByDistricts', 'villageByCommunes')  );
    }

    public function search( Request $request ){
        $lang = strtoupper(Lang::getLocale());
        $companiesName = trim( $request->get('companiesName'.$lang) );
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));

        $strWhere = ' tblCompanies.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;
        if( $companiesName != '' ){
            $strWhere .= " AND tblCompanies.companiesName".$lang." like '%". $companiesName . "%' ";
        }

        $companies = DB::table('Companies')
            ->select('pkCompaniesID', 'companiesName'.$lang, 'companiesNickName', 'companiesNumberOfWorker', 'companiesPhone', 'companiesEmail', 'created_at')
            ->whereRaw( $strWhere )
            ->where('companiesStatus', '<>', 3)
            ->orderby('pkCompaniesID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.companies.index', compact('companies', 'lang', 'fromDate', 'toDate', 'companiesName'));
    }

    public function getAutocompleteCompaniesAjax(){
        $companies = Company::select('pkCompaniesID', 'companiesNameEN', 'companiesNameKH')->get();
        $companiesEN = array();
        $companiesKH = array();
        foreach( $companies as $company ){
            $companiesEN[] = $company->companiesNameEN;
            $companiesKH[] = $company->companiesNameKH;
        }
        return ['companiesEN' => $companiesEN, 'companiesKH'=> $companiesKH, 'companies'=> $companies];
    }

    public function getFrmCompanyAjax( ){
        $lang =  strtoupper(Lang::getLocale());
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        //$zones = Zone::where('zonesStatus', true)->orderBy('zonesName'.$lang)->pluck('zonesName'.$lang, 'pkZonesID');

        return (String) view('accounts.users.employers.frmCompany', compact('sectors', 'provinces', 'zones'));
    }

    

}
