<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Flash;
use Illuminate\Support\Facades\Log;
use Lang;
use DB;
use App\Province;
use App\Country;

class ProvinceController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getProvincesAjax']]);
    }

    public function index()
    {
        $lang =  strtoupper(Lang::getLocale());

        $provinces = DB::table('Provinces')
            ->select('pkProvincesID','fkCountriesID', 'provincesName'.$lang, 'provincesStatus')
            ->paginate( 20 );
        return view('accounts.countries.provinces.index', ['provinces' => $provinces, 'lang'=>$lang]);
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $countries = Country::where('countriesStatus', true)->orderBy('countriesName'.$lang)->pluck('countriesName'.$lang, 'pkCountriesID');

        return view('accounts.countries.provinces.add', ['countries' => $countries]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fkCountriesID' => 'required|exists:Countries,pkCountriesID',
            'pkProvincesID' => 'required|unique:Provinces',
            'provincesNameEN' => 'required',
            'provincesNameKH' => 'required',
            'provincesNameZH' => 'required',
            'provincesNameTH' => 'required',
            'provincesStatus' => 'required'
        ]);
        $input = $request->all();
        Province::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/province');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $lang =  strtoupper(Lang::getLocale());
        $countries = Country::where('countriesStatus', true)->orderBy('countriesName'.$lang)->pluck('countriesName'.$lang, 'pkCountriesID');

       //get data of province where id =?
       $provinces = Province::findOrFail($id);

       return view('accounts.countries.provinces.edit', ['provinces' => $provinces, 'countries' => $countries]);
    }

    public function update( Request $request, $id)
    {
       $provinces = Province::findOrFail($id);

       $this->validate($request, [
         'fkCountriesID' => 'required',
         'pkProvincesID' => 'required|unique:Provinces,pkProvincesID,'.$id.',pkProvincesID',
         'provincesNameEN' => 'required',
         'provincesNameKH' => 'required',
         'provincesNameZH' => 'required',
         'provincesNameTH' => 'required',
         'provincesStatus' => 'required'
       ]);
       $input = $request->all();

       $provinces->fill($input)->save();
       Flash::message( trans('text_lang.updateSuccessful') );
       return redirect(\Lang::getLocale().'/account/province');
    }

    public function destroy($id)
    {
        $province = Province::findOrFail($id);
        $province->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/province');
    }

    public function getProvincesAjax(Request $request){
            $provinces = Province::select('provincesNameEN', 'provincesNameKH', 'provincesNameZH', 'provincesNameTH', 'pkProvincesID')
                ->where('fkCountriesID', '=', $request->input('countryId'))
                ->where('provincesStatus', '=', 1)
                ->get();

        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $provinces];
    }

}
