<?php

namespace App\Http\Controllers;

use App\JobApply;
use App\Postjob;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Shares;
use Illuminate\Support\Facades\Log;
use DB;
use Mail;

class BongPheakAPIController extends Controller
{
    public function storeSharedRecord(Request $request)
    {
        $sharesRecord = json_decode($request->input('sharesRecord'), true);
        $applyRecord = $request->input('sharesApplyRecord');
        $sharesRecord['sharesApplyRecord'] = $applyRecord;
        $shares = Shares::create($sharesRecord);
        $response = ['fkSharesID' => $shares->pkSharesID];
        return json_encode($response);
    }

    public function storeApplyRecord(Request $request)
    {
        $applyRecord = json_decode($request->input('sharesApplyRecord'), true);
        $jobApply = JobApply::where('jobApplyPhone', $applyRecord['jobApplyPhone'])
            ->where('fkAnnouncementsID', $applyRecord['fkAnnouncementsID'])
            ->where('jobApplyIsConfirm', 1)
            ->first();
        $response = ['pkJobApplyID' => null];
        if( empty($jobApply) ){
            $apply = JobApply::create($applyRecord);
            $response = ['pkJobApplyID' => $apply->pkJobApplyID];

            //Get data for Send Email
            $emails = DB::table('users')->Join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')->where('CompanyUsers.fkCompaniesID', '=', $applyRecord['fkCompaniesID'])->pluck('email');
            $emails = array_filter($emails);

            $job = Postjob::findOrFail($applyRecord['fkAnnouncementsID']);
            $positionName = DB:: table('Positions')->where('pkPositionsID', $applyRecord['fkPositionsID'] )->value('positionsNameEN');
            $provincesName_EN = DB::table('Provinces')->where('pkProvincesID', $job->fkProvincesID)->value('provincesNameEN');
            $sectorsName_EN = DB::table('Sectors')->where('pkSectorsID', $job->fkSectorsID)->value('sectorsNameEN');
            $province_name = fnConvertSlug($provincesName_EN);
            $sector_name = fnConvertSlug($sectorsName_EN);
            $position_name = fnConvertSlug($positionName);

            $data = array('title' => $applyRecord['jobApplyGender'], 'name' => $applyRecord['jobApplyName'], 'id' => $applyRecord['fkAnnouncementsID'], 'positionName' => $positionName, 'phone' => $applyRecord['jobApplyPhone'], 'fileName' => '', 'province_name' => $province_name, 'sector_name' => $sector_name, 'position_name' => $position_name);
            if( count($emails) > 0 ){
                Mail::send('emails.newApplicant', $data, function ($message) use ($emails, $positionName) {
                    $message->to($emails)->subject('Apply for ' . $positionName);
                });
            }
        }
        return json_encode($response);
    }

    public function confirmApplyRecord(Request $request){
        $result = false;
        $apply = JobApply::findOrFail($request->get('pkJobApplyID'));
        if( !empty($apply) ){
            JobApply::where('pkJobApplyID', $request->get('pkJobApplyID'))
                ->update(['jobApplyIsConfirm' => $request->get('isConfirm'), 'jobApplyConfirmLog' => $request->get('confirmLog')]);

            //if Confirm = true send email
            $positionName = DB::table('Positions')->where('pkPositionsID', $apply->fkPositionsID)->value('positionsNameEN');
            $phone = $apply->jobApplyPhone;
            $gender = $apply->jobApplyGender;
            $name = $apply->jobApplyName;
            $fileName = $apply->jobApplyCvFile;

            $emails = DB::table('users')->Join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')->where('CompanyUsers.fkCompaniesID', '=', $apply->fkCompaniesID )->pluck('email');
            $emails = array_filter($emails);

            $job = Postjob::findOrFail($apply->fkAnnouncementsID);
            $positionName = DB:: table('Positions')->where('pkPositionsID', $apply->fkPositionsID)->value('positionsNameEN');
            $provincesName_EN = DB::table('Provinces')->where('pkProvincesID', $job->fkProvincesID)->value('provincesNameEN');
            $sectorsName_EN = DB::table('Sectors')->where('pkSectorsID', $job->fkSectorsID)->value('sectorsNameEN');
            $province_name = fnConvertSlug($provincesName_EN);
            $sector_name = fnConvertSlug($sectorsName_EN);
            $position_name = fnConvertSlug($positionName);

            $data = array('title' => $gender, 'name' => $name, 'id' => $apply->fkAnnouncementsID, 'positionName' => $positionName, 'phone' => $phone, 'fileName' => $fileName, 'province_name' => $province_name, 'sector_name' => $sector_name, 'position_name' => $position_name);

            if (count($emails) > 0) {
                if($fileName != ''){
                    Mail::send('emails.newApplicant', $data, function ($message) use ($emails, $fileName, $positionName) {
                        $message->to($emails)->subject('Apply for ' . $positionName);
                        $message->attach(public_path('files/cv/') . $fileName);
                    });
                }else{
                    Mail::send('emails.newApplicant', $data, function ($message) use ($emails, $positionName) {
                        $message->to($emails)->subject('Apply for ' . $positionName);
                    });
                }
            }
            $result = true;
        }
        return json_encode($result);
    }
}
