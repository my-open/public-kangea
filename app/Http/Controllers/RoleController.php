<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \App\Role;
use Flash;
use Lang;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $roles = Role::all();
        return view('accounts.roles.index')->withRoles($roles);
    }

    public function create()
    {
        return view('accounts.roles.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'display_name' => 'required|max:255'
        ]);

        $input = $request->all();
        Role::create($input);

        Flash::message('Successfully added!');
        return redirect( \Lang::getLocale().'/account/role');
    }

    public function edit( $id )
    {
        $role = Role::findOrFail($id);
        return view('accounts.roles.edit')->withRole($role);
    }

    public function update( Request $request, $id)
    {
        $role = Role::findOrFail($id);

        $this->validate($request, [
            'name' => 'required|max:255',
            'display_name' => 'required|max:255'
        ]);

        $input = $request->all();

        $role->fill($input)->save();

        Flash::message('Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $role->delete();

        Flash::message('Successfully deleted!');
        return redirect(\Lang::getLocale().'/account/role');
    }
}