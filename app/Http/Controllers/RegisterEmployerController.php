<?php
/*
Author = Sorn Sea
Modified by = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Friday 19, August 2016
Modified Date = Friday 19, August 2016
*/

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Mail;

use DB;
use Flash;
use Lang;

class RegisterEmployerController extends Controller
{
    public function index(){
        return view('employer_home');
    }

    public function registerRep( ){
        return view('accounts.users.employers.register');
    }

    public function registerRepPost( Request $request ){
        $this->validate($request, [
            'gender' => 'required',
            'name' => 'required|max:150',
            'phone' => 'required|digits_between:9,10|unique:users',
            'email' => 'required|email|max:70|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        $remember = true;
        if( $request->get('remember') == null ){
            $remember = false;
        }

        DB::beginTransaction();
        //Add Company user
        $user = User::create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'gender' => $request['gender'],
            'password' => bcrypt($request['password']),
            'companyLevel' => 1,
            'status' => 1,
        ]);

        //Create role user
        $role_user = DB::table('role_user')->insert(
            ['user_id' => $user->id, 'role_id' => config("constants.EMPLOYER")]
        );

        //Add user log
        $data['fkUsersID'] = $user->id;
        $data['userLogsActivity'] = 'register';
        $data['userLogsActivityDescription'] = 'register';
        $userLog = fnUserLog($data);

        if( !$user || !$role_user || !$userLog )
        {
            DB::rollBack();
        } else {
            DB::commit();
            auth()->login($user, $remember);
            $user = array('title'=>$request['gender'], 'name'=>$request['name'], 'email'=>$request['email'], 'password'=>$request['password'], 'phone'=>$request['phone']);
            Mail::send('emails.userRegisterInfo', ['user' => $user], function ($m) use ($user) {
                $m->to($user['email'], $user['name'])->subject('Registration confirmation E-mail');
            });
            return redirect( \Lang::getLocale().'/account/employer/registerCompany');
        }
    }
}
