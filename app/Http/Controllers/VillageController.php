<?php

namespace App\Http\Controllers;

use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Flash;
use Lang;
use App\Commune;
use App\District;
use App\Village;

class VillageController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getVillagesAjax']]);
    }

    public function index()
    {
      $villages = DB::table('Villages')
            ->Join('Communes', 'fkCommunesID', '=', 'pkCommunesID')
            ->select('pkVillagesID', 'fkCommunesID', 'villagesNameEN', 'villagesNameKH', 'villagesStatus', 'CommunesNameEN', 'CommunesNameKH')
          ->paginate( 20 );
      return view('accounts.countries.villages.index', ['villages' => $villages]);
    }

    public function create()
    {
        //get province name
        if( Lang::getLocale() == 'en' ){
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
        }
        else{
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
        }

        return view('accounts.countries.villages.add', ['provinces' => $provinces] );
    }

    public function store(Request $request)
    {
        // 'fkCommunesID' => 'required|exists:Communes,pkCommunesID',
        $this->validate($request, [
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'fkDistrictsID' => 'required|exists:Districts,pkDistrictsID',
            'fkCommunesID' => 'required|exists:Communes,pkCommunesID',
            'pkVillagesID' => 'required|unique:Villages',
            'villagesNameEN' => 'required',
            'villagesNameKH' => 'required',
            'villagesStatus' => 'required'
        ]);
        $input = $request->all();
//        dump($input);
//        die();
        Village::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect(\Lang::getLocale().'/account/village');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
     {
         $villages = Village::findOrFail($id);

         //get field pkDistrict from table tblDistricts
         $district = DB::table('Districts')
             ->join('Communes', 'Communes.fkDistrictsID', '=', 'Districts.pkDistrictsID')
             ->where('Communes.pkCommunesID', $villages->fkCommunesID )
             ->select('Districts.pkDistrictsID', 'Communes.pkCommunesID')
             ->first();
         $districtID = $district->pkDistrictsID;

         //get field pkProvincesID from table tblProvinces
         $province = DB::table('Provinces')
             ->join('Districts', 'Districts.fkProvincesID', '=', 'Provinces.pkProvincesID')
             ->where('Districts.pkDistrictsID', $districtID)
             ->select('Provinces.pkProvincesID')
             ->first();
         $provincesID = $province->pkProvincesID;


         if( Lang::getLocale() == 'en' ){
             $communes = Commune::where('communesStatus', true)->where('Communes.fkDistrictsID', $districtID)->orderBy('CommunesNameEN')->pluck('CommunesNameEN', 'pkCommunesID');
             $districts = District::where('districtsStatus', true)->where('Districts.fkProvincesID', $provincesID)->orderBy('districtsNameEN')->pluck('districtsNameEN', 'pkDistrictsID');

             $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
         }
         else{
             $communes = Commune::where('communesStatus', true)->where('Communes.fkDistrictsID', $districtID)->orderBy('CommunesNameKH')->pluck('CommunesNameKH', 'pkCommunesID');
             $districts = District::where('districtsStatus', true)->where('Districts.fkProvincesID', $provincesID)->orderBy('districtsNameKH')->pluck('districtsNameKH', 'pkDistrictsID');

             $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
         }

         return view('accounts.countries.villages.edit', compact('villages', 'communes', 'districts', 'provinces', 'districtID', 'provincesID'));
     }

     public function update( Request $request, $id)
     {
         $villages = Village::findOrFail($id);

         $this->validate($request, [
             'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
             'fkDistrictsID' => 'required|exists:Districts,pkDistrictsID',
             'fkCommunesID' => 'required|exists:Communes,pkCommunesID',
             'pkVillagesID' => 'required|unique:Villages,pkVillagesID,'.$id.',pkVillagesID',
             'villagesNameEN' => 'required',
             'villagesNameKH' => 'required',
             'villagesStatus' => 'required'
         ]);
         $input = $request->all();

         $villages->fill($input)->save();
         Flash::message( trans('text_lang.updateSuccessful') );
         return redirect(\Lang::getLocale().'/account/village');
     }

    public function destroy($id)
    {
        Village::findOrFail($id)->delete();;

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/village');
    }

    public function getVillagesAjax(Request $request){
        $villages = Village::select('villagesNameEN', 'villagesNameKH', 'pkVillagesID')
            ->where('fkCommunesID', '=', $request->input('communeId'))
            ->where('villagesStatus', '=', 1)
            ->get();

        //array_add( $provinces, '', trans('text_lang.selectOption'));
        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $villages];
    }

}
