<?php

namespace App\Http\Controllers;

use App\Position;
use App\Sector;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Lang;
use Laracasts\Flash\Flash;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getPositionsAjax']]);
    }

    public function index()
    {
        $positionsCode = null;
        $positionsNameEN = null;
        $positionsNameKH = null;
        $lang =  strtoupper(Lang::getLocale());

        $positions = DB::table('Positions')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Positions.fkSectorsID')
            ->orderBy('Positions.positionsOrder')
            ->select(
                'Positions.pkPositionsID', 'Positions.positionsCode' ,'Positions.fkSectorsID',
                'Positions.positionsName'.$lang,
                'Positions.positionsOrder', 'Positions.positionsStatus',
                'Sectors.sectorsName'.$lang
                )
            ->orderBy('pkPositionsID', 'DESC')
            ->where('positionsStatus', '<>', 3)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.positions.index', compact('positions', 'positionsCode', 'positionsNameEN', 'positionsNameKH', 'lang') );
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        return view('accounts.positions.add', compact('sectors')  );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fkSectorsID' => 'required',
            'positionsNameEN' => 'required',
            'positionsNameKH' => 'required',
            'positionsNameZH' => 'required',
            'positionsNameTH' => 'required',
            'positionsStatus' => 'required',
            'positionsCode' => 'required|min:5'
        ]);

        $input = $request->all();
        Position::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/position');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $position = Position::findOrFail($id);

        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        return view('accounts.positions.edit', compact('position', 'sectors') );
    }

    public function update( Request $request, $id)
    {
        $position = Position::findOrFail($id);

        $this->validate($request, [
            'fkSectorsID' => 'required',
            'positionsNameEN' => 'required',
            'positionsNameKH' => 'required',
            'positionsNameZH' => 'required',
            'positionsNameTH' => 'required',
            'positionsStatus' => 'required',
            'positionsCode' => 'required|min:5'
        ]);

        $input = $request->all();
        $position->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/position');
    }

    public function destroy($id)
    {
        Position::where('pkPositionsID', '=', $id)
            ->update(['positionsStatus' => '3']);

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/position');
    }

    public function getPositionsAjax(Request $request){
        $positions = Position::select('Positions.pkPositionsID', 'Positions.positionsNameEN', 'Positions.positionsNameKH', 'Positions.positionsNameZH', 'Positions.positionsNameTH')
            ->where('fkSectorsID', '=', $request->input('sectorId'))
            ->where('positionsStatus', '=', 1)
            ->orderBy('positionsOrder')
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $positions];
    }

    public function searchPosition( Request $request )
    {
        $lang =  strtoupper(Lang::getLocale());
        $positionsCode = strtoupper( $request->get('positionsCode') );
        $positionsNameEN = $request->get('positionsNameEN');
        $positionsNameKH = $request->get('positionsNameKH');

        $searchCriteria = [
            'positionsCode' => trim(strtoupper( $request->get('positionsCode') )),
            'positionsNameEN' => trim($request->get('positionsNameEN')),
            'positionsNameKH' => trim($request->get('positionsNameKH')),
        ];

        $strWhere = '';
        foreach($searchCriteria as $column => $value){
            if( $value == '' ){
                $$column = null;
                continue;
            }

            $$column = trim($value);

            if( $strWhere == '' ){
                if( $column == 'positionsNameEN' ||  $column == 'positionsNameKH' ){
                    $strWhere = " ( tblPositions.positionsNameEN LIKE '%". $value . "%' OR tblPositions.positionsNameKH LIKE '%". $value . "%' ) ";
                }else{
                    $strWhere = 'tblPositions.' . $column. " = '". $value."' ";
                }
            }else{
                if( $column == 'positionsNameEN' ||  $column == 'positionsNameKH'){
                    $strWhere .= " AND ( tblPositions.positionsNameEN LIKE '%". $value . "%' OR tblPositions.positionsNameKH LIKE '%". $value . "%' ) ";
                }else{
                    $strWhere .= ' AND tblPositions.' . $column. " = '". $value."' ";
                }
            }

        }

        if( $strWhere != ''  ){
            $positions = DB::table('Positions')
                ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Positions.fkSectorsID')
                ->orderBy('Positions.positionsName'.$lang)
                ->select(
                    'Positions.pkPositionsID', 'Positions.positionsCode' ,'Positions.fkSectorsID', 'Positions.positionsNameEN', 'Positions.positionsNameKH', 'Positions.positionsOrder', 'Positions.positionsStatus',
                    'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH'
                )
                ->whereRaw($strWhere)
                ->where('positionsStatus', '<>', 3)
                ->paginate( config("constants.PAGINATION_NUM_MAX") );
        }else{
            $positions = DB::table('Positions')
                ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Positions.fkSectorsID')
                ->orderBy('Positions.positionsName'.$lang)
                ->select(
                    'Positions.pkPositionsID', 'Positions.positionsCode' ,'Positions.fkSectorsID', 'Positions.positionsNameEN', 'Positions.positionsNameKH', 'Positions.positionsOrder', 'Positions.positionsStatus',
                    'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH'
                )
                ->where('positionsStatus', '<>', 3)
                ->paginate( config("constants.PAGINATION_NUM_MAX") );
        }

        return view('accounts.positions.index', compact('lang', 'positions', 'positionsCode', 'positionsNameKH', 'positionsNameEN') );
    }

}
