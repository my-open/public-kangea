<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Flash;
use Lang;
use App\Sector;
use DB;

class SectorController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getSectorsAjax']]);
    }

    public function index()
    {
        $sectors = DB::table('Sectors')
            ->select('pkSectorsID', 'sectorsPhoto', 'sectorsNameEN', 'sectorsNameKH',  'sectorsNameZH', 'sectorsNameTH', 'sectorsStatus', 'created_at')
            ->where('sectorsStatus', '<>', 3)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );
        return view('accounts.sectors.index', ['sectors' => $sectors, 'lang' => strtoupper(Lang::getLocale())]);
    }

    public function create()
    {
        if( Lang::getLocale() == 'en' ) $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameEN')->pluck('sectorsNameEN', 'pkSectorsID');
        else $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameKH')->pluck('sectorsNameKH', 'pkSectorsID');

        return view('accounts.sectors.add', ['sectors' => $sectors]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'sectorsPhoto' => 'required|image|mimes:jpeg,jpg,png',
            'sectorsNameEN' => 'required|max:100',
            'sectorsNameKH' => 'required|max:100',
            'sectorsNameZH' => 'required|max:100',
            'sectorsNameTH' => 'required|max:100',
            'sectorsStatus' => 'required|max:1'
        ]);

        if ( $request->file('sectorsPhoto')->isValid() ) {
            $file = $request->file('sectorsPhoto');
            $extension = $file->getClientOriginalExtension();
            $fileName = time() . '.' . $extension;
            if( $file->move('images/sectors', $fileName))
            {
                $input = $request->all();
                $input['sectorsPhoto'] = $fileName;
                Sector::create($input);
            }
        }

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/sector');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $sector = Sector::findOrFail($id);
        return view( 'accounts.sectors.edit', ['sector' => $sector] );
    }

    public function update(Request $request, $id)
    {
        $sector = Sector::findOrFail($id);

        $this->validate($request, [
            'sectorsPhoto' => 'image|mimes:jpeg,jpg,png',
            'sectorsNameEN' => 'required|max:100',
            'sectorsNameKH' => 'required|max:100',
            'sectorsNameZH' => 'required|max:100',
            'sectorsNameTH' => 'required|max:100',
            'sectorsStatus' => 'required|max:1'
        ]);

        $input = $request->all();
        if( !empty( $input['sectorsPhoto'] )){
            if ( $request->file('sectorsPhoto')->isValid() ) {
                $file = $request->file('sectorsPhoto');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if( $file->move('images/sectors', $fileName))
                {
                    $input['sectorsPhoto'] = $fileName;
                }
            }
        }else{
            $input['sectorsPhoto'] = $sector->sectorsPhoto;
        }

        $sector->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect( \Lang::getLocale().'/account/sector');
    }

    public function destroy($id)
    {
        Sector::where('pkSectorsID', '=', $id)
            ->update(['sectorsStatus' => '3']);

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/sector');
    }

    public function getSectorsAjax(Request $request){
        $sectors = Sector::select('sectorsNameEN', 'sectorsNameKH', 'pkSectorsID')
            ->where('fkZonesID', '=', $request->input('zoneId'))
            ->where('sectorsStatus', '=', 1)
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $sectors];
    }

}
