<?php

namespace App\Http\Controllers;

use App\JobApply;
use Illuminate\Http\Request;

use App\Http\Requests;
use Flash;
use Lang;
use DB;
use Auth;

class SeekerController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:jobseeker');
    }

    public function seekerApplied()
    {
        $userId =  Auth::id();
        $lang =  strtoupper(Lang::getLocale());
        $jobApplied = DB::table('JobApply')
            ->leftjoin('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
            ->leftjoin('Companies', 'JobApply.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->select('JobApply.pkJobApplyID', 'JobApply.fkAnnouncementsID', 'Announcements.pkAnnouncementsID', 'JobApply.fkCompaniesID', 'JobApply.created_at', 'Companies.companiesNameEN', 'Companies.companiesNameKH', 'Companies.companiesEmail', 'Companies.companiesPhone',
                'Announcements.announcementsStatus', 'Positions.positionsName'.$lang,
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->where('companiesStatus', 1)
            ->where('positionsStatus', 1)
            ->where('fkUsersID', '=', $userId)
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.users.jobseekers.jobApply', compact('jobApplied', 'lang') );
    }
    public function message()
    {
        $messages = DB::table('Candidates')
            ->join('Announcements', 'Announcements.pkAnnouncementsID', '=', 'Candidates.fkAnnouncementsID')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'Candidates.fkCompaniesID')
            ->join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->where('Candidates.fkUsersID', '=', Auth::id() )
            ->orderBy('Candidates.pkCandidatesID', 'DESC')
            ->select(
                'Candidates.pkCandidatesID',
                'Candidates.fkAnnouncementsID',
                'Candidates.seekerStatus',
                'Candidates.fkCompaniesID', 'Companies.companiesNameEN', 'Companies.companiesNameKH',
                'Announcements.announcementsClosingDate',
                'Announcements.fkPositionsID', 'Positions.positionsNameEN', 'Positions.positionsNameKH',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.users.jobseekers.message', compact('messages', 'positonName'));
    }

}
