<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Wednesday 23, Nov 2016
Modified Date = Wednesday 23, Nov 2016
*/

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests;
use Illuminate\Http\Request;
use Lang;
use Auth;
use DB;

class SudoController extends Controller
{
    public function __construct()
    {

    }

    public function sudo()
    {
        $isAdmin = isAdmin();
        if(!$isAdmin) {
            return redirect( \Lang::getLocale());
        }
        $lang = strtoupper(Lang::getLocale());
        $companies = Company::where('companiesStatus', '<>', 3)->orderBy('companiesName'.$lang)->pluck('companiesName'.$lang, 'pkCompaniesID');

        return view('sudo.index', compact('companies','lang'));
    }

    public function sudoAsEmpPost( Request $request ){
        $isAdmin = isAdmin();
        if($isAdmin) {
            $this->validate($request, [
                'fkCompaniesID' => 'required'
            ]);

            DB::table('role_user')
                ->where('user_id', Auth::id())
                ->update(['role_id' => 2]);

            DB::table('CompanyUsers')->insert(
                ['fkCompaniesID' => $request->get('fkCompaniesID'), 'fkUsersID' => Auth::id()]
            );

            //Add user log
            $data['fkUsersID'] = Auth::id();
            $data['userLogsActivity'] = 'sudo';
            $activityDes['fkUsersID'] = Auth::id();
            $activityDes['fkCompaniesID'] = $request->get('fkCompaniesID');
            $data['userLogsActivityDescription'] = json_encode( $activityDes );
            fnUserLog($data);

            return redirect(\Lang::getLocale() . '/account/postjob/');
        }else{
            return redirect( \Lang::getLocale());
        }
    }

    public function clearSudo(){
        $isAdmin = isAdmin();
        if($isAdmin){

            //Add user log
            $comID = DB::table('CompanyUsers')->where('fkUsersID', Auth::id())->value('fkCompaniesID');
            $data['fkUsersID'] = Auth::id();
            $data['userLogsActivity'] = 'clearSudo';
            $activityDes['fkUsersID'] = Auth::id();
            $activityDes['fkCompaniesID'] = $comID;
            $data['userLogsActivityDescription'] = json_encode( $activityDes );
            fnUserLog($data);

            DB::table('role_user')
                ->where('user_id', Auth::id())
                ->update(['role_id' => 1]);

            DB::table('CompanyUsers')->where('fkUsersID', Auth::id())->delete();
            if( fnIsSale() ){
                return redirect( \Lang::getLocale().'/account/company');
            }else{
                return redirect( \Lang::getLocale().'/account');
            }
        }else{
            return redirect( \Lang::getLocale());
        }
    }
}
