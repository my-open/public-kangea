<?php

namespace App\Http\Controllers;

use App\Commune;
use App\District;
use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Flash;
use Lang;

class CommuneController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getCommunesAjax', 'getCountJobByCommuneAjax']]);
    }

    public function index()
    {
        $lang =  strtoupper(Lang::getLocale());
        $communes = DB::table('Communes')
            ->Join('Districts', 'fkDistrictsID', '=', 'pkDistrictsID')
            ->select('pkCommunesID', 'fkDistrictsID',
                'CommunesName'.$lang,
                'communesStatus',
                'districtsName'.$lang)
            ->paginate( 20 );
        return view('accounts.countries.communes.index', ['communes' => $communes, 'lang' => $lang]);
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

        return view('accounts.countries.communes.add', ['provinces' => $provinces]);
    }

    public function store(Request $request)
    {
          $this->validate($request, [
              'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
              'fkDistrictsID' => 'required|exists:Districts,pkDistrictsID',
              'pkCommunesID' => 'required|unique:Communes',
              'CommunesNameEN' => 'required',
              'CommunesNameKH' => 'required',
              'CommunesNameZH' => 'required',
              'CommunesNameTH' => 'required',
              'communesStatus' => 'required'
          ]);
          $input = $request->all();
          Commune::create($input);

          Flash::message( trans('text_lang.addSuccessful') );
          return redirect(\Lang::getLocale().'/account/commune');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
     {
         $communes = Commune::findOrFail($id);

         //get field pkProvincesID from table tblProvinces
         $province = DB::table('Provinces')
             ->join('Districts', 'Districts.fkProvincesID', '=', 'Provinces.pkProvincesID')
             ->where('Districts.pkDistrictsID', $communes->fkDistrictsID)
             ->select('Provinces.pkProvincesID')
             ->first();
         $provincesID = $province->pkProvincesID;

         $lang =  strtoupper(Lang::getLocale());
         $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
         $Districts = District::where('districtsStatus', true)->where('Districts.fkProvincesID', $provincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');

         return view('accounts.countries.communes.edit', compact('communes', 'Districts', 'provinces', 'provincesID'));
     }

     public function update( Request $request, $id)
     {
         $communes = Commune::findOrFail($id);
         $this->validate($request, [
             'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
             'fkDistrictsID' => 'required|exists:Districts,pkDistrictsID',
             'pkCommunesID' => 'required|unique:Communes,pkCommunesID,'.$id.',pkCommunesID',
             'CommunesNameEN' => 'required',
             'CommunesNameKH' => 'required',
             'CommunesNameZH' => 'required',
             'CommunesNameTH' => 'required',
             'communesStatus' => 'required'
         ]);
         $input = $request->all();

         $communes->fill($input)->save();
         Flash::message( trans('text_lang.updateSuccessful') );
         return redirect(\Lang::getLocale().'/account/commune');
     }

    public function destroy($id)
    {
        Commune::findOrFail($id)->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/commune');
    }

    public function getCommunesAjax(Request $request){
        $communes = Commune::select('communesNameEN', 'communesNameKH', 'communesNameZH', 'communesNameTH', 'pkCommunesID')
            ->where('fkDistrictsID', '=', $request->input('districtId'))
            ->where('communesStatus', '=', 1)
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $communes];
    }

    public function getCountJobByCommuneAjax(Request $request){
        $lang = strtoupper(Lang::getLocale());
        $districtId = $request->input('districtId'); //209

        $countJobByCommunes = array();
        if( $districtId != '' ){
            $strWhere = 'a.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND a.announcementsPublishDate <= ' . date('"Y-m-d 00:00:00"') . ' AND a.announcementsStatus = 1';
            $countJobByCommunes = DB::select(
                DB::raw("SELECT pkCommunesID, communesName{$lang}, count(pkAnnouncementsID) as Total_Announcement
                    FROM `tblCommunes`
                    LEFT JOIN (SELECT a.* FROM tblAnnouncements a WHERE $strWhere) b
                    on b.`fkCommunesID` = `pkCommunesID`
                    WHERE `tblCommunes`.fkDistrictsID = $districtId
                    group by `tblCommunes`.`pkCommunesID`
                    order BY Total_Announcement  DESC ")
            );
        }

        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $countJobByCommunes];
    }

}
