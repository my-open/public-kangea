<?php

namespace App\Http\Controllers;

use App\JobApply;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JobappliedController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:employer');
    }

    public function index()
    {
        //check user has company or not
        $checkExistsUserID = DB::table('CompanyUsers')->where('fkUsersID', '=', Auth::id())->count();
        if( $checkExistsUserID <= 0 ) {
            return redirect()->to(\Lang::getLocale() . '/account/employer/registerCompany');
        }

        $employerId =  Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->first();

        $lang =  strtoupper(Lang::getLocale());
        $jobApplied = DB::table('JobApply')
            ->leftjoin('Announcements', 'Announcements.pkAnnouncementsID', '=', 'JobApply.fkAnnouncementsID')
            ->leftjoin('Companies', 'JobApply.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->select('JobApply.pkJobApplyID', 'JobApply.fkAnnouncementsID', 'Announcements.pkAnnouncementsID', 'JobApply.fkCompaniesID', 'JobApply.created_at', 'Companies.companiesNameEN', 'Companies.companiesNameKH', 'Companies.companiesEmail', 'Companies.companiesPhone',
                'Announcements.announcementsStatus', 'Positions.positionsName'.$lang,
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->where('companiesStatus', 1)
            ->where('positionsStatus', 1)
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.users.employers.jobApply', compact('jobApplied', 'gender', 'lang') );
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $jobSeekerID = DB::table('JobApply')->where('pkJobApplyID', $id)->value('fkUsersID');

        $user = DB::table('users')
            ->select('id', 'gender', 'name', 'email' , 'phone', 'status', 'companyLevel', 'created_at')
            ->where('status', true)
            ->where('id', $jobSeekerID)
            ->first();

        $experiences = DB::table('JobExperiences')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'JobExperiences.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'JobExperiences.fkSubsectorsID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobExperiences.fkPositionsID')
            ->select(
                'JobExperiences.pkJobExperiencesID', 'JobExperiences.workExperience',
                'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH',
                'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH',
                'Positions.positionsNameEN','Positions.positionsNameKH'
            )
            ->where('JobExperiences.fkUsersID', $jobSeekerID)
            ->get();

        $targetJob = DB::table('TargetJobs')
            ->leftJoin('Sectors', 'Sectors.pkSectorsID', '=', 'TargetJobs.fkSectorsID')
            ->leftJoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'TargetJobs.fkSubsectorsID')
            ->leftJoin('Positions', 'Positions.pkPositionsID', '=', 'TargetJobs.fkPositionsID')
            ->leftJoin('Provinces', 'Provinces.pkProvincesID', '=', 'TargetJobs.fkProvincesID')
            ->leftJoin('Districts', 'Districts.pkDistrictsID', '=', 'TargetJobs.fkDistrictsID')
            ->leftJoin('Communes', 'Communes.pkCommunesID', '=', 'TargetJobs.fkCommunesID')
            ->leftJoin('Villages', 'Villages.pkVillagesID', '=', 'TargetJobs.fkVillagesID')
            ->where('TargetJobs.fkUsersID', '=', $jobSeekerID)
            ->select(
                'Sectors.pkSectorsID', 'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH',
                'Subsectors.pkSubsectorsID', 'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH',
                'Positions.pkPositionsID', 'Positions.positionsNameEN', 'Positions.positionsNameKH',
                'Provinces.pkProvincesID', 'Provinces.provincesNameEN', 'Provinces.provincesNameKH',
                'Districts.pkDistrictsID', 'Districts.districtsNameEN', 'Districts.districtsNameKH',
                'Communes.pkCommunesID', 'Communes.CommunesNameEN', 'Communes.CommunesNameKH',
                'Villages.pkVillagesID', 'Villages.villagesNameEN', 'Villages.villagesNameKH',
                'TargetJobs.pkTargetJobsID', 'TargetJobs.fkPositionsID', 'TargetJobs.fkSubsectorsID', 'TargetJobs.fkSectorsID', 'TargetJobs.fkZonesID',
                'TargetJobs.fkVillagesID', 'TargetJobs.fkCommunesID', 'TargetJobs.fkDistrictsID', 'TargetJobs.fkProvincesID', 'TargetJobs.fkCountriesID'
            )
            ->first();

        return view('accounts.users.employers.viewJobSeeker', compact('user', 'experiences', 'targetJob'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

}
