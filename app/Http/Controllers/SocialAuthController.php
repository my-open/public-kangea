<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback( SocialAccountService $service )
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());

        auth()->login($user);

        //Add user log
        $data['fkUsersID'] = Auth::id();
        $data['userLogsActivity'] = 'login';
        $data['userLogsActivityDescription'] = 'login';
        fnUserLog($data);

        //If company rep
        if( session('companyRep') ){
            Session::forget('companyRep');
            $checkExistsUserID = DB::table('CompanyUsers')->where('fkUsersID', '=', Auth::id())->count();
            if( $checkExistsUserID <= 0 ) {
                return redirect()->to(\Lang::getLocale() . '/account/employer/registerCompany');
            }
        }

        //If job seeker
        return redirect()->to( \Lang::getLocale(). '/account');
    }

    public function redirectEmp()
    {
        session(['companyRep' => true]);
        return Socialite::driver('facebook')->redirect();
    }
}