<?php

namespace App\Http\Controllers;

use App\Commune;
use App\Country;
use App\District;
use App\Experience;
use App\Province;
use App\Village;
use App\Zone;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Sector;
use App\Subsector;
use App\Position;
use Flash;
use Lang;


class ExperienceController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:jobseeker');
    }
    public function index()
    {
        $lang =  strtoupper(Lang::getLocale());
        $userLogin =  Auth::id();

        $experiences = DB::table('JobExperiences')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'JobExperiences.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'JobExperiences.fkSubsectorsID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobExperiences.fkPositionsID')
            ->select(
                'JobExperiences.pkJobExperiencesID', 'JobExperiences.workExperience',
                'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH','Sectors.sectorsNameZH', 'Sectors.sectorsNameTH',
                'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH','Subsectors.subsectorsNameZH', 'Subsectors.subsectorsNameTH',
                'Positions.positionsNameEN','Positions.positionsNameKH', 'Positions.positionsNameZH','Positions.positionsNameTH'
            )
            ->where('JobExperiences.fkUsersID', $userLogin)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.experiences.index', [ 'experiences' => $experiences, 'lang' => $lang]);
    }


    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        foreach( config("constants.EXPERIENCES") as $key => $value ){
            $workExperiences[$key] = $value[Lang::getLocale()];
        }

        return view('accounts.experiences.add', compact('lang', 'sectors', 'workExperiences'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'fkSectorsID' => 'required',
            'fkSubsectorsID' => 'required',
            'fkPositionsID' => 'required',
            'workExperience' => 'required',
        ]);

        $input = $request->all();

        $userLogin =  Auth::id();
        $input['fkUsersID'] = $userLogin;

        DB::table('users')
            ->where('id', $userLogin)
            ->update(['experience' => 1]);

        Experience::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/experience');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $lang =  strtoupper(Lang::getLocale());
        $pkJobExperiencesID = Experience::findOrFail($id);

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $subsectorsBySectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $pkJobExperiencesID->fkSectorsID)->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');
        $positionsBysectors = Position::where('positionsStatus', true)->where('fkSectorsID', $pkJobExperiencesID->fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName'.$lang, 'pkPositionsID');

        foreach( config("constants.EXPERIENCES") as $key => $value ){
            $workExperiences[$key] = $value[Lang::getLocale()];
        }

        return view('accounts.experiences.edit', compact('lang', 'pkJobExperiencesID', 'sectors', 'subsectorsBySectors', 'positionsBysectors', 'workExperiences'));
    }


    public function update(Request $request, $id)
    {
        $experience = Experience::findOrFail($id);

        $this->validate($request, [
            'fkSectorsID' => 'required',
            'fkSubsectorsID' => 'required',
            'fkPositionsID' => 'required',
            'workExperience' => 'required',
        ]);
        $input = $request->all();
        $experience->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/experience');
    }


    public function destroy($id)
    {
        Experience::findOrFail($id)->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/experience');
    }
}
