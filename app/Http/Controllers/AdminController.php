<?php

namespace App\Http\Controllers;

use App\Activity;
use Faker\Provider\File;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Position;
use App\Postjob;
use App\Zone;
use App\Province;
use App\District;
use App\Commune;
use App\Village;
use Mail;

use Auth;
use Flash;
use Lang;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{

    public function editJob($id)
    {
        $lang =  strtoupper(Lang::getLocale());
        $postjob = Postjob::findOrFail($id);

        $company = DB::table('Companies')
            ->select('Companies.*')
            ->where('Companies.pkCompaniesID', '=', $postjob->fkCompaniesID )
            ->first();

        $sectorName = DB::table('Sectors')->where('pkSectorsID', $company->fkSectorsID)->value('sectorsName'.$lang);
        $subsectorName = DB::table('Subsectors')->where('pkSubsectorsID', $company->fkSubsectorsID)->value('subsectorsName'.$lang);
        $activityName = DB::table('Activities')->where('pkActivitiesID', $company->fkActivitiesID)->value('activitiesName'.$lang);
        $provincesName = DB::table('Provinces')->where('pkProvincesID', $company->fkProvincesID)->value('provincesName'.$lang);
        $districtName = DB::table('Districts')->where('pkDistrictsID', $company->fkDistrictsID)->value('districtsName'.$lang);

        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $postjob->fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $postjob->fkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
        $positions = Position::where('positionsStatus', true)->where('fkSectorsID', $company->fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName'.$lang, 'pkPositionsID');
        $activities = Activity::where('activitiesStatus', true)->where('fkSubsectorsID', $company->fkSubsectorsID)->orderBy('activitiesOrder')->pluck('activitiesName'.$lang, 'pkActivitiesID');

        if( !empty($postjob) ){
            $languages = json_decode( $postjob->announcementsLanguage );
            $benefits = json_decode( $postjob->announcementsBenefit );
            $certificates = json_decode( $postjob->announcementsCertificate );
            $announcementsLanguageLevels = json_decode( $postjob->announcementsLanguageLevel );
            $announcementsWorkTime = json_decode( $postjob->announcementsWorkTime );
        }

        $days = array(''=>trans('text_lang.selectOption'), 'mon'=>trans('text_lang.monday'), 'tue'=>trans('text_lang.tuesday'), 'wed'=>trans('text_lang.wednesday'), 'thu'=>trans('text_lang.thursday'), 'fri'=>trans('text_lang.friday'), 'sat'=>trans('text_lang.saturday'), 'sun'=>trans('text_lang.sunday'));

        return view('accounts.postjobs.edit', compact('postjob', 'announcementsWorkTime', 'languages', 'announcementsLanguageLevels', 'benefits', 'certificates', 'positions', 'activities','zones', 'provinces', 'districtByProvinces', 'communeByDistricts', 'villageByCommunes', 'days', 'company', 'sectorName', 'subsectorName', 'activityName', 'zoneName', 'provincesName', 'districtName')  );
    }

    public function updateJob(Request $request, $id)
    {
        $isPositionIdOther = false;
        $postjob = Postjob::findOrFail($id);

        $company = DB::table('Companies')
            ->select('Companies.*')
            ->where('Companies.pkCompaniesID', '=', $postjob->fkCompaniesID )
            ->where('Companies.companiesStatus', 1)
            ->first();

        $required = [
            'announcementsPublishDate' => 'required|date',
            'announcementsClosingDate'=> 'required|date|after:announcementsPublishDate',

            'fkPositionsID'=>'required',
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'AnnouncementsAgeFrom'=>'required',
            'announcementsSalaryType' => 'required',

            'AnnouncementsFromDay' => 'required',
            'AnnouncementsToDay' => 'required',
            'announcementsRequiredDocType' => 'required',

            'announcementsIsFullTime' => 'required',
            'announcementsWorkTime' => 'required'
        ];

        if( empty($request['announcementsSalaryNegotiate'])){
            $required['announcementsSalaryFrom'] = 'required';
//            $required['announcementsSalaryTo'] = 'required';
        }

        if( $request->get('fkPositionsID') == '0' ){
            $required['positionOther'] = 'required';
            $isPositionIdOther = true;
        }

        // check Experince....
        if( !empty( $request['AnnouncementsExperiencePositionN'] ) ){
            $required['AnnouncementsPositionType'] = 'required';
        }
        if( !empty( $request['AnnouncementsExperienceSectorN'] ) ){
            $required['AnnouncementsSectorType'] = 'required';
        }

        $this->validate($request, $required);

        if($isPositionIdOther){
            $position = Position::create([
                'fkSectorsID' => $company->fkSectorsID,
                'positionsNameEN' => $request->get('positionOther'),
                'positionsNameKH' => $request->get('positionOther'),
                'positionsNameZH' => $request->get('positionOther'),
                'positionsNameTH' => $request->get('positionOther'),
                'positionsStatus' => 1,
            ]);

            $pkPositionsID = $position->pkPositionsID;
            if (strlen( $pkPositionsID ) == 2 ){
                $positionsCode = 'P00'.$pkPositionsID;
            }elseif( strlen( $pkPositionsID ) > 2 ){
                $positionsCode = 'P0'.$pkPositionsID;
            }else{
                $positionsCode = 'P000'.$pkPositionsID;
            }
            $request['fkPositionsID'] = $pkPositionsID;
            Position::where('pkPositionsID', $pkPositionsID)->update(array('positionsCode' => $positionsCode));

            $emails = config("constants.ADMIN_EMAIL");
            $positionName = $request->get('positionOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Hi, Admin of Bongpheak Job. Now you have new position has been created with '.$domainName.'. The new position name is '.$positionName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New Position created in '.$domainName);
            });
        }

        $input = $request->all();

        if( $request->get('AnnouncementsAgeUntil') == ''){ $input['AnnouncementsAgeUntil'] = $input['AnnouncementsAgeFrom']; }
        $input['fkSectorsID'] = $company->fkSectorsID;
        $input['fkSubsectorsID'] = $company->fkSubsectorsID;
        //$input['fkActivitiesID'] = $company->fkActivitiesID;

        if( $input['AnnouncementsExperiencePositionN'] == '12' && $input['AnnouncementsPositionType'] == 'month' ){
            $input['AnnouncementsExperiencePositionN'] = 1;
            $input['AnnouncementsPositionType'] = 'year';
        }

        if( $input['AnnouncementsExperienceSectorN'] == '12' && $input['AnnouncementsSectorType'] == 'month' ){
            $input['AnnouncementsExperienceSectorN'] = 1;
            $input['AnnouncementsSectorType'] = 'year';
        }

        $langLevel = null;

        //get fkPositionsCode from tblPositon
        $GetfkPositionsID = $request['fkPositionsID'];
        $GetfkPositionsCode = Position::where('positionsStatus', true)->where('pkPositionsID', $GetfkPositionsID)->value('positionsCode');
        $input['fkPositionsCode']= $GetfkPositionsCode;

        $input['announcementsCertificate'] = json_encode( $request->get('certificate') );
        $input['announcementsBenefit'] = json_encode( $request->get('benefit') );
        $input['announcementsLanguage'] = json_encode( $request->get('language') );
        $input['announcementsWorkTime'] = json_encode( $request->get('announcementsWorkTime') );
        if( !empty($request->get('language')) ){
            foreach($request->get('language') as $key => $value){
                $langLevel[$key] = $request->get('announcementsLanguageLevel')[$key];
            }
        }
        $input['announcementsLanguageLevel'] = json_encode( $langLevel );
        if( $request->get('announcementsAllowWithoutExperience') == null ){
            $input['announcementsAllowWithoutExperience'] = 0;
        }

        // check Experience depend on Salary
        if( empty($request['announcementsSalaryNegotiate']) ){
            if( (int)$request['announcementsSalaryTo'] <= (int)$request['announcementsSalaryFrom']  ){
                $input['AnnouncementsSalaryDependsOn'] = '' ;
            }
            $input['announcementsSalaryNegotiate']=0;
        }else{
            $input['announcementsSalaryTo']='';
            $input['announcementsSalaryFrom']='';
            $input['AnnouncementsSalaryDependsOn'] = '' ;
        }

        $redirect = '/account/company/view/'.$postjob->fkCompaniesID;
        if (isset($_REQUEST['previewJob']) ){
            $postjob->fill($input)->save();
            $postjob = Postjob::findOrFail($id);
            $provinceName = DB::table('Provinces')->where('pkProvincesID', $postjob->fkProvincesID)->value('provincesNameEN');
            $sectorName = DB::table('Sectors')->where('pkSectorsID', $postjob->fkSectorsID)->value('sectorsNameEN');
            $positionName = DB::table('Positions')->where('pkPositionsID', $postjob->fkPositionsID)->value('positionsNameEN');
            $province_name = fnConvertSlug($provinceName);
            $sector_name = fnConvertSlug($sectorName);
            $position_name = fnConvertSlug($positionName);
            $redirect = '/account/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $postjob->pkAnnouncementsID;
        }elseif( isset($_REQUEST['publishJob']) ){
            $input['announcementsStatus']  = 1 ;
            $postjob->fill($input)->save();
        }else{
            $postjob->fill($input)->save();
        }

        $job = Postjob::findOrFail($id);

        $mJob = new Postjob();
        $mJob->convertConfirmJobApplyMp3( $job );
        $mJob->convertConfirmJobApplyNoCompanyMp3($job);
        $mJob->convertJobMp3( $job );

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().$redirect);
    }

    public function viewCompany( $pkCompaniesID ) {
        $lang =  strtoupper(Lang::getLocale());
        //Get company info
        $company = DB::table('Companies')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Companies.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Companies.fkSubsectorsID')
            ->leftjoin('Activities','Activities.pkActivitiesID','=','Companies.fkActivitiesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Companies.fkZonesID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Companies.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Companies.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Companies.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Companies.fkVillagesID')
            ->where('Companies.pkCompaniesID', $pkCompaniesID)
            ->where('Companies.companiesStatus', '<>', 3)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName', 'Companies.companiesDescription'.$lang,
                'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesXCoordinate', 'Companies.companiesYCoordinate', 'Companies.companiesDescription'.$lang, 'Companies.companiesPhone', 'Companies.companiesEmail',
                'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Activities.activitiesName'.$lang,
                'Provinces.provincesName'.$lang,
                'Districts.districtsName'.$lang,
                'Communes.CommunesName'.$lang,
                'Villages.villagesNameEN'
            )
            ->first();

        //Get jobs
        $jobs = DB::table('Announcements')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Announcements.fkZonesID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.fkCompaniesID', $pkCompaniesID)
            ->where('Companies.companiesStatus', '<>', 3)
            ->where('Announcements.announcementsStatus', '<>', 3)
            ->orderby('Announcements.pkAnnouncementsID', 'DES')
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesNameEN', 'Companies.companiesName'.$lang, 'Companies.companiesNickName',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsClosingDate', 'Announcements.announcementsStatus',
                'Provinces.provincesName'.$lang,
                'Districts.districtsName'.$lang,
                'Communes.CommunesName'.$lang,
                'Villages.villagesNameEN',
                'Positions.positionsName'.$lang,
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts/companies/viewCompany', compact('lang', 'company', 'jobs'));
    }

    public function jobPublish( $pkAnnouncementsID ){

        DB::table('Announcements')
            ->where('pkAnnouncementsID', $pkAnnouncementsID)
            ->update(['announcementsStatus' => 1]);
         DB:: table('Announcements')->where('pkAnnouncementsID', $pkAnnouncementsID )->value('fkCompaniesID');

        return redirect()->back();
    }

    public function jobUnpublish( $pkAnnouncementsID ){

        DB::table('Announcements')
            ->where('pkAnnouncementsID', $pkAnnouncementsID)
            ->update(['announcementsStatus' => 0]);
        DB:: table('Announcements')->where('pkAnnouncementsID', $pkAnnouncementsID )->value('fkCompaniesID');

        return redirect()->back();
    }

    public function repost($id){
        Postjob::repost( $id );

        Flash::message( trans('text_lang.jobHasbeenRepost') );
        return redirect()->back();
    }

    public function deleteJob($id)
    {
        Postjob::where('pkAnnouncementsID', '=', $id)
            ->update(['announcementsStatus' => '3']);

        Postjob::findOrFail($id);

        Flash::message( trans('text_lang.deleteSuccessful') );

        return redirect()->back();
    }

    public function applicants(){
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        $companyID = null;
        $applyByValue = null;

        $companies = DB::table('Companies')
            ->orderBy('companiesName'.$lang)
            ->where('companiesStatus', 1)
            ->pluck('companiesName'.$lang, 'pkCompaniesID');

        $strWhere = 'tblJobApply.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';

        $applicants = DB::table('JobApply')
            ->leftjoin('Companies', 'JobApply.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->select(
                "pkJobApplyID", "JobApply.fkAnnouncementsID", "JobApply.fkSharesID", "JobApply.fkCompaniesID", "JobApply.fkUsersID", "JobApply.fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "JobApply.created_at", "JobApply.updated_at",
                'Companies.companiesName'.$lang, 'Companies.pkCompaniesID',
                'pkPositionsID', 'positionsName'.$lang
            )
            ->whereRaw( $strWhere )
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.applications.index', compact('fromDate', 'toDate', 'companies', 'applicants', 'companyID', 'lang', 'applyByValue'));
    }

    public function exportApplicant( Request $request )
    {
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $companyID = $request->get('fkCompaniesID');
        $applyByValue = $request->get('jobApplyBy');

        $strWhere = 'tblJobApply.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';
        if( $companyID != '' ){
            $strWhere .= ' AND tblJobApply.fkCompaniesID = '. $companyID;
        }

        if( $applyByValue != '' ){
            $strWhere .= ' AND tblJobApply.jobApplyBy = '.$applyByValue ;
        }

        $applicants = DB::table('JobApply')
            ->leftjoin('Companies', 'JobApply.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->select('pkJobApplyID', 'jobApplyName', 'jobApplyGender', 'jobApplyPhone', 'Positions.positionsName'.$lang, 'Companies.companiesName'.$lang, 'JobApply.created_at')
            ->whereRaw( $strWhere )
            ->where('companiesStatus', 1)
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->get();

        // Define the Excel spreadsheet headers
        $applicantsArray[] = [ trans('text_lang.id'), trans('text_lang.applyName'), trans('text_lang.gender'), trans('text_lang.phone'), trans('text_lang.position'), trans('text_lang.companiesName'),  trans('text_lang.date') ];

        // Convert each member of the returned collection into an array,
        foreach ($applicants as $applicant) {
            $applicantsArray[] = (array)$applicant;
        }

        // Generate and return the spreadsheet
        Excel::create('applicants_'.$fromDate.'-'.$toDate, function($excel) use ($applicantsArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Applicants');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('applicants file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($applicantsArray) {
                $sheet->fromArray($applicantsArray, null, 'A1', false, false);
            });

        })->download('xls');

    }

    public function searchApplicants(Request $request){
        $lang = strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $companyID = $request->get('fkCompaniesID');
        $applyByValue = $request->get('jobApplyBy');

        $companies = DB::table('Companies')
            ->orderBy('companiesName'.$lang)
            ->where('companiesStatus', 1)
            ->pluck('companiesName'.$lang, 'pkCompaniesID');

        $strWhere = 'tblJobApply.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';
        if( $companyID != '' ){
            $strWhere .= ' AND tblJobApply.fkCompaniesID = '. $companyID;
        }

        if( $request->jobApplyBy != '' ){
            $strWhere .= ' AND tblJobApply.jobApplyBy = '.$request->jobApplyBy ;
        }

        $applicants = DB::table('JobApply')
            ->leftjoin('Companies', 'JobApply.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->select(
                "pkJobApplyID", "JobApply.fkAnnouncementsID", "JobApply.fkSharesID", "JobApply.fkCompaniesID", "JobApply.fkUsersID", "JobApply.fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "JobApply.created_at", "JobApply.updated_at",
                'Companies.companiesName'.$lang, 'Companies.pkCompaniesID',
                'pkPositionsID', 'positionsName'.$lang
            )
            ->whereRaw( $strWhere )
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.applications.index', compact('applicants', 'companies', 'fromDate', 'toDate', 'companyID', 'lang', 'applyByValue'));
    }

    public function checkMp3Company ()
    {
        $lang = strtoupper(Lang::getLocale());
        $dir    = 'files/sounds/kh/companies-name';
        $allFiles = array_diff(scandir($dir), array('..', '.'));

        $companyIDs = [];
        foreach ( $allFiles as $index => $value )
        {
            $subString = substr($value, 10);
            $companyID = str_replace('_kh.mp3','', $subString);
            $companyIDs[] = $companyID;
        }

        $companies = DB::table('Companies')
            ->select('pkCompaniesID', 'companiesName'.$lang, 'companiesPhone', 'companiesEmail', 'created_at')
            ->whereNotIn('pkCompaniesID', $companyIDs)
            ->where('companiesStatus', '<>', 3)
            ->get();

        return view('accounts.mp3NotYetRecords.company', compact('companies', 'lang'));
    }

    public function checkMp3Subsector ()
    {
        $lang = strtoupper(Lang::getLocale());
        $dir    = 'files/sounds/kh/subsectors';
        $allFiles = array_diff(scandir($dir), array('..', '.'));

        $subsectorIDs = [];
        foreach ( $allFiles as $index => $value )
        {
            $subString = substr($value, 3);
            $subsectorID = str_replace('_kh.mp3','', $subString);
            $subsectorIDs[] = $subsectorID;
        }

        $subsectors = DB::table('Subsectors')
            ->select('pkSubsectorsID', 'subsectorsName'.$lang, 'created_at')
            ->whereNotIn('pkSubsectorsID', $subsectorIDs)
            ->where('subsectorsStatus', '<>', 3)
            ->get();

        return view('accounts.mp3NotYetRecords.subsector', compact('subsectors', 'lang'));
    }

    public function checkMp3Activity ()
    {
        $lang = strtoupper(Lang::getLocale());
        $dir    = 'files/sounds/kh/activities';
        $allFiles = array_diff(scandir($dir), array('..', '.'));

        $activiyIDs = [];
        foreach ( $allFiles as $index => $value )
        {
            $activityID = str_replace('_kh.mp3','', $value);
            $activiyIDs[] = $activityID;
        }

        $activities = DB::table('Activities')
            ->select('pkActivitiesID', 'activitiesName'.$lang, 'created_at')
            ->whereNotIn('pkActivitiesID', $activiyIDs)
            ->where('activitiesStatus', '<>', 3)
            ->get();

        return view('accounts.mp3NotYetRecords.activity', compact('activities', 'lang'));
    }

    public function checkMp3Position ()
    {
        $lang = strtoupper(Lang::getLocale());
        $dir    = 'files/sounds/kh/positions';
        $allFiles = array_diff(scandir($dir), array('..', '.'));

        $positionIDs = [];
        foreach ( $allFiles as $index => $value )
        {
            $positionID = str_replace('_kh.mp3','', $value);
            $positionIDs[] = $positionID;
        }

        $positions = DB::table('Positions')
            ->select('pkPositionsID', 'positionsName'.$lang, 'created_at')
            ->whereNotIn('pkPositionsID', $positionIDs)
            ->where('positionsStatus', '<>', 3)
            ->get();

        return view('accounts.mp3NotYetRecords.position', compact('positions', 'lang'));
    }

    public function viewCompaniesExport ( Request $request )
    {
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $create_from  = array('0'=> trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.facebook'));
        $from ='';
        $companiesName = null;

        $strWhere = ' tblCompanies.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;

        $companies = DB::table('users')
            ->join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'CompanyUsers.fkCompaniesID')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->select('pkCompaniesID', 'companiesName'.$lang, 'companiesEmail', 'companiesPhone', 'users.name', 'users.email', 'users.phone', 'Companies.created_at')
            ->where('users.status', '=', 1)
            ->whereRaw( $strWhere )
            ->where('Companies.companiesStatus', '<>', 3)
            ->orderBy('pkCompaniesID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.exports.company', compact('lang','fromDate', 'toDate', 'companiesName', 'companies', 'create_from', 'from'));
    }

    public function searchCompany ( Request $request )
    {
        $lang = strtoupper(Lang::getLocale());
        $companiesName = $request->get('companiesName'.$lang);
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $create_from  = array('0'=> trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.facebook'));
        $from = (int)$request->get('create_from');
        $isUsedPostJob = (int)$request->get('IsUsedPostJob');

        $strWhere = ' tblCompanies.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;
        if( $companiesName != '' ){
            $strWhere .= " AND tblCompanies.companiesName".$lang." like '%". $companiesName;
        }
        if($from == 1){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id IS NULL';
        }elseif($from == 2){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id <> "" ';
        }

        if( $request->get('IsUsedPostJob') == 1 ){
            $strWhere .= ' AND tblAnnouncements.fkCompaniesID <> "" ';
        }

        $companies = DB::table('users')
            ->join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'CompanyUsers.fkCompaniesID')
            ->leftjoin('Announcements', 'Announcements.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->select('pkCompaniesID', 'companiesName'.$lang, 'companiesEmail', 'companiesPhone', 'users.name', 'users.email', 'users.phone', 'Companies.created_at')
            ->where('users.status', '=', 1)
            ->whereRaw( $strWhere )
            ->where('Companies.companiesStatus', '<>', 3)
            ->distinct()
            ->groupBy('pkCompaniesID')
            ->orderBy('Companies.pkCompaniesID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.exports.company', compact('lang', 'isUsedPostJob', 'companies', 'fromDate', 'toDate', 'companiesName', 'create_from', 'from'));
    }

    public function exportCompany(Request $request)
    {
        $lang = strtoupper(Lang::getLocale());
        $companiesName = $request->get('companiesName'.$lang);
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $from = (int)$request->get('create_from');

        $strWhere = ' tblCompanies.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;
        if( $companiesName != '' ){
            $strWhere .= " AND tblCompanies.companiesName".$lang." like '%". $companiesName . "%' ";
        }

       // $create_from  = array('0'=> trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.facebook'));
        if($from == 1){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id IS NULL';
        }elseif($from == 2){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id <> "" ';
        }

        if( $request->get('isUsedPostJob') == 1 ){
            $strWhere .= ' AND tblAnnouncements.fkCompaniesID <> "" ';
        }

        $companies = DB::table('users')
            ->join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'CompanyUsers.fkCompaniesID')
            ->leftjoin('Announcements', 'Announcements.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Companies.fkSectorsID')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Companies.fkProvincesID')
            ->select('pkCompaniesID', 'companiesName'.$lang, 'companiesEmail','sectorsName'.$lang, 'provincesName'.$lang, 'companiesPhone', 'users.id','users.name', 'users.email', 'users.phone', 'social_accounts.provider_user_id', 'Companies.created_at')
            ->where('users.status', '=', 1)
            ->whereRaw( $strWhere )
            ->where('Companies.companiesStatus', '<>', 3)
            ->distinct()
            ->groupBy('pkCompaniesID')
            ->orderBy('Companies.pkCompaniesID', 'DESC')
            ->get();

        // Header
        $companiesArray[] = [ trans('text_lang.companyID'),  trans('text_lang.companyName'), trans('text_lang.companiesEmail'), trans('text_lang.sector'), trans('text_lang.province'), trans('text_lang.companiesPhone'), trans('text_lang.id'),trans('text_lang.companyRepresentative'),  trans('text_lang.userEmail'), trans('text_lang.userPhone'), trans('text_lang.facebookId'),  trans('text_lang.createdAt') ];

        // Convert each member of the returned collection into an array,
        foreach ($companies as $company) {
            $companiesArray[] = (array) $company;
        }

        // Generate and return the spreadsheet
        Excel::create('companies_'.$fromDate.'-'.$toDate, function($excel) use ($companiesArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Company');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('Company file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($companiesArray) {
                $sheet->fromArray($companiesArray, null, 'A1', false, false);
            });

        })->download('xls');
    }

    public function viewAnnouncementsExport(Request $request)
    {
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $companiesName = null;

        $strWhere = ' tblAnnouncements.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;

        $announcements = DB::table('Announcements')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsClosingDate', 'Announcements.announcementsStatus',
                'Positions.positionsName'.$lang,
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->where('Companies.companiesStatus', '=', 1)
            ->where('Positions.positionsStatus', '=', 1)
            ->where('Announcements.announcementsStatus', '<>', 3)
            ->whereRaw( $strWhere )
            ->orderBy('Announcements.pkAnnouncementsID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.exports.announcement', compact('lang','fromDate', 'toDate', 'companiesName', 'announcements'));
    }

    public function searchAnnouncement(Request $request)
    {
        $lang = strtoupper(Lang::getLocale());
        $companiesName = trim( $request->get('companiesName'.$lang) );
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));

        $strWhere = ' tblAnnouncements.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;
        if( $companiesName != '' ){
            $strWhere .= " AND tblCompanies.companiesName".$lang." like '%". $companiesName . "%' ";
        }

//        $announcements = DB::table('Announcements')
//            ->join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
//            ->join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
//            ->select(
//                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName',
//                'Announcements.pkAnnouncementsID', 'Announcements.announcementsClosingDate', 'Announcements.announcementsStatus',
//                'Positions.positionsName'.$lang
//            )
//            ->where('Companies.companiesStatus', '=', 1)
//            ->where('Positions.positionsStatus', '=', 1)
//            ->where('Announcements.announcementsStatus', '<>', 3)
//            ->whereRaw( $strWhere )
//            ->orderBy('Announcements.pkAnnouncementsID', 'DESC')
//            ->paginate( config("constants.PAGINATION_NUM") );
        $announcements = DB::table('Announcements')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsClosingDate', 'Announcements.announcementsStatus',
                'Positions.positionsName'.$lang,
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->where('Companies.companiesStatus', '=', 1)
            ->where('Positions.positionsStatus', '=', 1)
            ->where('Announcements.announcementsStatus', '<>', 3)
            ->whereRaw( $strWhere )
            ->orderBy('Announcements.pkAnnouncementsID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.exports.announcement', compact('lang', 'fromDate', 'toDate', 'companiesName', 'announcements'));
    }

    public function postJob(Request $request, $compId){
        $lang =  strtoupper(Lang::getLocale());
        $company = DB::table('Companies')
            ->select('Companies.*')
            ->where('Companies.companiesStatus', 1)
            ->where('Companies.pkCompaniesID', $compId)
            ->first();

        $sectorName = DB::table('Sectors')->where('pkSectorsID', $company->fkSectorsID)->value('sectorsName'.$lang);
        $subsectorName = DB::table('Subsectors')->where('pkSubsectorsID', $company->fkSubsectorsID)->value('subsectorsName'.$lang);
        $activityName = DB::table('Activities')->where('pkActivitiesID', $company->fkActivitiesID)->value('activitiesName'.$lang);
        $provincesName = DB::table('Provinces')->where('pkProvincesID', $company->fkProvincesID)->value('provincesName'.$lang);
        $districtName = DB::table('Districts')->where('pkDistrictsID', $company->fkDistrictsID)->value('districtsName'.$lang);
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $company->fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $company->fkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
        $positions = Position::where('positionsStatus', true)->where('fkSectorsID', $company->fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName'.$lang, 'pkPositionsID');
        $activities = Activity::where('activitiesStatus', true)->where('fkSubsectorsID', $company->fkSubsectorsID)->orderBy('activitiesOrder')->pluck('activitiesName'.$lang, 'pkActivitiesID');

        $days = array(''=>trans('text_lang.selectOption'), 'mon'=>trans('text_lang.monday'), 'tue'=>trans('text_lang.tuesday'), 'wed'=>trans('text_lang.wednesday'), 'thu'=>trans('text_lang.thursday'), 'fri'=>trans('text_lang.friday'), 'sat'=>trans('text_lang.saturday'), 'sun'=>trans('text_lang.sunday'));

        return view('accounts.postjobs.add', compact('lang','company', 'positions', 'activities', 'sectorName', 'subsectorName', 'activityName', 'provinces', 'provincesName', 'districtName', 'districtByProvinces', 'communeByDistricts', 'days')  );
    }

    public function postJobPost( Request $request, $compId ){
        $isActivityIdOther = false;
        $isPositionIdOther = false;
        $company = DB::table('Companies')
            ->select('Companies.*')
            ->where('Companies.companiesStatus', 1)
            ->where('Companies.pkCompaniesID', $compId)
            ->first();

        $required = [
            'fkPositionsID'=>'required',
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'announcementsClosingDate'=> 'required|date',
            'AnnouncementsAgeFrom'=>'required',
            'announcementsSalaryType' => 'required',

            'AnnouncementsFromDay' => 'required',
            'AnnouncementsToDay' => 'required',
            'announcementsRequiredDocType' => 'required',

            'announcementsIsFullTime' => 'required',
            'announcementsWorkTime' => 'required',

            'announcementsIsFullTime' => 'required',
            'announcementsWorkTime' => 'required',
        ];

        if( empty($request['announcementsSalaryNegotiate']) ){
            $required['announcementsSalaryFrom'] = 'required';
//            $required['announcementsSalaryTo'] = 'required';
        }

        if( $request->get('fkPositionsID') == '0' ){
            $required['positionOther'] = 'required';
            $isPositionIdOther = true;
        }

        if( $request->get('fkActivitiesID') == 'other' ){
            $required['activityOther'] = 'required';
            $isActivityIdOther = true;
        }

        // check Experince....
        if( !empty( $request['AnnouncementsExperiencePositionN'] ) ){
            $required['AnnouncementsPositionType'] = 'required';
        }
        if( !empty( $request['AnnouncementsExperienceSectorN'] ) ){
            $required['AnnouncementsSectorType'] = 'required';
        }

        $this->validate($request, $required);

        $langLevel = null;

        if($isPositionIdOther){
            $position = Position::create([
                'fkSectorsID' => $company->fkSectorsID,
                'positionsNameEN' => $request->get('positionOther'),
                'positionsNameKH' => $request->get('positionOther'),
                'positionsNameZH' => $request->get('positionOther'),
                'positionsNameTH' => $request->get('positionOther'),
                'positionsStatus' => 1,
            ]);

            $pkPositionsID = $position->pkPositionsID;
            if (strlen( $pkPositionsID ) == 2 ){
                $positionsCode = 'P00'.$pkPositionsID;
            }elseif( strlen( $pkPositionsID ) > 2 ){
                $positionsCode = 'P0'.$pkPositionsID;
            }else{
                $positionsCode = 'P000'.$pkPositionsID;
            }
            $request['fkPositionsID'] = $pkPositionsID;
            Position::where('pkPositionsID', $pkPositionsID)->update(array('positionsCode' => $positionsCode));

            $emails = config("constants.ADMIN_EMAIL");
            $positionName = $request->get('positionOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Hi, Admin of Bongpheak Job. Now you have new position has been created with '.$domainName.'. The new position name is '.$positionName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New Position created in '.$domainName);
            });
        }

        if($isActivityIdOther){
            $activity = Activity::create([
                'fkSubsectorsID' => $company->fkSubsectorsID,
                'activitiesNameEN' => $request->get('activityOther'),
                'activitiesNameKH' => $request->get('activityOther'),
                'activitiesNameZH' => $request->get('activityOther'),
                'activitiesNameTH' => $request->get('activityOther'),
                'activitiesStatus' => 1,
            ]);

            $pkActivitiesID = $activity->pkActivitiesID;
            $request['fkActivitiesID'] = $pkActivitiesID;

            $emails = config("constants.ADMIN_EMAIL");
            $activityName = $request->get('activityOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Hi, Admin of Bongpheak Job. Now you have new activity has been created with '.$domainName.'. The new activity name is '.$activityName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New Activity created in '.$domainName);
            });
        }

        $input = $request->all();
        if( $request->get('AnnouncementsAgeUntil') == ''){ $input['AnnouncementsAgeUntil'] = $input['AnnouncementsAgeFrom']; }
        $input['announcementsPublishDate'] = date('Y-m-d 00:00:00');

        // check Experience depend on Salary
        if( empty($request['announcementsSalaryNegotiate']) ){
            if( (int)$request['announcementsSalaryTo'] <= (int)$request['announcementsSalaryFrom']  ){
                $input['AnnouncementsSalaryDependsOn'] = '' ;
            }
        }else{
            $input['announcementsSalaryTo']='';
            $input['announcementsSalaryFrom']='';
            $input['AnnouncementsSalaryDependsOn'] = '' ;
        }

        $input['announcementsCertificate'] = json_encode( $request->get('certificate') );
        $input['announcementsBenefit'] = json_encode( $request->get('benefit') );
        $input['announcementsLanguage'] = json_encode( $request->get('language') );
        $input['announcementsWorkTime'] = json_encode( $request->get('announcementsWorkTime') );
        if( !empty($request->get('language')) ){
            foreach($request->get('language') as $key => $value){
                $langLevel[$key] = $request->get('announcementsLanguageLevel')[$key];
            }
        }
        $input['announcementsLanguageLevel'] = json_encode( $langLevel );

        if( $input['AnnouncementsExperiencePositionN'] == '12' && $input['AnnouncementsPositionType'] == 'month' ){
            $input['AnnouncementsExperiencePositionN'] = 1;
            $input['AnnouncementsPositionType'] = 'year';
        }

        if( $input['AnnouncementsExperienceSectorN'] == '12' && $input['AnnouncementsSectorType'] == 'month' ){
            $input['AnnouncementsExperienceSectorN'] = 1;
            $input['AnnouncementsSectorType'] = 'year';
        }

        //get fkPositionsCode from tblPositon
        $GetfkPositionsID = $request['fkPositionsID'];
        $GetfkPositionsCode = Position::where('positionsStatus', true)->where('pkPositionsID', $GetfkPositionsID)->value('positionsCode');
        $input['fkPositionsCode']= $GetfkPositionsCode ;

        $input['fkSectorsID'] = $company->fkSectorsID;
        $input['fkSubsectorsID'] = $company->fkSubsectorsID;
        $input['fkCompaniesID'] = $company->pkCompaniesID;
        $input['fkCountriesID'] = 855;

        $redirectUrl = '/account/company/view/'.$compId;
        if (isset($_REQUEST['previewJob']) ){
            $job = Postjob::create($input);
            $provinceName = DB::table('Provinces')->where('pkProvincesID', $job->fkProvincesID)->value('provincesNameEN');
            $sectorName = DB::table('Sectors')->where('pkSectorsID', $job->fkSectorsID)->value('sectorsNameEN');
            $positionName = DB::table('Positions')->where('pkPositionsID', $job->fkPositionsID)->value('positionsNameEN');
            $province_name = fnConvertSlug($provinceName);
            $sector_name = fnConvertSlug($sectorName);
            $position_name = fnConvertSlug($positionName);
            $redirectUrl = '/account/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID;
        }elseif( isset($_REQUEST['publishJob']) ){
            $input['announcementsStatus']  = 1 ;
            $job = Postjob::create($input);
        }else{
            $job = Postjob::create($input);
        }

        $mJob = new Postjob();
        $mJob->convertConfirmJobApplyMp3( $job );
        $mJob->convertConfirmJobApplyNoCompanyMp3($job);
        $mJob->convertJobMp3( $job );

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().$redirectUrl);
    }

    public function exportAnnouncement(Request $request)
    {
        $lang = strtoupper(Lang::getLocale());
        $companiesName = $request->get('companiesName'.$lang);
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));

        $strWhere = ' tblAnnouncements.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"' ;
        if( $companiesName != '' ){
            $strWhere .= " AND tblCompanies.companiesName".$lang." like '%". $companiesName . "%' ";
        }

        $announcements = DB::table('Announcements')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->select(
                'Announcements.pkAnnouncementsID', 'Positions.positionsName'.$lang, 'Companies.companiesName'.$lang, 'Announcements.announcementsHiring', 'Announcements.announcementsPublishDate', 'Announcements.announcementsClosingDate'
            )
            ->where('Companies.companiesStatus', '=', 1)
            ->where('Positions.positionsStatus', '=', 1)
            ->where('Announcements.announcementsStatus', '<>', 3)
            ->whereRaw( $strWhere )
            ->orderBy('Announcements.pkAnnouncementsID', 'DESC')
            ->get();

        // Header
        $announcementsArray[] = [ trans('text_lang.id'),  trans('text_lang.positionName'), trans('text_lang.companyName'), trans('text_lang.announcementsHiring'), trans('text_lang.announcementsPublishDate'), trans('text_lang.announcementsClosingDate')];

        // Convert each member of the returned collection into an array,
        foreach ($announcements as $company) {
            $announcementsArray[] = (array) $company;
        }

        // Generate and return the spreadsheet
        Excel::create('companies_'.$fromDate.'-'.$toDate, function($excel) use ($announcementsArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Announcement');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('Announcement file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($announcementsArray) {
                $sheet->fromArray($announcementsArray, null, 'A1', false, false);
            });

        })->download('xls');
    }

    public function viewShareJob(){
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        $shareBy = null;
        $fkAnnouncementsID = '';

        $strWhere = 'tblShares.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';

        $shares = DB::table('Shares')
            ->join('Announcements', 'Shares.fkAnnouncementsID', '=', 'Announcements.pkAnnouncementsID')
            ->join('Positions', 'Announcements.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->select(
                "Shares.pkSharesID", "Shares.sharesGender", "Shares.sharesName", "Shares.sharesPhone", "Shares.sharesWho",
                "Positions.positionsName".$lang,"pkPositionsID",
                "Shares.created_at"
            )
            ->whereRaw( $strWhere )
            ->where('announcementsStatus','=', 1 )
            ->where('positionsStatus', '=', 1)
            ->orderBy('Shares.pkSharesID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.exports.sharejob', compact('lang', 'fromDate', 'toDate', 'shares', 'shareBy', 'fkAnnouncementsID'));
    }

    public function searchShareJob(request $request){
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $shareBy = (int)$request->get('shareBy');
        $fkAnnouncementsID = ($request->get('fkAnnouncementsID'))?(int)$request->get('fkAnnouncementsID'):'';

        $strWhere = 'tblShares.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';
        if( $shareBy != 0 ){
            $strWhere .= " AND tblShares.sharesType = ".$shareBy;
        }
        if(!empty($fkAnnouncementsID)){
            $strWhere .= " AND tblAnnouncements.pkAnnouncementsID = ".$fkAnnouncementsID;
        }

        $shares = DB::table('Shares')
            ->join('Announcements', 'Shares.fkAnnouncementsID', '=', 'Announcements.pkAnnouncementsID')
            ->join('Positions', 'Announcements.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->select(
                "Shares.pkSharesID", "Shares.sharesGender", "Shares.sharesName", "Shares.sharesPhone", "Shares.sharesWho", "Shares.sharesType",
                "Positions.positionsName".$lang,"pkPositionsID", "Announcements.pkAnnouncementsID",
                "Shares.created_at"
            )
            ->whereRaw( $strWhere )
            ->where('announcementsStatus','=', 1 )
            ->where('positionsStatus', '=', 1)
            ->orderBy('Shares.pkSharesID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.exports.sharejob', compact('lang', 'fromDate', 'toDate', 'shares', 'shareBy', 'fkAnnouncementsID'));
    }

    public function exportShareJob(Request $request){
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $shareBy = (int)$request->get('shareBy');
        $fkAnnouncementsID = ($request->get('fkAnnouncementsID'))?(int)$request->get('fkAnnouncementsID'):'';

        $strWhere = 'tblShares.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';
        if( $shareBy != 0 ){
            $strWhere .= " AND tblShares.sharesType = ".$shareBy;
        }
        if(!empty($fkAnnouncementsID)){
            $strWhere .= " AND tblAnnouncements.pkAnnouncementsID = ".$fkAnnouncementsID;
        }

        $shares = DB::table('Shares')
            ->join('Announcements', 'Shares.fkAnnouncementsID', '=', 'Announcements.pkAnnouncementsID')
            ->join('Positions', 'Announcements.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->select(
                "Shares.pkSharesID", "Shares.sharesGender", "Shares.sharesName", "Shares.sharesPhone",
                "Positions.positionsName".$lang,"Shares.sharesDate"
            )
            ->whereRaw( $strWhere )
            ->where('announcementsStatus','=', 1 )
            ->where('positionsStatus', '=', 1)
            ->orderBy('Shares.pkSharesID', 'DESC')
            ->get();

        // Define the Excel spreadsheet headers
        $sharesArray[] = [
            trans('text_lang.id'), trans('text_lang.gender'), trans('text_lang.name'), trans('text_lang.phone'),
            trans('text_lang.position'), trans('text_lang.createdAt')
        ];

        // Convert each member of the returned collection into an array,
        foreach ($shares as $share) {
            $sharesArray[] = (array)$share;
        }

        // Generate and return the spreadsheet
        Excel::create('shares', function($excel) use ($sharesArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Shares');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('shares file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($sharesArray) {
                $sheet->fromArray($sharesArray, null, 'A1', false, false);
            });

        })->download('xls');
    }

    public function applicantRigisterAndNotRegister(){
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        $applyByValue = null;

        $strWhere = 'tblJobApply.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';

        $applicants = DB::table('JobApply')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->select(
                "pkJobApplyID", "JobApply.fkAnnouncementsID", "JobApply.fkSharesID", "JobApply.fkCompaniesID", "JobApply.fkUsersID", "JobApply.fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "JobApply.created_at", "JobApply.updated_at",
                'pkPositionsID', 'positionsName'.$lang
            )
            ->whereRaw( $strWhere )
            ->groupby('jobApplyPhone')
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.applicantsRegisterAndNot.index', compact('fromDate', 'toDate', 'applicants', 'lang', 'applyByValue'));
    }

    public function searchApplicantRegisterAndNot(Request $request){
        $lang = strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $applyByValue = $request->get('jobApplyBy');
        $isCheckNotRegister = $request->get('isCheckNotRegister');

        $strWhere = 'tblJobApply.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';

        if( $request->jobApplyBy != '' ){
            $strWhere .= ' AND tblJobApply.jobApplyBy = '.$request->jobApplyBy ;
        }

        if( $isCheckNotRegister == 1 ){
            $strWhere .= ' AND tblJobApply.fkUsersID = 0 ';
        }

        $applicants = DB::table('JobApply')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->select(
                "pkJobApplyID", "JobApply.fkAnnouncementsID", "JobApply.fkSharesID", "JobApply.fkCompaniesID", "JobApply.fkUsersID", "JobApply.fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "JobApply.created_at", "JobApply.updated_at",
                'pkPositionsID', 'positionsName'.$lang
            )
            ->whereRaw( $strWhere )
            ->groupby('jobApplyPhone')
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM") );

        return view('accounts.applicantsRegisterAndNot.index', compact('applicants','fromDate', 'toDate','lang', 'applyByValue', 'isCheckNotRegister'));
    }

    public function exportApplicantRegisterAndNot( Request $request )
    {
        $lang =  strtoupper(Lang::getLocale());
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $applyByValue = $request->get('jobApplyBy');
        $isCheckNotRegister = $request->get('isCheckNotRegister');

        $strWhere = 'tblJobApply.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59"';

        if( $applyByValue != '' ){
            $strWhere .= ' AND tblJobApply.jobApplyBy = '.$applyByValue ;
        }
        if( $isCheckNotRegister == 1 ){
            $strWhere .= ' AND tblJobApply.fkUsersID = 0 ';
        }

        $applicants = DB::table('JobApply')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobApply.fkPositionsID')
            ->select('pkJobApplyID', 'jobApplyName', 'jobApplyGender', 'jobApplyPhone', 'Positions.positionsName'.$lang, 'JobApply.created_at')
            ->whereRaw( $strWhere )
            ->groupby('jobApplyPhone')
            ->orderBy('JobApply.pkJobApplyID', 'DESC')
            ->get();

        // Define the Excel spreadsheet headers
        $applicantsArray[] = [ trans('text_lang.id'), trans('text_lang.applyName'), trans('text_lang.gender'), trans('text_lang.phone'), trans('text_lang.position'),  trans('text_lang.date') ];

        // Convert each member of the returned collection into an array,
        foreach ($applicants as $applicant) {
            $applicantsArray[] = (array)$applicant;
        }

        // Generate and return the spreadsheet
        Excel::create('applicants_'.$fromDate.'-'.$toDate, function($excel) use ($applicantsArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Applicants');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('applicants file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($applicantsArray) {
                $sheet->fromArray($applicantsArray, null, 'A1', false, false);
            });

        })->download('xls');

    }

}