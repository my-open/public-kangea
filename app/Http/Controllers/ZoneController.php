<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \App\Zone;
use Flash;
use Lang;
use DB;

use App\Country;
use App\Province;
use App\District;
use App\Commune;
use App\Village;

class ZoneController extends Controller
{
    public function __construct()
    {
        //$this->middleware('role:admin');
    }

    public function index()
    {
        $zones = DB::table('Zones')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Zones.fkProvincesID')
            ->select(
                'Zones.*',
                'Provinces.pkProvincesID', 'Provinces.provincesNameEN', 'Provinces.provincesNameKH'
            )
            ->paginate( config("constants.PAGINATION_NUM_MAX") );
        return view('accounts.zones.index', compact('zones') );
    }

    public function create()
    {
        if( Lang::getLocale() == 'en' ){
            $countries = Country::where('countriesStatus', true)->orderBy('countriesNameEN')->pluck('countriesNameEN', 'pkCountriesID');
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
            $districts = District::where('districtsStatus', true)->orderBy('districtsNameEN')->pluck('districtsNameEN', 'pkDistrictsID');
            $communes = Commune::where('communesStatus', true)->orderBy('CommunesNameEN')->pluck('CommunesNameEN', 'pkCommunesID');
            $villages = Village::where('villagesStatus', true)->orderBy('villagesNameEN')->pluck('villagesNameEN', 'pkVillagesID');
        }
        else{
            $countries = Country::where('countriesStatus', true)->orderBy('countriesNameKH')->pluck('countriesNameKH', 'pkCountriesID');
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
            $districts = District::where('districtsStatus', true)->orderBy('districtsNameKH')->pluck('districtsNameKH', 'pkDistrictsID');
            $communes = Commune::where('communesStatus', true)->orderBy('CommunesNameKH')->pluck('CommunesNameKH', 'pkCommunesID');
            $villages = Village::where('villagesStatus', true)->orderBy('villagesNameKH')->pluck('villagesNameKH', 'pkVillagesID');
        }

        return view('accounts.zones.add', compact('countries', 'provinces', 'districts','communes' ,'villages') );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fkCountriesID'=> 'required|exists:Countries,pkCountriesID',
            'fkProvincesID'=> 'required|exists:Provinces,pkProvincesID',
            'zonesNameEN' => 'required|max:255',
            'zonesNameKH' => 'required|max:255',
            'zonesStatus' => 'required|max:1'
        ]);
        $input = $request->all();
        Zone::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/zone');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $zones = Zone::findOrFail($id);

        if( Lang::getLocale() == 'en' ){
            $countries = Country::where('countriesStatus', true)->orderBy('countriesNameEN')->pluck('countriesNameEN', 'pkCountriesID');
            $provincesByCountries = Province::where('provincesStatus', true)->where('fkCountriesID', $zones->fkCountriesID)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
            $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $zones->fkProvincesID)->orderBy('districtsNameEN')->pluck('districtsNameEN', 'pkDistrictsID');
            $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $zones->fkDistrictsID)->orderBy('CommunesNameEN')->pluck('CommunesNameEN', 'pkCommunesID');
            $villageByCommunes = Village::where('villagesStatus', true)->where('fkCommunesID', $zones->fkCommunesID)->orderBy('villagesNameEN')->pluck('villagesNameEN', 'pkVillagesID');
        }
        else{
            $countries = Country::where('countriesStatus', true)->orderBy('countriesNameKH')->pluck('countriesNameKH', 'pkCountriesID');
            $provincesByCountries = Province::where('provincesStatus', true)->where('fkCountriesID', $zones->fkCountriesID)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
            $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $zones->fkProvincesID)->orderBy('districtsNameKH')->pluck('districtsNameKH', 'pkDistrictsID');
            $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $zones->fkDistrictsID)->orderBy('CommunesNameKH')->pluck('CommunesNameKH', 'pkCommunesID');
            $villageByCommunes = Village::where('villagesStatus', true)->where('fkCommunesID', $zones->fkCommunesID)->orderBy('villagesNameKH')->pluck('villagesNameKH', 'pkVillagesID');
        }

        return view('accounts.zones.edit', compact('zones', 'countries', 'provincesByCountries', 'districtByProvinces', 'communeByDistricts', 'villageByCommunes')  );
    }

    public function update(Request $request, $id)
    {
        $zone = Zone::findOrFail($id);

        $this->validate($request, [
            'fkCountriesID'=> 'required|exists:Countries,pkCountriesID',
            'fkProvincesID'=> 'required|exists:Provinces,pkProvincesID',
            'zonesNameEN' => 'required|max:255',
            'zonesNameKH' => 'required|max:255',
            'zonesStatus' => 'required|max:1'
        ]);

        $input = $request->all();
        $zone->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/zone');
    }

    public function destroy($id)
    {
        $zone = Zone::findOrFail($id);
        $zone->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/zone');
    }

}
