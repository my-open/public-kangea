<?php

namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Request;

use App\Http\Requests;

use Flash;
use Lang;
use DB;

class LanguageController extends Controller
{

    public function index()
    {
        $languages = DB::table('Languages')
            ->select('pkLanguagesID', 'languagesName', 'languagesStatus')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );
        return view('accounts.languages.index', ['languages' => $languages]);
    }

    public function create()
    {
        return view('accounts.languages.add');
    }

    public function store(Request $request)
    {
        $this->validate($request,['languagesName'=>'required|unique:Languages', 'languagesStatus' => 'required']);
        $input = $request->all();
        Language::create($input);

        Flash::message(trans('text_lang.addSuccessful'));
        return redirect( \Lang::getLocale().'/account/language');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $language = Language::findOrFail($id);
        return view('accounts.languages.edit', ['language' => $language]);
    }

    public function update(Request $request, $id)
    {
        $language = Language::findOrFail($id);
        $this->validate($request,['languagesName'=>'required', 'languagesStatus' => 'required']);

        $input = $request->all();
        $language->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/language');
    }

    public function destroy($id)
    {
        Language::findOrFail($id)->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/language');
    }
}
