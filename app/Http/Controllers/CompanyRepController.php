<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use App\Activity;
use App\Candidate;
use App\CompanyUser;
use App\Http\Requests;

use App\Province;
use App\Sector;
use App\Subsector;
use App\User;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use GuzzleHttp\Client as GuzzleHttpClient;
use Auth;
use Mail;
use Laracasts\Flash\Flash;


class CompanyRepController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:employer');
    }

    public function approveOrUnapproveCompanyRep( Request $request, $compRepId ){
        $user = Auth::user();

        $companyID = DB::table('CompanyUsers')->where('fkUsersID', $user->id)->value('fkCompaniesID');
        $existCompanyRep = DB::table('CompanyUsers')->where('fkUsersID', $compRepId)->where('fkCompaniesID', $companyID)->value('fkCompaniesID');

        if( $user->companyLevel != 1 || !$existCompanyRep ){
            Flash::error( trans('text_lang.cannotApprovedThisCompanyRep') );
            return redirect( \Lang::getLocale().'/account/userProfile');
        }

        $companyRepInfo = User::findOrFail($compRepId);
        $companyLevel = 3;

        if( $companyRepInfo->companyLevel == 3 )
        {
            $companyLevel = 2;
        }

        if( $existCompanyRep && $companyRepInfo->companyLevel != 1)
        {
            User::where('id', $compRepId)->update(array('companyLevel' => $companyLevel));
            Flash::message( trans('text_lang.approvedSuccessful') );
            return redirect( \Lang::getLocale().'/account/userProfile');
        }
    }

    public function deleteCompanyRep( Request $request, $compRepId ){
        $user = Auth::user();

        $companyID = DB::table('CompanyUsers')->where('fkUsersID', $user->id)->value('fkCompaniesID');
        $existCompanyRep = DB::table('CompanyUsers')->where('fkUsersID', $compRepId)->where('fkCompaniesID', $companyID)->value('fkCompaniesID');

        if( $user->companyLevel != 1 || !$existCompanyRep ){
            Flash::error( trans('text_lang.cannotDeleteThisCompanyRep') );
            return redirect( \Lang::getLocale().'/account/userProfile');
        }

        $companyRepInfo = User::findOrFail($compRepId);
        if( $companyRepInfo->companyLevel == 0 ){
            DB::table('role_user')
                ->where('user_id', Auth::id())
                ->update(['role_id' => 1]);
        }else{
            User::findOrFail($compRepId)->delete();
        }
        DB::table('CompanyUsers')->where('fkUsersID', '=', $compRepId)->delete();

        Flash::message( trans('text_lang.deleteCompanyRepSuccessful') );
        return redirect( \Lang::getLocale().'/account/userProfile');
    }

    //Company Register
    public function registerCompany(){
        //check user has company or not
        $checkExistsUserID = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', Auth::id() )
            ->where('Companies.companiesStatus', 1)
            ->count();
        if( $checkExistsUserID > 0 ) {
            return redirect()->to(\Lang::getLocale() . '/account/companyProfile/additional/');
        }

        $subsectors = [];
        $getLang = strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$getLang)->pluck('sectorsName'.$getLang, 'pkSectorsID');
        $subsectors = Subsector::where('subsectorsStatus', true)->orderBy('subsectorsName'.$getLang)->pluck('subsectorsName'.$getLang, 'pkSubsectorsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$getLang)->pluck('provincesName'.$getLang, 'pkProvincesID');

        return view('accounts.companies.registerCompany', compact('sectors', 'subsectors', 'provinces') );
    }
    //Company Register Post
    public function registerCompanyPost( Request $request ){
        //check user has company or not
        $checkExistsUserID = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', Auth::id() )
            ->where('Companies.companiesStatus', 1)
            ->count();
        if( $checkExistsUserID > 0 ) {
            return redirect()->to(\Lang::getLocale() . '/account/companyProfile/additional/');
        }

        $isSubsectorIdOther = false;
        ////$isAcitivityIdOther = false;
        $required = [
            'companiesNameEN' => 'required|max:150',
            /////'fkSectorsID' => 'required|max:255',
            'fkSubsectorsID' => 'required',
            ////'fkActivitiesID' => 'required',
            'fkProvincesID' => 'required',
            'fkDistrictsID' => 'required'
        ];

        if( $request->get('fkSubsectorsID') == 'other' ){
            $required['fkSectorsID'] = 'required';
            $required['subsectorOther'] = 'required';
            $isSubsectorIdOther = true;
        }

        ////if( $request->get('fkActivitiesID') == 'other' ){
            ////$required['activitiesOther'] = 'required';
            ////$isAcitivityIdOther = true;
        ////}

        $this->validate($request, $required);

        $fkSubsectorsID = $request->get('fkSubsectorsID');
        ////$fkActivitiesID = $request->get('fkActivitiesID');

        if($isSubsectorIdOther){
            $subsector = Subsector::create([
                'fkSectorsID' => $request['fkSectorsID'],
                'subsectorsNameEN' => $request['subsectorOther'],
                'subsectorsNameKH' => $request['subsectorOther'],
                'subsectorsNameZH' => $request['subsectorOther'],
                'subsectorsNameTH' => $request['subsectorOther'],
                'subsectorsStatus' => 1,
            ]);

            $fkSubsectorsID = $subsector->pkSubsectorsID;

            $emails = config("constants.ADMIN_EMAIL");
            $subName = $request->get('subsectorOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Dear Admin of Bongpheak Job. A new sub-sector has just registered in  '.$domainName.'. The new sub-sector name is '.$subName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New sub-sector created in '.$domainName);
            });
        }

        $fksectorsID = DB::table('Subsectors')->where('pkSubsectorsID', $fkSubsectorsID)->value('fkSectorsID');

        /*////if($isAcitivityIdOther){
            $activity = Activity::create([
                'fkSubsectorsID' => $fkSubsectorsID,
                'activitiesNameEN' => $request['activitiesOther'],
                'activitiesNameKH' => $request['activitiesOther'],
                'activitiesNameZH' => $request['activitiesOther'],
                'activitiesNameTH' => $request['activitiesOther'],
                'activitiesStatus' => 1,
            ]);

            $fkActivitiesID = $activity->pkActivitiesID;

            $emails = config("constants.ADMIN_EMAIL");
            $actName = $request->get('activitiesOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Hi, Admin of Bongpheak Job. Now you have new main activity has been created with '.$domainName.'. The new main activity name is '.$actName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New main activity created in '.$domainName);
            });
        }*////

        if (empty($request['companiesNameKH'])) {
            $request['companiesNameKH'] = $request['companiesNameEN'];
        }

        $company = Company::create([
            'fkCountriesID' => 855,
            'fkProvincesID' => $request['fkProvincesID'],
            'fkDistrictsID' => $request['fkDistrictsID'],
            'fkCommunesID' => $request['fkCommunesID'],
            'fkSectorsID' => $fksectorsID,
            'fkSubsectorsID' => $fkSubsectorsID,
            ////'fkActivitiesID' => $fkActivitiesID,
            'companiesStatus' => 1,
            'companiesNameEN' => $request['companiesNameEN'],
            'companiesNameKH' => $request['companiesNameKH'],
            'companiesNameZH' => $request['companiesNameEN'],
            'companiesNameTH' => $request['companiesNameEN'],
        ]);

        //Create table tblCompanyUsers
        $companyUser = DB::table('CompanyUsers')->insert(
            ['fkCompaniesID' => $company->pkCompaniesID, 'fkUsersID' => Auth::id()]
        );

        if( !$company || !$companyUser )
        {
            DB::rollBack();
        } else {
            DB::commit();
            $emails = config("constants.ADMIN_EMAIL");
            $companyName = $request['companiesNameEN'];
            $domainName = env('HTTP_HOST');
            Mail::raw('Dear Admin of Bongpheak Job. A new company has just registered in '.$domainName.'. The company name is '.$companyName.'.' , function ($message) use ($companyName, $emails){
                $message->to($emails)->subject('New Register');
            });
            return redirect( \Lang::getLocale().'/account/companyProfile/additional');
        }
    }

    public function additional(){
        $employerId =  Auth::id();

        $companyID = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->first();

        return view('accounts.companies.additional');
    }

    public function additionalPost(  Request $request ){
        $employerId =  Auth::id();

        $companyID = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->first();

        $company = Company::findOrFail($companyID->pkCompaniesID);

        $this->validate($request, [
            'companiesSite' => 'url',
            'companiesLogo' => 'max:'.config("constants.IMAGE_SIZE_MAX").'|image|mimes:jpeg,jpg,png',
        ]);

        $input = $request->all();
        $input['companiesLogo'] = '';
        if ($request->hasFile('companiesLogo')) {
            if ($request->file('companiesLogo')->isValid()) {
                $file = $request->file('companiesLogo');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if ($file->move('images/companyLogos', $fileName)) {
                    $img = Image::make('images/companyLogos/'.$fileName);
                    $img->resize(config("constants.COMPANY_LOGO_W"), config("constants.COMPANY_LOGO_H"), function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save('images/companyLogos/thumbnails/'.$fileName);
                    $input['companiesLogo'] = $fileName;
                }
            }
        }
        $company->fill($input)->save();

        return redirect( \Lang::getLocale().'/account/postjob/');
    }

    public function candidatePost(Request $request)
    {
        $chk_user = $request->input('chk_user');
        $fkAnnouncementsID = $request->input('pkAnnouncementsID');
        $message =  trans('text_lang.pleaseSelectYourCandidate');
        if( !empty($chk_user) ){
            foreach( $chk_user as $key => $value ){
                Candidate::create([
                    'fkUsersID' => $value,
                    'fkCompaniesID' => $request->input('fkCompaniesID'),
                    'fkAnnouncementsID' => $fkAnnouncementsID,
                ]);

                $user = User::findOrFail($value);
                if( count($user)> 0 && $user->phone != '' ){
                    $jobMp3 = 'files/sounds/kh/jobs/job_' . $fkAnnouncementsID . '.mp3';
                    $isJobMp3 = isExistMp3($jobMp3);
                    if( $isJobMp3 ){
                        $api_token = config("constants.API_TOKEN");
                        $soundUrl = "http://". env('HTTP_HOST') ."/files/sounds/kh/jobs/job_".$fkAnnouncementsID.".mp3";

                        $data = ["sharesGender" => $user->gender, "sharesName" => $user->name,
                            "sharesPhone" => $user->phone, "sharesWho" => 7,
                            "fkAnnouncementsID" => $fkAnnouncementsID, "soundUrl" => $soundUrl,
                            "fkUsersID" => 0, "fkCompaniesID" => $request->input('fkCompaniesID'), "fkPositionsID" => $request->input('fkPositionsID'), "api_token" => $api_token ];

                        $data = json_encode($data, JSON_FORCE_OBJECT);
                        $headers = ['Content-Type' => 'application/json'];
                        // Using laravel php libray GuzzleHttp for execute external API
                        $client = new GuzzleHttpClient();
                        $client->request('POST', config("constants.REQUEST_IVR_API_SHARE"), ['headers'=>$headers,'body' => $data]);
                    }
                }
            }
            $message =  trans('text_lang.addSuccessful');
        }

        Flash::message( $message );
        return redirect( \Lang::getLocale().'/account/postjob/searchCandidate/'.$fkAnnouncementsID);
    }

    public function getSubsectorsBySectorsIDAjax( Request $request ){
        $getLang = strtoupper(Lang::getLocale());
        $subsectors = Subsector::select('subsectorsName'.$getLang.' AS subsectorsName', 'pkSubsectorsID')
            ->where('fkSectorsID', '=', $request->input('sectorId'))
            ->where('subsectorsStatus', '=', 1)
            ->orderBy('subsectorsOrder')
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'), 'data' => $subsectors];
    }

    public function getActivitiesBySubsectorsIDAjax( Request $request ){
        $getLang = strtoupper(Lang::getLocale());
        $activities = Activity::select('activitiesName'.$getLang.' AS activitiesName', 'pkActivitiesID')
            ->where('fkSubsectorsID', '=', $request->input('subsectorId'))
            ->where('activitiesStatus', '=', 1)
            ->orderBy('activitiesOrder')
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'), 'data' => $activities];
    }

}
