<?php

namespace App\Http\Controllers;

use App\Position;
use App\Province;
use App\Sector;
use App\Subsector;
use Illuminate\Http\Request;

use App\Http\Requests;
use Flash;
use Lang;
use DB;
use App\User;
use Auth;
use Maatwebsite\Excel\Facades\Excel;

class JobseekerController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        $create_from = array('0'=> trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.facebook'));

        $name = null;
        $phone = null;
        $email = null;

        $strWhere = 'tblusers.status <> 3 AND (tblusers.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59")' ;


        $rows = DB::table('users')
            ->Join('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->select('id','fkPositionsID', 'name', 'phone', 'email', 'users.created_at', 'password', 'gender', 'address', 'social_accounts.provider_user_id', 'verified', 'status')
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->where('users.status', 1)
            ->orderBy('users.id', 'DESC')
            ->whereRaw( $strWhere )
            ->paginate( config("constants.PAGINATION_NUM_MAX") );
        return view('accounts.users.jobseekers.index', compact('rows', 'name', 'phone', 'email', 'fromDate', 'toDate', 'create_from'));
    }

    public function create()
    {
        return view('accounts.users.jobseekers.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|max:70|unique:users',
            'phone' => 'required|digits_between:9,10|unique:users',
            'gender'=> 'required',
            'status' => 'required|integer|max:1',
        ]);

        DB::beginTransaction();

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'gender' => $request['gender'],
            'address' => $request['address'],

            'password' => bcrypt($request['phone']),
            'status' => $request['status'],
        ]);

        // Create role user
        $role_user = DB::table('role_user')->insert(
            ['user_id' => $user->id, 'role_id' => config("constants.JOBSEEKER")]
        );

        if( !$user || !$role_user )
        {
            DB::rollBack();
        } else {
            DB::commit();
        }

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/jobseeker');
    }

    public function edit($id)
    {
        $rows = User::findOrFail($id);
        return view('accounts.users.jobseekers.edit', ['rows' => $rows]);
    }

    public function update( Request $request, $id)
    {
        $rows = User::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|max:255|unique:users,email,'.$id.',id',
            'phone' => 'required|digits_between:9,10|unique:users,phone,'.$id.',id',
            'status' => 'required|integer|max:1',
        ]);
        $input = $request->all();
        $input['password'] = bcrypt( $request['phone'] );
        $rows->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/jobseeker');
    }

    public function destroy($id)
    {
        User::where('id', '=', $id)
            ->update(['status' => '3']);

//        User::findOrFail($id)->delete();
//        DB::table('role_user')->where('user_id', '=', $id)->delete();
//        DB::table('social_accounts')->where('user_id', '=', $id)->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/jobseeker');
    }

    public function search(Request $request)
    {
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $create_from  = array('0'=> trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.facebook'));
        $from = (int)$request->get('create_from');

        $name = trim($request->get('name'));
        $phone = trim($request->get('phone'));
        $email = trim($request->get('email'));

        $searchCriteria = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
        ];
        $strWhere = 'tblusers.status <> 3 AND (tblusers.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59")' ;
        if($from == 1){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id IS NULL';
        }elseif($from == 2){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id <> "" ';
        }

        foreach ($searchCriteria as $key => $value)
        {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = trim($value);
            $strWhere .=" AND tblusers." . $key ." like '%".$value."%' ";
        }

        $rows = DB::table('users')
            ->Join('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->select('id','fkPositionsID', 'name', 'phone', 'email', 'users.created_at', 'password', 'gender', 'address', 'social_accounts.provider_user_id', 'verified', 'status')
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->whereRaw($strWhere)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.users.jobseekers.index', compact('rows', 'name', 'phone', 'email', 'fromDate', 'toDate', 'create_from', 'from'));
    }

    public function exportJobseeker(Request $request)
    {
        $name = trim($request->get('name'));
        $phone = trim($request->get('phone'));
        $email = trim($request->get('email'));

        $searchCriteria = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
        ];
        $strWhere = "tblusers.status <> 3 ";
        foreach ($searchCriteria as $key => $value)
        {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = trim($value);
            $strWhere .=" AND tblusers." . $key ." like '%".$value."%' ";
        }

        $jobseekers = DB::table('users')
            ->leftjoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->select('users.id', 'users.name', 'users.email', 'phone', 'social_accounts.provider_user_id', 'users.created_at')
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->whereRaw($strWhere)
            ->get();

        // Define the Excel spreadsheet headers
        $jobseekersArray[] = [ trans('text_lang.id'), trans('text_lang.name'), trans('text_lang.email'), trans('text_lang.phone'), trans('text_lang.facebookId'), trans('text_lang.createdAt') ];

        // Convert each member of the returned collection into an array,
        foreach ($jobseekers as $jobseeker) {
            $jobseekersArray[] = (array)$jobseeker;
        }

        // Generate and return the spreadsheet
        Excel::create('jobseekers', function($excel) use ($jobseekersArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Jobseeker');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('jobseekers file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($jobseekersArray) {
                $sheet->fromArray($jobseekersArray, null, 'A1', false, false);
            });

        })->download('xls');

    }

    public function viewJobseekerTargetjob()
    {
        $lang =  strtoupper(Lang::getLocale());

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $subsectors = array();
        $positions = array();

        $targetJobs = DB::table('users')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('TargetJobs', 'TargetJobs.fkUsersID', '=', 'users.id')
            ->join('Sectors', 'TargetJobs.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->join('Subsectors', 'TargetJobs.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->join('Positions', 'TargetJobs.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->join('Provinces', 'TargetJobs.fkProvincesID', '=', 'Provinces.pkProvincesID')
            ->select(
                'id', 'name', 'gender', 'phone', 'users.status',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Positions.positionsName'.$lang,
                'Provinces.provincesName'.$lang
            )
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.exports.jobseekerTarget', compact('targetJobs', 'lang', 'sectors', 'provinces', 'subsectors', 'positions'));
    }

    public function searchTargetjob(Request $request)
    {
        $lang =  strtoupper(Lang::getLocale());

        $fkSectorsID = $request->get('fkSectorsID');
        $fkSubsectorsID = $request->get('fkSubsectorsID');
        $fkPositionsID = $request->get('fkPositionsID');
        $fkProvincesID = $request->get('fkProvincesID');
        $name = $request->get('name');
        $phone = $request->get('phone');
        $subsectors = [];
        $positions = [];

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        if( !empty($fkSectorsID) ){
            $subsectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('subsectorsName' . $lang)->pluck('subsectorsName' . $lang, 'pkSubsectorsID');
            $positions = Position::where('positionsStatus', '=', 1)->where('fkSectorsID', '=', $fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName' . $lang, 'pkPositionsID');
        }

        $searchCriteria = [
            'name' => trim($request->get('name')),
            'phone' => trim($request->get('phone')),
            'fkSectorsID' => trim($request->get('fkSectorsID')),
            'fkSubsectorsID' => trim($request->get('fkSubsectorsID')),
            'fkPositionsID' => trim($request->get('fkPositionsID')),
            'fkProvincesID' => trim($request->get('fkProvincesID')),
        ];

        $strWhere = 'tblusers.status != 3';
        foreach ($searchCriteria as $key => $value) {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = trim($value);
            if( $key == 'name' || $key == 'phone' ){
                $strWhere .= ' AND tblusers.' . $key ." like '%". $value . "%' ";
            }else{
                $strWhere .= ' AND tblTargetJobs.' . $key . " = " . $value;
            }
        }

        $targetJobs = DB::table('users')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('TargetJobs', 'TargetJobs.fkUsersID', '=', 'users.id')
            ->join('Sectors', 'TargetJobs.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->leftjoin('Subsectors', 'TargetJobs.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->leftjoin('Positions', 'TargetJobs.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->join('Provinces', 'TargetJobs.fkProvincesID', '=', 'Provinces.pkProvincesID')
            ->select(
                'id', 'name', 'gender', 'phone', 'users.status',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Positions.positionsName'.$lang,
                'Provinces.provincesName'.$lang
            )
            ->whereRaw($strWhere)
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.exports.jobseekerTarget', compact('targetJobs', 'lang', 'sectors', 'provinces', 'subsectors', 'positions', 'name', 'phone', 'fkSectorsID', 'fkSubsectorsID', 'fkPositionsID', 'fkProvincesID'));
    }

    public function exportTargetjob(Request $request)
    {
        $lang =  strtoupper(Lang::getLocale());

        $searchCriteria = [
            'name' => trim($request->get('name')),
            'phone' => trim($request->get('phone')),
            'fkSectorsID' => trim($request->get('fkSectorsID')),
            'fkSubsectorsID' => trim($request->get('fkSubsectorsID')),
            'fkPositionsID' => trim($request->get('fkPositionsID')),
            'fkProvincesID' => trim($request->get('fkProvincesID')),
        ];

        $strWhere = 'tblusers.status = 1';
        foreach ($searchCriteria as $key => $value) {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = trim($value);
            if( $key == 'name' || $key == 'phone' ){
                $strWhere .= ' AND tblusers.' . $key ." like '%". $value . "%' ";
            }else{
                $strWhere .= ' AND tblTargetJobs.' . $key . " = " . $value;
            }
        }

        $targetJobs = DB::table('users')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('TargetJobs', 'TargetJobs.fkUsersID', '=', 'users.id')
            ->join('Sectors', 'TargetJobs.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->leftjoin('Subsectors', 'TargetJobs.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->leftjoin('Positions', 'TargetJobs.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->join('Provinces', 'TargetJobs.fkProvincesID', '=', 'Provinces.pkProvincesID')
            ->select(
                'id', 'name', 'gender', 'phone',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Positions.positionsName'.$lang,
                'Provinces.provincesName'.$lang,
                'users.created_at'
            )
            ->whereRaw($strWhere)
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->get();

        // Header
        $targetJobsArray[] = [ trans('text_lang.id'),  trans('text_lang.name'), trans('text_lang.gender'), trans('text_lang.phone'),
            trans('text_lang.sector'),
            trans('text_lang.subsector'),
            trans('text_lang.position'),
            trans('text_lang.province'),
            trans('text_lang.dateOfRegister')
        ];

        // Convert each member of the returned collection into an array,
        foreach ($targetJobs as $targetJob) {
            $targetJobsArray[] = (array) $targetJob;
        }

        // Generate and return the spreadsheet
        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        Excel::create('targetjob_'.$fromDate.'-'.$toDate, function($excel) use ($targetJobsArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Targetjob');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('Targetjob file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($targetJobsArray) {
                $sheet->fromArray($targetJobsArray, null, 'A1', false, false);
            });

        })->download('xls');
    }

    public function viewJobseekerExperience()
    {
        $lang =  strtoupper(Lang::getLocale());

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $subsectors = array();
        $positions = array();
        foreach( config("constants.EXPERIENCES") as $key => $value ){
            $workExperiences[$key] = $value[Lang::getLocale()];
        }

        $jobexperiences = DB::table('users')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('JobExperiences', 'JobExperiences.fkUsersID', '=', 'users.id')
            ->join('Sectors', 'JobExperiences.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->join('Subsectors', 'JobExperiences.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->join('Positions', 'JobExperiences.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->select(
                'id', 'name', 'gender', 'phone', 'users.status',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Positions.positionsName'.$lang,
                'JobExperiences.workExperience'
            )
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.exports.jobseekerExperience', compact('jobexperiences', 'lang', 'sectors', 'subsectors', 'positions', 'workExperiences'));
    }

    public function searchExperience(Request $request)
    {
        $lang =  strtoupper(Lang::getLocale());

        $fkSectorsID = $request->get('fkSectorsID');
        $fkSubsectorsID = $request->get('fkSubsectorsID');
        $fkPositionsID = $request->get('fkPositionsID');
        $name = $request->get('name');
        $phone = $request->get('phone');
        $subsectors = [];
        $positions = [];
        $keyWorkExperiences = $request->get('workExperience');

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        if( !empty($fkSectorsID) ){
            $subsectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('subsectorsName' . $lang)->pluck('subsectorsName' . $lang, 'pkSubsectorsID');
            $positions = Position::where('positionsStatus', '=', 1)->where('fkSectorsID', '=', $fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName' . $lang, 'pkPositionsID');
        }
        foreach( config("constants.EXPERIENCES") as $key => $value ){
            $workExperiences[$key] = $value[Lang::getLocale()];
        }

        $searchCriteria = [
            'name' => trim($request->get('name')),
            'phone' => trim($request->get('phone')),
            'fkSectorsID' => trim($request->get('fkSectorsID')),
            'fkSubsectorsID' => trim($request->get('fkSubsectorsID')),
            'fkPositionsID' => trim($request->get('fkPositionsID')),
        ];

        $strWhere = 'tblusers.status != 3';
        foreach ($searchCriteria as $key => $value) {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = trim($value);
            if( $key == 'name' || $key == 'phone' ){
                $strWhere .= ' AND tblusers.' . $key ." like '%". $value . "%' ";
            }else{
                $strWhere .= ' AND tblJobExperiences.' . $key . " = " . $value;
            }
        }
        if(!empty($keyWorkExperiences)){
            $strWhere .=' AND tblJobExperiences.workExperience = "'.$keyWorkExperiences.'" ';
        }

        $jobexperiences = DB::table('users')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('JobExperiences', 'JobExperiences.fkUsersID', '=', 'users.id')
            ->join('Sectors', 'JobExperiences.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->leftjoin('Subsectors', 'JobExperiences.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->leftjoin('Positions', 'JobExperiences.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->select(
                'id', 'name', 'gender', 'phone', 'users.status',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Positions.positionsName'.$lang,
                'JobExperiences.workExperience'
            )
            ->whereRaw($strWhere)
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.exports.jobseekerExperience', compact('jobexperiences', 'lang', 'sectors', 'subsectors', 'positions', 'name', 'phone', 'fkSectorsID', 'fkSubsectorsID', 'fkPositionsID', 'workExperiences', 'keyWorkExperiences'));
    }

    public function exportExperience(Request $request)
    {
        $lang =  strtoupper(Lang::getLocale());

        $searchCriteria = [
            'name' => trim($request->get('name')),
            'phone' => trim($request->get('phone')),
            'fkSectorsID' => trim($request->get('fkSectorsID')),
            'fkSubsectorsID' => trim($request->get('fkSubsectorsID')),
            'fkPositionsID' => trim($request->get('fkPositionsID')),
        ];

        $strWhere = 'tblusers.status = 1';
        foreach ($searchCriteria as $key => $value) {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = trim($value);
            if( $key == 'name' || $key == 'phone' ){
                $strWhere .= ' AND tblusers.' . $key ." like '%". $value . "%' ";
            }else{
                $strWhere .= ' AND tblJobExperiences.' . $key . " = " . $value;
            }
        }

        $keyWorkExperiences = $request->get('workExperience');
        if(!empty($keyWorkExperiences)){
            $strWhere .=' AND tblJobExperiences.workExperience = "'.$keyWorkExperiences.'" ';
        }

        $jobexperiences = DB::table('users')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('JobExperiences', 'JobExperiences.fkUsersID', '=', 'users.id')
            ->join('Sectors', 'JobExperiences.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->leftjoin('Subsectors', 'JobExperiences.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->leftjoin('Positions', 'JobExperiences.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->select(
                'id', 'name', 'gender', 'phone',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Positions.positionsName'.$lang,
                'JobExperiences.workExperience',
                'users.created_at'
            )
            ->whereRaw($strWhere)
            ->where('role_user.role_id', '=', config("constants.JOBSEEKER") )
            ->orderBy('users.id', 'DESC')
            ->get();

        // Header
        $jobexperiencesArray[] = [ trans('text_lang.id'),  trans('text_lang.name'), trans('text_lang.gender'), trans('text_lang.phone'),
            trans('text_lang.sector'),
            trans('text_lang.subsector'),
            trans('text_lang.position'),
            trans('text_lang.experience'),
            trans('text_lang.dateOfRegister')
        ];

        // Convert each member of the returned collection into an array,
        foreach ($jobexperiences as $jobexperience) {
            $jobexperiencesArray[] = (array) $jobexperience;
        }

        // Generate and return the spreadsheet
        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        Excel::create('jobexperience_'.$fromDate.'-'.$toDate, function($excel) use ($jobexperiencesArray) {

            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Experience');
            $excel->setCreator('Laravel')->setCompany('WJ Gilmore, LLC');
            $excel->setDescription('Experience file');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($jobexperiencesArray) {
                $sheet->fromArray($jobexperiencesArray, null, 'A1', false, false);
            });

        })->download('xls');
    }


}
