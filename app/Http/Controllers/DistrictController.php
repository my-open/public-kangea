<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Flash;
use Lang;
use DB;
use App\Province;
use App\District;

class DistrictController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getDistrictsAjax', 'getCountJobByDistrictAjax']]);
    }

    public function index()
    {
        $lang =  strtoupper(Lang::getLocale());
        $districts = DB::table('Districts')
            ->leftjoin('Provinces', 'Districts.fkProvincesID', '=', 'Provinces.pkProvincesID')
            ->select('Districts.pkDistrictsID','Districts.fkProvincesID',
                'Districts.districtsName'.$lang,
                'Districts.districtsStatus',
                'Provinces.provincesName'.$lang )
            ->paginate( 20 );
        return view('accounts.countries.districts.index', ['districts' => $districts, 'lang' => $lang]);
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

        return view('accounts.countries.districts.add', ['provinces' => $provinces]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'pkDistrictsID' => 'required|unique:Districts',
            'districtsNameEN' => 'required',
            'districtsNameKH' => 'required',
            'districtsNameZH' => 'required',
            'districtsNameTH' => 'required',
            'districtsStatus' => 'required'
        ]);

        $input = $request->all();
        District::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/district');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
     {
         $lang =  strtoupper(Lang::getLocale());
         $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

       //get data of district where id = ?
         $districts = District::findOrFail($id);
         return view('accounts.countries.districts.edit', ['districts' => $districts, 'provinces' => $provinces]);
     }

     public function update( Request $request, $id)
     {
         $districts = District::findOrFail($id);

         $this->validate($request, [
             'fkProvincesID' => 'required',
             'pkDistrictsID' => 'required|unique:Districts,pkDistrictsID,'.$id.',pkDistrictsID',
             'districtsNameEN' => 'required',
             'districtsNameKH' => 'required',
             'districtsNameZH' => 'required',
             'districtsNameTH' => 'required',
             'districtsStatus' => 'required'
         ]);
         $input = $request->all();

         $districts->fill($input)->save();
         Flash::message( trans('text_lang.updateSuccessful') );
         return redirect(\Lang::getLocale().'/account/district');
     }

    public function destroy($id)
    {
        District::findOrFail($id)->delete();;

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/district');
    }

    public function getDistrictsAjax(Request $request){
        $districts = District::select('districtsNameEN', 'districtsNameKH', 'districtsNameZH', 'districtsNameTH', 'pkDistrictsID')
            ->where('fkProvincesID', '=', $request->input('provincesId'))
            ->where('districtsStatus', '=', 1)
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $districts];
    }

    public function getCountJobByDistrictAjax(Request $request){
        $lang = strtoupper(Lang::getLocale());
        $provinceId = $request->input('provincesId');

        $countJobByDistricts = array();
        if( $countJobByDistricts != ''){
            $strWhere = 'a.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND a.announcementsPublishDate <= ' . date('"Y-m-d 00:00:00"') . ' AND a.announcementsStatus = 1';
            $countJobByDistricts = DB::select(
                DB::raw("SELECT pkDistrictsID, districtsName{$lang}, count(pkAnnouncementsID) as Total_Announcement
                    FROM `tblDistricts`
                    LEFT JOIN (SELECT a.* FROM tblAnnouncements a WHERE $strWhere) b
                    on b.`fkDistrictsID` = `pkDistrictsID`
                    WHERE `tblDistricts`.fkProvincesID = $provinceId
                    group by `tblDistricts`.`pkDistrictsID`
                    order BY Total_Announcement  DESC ")
            );
        }

        return ['selectOption' => trans('text_lang.selectOption'),'data'=> $countJobByDistricts];
    }

}



