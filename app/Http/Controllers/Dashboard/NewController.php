<?php

namespace App\Http\Controllers\Dashboard;
use App\DashboardModel\Dashboard;
use App\DashboardModel\NewChart;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Input;
use Carbon\Carbon;


class NewController extends Controller
{
    public function newChart(Request $request)
    {
        $data =$request->all();
        $arrayChart = array(
            'registeredCompany' => array('label'=>'Registered Companies','value' =>false),
            'registeredCompanyAvg' => array('label'=>'Registered Companies Average','value' =>false),
            'jobSeeker' => array('label'=>'Registered Job Seekers','value' =>false),
            'jobSeekerAvg' => array('label'=>'Registered Job Seekers','value' =>false),
            'announcement' => array('label'=>'Announcements','value' =>true),
            'announcementAvg' => array('label'=>'Announcements Average','value' =>true),
            'applicantsWeb' => array('label'=>'Applicants Web','value' =>true),
            'applicantsWebAvg' => array('label'=>'Applicants Web Average','value' =>true),
            'applicantsPhone' => array('label'=>'Applicants Phone','value' =>true),
            'applicantsPhoneAvg' => array('label'=>'Applicants Phone Average','value' =>true),
            'mainSearches' => array('label'=>'Searches (Main)','value' =>true),
            'mainSearchesAvg' => array('label'=>'Searches (Main) Average','value' =>true),
            'otherSearches' => array('label'=>'Searches (Other)','value' =>false),
            'otherSearchesAvg' => array('label'=>'Searches (Other) Average','value' =>false),
            'jobViews' => array('label'=>'Job Views','value' =>false),
            'jobViewsAvg' => array('label'=>'Job Views Average','value' =>false),
            'phoneShares' => array('label'=>'Phone Shares','value' =>false),
            'phoneSharesAvg' => array('label'=>'Phone Shares Average','value' =>false),
            'fbShares' => array('label'=>'Facebook Shares','value' =>false),
            'fbSharesAvg' => array('label'=>'Facebook Shares Average','value' =>false)
        );
        if ($data != null) {
//            var_dump($data);
            foreach ($arrayChart as $key => $chart){
                if(!isset($_POST[$key])){
                    $arrayChart[$key]['value'] = false;
                }else{
                    $arrayChart[$key]['value'] = true;
                }
            }
            $fkProvincesID = (int)$request->input('fkProvincesID');
            $fkDistrictsID = (int)$request->input('fkDistrictsID');
            $fkCommunesID = (int)$request->input('fkCommunesID');
            $fkSectorID = (int)$request->input('fkSectorID');
            if($fkProvincesID ==0){
                $fkProvincesID = null;
                $fkDistrictsID = null;
                $fkCommunesID = null;
            }
            if($fkSectorID == 0 ){
                $fkSectorID = null;
            }
            $from = $request->input('from');
            $to = $request->input('to');
            if ($from == null || $from <= '2016-10-19'){
                $from = '2016-10-19';
            }else{
                $from = $from;
            }
            if($to == null){
                $to = Carbon::now();
            }
            $location = null;
            $unit = $request->input('unit');
            $new = NewChart::getAllNew($from, $to , $fkProvincesID , $fkDistrictsID , $fkCommunesID ,$fkSectorID, $unit);
        } else {
            session(['districts' => null]);
            session(['communes' => null]);
            $to = Carbon::now();
            $new = NewChart::getAllNew($from = '2016-10-19', $to, $fkProvincesID = null , $fkDistrictsID = null , $fkCommunesID = null ,$fkSectorID=null, $unit = 0);
        }
        $chartName = \Lang::get('text_lang.new');
        $logTable = Lava::DataTable();
        $logTable->addDateColumn('Day of Month');
        foreach ($arrayChart as $chart){
            $logTable->addNumberColumn($chart['label']);
        }
        //date of insert data will be assume to 19 Oct 2016 if $from >  19 Oct 2016
        $startFrom = date_create($new['FromStartDate']);
        $date2= date_create($to);
        $avgDate= date_diff($startFrom,$date2)->format("%R%a ");
        //New Registered Company
        if($arrayChart['registeredCompany']['label'] == 'Registered Companies') {
            if($arrayChart['registeredCompany']['value']){
                if($new['companiesFromStart'] > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], $new['companiesFromStart']
                    ];
                    $logTable->addRow($rowData);
                }
                $totalNewComapanies = $new['companiesFromStart'];
                foreach ($new['companies'] as $key =>$company) {
                    $totalNewComapanies += $company->totlePerDay;
                    if($company->totlePerDay > 0){
                        $rowData = [
                            $company->day, $company->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                //for average of New Company
                if($totalNewComapanies > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, $totalNewComapanies / $avgDate
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $to, null, $totalNewComapanies / $avgDate
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        // New Job Seeker
        if($arrayChart['jobSeeker']['label'] == 'Registered Job Seekers') {
            if ($arrayChart['jobSeeker']['value'] && $fkProvincesID == null) {
                if($new['jobSeekersFromStart'] >0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, $new['jobSeekersFromStart']
                    ];
                    $logTable->addRow($rowData);
                }
                $totalNewJobSeekers = $new['jobSeekersFromStart'];
                foreach ($new['jobSeekers'] as $key => $jobSeekers) {
                    $totalNewJobSeekers += $jobSeekers->totlePerDay;
                    if($jobSeekers->totlePerDay > 0) {
                        $rowData = [
                            $jobSeekers->day, null, null, $jobSeekers->totlePerDay, null, null, null
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                // For average of new job seeker
                if($totalNewJobSeekers > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, $totalNewJobSeekers / $avgDate, null, null, null, null, null, null
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $to, null, null, null, $totalNewJobSeekers / $avgDate, null, null, null, null, null, null
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['announcement']['label'] == 'Announcements') {
            if ($arrayChart['announcement']['value']) {
                if($new['announcementsFromStart'] >0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, $new['announcementsFromStart']
                    ];
                    $logTable->addRow($rowData);
                }
                $totalNewAnnouncements = $new['announcementsFromStart'];
                foreach ($new['announcements'] as $key => $announcement) {
                    $totalNewAnnouncements += $announcement->totlePerDay;
                    if($announcement->totlePerDay > 0) {
                        $rowData = [
                            $announcement->day, null, null, null, null, $announcement->totlePerDay, null, null, null, null, null
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                // For average of new job seeker
                if($totalNewAnnouncements > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, $totalNewAnnouncements / $avgDate, null, null, null, null, null, null
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $to, null, null, null, null, null, $totalNewAnnouncements / $avgDate
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['applicantsWeb']['label'] == 'Applicants Web') {
            if ($arrayChart['applicantsWeb']['value']) {
                if($new['applicantsWebFromStart'] > 0) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, $new['applicantsWebFromStart']
                    ];
                    $logTable->addRow($rowData);
                }
                $totalNewApplicantsWeb = $new['applicantsWebFromStart'];
                foreach ($new['applicantsWeb'] as $key => $applicantsWeb) {
                    $totalNewApplicantsWeb += $applicantsWeb->totlePerDay;
                    if($applicantsWeb->totlePerDay > 0) {
                        $rowData = [
                            $applicantsWeb->day, null, null, null, null, null, null, $applicantsWeb->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                // For average of new applicants Web
                if($totalNewApplicantsWeb > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, null, $totalNewApplicantsWeb / $avgDate
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $to, null, null, null, null, null, null, null, $totalNewApplicantsWeb / $avgDate
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['applicantsPhone']['label'] == 'Applicants Phone') {
            if ($arrayChart['applicantsPhone']['value']) {
                if($new['applicantsPhoneFromStart'] > 0) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, null, null, $new['applicantsPhoneFromStart']
                    ];
                    $logTable->addRow($rowData);
                }
                $totalNewApplicantsPhone = $new['applicantsPhoneFromStart'];
                foreach ($new['applicantsPhone'] as $key => $applicantPhone) {
                    $totalNewApplicantsPhone += $applicantPhone->totlePerDay;
                    if($totalNewApplicantsPhone > 0) {
                        if($applicantPhone->totlePerDay > 0) {
                            $rowData = [
                                $applicantPhone->day, null, null, null, null, null, null, null, null, $applicantPhone->totlePerDay
                            ];
                            $logTable->addRow($rowData);
                        }
                    }
                }
                // For average of new applicants Phone
                if($totalNewApplicantsPhone > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, null, null, null, $totalNewApplicantsPhone / $avgDate
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $to, null, null, null, null, null, null, null, null, null, $totalNewApplicantsPhone / $avgDate
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['mainSearches']['label'] == 'Searches (Main)') {
            if ($arrayChart['mainSearches']['value'] && empty($fkProvincesID)) {
                $totalMainSearches = $new['mainSearchesFromStart'];
                $startDate = null;
                $endDate = null;
                foreach ($new['mainSearches'] as $key => $search) {
                    if ($key != 0){
                        $endDate = $search->day;
                        $totalMainSearches += $search->totlePerDay;
                    }else{
                        $startDate = $search->day;
                    }
                    if($search->totlePerDay > 0) {
                        $rowData = [
                            $search->day, null, null, null, null, null, null, null, null, null, null, $search->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                //calc number of day from start to end
                if($startDate != null && $endDate  != null ) {
                    $avgDateSearch = date_diff(date_create($startDate), date_create($endDate))->format("%R%a ");
                    // For average of new main search
                    if ($totalMainSearches > 0) {
                        $rowData = [
                            $startDate, null, null, null, null, null, null, null, null, null, null, null, $totalMainSearches / $avgDateSearch
                        ];
                        $logTable->addRow($rowData);
                        $rowData = [
                            $endDate, null, null, null, null, null, null, null, null, null, null, null, $totalMainSearches / $avgDateSearch
                        ];
                        $logTable->addRow($rowData);
                    }
                }
            }
        }
        if($arrayChart['otherSearches']['label'] == 'Searches (Other)') {
            if ($arrayChart['otherSearches']['value']) {
                $totalOtherSearches = $new['otherSearchesFromStart'];
                foreach ($new['otherSearches'] as $key => $search) {
                    if ($key != 0){
                        $endDate = $search->day;
                        $totalOtherSearches += $search->totlePerDay;
                    }else{
                        $endDate = $search->day;
                        $startDate = $search->day;
                    }
                    if($search->totlePerDay > 0) {
                        $rowData = [
                            $search->day, null, null, null, null, null, null, null, null, null, null, null, null, $search->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                $avgDateSearch = date_diff(date_create($startDate),date_create($endDate))->format("%R%a ");
                // For average of new main search
                if($totalOtherSearches > 0 ) {
                    $rowData = [
                        $startDate, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalOtherSearches / $avgDateSearch
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $endDate, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalOtherSearches / $avgDateSearch
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['jobViews']['label'] ==  'Job Views') {
            if ($arrayChart['jobViews']['value'] ) {
                $totalJobViews = $new['jobViewsFromStart'];
                foreach ($new['jobViews'] as $key => $jobView) {
                    if ($key != 0){
                        $endDate = $jobView->day;
                        $totalJobViews += $jobView->totlePerDay;
                    }else{
                        $startDate = $jobView->day;
                    }
                    if($jobView->totlePerDay > 0) {
                        $rowData = [
                            $jobView->day, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $jobView->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                $avgDateJobView = date_diff(date_create($startDate),date_create($endDate))->format("%R%a ");
                // For average of new Job views
                if($totalJobViews > 0 ) {
                    $rowData = [
                        $startDate, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalJobViews / $avgDateJobView
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $endDate, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalJobViews / $avgDateJobView
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['phoneShares']['label'] ==  'Phone Shares') {
            if ($arrayChart['phoneShares']['value'] ) {
                if($new['phoneSharesFromStart'] >0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $new['phoneSharesFromStart']
                    ];
                    $logTable->addRow($rowData);
                }
                $totalPhoneShares = $new['phoneSharesFromStart'];
                foreach ($new['phoneShares'] as $key => $phoneShare) {
                    $totalPhoneShares += $phoneShare->totlePerDay;
                    if($phoneShare->totlePerDay > 0) {
                        $rowData = [
                            $phoneShare->day, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $phoneShare->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                // For average of new Phone Share
                if($totalPhoneShares > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalPhoneShares / $avgDate
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $to, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalPhoneShares / $avgDate
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['fbShares']['label'] ==  'Facebook Shares') {
            if ($arrayChart['fbShares']['value'] ) {
                if($new['phoneSharesFromStart'] >0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $new['phoneSharesFromStart']
                    ];
                    $logTable->addRow($rowData);
                }
                $totalFBShares = $new['phoneSharesFromStart'];
                foreach ($new['fbShares'] as $key => $fbShare) {
                    $totalFBShares += $fbShare->totlePerDay;
                    if($fbShare->totlePerDay > 0) {
                        $rowData = [
                            $fbShare->day, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $fbShare->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
                // For average of new Fb Share
                if($totalFBShares > 0 ) {
                    $rowData = [
                        $new['FromStartDate'], null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalFBShares / $avgDate
                    ];
                    $logTable->addRow($rowData);
                    $rowData = [
                        $to, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, $totalFBShares / $avgDate
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        $breadcrumbs = 'newChart';
        $charts = [];
        foreach ($arrayChart as $key=>$value){
            $index = array_search($key,array_keys($arrayChart));
            if($index%2 == 0){
                $charts[$key] =  $value;
            }
        }
        $action = \Lang::locale().'/dashboard/new';
        session(['lang' =>  \Lang::locale()]);//for district
        $provinces = Dashboard::getAllProvinces();
        $sectors = Dashboard::getAllSector();
        Lava::LineChart('Stocks', $logTable, ['title' => 'Statistics for New','vAxis'=> ['viewWindowMode'=> "explicit", 'viewWindow'=>['min' => 0 ]]])->setOptions(['pointSize' => 3,'curveType' => 'function','height' => 800,'colors'=> ['#0073e6','#0073e6' , '#e62e00','#e62e00',"#2eb82e","#2eb82e","#e6e600","#e6e600","#990099","#990099","#800000","#800000","#cc6600","#cc6600","#cc6699","#cc6699","#000080","#000080","#666600","#666600"]]);
        return view('dashboard.chart',compact('chartName','breadcrumbs','charts','sectors','action','provinces'));
    }
}