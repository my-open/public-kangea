<?php
namespace App\Http\Controllers\Dashboard;
use App\DashboardModel\Dashboard;
use App\DashboardModel\Active;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Input;
use Carbon\Carbon;

class AccumulativeAllSectorController extends Controller
{
    public function index()
    {

    }
    public function accumulative()
    {
        $chartName = \Lang::get('text_lang.accumulative');
        $breadcrumbs = 'accumulative';
        $action = 'dashboard/accumulative';
        $provinces = Dashboard::getAllProvinces();
        $sectors = Dashboard::getAllSector();
        return view('dashboard.chart3d',compact('chartName','breadcrumbs','sectors','action','provinces'));
    }
}