<?php

namespace App\Http\Controllers\Dashboard;

use App\DashboardModel\Dashboard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Input;
use Carbon\Carbon;


class DashboardController extends Controller
{
    public function index()
    {

    }
    public function accumulative(Request $request)
    {
        $data =$request->all();
        $arrayChart = array(
            'registeredCompany' => array('label'=>'Registered Companies','value' =>true),
            'announcement' => array('label'=>'Announcements','value' =>true),
            'searches' => array('label'=>'Searches','value' =>true),
            'jobSeeker' => array('label'=>'Job Seekers','value' =>true),
            'applicantsWeb' => array('label'=>'Applicants Web','value' =>true),
            'applicantsPhone' => array('label'=>'Applicants Phone','value' =>true),
            'jobViews' => array('label'=>'Job Views','value' =>false)
        );
        if ($data != null) {
            foreach ($arrayChart as $key => $chart){
                if(!isset($_POST[$key])){
                    $arrayChart[$key]['value'] = false;
                }else{
                    $arrayChart[$key]['value'] = true;
                }
            }
            $fkProvincesID = (int)$request->input('fkProvincesID');
            $fkDistrictsID = (int)$request->input('fkDistrictsID');
            $fkCommunesID = (int)$request->input('fkCommunesID');
            $fkSectorID = (int)$request->input('fkSectorID');
            if($fkProvincesID ==0){
                $fkProvincesID = null;
                $fkDistrictsID = null;
                $fkCommunesID = null;
            }
            if($fkDistrictsID == 0){
                $fkDistrictsID = null;
                $fkCommunesID = null;
            }
            if($fkCommunesID == 0){
                $fkCommunesID = null;
            }
            if($fkSectorID == 0 ){
                $fkSectorID = null;
            }
            $from = $request->input('from');
            $to = $request->input('to');
            if ($from == null){
                $from = '2015-01-01';
            }
            if($to == null){
                $to = Carbon::now();
            }
            $unit = $request->input('unit');
            $accumulative = Dashboard::getAllAccumulative($from, $to , $fkProvincesID , $fkDistrictsID , $fkCommunesID ,$fkSectorID, $unit);
        } else {
            session(['districts' => null]);
            session(['communes' => null]);
            $to = Carbon::now();
            $accumulative = Dashboard::getAllAccumulative($from = '2015-01-01', $to, $fkProvincesID = null , $fkDistrictsID = null , $fkCommunesID = null, $fkSectorID = null , $unit = 0);
        }
//        dd($accumulative);
        $chartName = \Lang::get('text_lang.accumulative');
        $logTable = Lava::DataTable();

        $logTable->addDateColumn('Day of Month');
        foreach ($arrayChart as $chart){
            $logTable->addNumberColumn($chart['label']);
        }
        //get data from db
        $CompanytotalDayToDay = 0;
        $AnnouncementTotalDaytoDay = 0;
        $searchTotalDaytoDay = 0;
        $jobSeekersTotalDaytoDay = 0;
        $applicantsWebTotalDaytoDay = 0;
        $applicantsPhoneTotalDaytoDay = 0;
        $jobViewsTotalDaytoDay = 0;
        if($arrayChart['registeredCompany']['label'] == 'Registered Companies') {
            if($arrayChart['registeredCompany']['value']){
                if(empty($request->input('from'))){
                    $accumulative['companiesFromStart'] = 0;
                }
                foreach ($accumulative['companies'] as $key =>$company) {
                    if ($key == 0){
                        $CompanytotalDayToDay = $accumulative['companiesFromStart'] + $company->totlePerDay;
                    }else {
                        $CompanytotalDayToDay += $company->totlePerDay;
                    }
                    $rowData = [
                        $company->day, $CompanytotalDayToDay, null, null, null, null, null, null
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['announcement']['label'] == 'Announcements') {
            if ($arrayChart['announcement']['value']) {
                if(empty($request->input('from'))){
                    $accumulative['announcementsFromStart'] = 0;
                }
                foreach ($accumulative['announcements'] as $key => $announcement) {
                    if ($key == 0){
                        $AnnouncementTotalDaytoDay = $accumulative['announcementsFromStart'] + $announcement ->totlePerDay;
                    }else {
                        $AnnouncementTotalDaytoDay += $announcement->totlePerDay;
                    }
                    $rowData = [
                        $announcement->day, null, $AnnouncementTotalDaytoDay, null, null, null, null, null
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['searches']['label'] == 'Searches') {
            if ($arrayChart['searches']['value']) {
                if(empty($request->input('from'))){
                    $accumulative['searchesFromStart'] = 0;
                }
                foreach ($accumulative['searches'] as $key => $search) {
                    if ($key == 0){
                        $searchTotalDaytoDay = $accumulative['searchesFromStart'] + $search->totlePerDay;
                    }else {
                        $searchTotalDaytoDay += $search->totlePerDay;
                    }
                    $rowData = [
                        $search->day, null, null, $searchTotalDaytoDay, null, null, null, null
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['jobSeeker']['label'] == 'Job Seekers') {
            if ($arrayChart['jobSeeker']['value'] && empty($fkProvincesID)) {
                if(empty($request->input('from'))){
                    $accumulative['jobSeekersFromStart'] = 0;
                }
                foreach ($accumulative['jobSeekers'] as $key => $jobSeekers) {
                    if ($key == 0){
                        $jobSeekersTotalDaytoDay = $accumulative['jobSeekersFromStart'] + $jobSeekers->totlePerDay;
                    }else {
                        $jobSeekersTotalDaytoDay += $jobSeekers->totlePerDay;
                    }
                    $rowData = [
                        $jobSeekers->day, null, null, null, $jobSeekersTotalDaytoDay, null, null, null
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['applicantsWeb']['label'] == 'Applicants Web') {
            if ($arrayChart['applicantsWeb']['value']) {
                if(empty($request->input('from'))){
                    $accumulative['applicantsWebFromStart'] = 0;
                }
                foreach ($accumulative['applicantsWeb'] as $key => $applicantsWeb) {
                    if ($key == 0){
                        $applicantsWebTotalDaytoDay = $accumulative['applicantsWebFromStart'] + $applicantsWeb->totlePerDay;
                    }else {
                        $applicantsWebTotalDaytoDay += $applicantsWeb->totlePerDay;
                    }
                    $rowData = [
                        $applicantsWeb->day, null, null, null, null, $applicantsWebTotalDaytoDay, null, null
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        if($arrayChart['applicantsPhone']['label'] == 'Applicants Phone') {
            if ($arrayChart['applicantsPhone']['value']) {
                if(empty($request->input('from'))){
                    $accumulative['applicantsPhoneFromStart'] = 0;
                }
                foreach ($accumulative['applicantsPhone'] as $key => $applicantPhone) {
                    if ($key == 0){
                        $applicantsPhoneTotalDaytoDay = $accumulative['applicantsPhoneFromStart'] + $applicantPhone->totlePerDay;
                    }else {
                        $applicantsPhoneTotalDaytoDay += $applicantPhone->totlePerDay;
                    }
                    if($applicantsPhoneTotalDaytoDay > 0) {
                        $rowData = [
                            $applicantPhone->day, null, null, null, null, null, $applicantsPhoneTotalDaytoDay, null
                        ];
                        $logTable->addRow($rowData);
                    }
                }
            }
        }
        if($arrayChart['jobViews']['label'] ==  'Job Views') {
            if ($arrayChart['jobViews']['value'] ) {
                if(empty($request->input('from'))){
                    $accumulative['jobViewsFromStart'] = 0;
                }
                foreach ($accumulative['jobViews'] as $key => $jobView) {
                    if ($key == 0){
                        $jobViewsTotalDaytoDay = $accumulative['jobViewsFromStart'] + $jobView->totlePerDay;
                    }else {
                        $jobViewsTotalDaytoDay += $jobView->totlePerDay;
                    }
                    $rowData = [
                        $jobView->day, null, null, null, null, null, null, $jobViewsTotalDaytoDay
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        $breadcrumbs = 'accumulative';
        $charts = $arrayChart;
        $action = \Lang::locale().'/dashboard/accumulative';
        session(['lang' =>  \Lang::locale()]);//for district
        $provinces = Dashboard::getAllProvinces();
        $sectors = Dashboard::getAllSector();
        Lava::LineChart('Stocks', $logTable, ['title' => 'Statistics for Accumulative','vAxis'=> ['viewWindowMode'=> "explicit", 'viewWindow'=>['min' => 0 ]]])->setOptions(['pointSize' => 3,'curveType' => 'function','height' => 800]);
        return view('dashboard.chart',compact('chartName','breadcrumbs','charts','sectors','action','provinces'));
    }
    public function getDistricts()
    {
        $districts = Dashboard::getAllDistricts($_GET['proID']);
        session(['districts' => $districts]);
        echo json_encode($districts);
    }
    public function getCommunes()
    {
        $communes = Dashboard::getAllCommunes($_GET['disID']);
        session(['communes' => $communes]);
        echo json_encode($communes);
    }
}
