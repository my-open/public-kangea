<?php

namespace App\Http\Controllers\Dashboard;
use App\DashboardModel\Dashboard;
use App\DashboardModel\Active;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Input;
use Carbon\Carbon;


class ActiveController extends Controller
{
    public function index()
    {

    }
    public function activeChart (Request $request)
    {
        $data =$request->all();
        $arrayChart = array(
            'loggedComapany' => array('label'=>'Companies Logged in','value' =>true),
            'postingCompany' => array('label'=>'Companies Posting Job','value' =>true),
            'activeAccount' => array('label'=>'Active Account User','value' =>false),
            'announcement' => array('label'=>'Announcements','value' =>true),
            'announcementReceivedApplication' => array('label'=>'Announcements that Have Received Application','value' =>true),
            'job' => array('label'=>'Job','value' =>true),
        );
        if ($data != null) {
            foreach ($arrayChart as $key => $chart){
                if(!isset($_POST[$key])){
                    $arrayChart[$key]['value'] = false;
                }else{
                    $arrayChart[$key]['value'] = true;
                }
            }
            $fkProvincesID = (int)$request->input('fkProvincesID');
            $fkDistrictsID = (int)$request->input('fkDistrictsID');
            $fkCommunesID = (int)$request->input('fkCommunesID');
            $fkSectorID = (int)$request->input('fkSectorID');
            if($fkProvincesID ==0){
                $fkProvincesID = null;
                $fkDistrictsID = null;
                $fkCommunesID = null;
            }
            if($fkSectorID == 0 ){
                $fkSectorID = null;
            }
            $from = $request->input('from');
            $to = $request->input('to');
            if ($from == null || $from <= '2016-10-19'){
                $from = '2016-10-19';
            }else{
                $from = $from;
            }
            if($to == null){
                $to = Carbon::now();
            }
            $location = null;
            $unit = $request->input('unit');
            $active = Active::getAllActive($from, $to , $fkProvincesID , $fkDistrictsID , $fkCommunesID ,$fkSectorID, $unit,$arrayChart);
        } else {
            session(['districts' => null]);
            session(['communes' => null]);
            $to = Carbon::now();
            $active = Active::getAllActive($from = '2016-10-19', $to, $fkProvincesID = null , $fkDistrictsID = null , $fkCommunesID = null ,$fkSectorID=null, $unit = 0,$arrayChart);
        }
        $chartName = \Lang::get('text_lang.active');
        $logTable = Lava::DataTable();
        $logTable->addDateColumn('Day of Month');
        foreach ($arrayChart as $chart){
            $logTable->addNumberColumn($chart['label']);
        }
        if($arrayChart['loggedComapany']['label'] == 'Companies Logged in') {
            if($arrayChart['loggedComapany']['value']){
                foreach ($active['loggedComapany'] as $key =>$company) {
                    if($company->totlePerDay >0) {
                        $rowData = [
                            $company->day, $company->totlePerDay
                        ];
                        $logTable->addRow($rowData);
                    }
                }
            }
        }
        //postingCompany 'postingCompany' => array('label'=>'Companies Posting Job','value' =>true),
        if($arrayChart['postingCompany']['label'] == 'Companies Posting Job') {
            if($arrayChart['postingCompany']['value']){
                foreach ($active['postingCompany'] as $key =>$company) {
                    $rowData = [
                        $company->day,null, $company->totlePerDay
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
//        dd($active['activeAccount']);
        // Active Account User
        if($arrayChart['activeAccount']['label'] == 'Active Account User') {
            if ($arrayChart['activeAccount']['value'] && empty($fkProvincesID)) {
                foreach ($active['activeAccount'] as $key => $jobSeekers) {
                    $rowData = [
                        $jobSeekers->day,null,null ,$jobSeekers->totlePerDay
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        //active announcements
        if($arrayChart['announcement']['label'] == 'Announcements') {
            if ($arrayChart['announcement']['value']) {
                foreach ($active['announcements'] as $key => $announcement) {
                    $rowData = [
                        $announcement->day,null, null, null ,$announcement->totlePerDay
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        //active Announcements that Have Received Application
        if($arrayChart['announcementReceivedApplication']['label'] == 'Announcements that Have Received Application') {
            if ($arrayChart['announcementReceivedApplication']['value']) {
                foreach ($active['announcementReceivedApplication'] as $key => $announcementReceivedApplication) {
                    $rowData = [
                        $announcementReceivedApplication->day,null, null, null ,null,$announcementReceivedApplication->totlePerDay
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        //active job
        if($arrayChart['job']['label'] == 'Job') {
            if ($arrayChart['job']['value']) {
                foreach ($active['job'] as $key => $job) {
                    $rowData = [
                        $job->day,null, null, null ,null,null,$job->totlePerDay
                    ];
                    $logTable->addRow($rowData);
                }
            }
        }
        $breadcrumbs = 'active';
        $charts = $arrayChart;
        $action = \Lang::locale().'/dashboard/active';
        session(['lang' =>  \Lang::locale()]);//for district
        $provinces = Dashboard::getAllProvinces();
        $sectors = Dashboard::getAllSector();
        Lava::LineChart('Stocks', $logTable, ['title' => 'Statistics for Active','vAxis'=> ['viewWindowMode'=> "explicit", 'viewWindow'=>['min' => 0 ]]])->setOptions(['pointSize' => 3,'curveType' => 'function','height' => 800]);
            return view('dashboard.activeChart',compact('chartName','breadcrumbs','charts','sectors','action','provinces'));
    }
}