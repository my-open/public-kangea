<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('accounts.index');
    }
}
