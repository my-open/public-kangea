<?php

namespace App\Http\Controllers;

use App\Commune;
use App\District;
use App\Position;
use App\Province;
use App\Sector;
use App\Subsector;
use App\TargetJob;
use App\User;
use App\Village;
use App\Zone;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Lang;
use Laracasts\Flash\Flash;

class TargetjobController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:jobseeker',[ 'except' => ['searchCandidate', 'viewCandidate'] ]);
    }

    public function index()
    {
        $lang =  strtoupper(Lang::getLocale());
        $seekerId =  Auth::id();
        $targetJob = DB::table('TargetJobs')
            ->leftJoin('Sectors', 'Sectors.pkSectorsID', '=', 'TargetJobs.fkSectorsID')
            ->leftJoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'TargetJobs.fkSubsectorsID')
            ->leftJoin('Positions', 'Positions.pkPositionsID', '=', 'TargetJobs.fkPositionsID')
            ->leftJoin('Provinces', 'Provinces.pkProvincesID', '=', 'TargetJobs.fkProvincesID')
            ->leftJoin('Districts', 'Districts.pkDistrictsID', '=', 'TargetJobs.fkDistrictsID')
            ->leftJoin('Communes', 'Communes.pkCommunesID', '=', 'TargetJobs.fkCommunesID')
            ->where('TargetJobs.fkUsersID', '=', $seekerId)
            ->select(
                'Sectors.pkSectorsID', 'Sectors.sectorsName'.$lang, 'Sectors.sectorsNameKH', 'Sectors.sectorsNameZH', 'Sectors.sectorsNameTH',
                'Subsectors.pkSubsectorsID', 'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH', 'Subsectors.subsectorsNameZH', 'Subsectors.subsectorsNameTH',
                'Positions.pkPositionsID', 'Positions.positionsNameEN', 'Positions.positionsNameKH', 'Positions.positionsNameZH', 'Positions.positionsNameTH',
                'Provinces.pkProvincesID', 'Provinces.provincesNameEN', 'Provinces.provincesNameKH', 'Provinces.provincesNameZH', 'Provinces.provincesNameTH',
                'Districts.pkDistrictsID', 'Districts.districtsNameEN', 'Districts.districtsNameKH', 'Districts.districtsNameZH', 'Districts.districtsNameTH',
                'Communes.pkCommunesID', 'Communes.CommunesNameEN', 'Communes.CommunesNameKH', 'Communes.CommunesNameZH', 'Communes.CommunesNameTH',
                'TargetJobs.pkTargetJobsID'
            )
            ->first();
        return view('accounts.targetjobs.index', compact('lang', 'targetJob'));
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

        return view('accounts.targetjobs.add', compact('lang', 'sectors', 'provinces') );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fkSectorsID' => 'required',
            'fkSubsectorsID'=>'required',
            'fkPositionsID'=>'required',
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'fkDistrictsID' => 'required',
        ]);

        $input = $request->all();
        $input['fkUsersID'] = Auth::id();
        $input['fkCountriesID'] = 855;
        TargetJob::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/targetJob');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function editTargetJob(){
        $lang =  strtoupper(Lang::getLocale());
        $seekerId =  Auth::id();
        $targetJob = DB::table('TargetJobs')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'TargetJobs.fkSectorsID')
            ->leftJoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'TargetJobs.fkSubsectorsID')
            ->leftJoin('Positions', 'Positions.pkPositionsID', '=', 'TargetJobs.fkPositionsID')
            ->leftJoin('Provinces', 'Provinces.pkProvincesID', '=', 'TargetJobs.fkProvincesID')
            ->leftJoin('Districts', 'Districts.pkDistrictsID', '=', 'TargetJobs.fkDistrictsID')
            ->leftJoin('Communes', 'Communes.pkCommunesID', '=', 'TargetJobs.fkCommunesID')
            ->where('TargetJobs.fkUsersID', '=', $seekerId)
            ->select(
                'Sectors.pkSectorsID', 'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH',
                'Subsectors.pkSubsectorsID', 'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH',
                'Positions.pkPositionsID', 'Positions.positionsNameEN', 'Positions.positionsNameKH',
                'Provinces.pkProvincesID', 'Provinces.provincesNameEN', 'Provinces.provincesNameKH',
                'Districts.pkDistrictsID', 'Districts.districtsNameEN', 'Districts.districtsNameKH',
                'Communes.pkCommunesID', 'Communes.CommunesNameEN', 'Communes.CommunesNameKH',
                'TargetJobs.pkTargetJobsID'
            )
            ->first();

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $subsectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $targetJob->pkSectorsID)->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');
        $positions = Position::where('positionsStatus', true)->where('fkSectorsID', $targetJob->pkSectorsID)->orderBy('positionsOrder')->pluck('positionsName'.$lang, 'pkPositionsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $targetJob->pkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $targetJob->pkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');

        return view('accounts.targetjobs.edit', compact('targetJob', 'sectors', 'subsectors', 'districtByProvinces', 'communeByDistricts', 'villageByCommunes', 'positions', 'provinces', 'zones') );
    }

    public function updateTargetJob( Request $request ){
        $seekerId =  Auth::id();
        $TJob = DB::table('TargetJobs')
            ->where('TargetJobs.fkUsersID', '=', $seekerId)
            ->select( 'TargetJobs.pkTargetJobsID')
            ->first();

        $targetJob = TargetJob::findOrFail( $TJob->pkTargetJobsID );
        $this->validate($request, [
            'fkSectorsID' => 'required',
            'fkSubsectorsID'=>'required',
            'fkPositionsID'=>'required',
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'fkDistrictsID' => 'required',
        ]);

        $input = $request->all();

        $targetJob->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/targetJob');
    }

    public function searchCandidate( Request $request ){
        $fkSectorsID = null;
        $fkSubsectorsID = null;
        $fkPositionsID = null;
        $fkProvincesID = null;
        $subsectors = [];
        $positions = [];

        if( Lang::getLocale() == 'en' ){
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameEN')->pluck('sectorsNameEN', 'pkSectorsID');
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
        }
        else{
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameKH')->pluck('sectorsNameKH', 'pkSectorsID');
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
        }

        $searchCriteria = [
            'fkSectorsID' => $request->get('fkSectorsID'),
            'fkSubsectorsID' => $request->get('fkSubsectorsID'),
            'fkPositionsID' => $request->get('fkPositionsID'),
            'fkProvincesID' => $request->get('fkProvincesID'),
        ];
        $strWhere = '';
        foreach($searchCriteria as $key => $value){
            if( $value == '' ){
                $$key = null;
                continue;
            }
            $$key = $value;

            if( $strWhere != '' ){
                $strWhere .= ' AND tblTargetJobs.' . $key. " = ". $value;
            }else{
                $strWhere = 'tblTargetJobs.' . $key. " = ". $value;
            }
        }

        if( $fkSectorsID != null) {
            if (Lang::getLocale() == 'en') {
                $subsectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('subsectorsNameEN')->pluck('subsectorsNameEN', 'pkSubsectorsID');
                $positions = Position::where('positionsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('positionsOrder')->pluck('positionsNameEN', 'pkPositionsID');
            } else {
                $subsectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('subsectorsNameKH')->pluck('subsectorsNameKH', 'pkSubsectorsID');
                $positions = Position::where('positionsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('positionsOrder')->pluck('positionsNameKH', 'pkPositionsID');
            }
        }

        if( $strWhere != null ){
            $seekers = DB::table('users')
                ->Join('TargetJobs', 'TargetJobs.fkUsersID', '=', 'users.id')
                ->whereRaw($strWhere)
                ->select('users.id', 'users.gender', 'users.name', 'users.phone', 'users.email', 'users.fkPositionsID', 'users.created_at')
                ->paginate( config("constants.PAGINATION_NUM") );
        }else{
            $seekers = DB::table('users')
                ->Join('TargetJobs', 'TargetJobs.fkUsersID', '=', 'users.id')
                ->select('users.id', 'users.gender', 'users.name', 'users.phone', 'users.email', 'users.fkPositionsID', 'users.created_at')
                ->paginate( config("constants.PAGINATION_NUM") );
        }

        return view('accounts.users.employers.searchCandidate', compact( 'searchCriteria', 'fkSectorsID', 'fkSubsectorsID', 'fkPositionsID', 'fkProvincesID', 'seekers', 'sectors', 'provinces', 'subsectors', 'positions'));
    }

    public function viewCandidate($jobseekerID, $jobId = 0)
    {
        $lang =  strtoupper(Lang::getLocale());
        $companyId = DB::table('Announcements')
            ->where('pkAnnouncementsID', $jobId)
            ->value('fkCompaniesID');

        $positionId = DB::table('Announcements')
            ->where('pkAnnouncementsID', $jobId)
            ->value('fkPositionsID');

        $user =DB::table('users')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'users.fkPositionsID')
            ->select(
                'users.id', 'users.name', 'users.phone', 'users.email', 'users.fkPositionsID', 'users.gender', 'users.address', 'users.status', 'users.experience',
                'Positions.positionsName'.$lang
            )
            ->where('users.id', '=', $jobseekerID)
            ->first();

        if( $user->experience == 1 ){
            $experiences = DB::table('JobExperiences')
                ->leftjoin('users' , 'users.id', '=', 'JobExperiences.fkUsersID')
                ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'JobExperiences.fkSectorsID')
                ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'JobExperiences.fkSubsectorsID')
                ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'JobExperiences.fkPositionsID')
                ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'JobExperiences.fkProvincesID')
                ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'JobExperiences.fkCommunesID')
                ->where('JobExperiences.fkUsersID', '=', $jobseekerID )
                ->orderBy('JobExperiences.pkJobExperiencesID', 'DESC')
                ->select(
                    'JobExperiences.fkCountriesID', 'JobExperiences.fkProvincesID', 'JobExperiences.fkDistrictsID', 'JobExperiences.fkCommunesID',
                    'JobExperiences.fkSectorsID', 'JobExperiences.fkSubsectorsID', 'JobExperiences.fkPositionsID','JobExperiences.workExperience', 'JobExperiences.jobExperiencesDescriptionEN', 'JobExperiences.jobExperiencesDescriptionKH',
                    'users.id', 'users.gender', 'users.name', 'users.phone', 'users.email', 'users.experience',
                    'Positions.positionsName'.$lang,
                    'Subsectors.subsectorsName'.$lang,
                    'Sectors.sectorsName'.$lang,
                    'Provinces.provincesName'.$lang
                )
                ->get();
        }

        $targetJob = DB::table('TargetJobs')
            ->leftjoin('users', 'users.id', '=', 'TargetJobs.fkUsersID')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'TargetJobs.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'TargetJobs.fkSubsectorsID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'TargetJobs.fkPositionsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'TargetJobs.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'TargetJobs.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'TargetJobs.fkCommunesID')
            ->where('TargetJobs.fkUsersID', '=', $jobseekerID)
            ->select(
                'TargetJobs.fkCountriesID', 'TargetJobs.fkProvincesID', 'TargetJobs.fkDistrictsID', 'TargetJobs.fkCommunesID', 'TargetJobs.fkVillagesID',
                'TargetJobs.fkZonesID', 'TargetJobs.fkSectorsID', 'TargetJobs.fkSubsectorsID', 'TargetJobs.fkPositionsID',
                'Positions.positionsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Sectors.sectorsName'.$lang,
                'Provinces.provincesName'.$lang,
                'Districts.districtsName'.$lang,
                'Communes.CommunesName'.$lang
            )
            ->first();

        return view('accounts.users.employers.viewCandidate', compact('lang', 'user', 'experiences', 'targetJob', 'experiencesGetPositions', 'jobId', 'companyId', 'positionId'));
    }

}
