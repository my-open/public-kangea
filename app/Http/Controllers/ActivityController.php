<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Flash;
use Lang;
use App\Subsector;
use App\Sector;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['getActivitiesAjax']]);
    }

    public function index()
    {
        $activitiesNameEN = null;
        $activitiesNameKH = null;
        $activitiesNameZH = null;
        $activitiesNameTH = null;
        $fkSubsectorsID = null;
        $activitiesOrder = null;
        $lang =  strtoupper(Lang::getLocale());
        $subsectors = Subsector::where('subsectorsStatus', true)->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');

        $activities = DB::table('Activities')
            ->join('Subsectors', 'Activities.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->select('Activities.pkActivitiesID',
                'Activities.activitiesName'.$lang,
                'Activities.activitiesOrder', 'Activities.activitiesStatus',
                'Subsectors.subsectorsName'.$lang )
            ->orderBy('pkActivitiesID', 'DESC')
            ->where('activitiesStatus', '<>', 3)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.activities.index', compact( 'activitiesNameEN', 'activitiesNameKH', 'activitiesNameZH', 'activitiesNameTH', 'fkSubsectorsID', 'activitiesOrder', 'activities', 'subsectors', 'lang' ) );
    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');

        return view('accounts.activities.add',  compact('sectors'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'fkSubsectorsID' => 'required|max:11',
            'activitiesNameEN' => 'required|max:150',
            'activitiesNameKH' => 'required|max:150',
            'activitiesNameZH' => 'required|max:150',
            'activitiesNameTH' => 'required|max:150',
            'activitiesStatus' => 'required|max:1'
        ]);

        $input = $request->all();
        Activity::create($input);

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/activity');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $lang =  strtoupper(Lang::getLocale());
        $activity = Activity::findOrFail($id);

        $sector = DB::table('Sectors')
            ->join('Subsectors', 'Subsectors.fkSectorsID', '=', 'Sectors.pkSectorsID' )
            ->where('Subsectors.pkSubsectorsID', $activity->fkSubsectorsID)
            ->select(
                'Sectors.pkSectorsID'
            )
            ->first();
        $sectorId = $sector->pkSectorsID;

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $subsectors = Subsector::where('subsectorsStatus', true)
            ->where('Subsectors.fkSectorsID', $sectorId)
            ->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');

        return view('accounts.activities.edit', compact('activity', 'sectors', 'subsectors', 'sectorId'));
    }

    public function update(Request $request, $id)
    {
        $activity = Activity::findOrFail($id);

        $this->validate($request, [
            'fkSubsectorsID' => 'required|max:11',
            'activitiesNameEN' => 'required|max:150',
            'activitiesNameKH' => 'required|max:150',
            'activitiesStatus' => 'required|max:1'
        ]);
        $input = $request->all();
        $activity->fill($input)->save();

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect( \Lang::getLocale().'/account/activity');
    }

    public function destroy($id)
    {
        Activity::where('pkActivitiesID', $id)
            ->update(['activitiesStatus' => '3']);

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/activity');
    }

    public function getActivitiesAjax(Request $request){
        $activities = Activity::select('activitiesNameEN', 'activitiesNameKH', 'activitiesNameZH', 'activitiesNameTH', 'pkActivitiesID')
            ->where('fkSubsectorsID', '=', $request->input('subsectorId'))
            ->where('activitiesStatus', 1)
            ->orderBy('activitiesOrder')
            ->get();

        return ['selectOption' => trans('text_lang.selectOption'), 'data' => $activities];
    }

    public function searchActivity( Request $request )
    {
        $lang =  strtoupper(Lang::getLocale());
        $subsectors = Subsector::where('subsectorsStatus', true)->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');

        $activitiesNameEN = $request->get('activitiesNameEN');
        $activitiesNameKH = $request->get('activitiesNameKH');
        $activitiesNameZH = $request->get('activitiesNameZH');
        $activitiesNameTH = $request->get('activitiesNameTH');
        $fkSubsectorsID = $request->get('fkSubsectorsID');
        $activitiesOrder = $request->get('activitiesOrder');

        $searchCriteria = [
            'activitiesNameEN' => trim( $request->get('activitiesNameEN') ),
            'activitiesNameKH' => trim( $request->get('activitiesNameKH') ),
            'activitiesNameZH' => trim( $request->get('activitiesNameZH') ),
            'activitiesNameTH' => trim( $request->get('activitiesNameTH') ),
            'fkSubsectorsID' => $request->get('fkSubsectorsID'),
        ];

        $strWhere = ' activitiesStatus <> 3 ';
        foreach($searchCriteria as $column => $value){
            if( $value == '' ){
                $$column = null;
                continue;
            }

            $$column = trim($value);

            if( $strWhere == '' ){
                if( $column == 'activitiesName'.$lang){
                    $strWhere = ' AND tblActivities.' . $column. " LIKE '". $value . "%'";
                }else{
                    $strWhere = ' AND tblActivities.' . $column. " = '". $value."' ";
                }
            }else{
                if( $column == 'activitiesName'.$lang){
                    $strWhere .= ' AND tblActivities.' . $column. " LIKE '". $value . "%'";
                }else{
                    $strWhere .= ' AND tblActivities.' . $column. " = '". $value."' ";
                }
            }
        }

        $activities = DB::table('Activities')
            ->join('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Activities.fkSubsectorsID' )
            ->select('Activities.pkActivitiesID', 'Activities.activitiesNameEN', 'Activities.activitiesNameKH', 'Activities.activitiesNameZH', 'Activities.activitiesNameTH', 'Activities.activitiesOrder', 'Activities.activitiesStatus', 'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH')
            ->orderBy('pkActivitiesID', 'DESC')
            ->whereRaw($strWhere)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.activities.index', compact( 'lang', 'activitiesNameEN', 'activitiesNameKH', 'activitiesNameZH', 'activitiesNameTH', 'fkSubsectorsID', 'activitiesOrder', 'activities', 'subsectors') );
    }

}
