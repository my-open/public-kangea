<?php
/*
Author = Sorn Sea
Modified by = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 14, March 2016
Modified Date = Monday 14, March 2016
*/

namespace App\Http\Controllers;

use App\CompanyUser;
use App\Position;
use App\Zone;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

use DB;
use Flash;
use Lang;

use App\Company;
use App\Sector;

use App\Province;

class RegisterController extends Controller
{

    public function index()
    {
        return view('accounts.users.register');
    }

    public function getCompanyByIDAjax(Request $request){
        $company = Company::findOrFail( $request['companiesId']);

        if( Lang::getLocale() == 'en' ){
            $provinceName = DB::table('Provinces')->where('pkProvincesID', $company->fkProvincesID)->value('provincesNameEN');
            $districtName = DB::table('Districts')->where('pkDistrictsID', $company->fkDistrictsID)->value('districtsNameEN');
            $communeName = DB::table('Communes')->where('pkCommunesID', $company->fkCommunesID)->value('CommunesNameEN');
            $villageName = DB::table('Villages')->where('pkVillagesID', $company->fkVillagesID)->value('villagesNameEN');
            $sectorName = DB::table('Sectors')->where('pkSectorsID', $company->fkSectorsID)->value('sectorsNameEN');
            $subsectorName = DB::table('Subsectors')->where('pkSubsectorsID', $company->fkSubsectorsID)->value('subsectorsNameEN');
            $activityName = DB::table('Activities')->where('pkActivitiesID', $company->fkActivitiesID)->value('ActivitiesNameEN');
            $zoneName = DB::table('Zones')->where('pkZonesID', $company->fkZonesID)->value('zonesNameEN');
        }
        else{
            $provinceName = DB::table('Provinces')->where('pkProvincesID', $company->fkProvincesID)->value('provincesNameKH');
            $districtName = DB::table('Districts')->where('pkDistrictsID', $company->fkDistrictsID)->value('districtsNameKH');
            $communeName = DB::table('Communes')->where('pkCommunesID', $company->fkCommunesID)->value('CommunesNameKH');
            $villageName = DB::table('Villages')->where('pkVillagesID', $company->fkVillagesID)->value('villagesNameKH');
            $sectorName = DB::table('Sectors')->where('pkSectorsID', $company->fkSectorsID)->value('sectorsNameKH');
            $subsectorName = DB::table('Subsectors')->where('pkSubsectorsID', $company->fkSubsectorsID)->value('subsectorsNameKH');
            $activityName = DB::table('Activities')->where('pkActivitiesID', $company->fkActivitiesID)->value('ActivitiesNameKH');
            $zoneName = DB::table('Zones')->where('pkZonesID', $company->fkZonesID)->value('zonesNameKH');
        }

        $sectorAndSub = '<div class="form-group ">'.
                    '<label class="col-md-5 control-label">'. trans('text_lang.sector') .' <span class="requredStar">***</span></label>'.
                    '<label class="col-md-7 control-label" style="text-align: left">'.
                        $sectorName
                    .'</label></div>'.
                    '<div class="form-group ">'.
                    '<label class="col-md-5 control-label">'. trans('text_lang.subsector') .' <span class="requredStar">***</span></label>'.
                    '<label class="col-md-7 control-label" style="text-align: left">'.
                        $subsectorName
                    .'</label></div>'.
                    '<div class="form-group ">'.
                    '<label class="col-md-5 control-label">'. trans('text_lang.mainActivity') .' <span class="requredStar">***</span></label>'.
                    '<label class="col-md-7 control-label" style="text-align: left">'.
                        $activityName
                    .'</label></div>';
        $companyProfile = '<div class="form-group ">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.companiesPhone') .' <span class="requredStar">***</span></label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                                $company->companiesPhone
                            .'</label></div>'.
                            '<div class="form-group ">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.companiesEmail') .' <span class="requredStar">***</span></label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                                $company->companiesEmail
                            .'</label></div>'.
                            '<div class="form-group ">
                                <label class="col-md-8 control-label">
                                    '.trans('text_lang.locationOfTheMainOfficeOfYourCompany').'
                                </label>
                            </div>
                            <div class="form-group ">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.province') .' <span class="requredStar">***</span></label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                                $provinceName
                            .'</label></div>'.
                            '<div class="form-group ">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.district') .' <span class="requredStar">***</span></label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                            $districtName
                            .'</label></div>'
                            .'<div class="form-group ">'.
                                '<label class="col-md-5 control-label">'. trans('text_lang.commune') .'</label>'.
                                '<label class="col-md-7 control-label" style="text-align: left">'.
                                $communeName
                                .'</label></div>'
                            .'<div class="form-group ">'.
                                '<label class="col-md-5 control-label">'. trans('text_lang.village') .'</label>'.
                                '<label class="col-md-7 control-label" style="text-align: left">'.
                                $villageName
                                .'</label></div>'
                            .'<div class="form-group ">'.
                                '<label class="col-md-5 control-label">'. trans('text_lang.zone') .'</label>'.
                                '<label class="col-md-7 control-label" style="text-align: left">'.
                                $zoneName
                                .'</label></div>'
                            .'<div class="form-group ">'.
                                '<label class="col-md-5 control-label">'. trans('text_lang.companiesAddress') .'</label>'.
                                '<label class="col-md-7 control-label" style="text-align: left">'.
                                $company->companiesAddress
                                .'</label></div>';

        $companyDescription = '<div class="form-group ">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.numberOfWorker') .'</label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                            $company->companiesNumberOfWorker
                            .'</label></div>'.
                            '<div class="form-group "><label class="col-md-11 control-label">
                                    '.trans('text_lang.doYourWorkersCallYourCompanyByAdifferentName').'
                                </label>
                            </div>
                            <div class="form-group ">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.companiesNickName') .'</label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                            $company->companiesNickName
                            .'</label></div>'.
                            '<div class="form-group ">'.
                            '<label class="col-md-5 control-label" for="companiesDescriptionEN">'. trans('text_lang.companyDescriptionEN') .'<br/><br/><em>'. trans('text_lang.companyDescriptionNote') .'</em></label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                            $company->companiesDescriptionEN
                            .'</label></div>'
                            .'<div class="form-group ">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.companyDescriptionKH') .'</label>'.
                            '<label class="col-md-7 control-label" style="text-align: left">'.
                            $company->companiesDescriptionKH
                            .'</label></div>'
                            .'<div class="form-group">'.
                            '<label class="col-md-5 control-label">'. trans('text_lang.companiesLogo') .'</label>'.
                            '<div class="col-md-7">'.
                            '<img src="'. URL::asset("images/companyLogos/thumbnails/'. $company->companiesLogo.'") .'" class="img-responsive" width="130" alt="Company Logo">'
                            .'</div></div>';

        //return (String) view('accounts.users.employers.show', compact('company', 'provinceName', 'districtName','communeName', 'villageName', 'sectorName', 'subsectorName', 'zoneName', 'positions'));
        return ['sectorAndSub' => $sectorAndSub, 'companyProfile' => $companyProfile, 'companyDescription' => $companyDescription];
    }

//// Company Register
    public function employer()
    {
        if( Lang::getLocale() == 'en' ){
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameEN')->pluck('sectorsNameEN', 'pkSectorsID');
            $zones = Zone::where('zonesStatus', true)->orderBy('zonesNameEN')->pluck('zonesNameEN', 'pkZonesID');
        }
        else{
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameKH')->pluck('sectorsNameKH', 'pkSectorsID');
            $zones = Zone::where('zonesStatus', true)->orderBy('zonesNameKH')->pluck('zonesNameKH', 'pkZonesID');
        }

        return view('accounts.users.employers.register', compact('sectors', 'provinces', 'zones') );
    }

    //Validate Company Rep Tap 1 register
    public function checkValidateCompanyRepAjax( Request $request ){

        if (Session::token() !== $request->input('_token')) {
            return response()->json(array(
                'msg' => 'Unauthorized attempt to create setting'
            ));
        }

        if( $request->get('companiesID') != '' ){
            $rules = array(
                'companiesNameEN' => 'required|max:150',
                'gender' => 'required',
                'name' => 'required|max:150',
                'phone' => 'required|digits_between:9,10',
                'email' => 'required|email|max:70|unique:users',
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required|min:6',
            );
        }else {
            $rules = array(
                'companiesNameEN' => 'required|max:150',
                'fkSectorsID' => 'required|max:255',
                'fkSubsectorsID' => 'required',
                'fkActivitiesID' => 'required',
                'gender' => 'required',
                'name' => 'required|max:150',
                'phone' => 'required|digits_between:9,10',
                'email' => 'required|email|max:70|unique:users',
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required|min:6',
            );
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json($messages);
        }

        return 'success';
    }

    //Validate company profile Tap 2 register
    public function checkValidateCompanyProAjax( Request $request ){
        if (Session::token() !== $request->input('_token')) {
            return response()->json(array(
                'msg' => 'Unauthorized attempt to create setting'
            ));
        }

        if( $request->get('companiesID') == '' ){
            $rules = array(
                'companiesPhone' => 'digits_between:9,10',
                'companiesEmail' => 'email|max:70',
                'fkProvincesID' => 'required',
                'fkDistrictsID' => 'required',
                'companiesSite' => 'url'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $messages = $validator->errors();
                return response()->json($messages);
            }
        }

        return 'success';
    }

    //Company Rep Register Tap 3
    public function employerRegisterAjax(Request $request)
    {
        if (Session::token() !== $request->input('_token')) {
            return response()->json(array(
                'msg' => 'Unauthorized attempt to create setting'
            ));
        }

        //Log::info( $request->file('companiesLogo') );

        if( $request->get('companiesID') == '' ){
            $rules = array(
                //Company Rep
                'companiesNameEN' => 'required|max:150',
                'fkSectorsID' => 'required|max:255',
                'fkSubsectorsID' => 'required',
                'fkActivitiesID' => 'required',
                'gender' => 'required',
                'name' => 'required|max:150',
                'phone' => 'required|digits_between:9,10',
                'email' => 'required|email|max:70|unique:users',
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required|min:6',

                //Company Pro
                'companiesPhone' => 'digits_between:9,10',
                'companiesEmail' => 'email|max:70',
                'fkProvincesID' => 'required',
                'fkDistrictsID' => 'required',
                'companiesSite' => 'url',

                //Company Des
                'companiesNumberOfWorker' => 'digits_between:1,4',
                'companiesLogo' => 'max:'.config("constants.IMAGE_SIZE_MAX").'|image|mimes:jpeg,jpg,png',
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 203);
            }
        }

        if( $request->file('companiesLogo') != '' ) {
            if ($request->file('companiesLogo')->isValid()) {
                $file = $request->file('companiesLogo');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if ($file->move('images/companyLogos', $fileName)) {
                    $img = Image::make('images/companyLogos/'.$fileName);
                    $img->resize(config("constants.COMPANY_LOGO_W"), config("constants.COMPANY_LOGO_H"), function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save('images/companyLogos/thumbnails/'.$fileName);
                    $companiesLogoName = $fileName;
                }
            }
        } else {
            $companiesLogoName = 'defaultLogo.png';
        }

        DB::beginTransaction();
        //Add Company user
        $user = User::create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'gender' => $request['gender'],
            'password' => bcrypt($request['password']),
            'status' => 1,
        ]);

        //Create role user
        $role_user = DB::table('role_user')->insert(
            ['user_id' => $user->id, 'role_id' => config("constants.EMPLOYER")]
        );

        if( empty( $request['companiesID'] )) {
            //Add Company Info
            if (!empty($request['companiesNameKH'])) {
                $companyNameKH = $request['companiesNameKH'];
            } else {
                $companyNameKH = $request['companiesNameEN'];
            }

            if (!empty($request['companiesDescriptionKH'])) {
                $companyDesKH = $request['companiesDescriptionKH'];
            } else {
                $companyDesKH = $request['companiesDescriptionEN'];
            }

            $company = Company::create([
                'fkCountriesID' => $request['fkCountriesID'],
                'fkProvincesID' => $request['fkProvincesID'],
                'fkDistrictsID' => $request['fkDistrictsID'],
                'fkCommunesID' => $request['fkCommunesID'],
                'fkVillagesID' => $request['fkVillagesID'],
                'fkZonesID' => $request['fkZonesID'],
                'fkSectorsID' => $request['fkSectorsID'],
                'fkSubsectorsID' => $request['fkSubsectorsID'],
                'fkActivitiesID' => $request['fkActivitiesID'],

                'companiesNameEN' => $request['companiesNameEN'],
                'companiesNameKH' => $companyNameKH,
                'companiesNickName' => $request['companiesNickName'],
                'companiesNumberOfWorker' => $request['companiesNumberOfWorker'],
                'companiesPhone' => $request['companiesPhone'],
                'companiesEmail' => $request['companiesEmail'],
                'companiesSite' => $request['companiesSite'],
                'companiesAddress' => $request['companiesAddress'],
                'companiesDescriptionEN' => $request['companiesDescriptionEN'],
                'companiesDescriptionKH' => $companyDesKH,
                'companiesLogo' => $companiesLogoName,
            ]);

            $companyID = $company->pkCompaniesID;

        }else{
            $companyID = $request['companiesID'];
        }

        $countCompanyID = DB::table('CompanyUsers')->where('fkCompaniesID', '=', $companyID)->count();
        if( $countCompanyID > 0 ){
            User::where('id', $user->id)->update(array('companyLevel' => 3));
        }else{
            User::where('id', $user->id)->update(array('companyLevel' => 1));
        }

        //Create table tblCompanyUsers
        $companyUser = DB::table('CompanyUsers')->insert(
            ['fkCompaniesID' => $companyID, 'fkUsersID' => $user->id]
        );

        if( !$user || !$role_user || !$companyUser )
        {
            DB::rollBack();
        } else {
            DB::commit();
            if( $countCompanyID > 0 ){
                return response()->json( 'http://'.$_SERVER['SERVER_NAME'].'/'. \Lang::getLocale().'/account/employer/register/success', 200);
            }
            auth()->login($user);
            return response()->json( 'http://'.$_SERVER['SERVER_NAME'].'/'. \Lang::getLocale().'/account', 200);
        }
    }

//// Seeker Register
    public function jobseeker()
    {
        $lang =  strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

        return view('accounts.users.jobseekers.register', compact('sectors', 'provinces') );
    }

    public function jobseekerRegisterAjax(Request $request)
    {
        if (Session::token() !== $request->input('_token')) {
            return response()->json(array(
                'msg' => 'Unauthorized attempt to create setting'
            ));
        }

        $rules = array(
            'name' => 'required|max:255',
            'phone' => 'required|digits_between:9,10|unique:users',
            'fkSectorsIDTargetJob' => 'required',
            'fkProvincesID' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 203);
        }

        $remember = true;
        if( $request->get('remember') == null ){
            $remember = false;
        }

        DB::beginTransaction();
        //Add seeker
        $user = User::create([
            'gender'=> $request['gender'],
            'name' => $request['name'],
            'phone' => $request['phone'],
            'password' => bcrypt($request['phone']),
            'experience' => $request['experience'],
            'status' => 1,
        ]);

        //Create role user
        $role_user = DB::table('role_user')->insert(
            ['user_id' => $user->id, 'role_id' => config("constants.JOBSEEKER")]
        );

        //Add Target Jobs
        $targetJob = DB::table('TargetJobs')->insert(
            ['fkUsersID' => $user->id, 'fkCountriesID' => 855, 'fkProvincesID' => $request['fkProvincesID'], 'fkDistrictsID' => $request['fkDistrictsID'], 'fkCommunesID' => $request['fkCommunesID'], 'fkSectorsID' => $request['fkSectorsIDTargetJob'], 'fkSubsectorsID' => $request['fkSubsectorsIDTargetJob'], 'fkPositionsID' => $request['fkPositionsIDTargetJob'], 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
        );

        //Add Experience
        foreach( $request['fkSectorsID'] as $key => $fkSectorsID ) {
            if( $request['experience'] == 1 ){
                $experience = DB::table('JobExperiences')->insert(
                    ['fkUsersID' => $user->id, 'fkSectorsID' => $fkSectorsID, 'fkSubsectorsID' => $request['fkSubsectorsID'][$key], 'fkPositionsID' => $request['fkPositionsID'][$key], 'workExperience' => $request['workExperience'][$key], 'created_at' =>  date('Y-m-d H:i:s'), 'updated_at' =>  date('Y-m-d H:i:s')]
                );
            }
        }

        //Add user log
        $data['fkUsersID'] = $user->id;
        $data['userLogsActivity'] = 'register';
        $data['userLogsActivityDescription'] = 'register';
        $userLog = fnUserLog($data);

        if( !$user || !$role_user || !$userLog )
        {
            DB::rollBack();
            //return redirect( \Lang::getLocale(). '/account/jobseeker/register' );
        } else {
            DB::commit();
            auth()->login($user, $remember);
            return response()->json( 'http://'.$_SERVER['SERVER_NAME'].'/'. \Lang::getLocale().'/account', 200);
        }
    }

    //Validate seeker experience when register
    public function checkValidateExperienceAjax( Request $request ){
        if (Session::token() !== $request->input('_token')) {
            return response()->json(array(
                'msg' => 'Unauthorized attempt to create setting'
            ));
        }

        $rules = array();
        for($i = 0; $i < count($request['fkSectorsID']); $i++){
            Log::Info($request['fkSectorsID'][$i]);
            if($request['fkSectorsID'][$i] == ''){
                $rules['fkSectorsID' . $i] = 'required';
            }
            if($request['fkSubsectorsID'][$i] == ''){
                $rules['fkSubsectorsID' . $i] = 'required';
            }
            if($request['fkPositionsID'][$i] == ''){
                $rules['fkPositionsID' . $i] = 'required';
            }
            if($request['workExperience'][$i] == ''){
                $rules['workExperience' . $i] = 'required';
            }
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json($messages);
        }

        return 'success';
    }

    public function checkValidateUserInfoAjax( Request $request ){
        if (Session::token() !== $request->input('_token')) {
            return response()->json(array(
                'msg' => 'Unauthorized attempt to create setting'
            ));
        }

        $rules = array(
            'name' => 'required|max:255',
            'phone' => 'required|digits_between:9,10|unique:users'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json($messages);
        }

        return 'success';
    }

    public function registerSuccess(){
        return view('companyRepCompletedRegister');
    }
}
