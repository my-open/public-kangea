<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Commune;
use App\District;
use App\JobApply;
use App\Position;
use Illuminate\Http\Request;
use App\Sector;
use App\Subsector;
use App\Province;
use App\Postjob;

use Mail;
use Auth;
use Flash;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleHttpClient;
use Lang;
use DB;

use App\Http\Requests;

class JobController extends Controller
{
    public function postData(Request $request)
    {
        Log::info($request);
    }

    public function search(Request $request)
    {
        $lang = strtoupper(Lang::getLocale());

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName' . $lang)->pluck('sectorsName' . $lang, 'pkSectorsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName' . $lang)->pluck('provincesName' . $lang, 'pkProvincesID');

        $fkSectorsID = null;
        $fkSubsectorsID = null;
        $fkActivitiesID = null;
        $fkDistrictsID = null;
        $activities = [];
        $subsectors = [];
        $positions = [];
        $districts = [];
        $communes = [];

        $searchCriteria = [
            'fkSectorsID' => $request->get('fkSectorsID'),
            'fkSubsectorsID' => $request->get('fkSubsectorsID'),
            'fkActivitiesID' => $request->get('fkActivitiesID'),
            'fkPositionsID' => $request->get('fkPositionsID'),
            'fkProvincesID' => $request->get('fkProvincesID'),
            'fkDistrictsID' => $request->get('fkDistrictsID'),
            'fkCommunesID' => $request->get('fkCommunesID'),
        ];

        $positionName = null;
        if( $request->get('fkPositionsID') > 0 ){
            $positionName = DB:: table('Positions')->where('pkPositionsID', $request->get('fkPositionsID') )->value('positionsName'. $lang);
        }

        //Add search Log
        $data = array_filter($searchCriteria);
        if(count($data) > 0){
            fnSearchLog($data);
        }

        $strWhere = 'tblAnnouncements.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsPublishDate <= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsStatus = 1';
        foreach ($searchCriteria as $key => $value) {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = $value;
            $strWhere .= ' AND tblAnnouncements.' . $key . " = " . $value;
        }

        if ($fkSectorsID != null) {
            $subsectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('subsectorsName' . $lang)->pluck('subsectorsName' . $lang, 'pkSubsectorsID');
            $positions = Position::where('positionsStatus', '=', 1)->where('fkSectorsID', '=', $fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName' . $lang, 'pkPositionsID');
        }

        if ($fkSubsectorsID != null) {
            $activities = Activity::where('activitiesStatus', '=', 1)->where('fkSubsectorsID', '=', $fkSubsectorsID)->orderBy('activitiesOrder')->pluck('activitiesName' . $lang, 'pkActivitiesID');
        }

        if ($fkProvincesID != null) {
            $districts = District::where('districtsStatus', '=', 1)->where('fkProvincesID', '=', $fkProvincesID)->orderBy('districtsName' . $lang)->pluck('districtsName' . $lang, 'pkDistrictsID');
        }

        if ($fkDistrictsID != null) {
            $communes = Commune::where('communesStatus', '=', 1)->where('fkDistrictsID', '=', $fkDistrictsID)->orderBy('communesName' . $lang)->pluck('communesName' . $lang, 'pkCommunesID');
        }

        //Get jobs
        $jobs = DB::table('Announcements')
            ->Join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->Join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->join('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->whereRaw($strWhere)
            ->where('Companies.CompaniesStatus', '<>', 3)
            ->orderBy('Announcements.pkAnnouncementsID', 'DESC')
            ->select('Companies.pkCompaniesID', 'Companies.companiesNameEN', 'Companies.companiesNameKH', 'Companies.companiesNameZH', 'Companies.companiesNameTH', 'Companies.companiesNickName',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsTitleEN', 'Announcements.announcementsTitleKH', 'Announcements.announcementsClosingDate',
                'Positions.positionsNameEN', 'Positions.positionsNameKH', 'Positions.positionsNameZH', 'Positions.positionsNameTH', 'Provinces.provincesNameEN', 'Sectors.sectorsNameEN', 'Companies.companiesNameEN as companiesName_EN')
            ->paginate(config("constants.PAGINATION_NUM"));

        return view('job', compact('positionName', 'searchCriteria', 'sectors', 'districts', 'communes', 'jobs', 'subsectors', 'activities', 'positions', 'fkSectorsID', 'fkSubsectorsID', 'fkActivitiesID', 'fkPositionsID', 'fkProvincesID', 'provinces', 'fkDistrictsID', 'fkCommunesID', 'lang'));
    }

    public function jobsByPosition($sector_name = '', $position_name = '', $pkPositionsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $fkSectorsID = null;
        $fkSubsectorsID = null;
        $fkActivitiesID = null;
        $fkPositionsID = $pkPositionsID;
        $fkProvincesID = null;
        $fkDistrictsID = null;
        $fkCommunesID = null;
        $subsectors = [];
        $activities = [];
        $searchCriteria = [];
        $districts = [];
        $communes = [];

        $positionName = DB:: table('Positions')->where('pkPositionsID', $pkPositionsID )->value('positionsName'. $lang);

        $getSector = DB::table('Positions')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Positions.fkSectorsID')
            ->select('Sectors.pkSectorsID')
            ->where('Positions.pkPositionsID', '=', $pkPositionsID)
            ->first();
        $fkSectorsID = $getSector->pkSectorsID;

        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName' . $lang)->pluck('sectorsName' . $lang, 'pkSectorsID');
        $positions = Position::where('positionsStatus', '=', 1)->where('fkSectorsID', '=', $fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName' . $lang, 'pkPositionsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName' . $lang)->pluck('provincesName' . $lang, 'pkProvincesID');

        //add Search Log
        $data['fkPositionsID'] = $pkPositionsID;
        $data['fkSectorsID'] = $fkSectorsID;
        fnSearchLog($data);

        //Get jobs
        $jobs = DB::table('Announcements')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Announcements.fkZonesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.fkPositionsID', $pkPositionsID)
            ->where('Companies.CompaniesStatus', '<>', 3)
            ->where('Announcements.announcementsPublishDate', '<=', date('Y-m-d 00:00:00'))
            ->where('Announcements.announcementsClosingDate', '>=', date('Y-m-d 00:00:00'))
            ->where('Announcements.announcementsStatus', '=', 1)
            ->orderby('Announcements.pkAnnouncementsID', 'DESC')
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesNameEN', 'Companies.companiesNameKH', 'Companies.companiesNameZH', 'Companies.companiesNameTH', 'Companies.companiesNickName',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsClosingDate',
                'Provinces.provincesNameEN', 'Provinces.provincesNameKH', 'Provinces.provincesNameZH', 'Provinces.provincesNameTH',
                'Districts.districtsNameEN', 'Districts.districtsNameKH', 'Districts.districtsNameZH', 'Districts.districtsNameTH',
                'Communes.CommunesNameEN', 'Communes.CommunesNameKH', 'Communes.CommunesNameZH', 'Communes.CommunesNameTH',
                'Villages.villagesNameEN', 'Villages.villagesNameKH',
                'Zones.zonesNameEN', 'Zones.zonesNameKH',
                'Positions.positionsNameEN', 'Positions.positionsNameKH', 'Positions.positionsNameZH', 'Positions.positionsNameTH', 'Sectors.sectorsNameEN', 'Companies.companiesNameEN as companiesName_EN'
            )
            ->paginate(config("constants.PAGINATION_NUM"));

        return view('job', compact('positionName', 'searchCriteria', 'sectors', 'districts', 'communes', 'jobs', 'positions', 'subsectors', 'activities', 'provinces', 'fkSectorsID', 'fkSubsectorsID', 'fkActivitiesID', 'fkPositionsID', 'fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'lang'));
    }

    public function jobsByCompany($company_name='', $pkCompaniesID)
    {
        $lang = strtoupper(Lang::getLocale());
        //Get company info
        $company = DB::table('Companies')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Companies.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Companies.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Companies.fkActivitiesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Companies.fkZonesID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Companies.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Companies.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Companies.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Companies.fkVillagesID')
            ->where('Companies.pkCompaniesID', $pkCompaniesID)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesNameEN', 'Companies.companiesNameKH', 'Companies.companiesNameZH', 'Companies.companiesNameTH', 'Companies.companiesNickName', 'Companies.companiesDescriptionEN', 'Companies.companiesDescriptionKH', 'Companies.companiesDescriptionZH', 'Companies.companiesDescriptionTH',
                'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesXCoordinate', 'Companies.companiesYCoordinate', 'Companies.companiesPhone', 'Companies.companiesEmail',
                'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.sectorsNameEN','Sectors.sectorsNameKH','Sectors.sectorsNameZH','Sectors.sectorsNameTH',
                'Subsectors.subsectorsNameEN','Subsectors.subsectorsNameKH','Subsectors.subsectorsNameZH','Subsectors.subsectorsNameTH',
                'Activities.activitiesNameEN','Activities.activitiesNameKH','Activities.activitiesNameZH','Activities.activitiesNameTH',
                'Zones.zonesNameEN',
                'Provinces.provincesNameEN','Provinces.provincesNameKH','Provinces.provincesNameZH','Provinces.provincesNameTH',
                'Districts.districtsNameEN','Districts.districtsNameKH','Districts.districtsNameZH','Districts.districtsNameTH',
                'Communes.CommunesNameEN','Communes.CommunesNameKH','Communes.CommunesNameZH','Communes.CommunesNameTH',
                'Villages.villagesNameEN'
            )
            ->first();

        //Get jobs
        $jobs = DB::table('Announcements')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Announcements.fkZonesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.fkCompaniesID', $pkCompaniesID)
            ->where('Companies.CompaniesStatus', '<>', 3)
            ->where('Announcements.announcementsPublishDate', '<=', date('Y-m-d 00:00:00'))
            ->where('Announcements.announcementsClosingDate', '>=', date('Y-m-d 00:00:00'))
            ->where('Announcements.announcementsStatus', '=', 1)
            ->orderby('Announcements.pkAnnouncementsID', 'DESC')
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesNameEN', 'Companies.companiesNameKH', 'Companies.companiesNameZH', 'Companies.companiesNameTH', 'Companies.companiesNickName',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsClosingDate',
                'Provinces.provincesNameEN','Provinces.provincesNameKH','Provinces.provincesNameZH','Provinces.provincesNameTH',
                'Districts.districtsNameEN','Districts.districtsNameKH','Districts.districtsNameZH','Districts.districtsNameTH',
                'Communes.CommunesNameEN','Communes.CommunesNameKH','Communes.CommunesNameZH','Communes.CommunesNameTH',
                'Villages.villagesNameEN',
                'Zones.zonesNameEN',
                'Positions.positionsNameEN','Positions.positionsNameKH','Positions.positionsNameZH','Positions.positionsNameTH', 'Sectors.sectorsNameEN'
            )
            ->paginate(config("constants.PAGINATION_NUM"));

        return view('jobsByCompany', compact('lang', 'company', 'jobs'));
    }

    public function jobApplyChoose( Request $request, $pkAnnouncementsID ){
        $jobLogs['fkAnnouncementsID'] = $pkAnnouncementsID;
        $jobLogs['jobLogsType'] = 'apply';
        fnJobLog($jobLogs);

        $lang = strtoupper(Lang::getLocale());
        $user = \Auth::user();
        $job = DB::table('Announcements')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            ->where('Companies.CompaniesStatus', '<>', 3)
            ->where('Positions.positionsStatus', 1)
            ->where('Announcements.announcementsStatus', 1)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNameEN AS companiesName_EN',
                'Positions.positionsName'.$lang, 'Positions.positionsNameEN AS positionsName_EN',
                'Announcements.pkAnnouncementsID', 'Sectors.sectorsNameEN AS sectorsName_EN', 'Provinces.provincesNameEN AS provincesName_EN', 'Companies.companiesNameEN as companiesName_EN'
            )
            ->first();
        return view('jobApply', compact('user', 'job', 'lang') );
    }

    public function jobApplyForMyself( Request $request, $pkAnnouncementsID ){

        $lang = strtoupper(Lang::getLocale());
        $user = \Auth::user();
        $job = DB::table('Announcements')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Announcements.fkZonesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            ->where('Companies.CompaniesStatus', '<>', 3)
            ->where('Announcements.announcementsStatus', 1)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName', 'Companies.companiesDescription'.$lang, 'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesPhone', 'Companies.companiesEmail', 'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH',
                'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH',
                'Zones.zonesNameEN', 'Zones.zonesNameKH',
                'Positions.positionsNameEN', 'Positions.positionsNameKH','Positions.positionsNameZH', 'Positions.positionsNameTH',
                'Provinces.provincesNameEN', 'Provinces.provincesNameKH',
                'Districts.districtsNameEN', 'Districts.districtsNameKH',
                'Communes.CommunesNameEN', 'Communes.CommunesNameKH',
                'Villages.villagesNameEN', 'Villages.villagesNameKH',
                'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsTitleEN', 'Announcements.announcementsTitleKH', 'Announcements.announcementsClosingDate',
                'Announcements.announcementsHiring', 'Announcements.announcementsPublishDate', 'Announcements.announcementsStatus',
                'Companies.companiesNameEN as companiesName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($job->provincesNameEN);
        $sector_name = fnConvertSlug($job->sectorsNameEN);
        $position_name = fnConvertSlug($job->positionsNameEN);

        return view('jobApplyForMyself', compact('user', 'job', 'lang', 'province_name', 'sector_name', 'position_name', 'province_name', 'sector_name', 'position_name') );
    }
    public function jobApplyForSO( Request $request, $pkAnnouncementsID ){
        $lang = strtoupper(Lang::getLocale());
        $user = \Auth::user();
        $job = DB::table('Announcements')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Announcements.fkZonesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            ->where('Companies.CompaniesStatus', '<>', 3)
            ->where('Announcements.announcementsStatus', 1)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName', 'Companies.companiesDescription'.$lang, 'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesPhone', 'Companies.companiesEmail', 'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH',
                'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH',
                'Zones.zonesNameEN', 'Zones.zonesNameKH',
                'Positions.positionsNameEN', 'Positions.positionsNameKH','Positions.positionsNameZH', 'Positions.positionsNameTH',
                'Provinces.provincesNameEN', 'Provinces.provincesNameKH',
                'Districts.districtsNameEN', 'Districts.districtsNameKH',
                'Communes.CommunesNameEN', 'Communes.CommunesNameKH',
                'Villages.villagesNameEN', 'Villages.villagesNameKH',
                'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsTitleEN', 'Announcements.announcementsTitleKH', 'Announcements.announcementsClosingDate',
                'Announcements.announcementsHiring', 'Announcements.announcementsPublishDate', 'Announcements.announcementsStatus',
                'Companies.companiesNameEN as companiesName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($job->provincesNameEN);
        $sector_name = fnConvertSlug($job->sectorsNameEN);
        $position_name = fnConvertSlug($job->positionsNameEN);

        return view('jobApplyForSO', compact('user', 'job', 'lang', 'province_name', 'sector_name', 'position_name') );
    }

    //jobApplyPost
    public function jobApplyForMyselfPost(Request $request, $pkAnnouncementsID)
    {
        $input = $request->all();
        $input['fkAnnouncementsID'] = $pkAnnouncementsID;
        $input['jobApplyBy'] = 1; //1=web

        $fileName = '';
        if (Auth::check()) {
            $user = \Auth::user();
            $input['fkUsersID'] = $user->id;
            $input['fkCompaniesID'] = $request->get('fkCompaniesID');
            $input['fkPositionsID'] = $request->get('fkPositionsID');
            $input['jobApplyGender'] = $user->gender;
            $input['jobApplyName'] = $user->name;
            $input['jobApplyPhone'] = $user->phone;
            $input['jobApplyEmail'] = $user->email;
        } else {
            $this->validate($request, [
                'jobApplyGender' => 'required',
                'jobApplyName' => 'required|max:100',
                'jobApplyPhone' => 'required|digits_between:9,10',
                'jobApplyCvFile' => 'mimes:pdf,zip,doc,docx|max:1000'
            ]);

            $input['fkUsersID'] = 0;
            $input['fkCompaniesID'] = $request->get('fkCompaniesID');
            $input['fkPositionsID'] = $request->get('fkPositionsID');
            $input['jobApplyGender'] = $request->input('jobApplyGender');
            $input['jobApplyName'] = $request->input('jobApplyName');
            $input['jobApplyPhone'] = $request->input('jobApplyPhone');

            if(isset($input['jobApplyExperience']) && $input['jobApplyExperience'] == 1 ){
                $input['jobApplyExperience'] = 1;
            }else{
                $input['jobApplyExperience'] = 2;
            }

            $input['jobApplyCvFile'] = '';
            if ($request->hasFile('jobApplyCvFile')) {
                if ($request->file('jobApplyCvFile')->isValid()) {
                    $file = $request->file('jobApplyCvFile');
                    $extension = $file->getClientOriginalExtension();
                    $fileName = time() . '.' . $extension;
                    if ($file->move('files/cv', $fileName)) {
                        $input['jobApplyCvFile'] = $fileName;
                    }
                }
            }
        }

        $jobApply = JobApply::where('jobApplyPhone', $input['jobApplyPhone'])
                    ->where('fkAnnouncementsID', $input['fkAnnouncementsID'])
                    ->where('jobApplyIsConfirm', 1)
                    ->first();

        $job = Postjob::findOrFail($input['fkAnnouncementsID']);
        $positionName = DB:: table('Positions')->where('pkPositionsID', $job->fkPositionsID)->value('positionsNameEN');
        $provincesName_EN = DB::table('Provinces')->where('pkProvincesID', $job->fkProvincesID)->value('provincesNameEN');
        $sectorsName_EN = DB::table('Sectors')->where('pkSectorsID', $job->fkSectorsID)->value('sectorsNameEN');
        $province_name = fnConvertSlug($provincesName_EN);
        $sector_name = fnConvertSlug($sectorsName_EN);
        $position_name = fnConvertSlug($positionName);

        if( empty($jobApply) ){
            $jobLogs['fkAnnouncementsID'] = $pkAnnouncementsID;
            $jobLogs['jobLogsType'] = 'applyOwnSubmit';
            fnJobLog($jobLogs);

            $jobApply = JobApply::create($input);
            $text_message = trans('text_lang.AppliedSuccessful');

            //Call confirm
            $isOneConfirm = JobApply::where('jobApplyPhone', $input['jobApplyPhone'])
                ->where('jobApplyIsConfirm', 1)
                ->first();
            if( empty($isOneConfirm) ){
                $text_message = trans('text_lang.verifiedYourPhoneNumberThroughACall');
                $jobMp3 = public_path('files/sounds/kh/confirmJobs/confirmJob_' . $pkAnnouncementsID . '.mp3');
                $jobNoCompanyMp3 = public_path('files/sounds/kh/confirmJobsNoCompany/confirmJob_' . $pkAnnouncementsID . '.mp3');
                $isJobMp3 = isExistMp3($jobMp3);
                $isJobNoCompanyMp3 = isExistMp3($jobNoCompanyMp3);
                if( $isJobMp3 || $isJobNoCompanyMp3){
                    $api_token = config("constants.API_TOKEN");

                    if( $isJobMp3 ){
                        $soundUrl = "http://". env('HTTP_HOST') ."/files/sounds/kh/confirmJobs/confirmJob_".$pkAnnouncementsID.".mp3";
                    }else{
                        $soundUrl = "http://". env('HTTP_HOST') ."/files/sounds/kh/confirmJobsNoCompany/confirmJob_".$pkAnnouncementsID.".mp3";
                    }

                    $data = [
                        "sharesGender" => $input['jobApplyGender'],
                        "sharesName" => $input['jobApplyName'],
                        "sharesPhone" => $input['jobApplyPhone'],
                        "sharesWho" => 6,
                        "fkAnnouncementsID" => $pkAnnouncementsID,
                        "soundUrl" => $soundUrl,
                        "fkUsersID" => 0,
                        "fkCompaniesID" => $input['fkCompaniesID'],
                        "fkPositionsID" => $input['fkPositionsID'],
                        "api_token" => $api_token,
                        "self-apply" => "1",
                        "pkJobApplyID"=>$jobApply->pkJobApplyID
                    ];

                    $data = json_encode($data, JSON_FORCE_OBJECT);
                    $headers = ['Content-Type' => 'application/json'];
                    // Using laravel php library GuzzleHttp for execute external API
                    $client = new GuzzleHttpClient();
                    $client->request('POST', config("constants.REQUEST_IVR_API_SHARE"), ['headers'=>$headers,'body' => $data]);
                }
            }else{
                JobApply::where('pkJobApplyID', '=', $jobApply->pkJobApplyID)
                    ->update(['jobApplyIsConfirm' => '1']);

                //if Confirm = true send email
                $positionName = DB::table('Positions')->where('pkPositionsID', $input['fkPositionsID'])->value('positionsNameEN');
                $phone = $input['jobApplyPhone'];
                $gender = $input['jobApplyGender'];
                $name = $input['jobApplyName'];
                $fileName = $input['jobApplyCvFile'];

                $emails = DB::table('users')->Join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')->where('CompanyUsers.fkCompaniesID', '=', $input['fkCompaniesID'] )->pluck('email');
                $emails = array_filter($emails);

                $data = array('title' => $gender, 'name' => $name, 'id' => $pkAnnouncementsID, 'positionName' => $positionName, 'phone' => $phone, 'fileName' => $fileName, 'province_name' => $province_name, 'sector_name' => $sector_name, 'position_name' => $position_name);

                if (count($emails) > 0) {
                    if($fileName != ''){
                        Mail::send('emails.newApplicant', $data, function ($message) use ($emails, $fileName, $positionName) {
                            $message->to($emails)->subject('Apply for ' . $positionName);
                            $message->attach(public_path('files/cv/') . $fileName);
                        });
                    }else{
                        Mail::send('emails.newApplicant', $data, function ($message) use ($emails, $positionName) {
                            $message->to($emails)->subject('Apply for ' . $positionName);
                        });
                    }
                }

            }
        }else{
            $jobLogs['fkAnnouncementsID'] = $pkAnnouncementsID;
            $jobLogs['jobLogsType'] = 'applyOwnSubmit';
            fnJobLog($jobLogs);

            JobApply::where('jobApplyPhone', $input['jobApplyPhone'])
                ->where('fkAnnouncementsID', $input['fkAnnouncementsID'])
                ->update(['jobApplyCvFile' => $input['jobApplyCvFile'], 'jobApplyGender' => $input['jobApplyGender'], 'jobApplyName' => $input['jobApplyName']]);
            $text_message = trans('text_lang.AppliedUpdateSuccessful');
        }

        $positionName = $request->get('positionsNameEN');
        $phone = $input['jobApplyPhone'];
        $gender = $input['jobApplyGender'];
        $name = $input['jobApplyName'];

        $emails = DB::table('users')->Join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')->where('CompanyUsers.fkCompaniesID', '=', $request->get('fkCompaniesID') )->pluck('email');
        $emails = array_filter($emails);

        $data = array('title' => $gender, 'name' => $name, 'id' => $pkAnnouncementsID, 'positionName' => $positionName, 'phone' => $phone, 'fileName' => $fileName, 'province_name' => $province_name, 'sector_name' => $sector_name, 'position_name' => $position_name);

        if (count($emails) > 0) {
            if($fileName != ''){
                Mail::send('emails.newApplicant', $data, function ($message) use ($emails, $fileName, $positionName) {
                    $message->to($emails)->subject('Apply for ' . $positionName);
                    $message->attach(public_path('files/cv/') . $fileName);
                });
            }else{
                Mail::send('emails.newApplicant', $data, function ($message) use ($emails, $positionName) {
                    $message->to($emails)->subject('Apply for ' . $positionName);
                });
            }
        }

        Flash::message($text_message);
        return redirect(\Lang::getLocale() . '/jobApply/' . $pkAnnouncementsID);
    }
    public function jobApplyForSOPost(Request $request, $pkAnnouncementsID)
    {
        $this->validate($request, [
            'jobApplyGender' => 'required',
            'sharesWho' => 'required',
            'jobApplyName' => 'required|max:100',
            'jobApplyPhone' => 'required|digits_between:9,10',
        ]);

        $jobMp3 = 'files/sounds/kh/jobs/job_' . $pkAnnouncementsID . '.mp3';
        $isJobMp3 = isExistMp3($jobMp3);
        if( $isJobMp3 ){
            //Log share to phone
            $jobLogs['fkAnnouncementsID'] = $pkAnnouncementsID;
            $jobLogs['jobLogsType'] = 'sharePhoneSubmit';
            fnJobLog($jobLogs);

            $api_token = config("constants.API_TOKEN");
            $soundUrl = "http://". env('HTTP_HOST') ."/files/sounds/kh/jobs/job_".$pkAnnouncementsID.".mp3";

            $data = [
                "sharesGender" => $request->get('jobApplyGender'),
                "sharesName" => $request->get('jobApplyName'),
                "sharesPhone" => $request->get('jobApplyPhone'),
                "sharesWho" => $request->get('sharesWho'),
                "fkAnnouncementsID" => $pkAnnouncementsID,
                "soundUrl" => $soundUrl,
                "fkUsersID" => 0,
                "fkCompaniesID" => $request->get('fkCompaniesID'),
                "fkPositionsID" => $request->get('fkPositionsID'),
                "api_token" => $api_token
            ];

            $data = json_encode($data, JSON_FORCE_OBJECT);
            $headers = ['Content-Type' => 'application/json'];
            // Using laravel php libray GuzzleHttp for execute external API
            $client = new GuzzleHttpClient();
            $client->request('POST', config("constants.REQUEST_IVR_API_SHARE"), ['headers'=>$headers,'body' => $data]);
        }

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect(\Lang::getLocale() . '/jobApply/' . $pkAnnouncementsID);
    }

    public function jobAnnouncementText($province_name ='', $sector_name = '', $position_name = '', $pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsName'.$lang)->pluck('sectorsName'.$lang, 'pkSectorsID');
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');

        //Get job Detail
        $jobDetail = DB::table('Announcements')
            ->Join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Announcements.fkZonesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName', 'Companies.companiesDescription'.$lang,
                'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesXCoordinate', 'Companies.companiesYCoordinate', 'Companies.companiesDescription'.$lang, 'Companies.companiesPhone', 'Companies.companiesEmail',
                'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.pkSectorsID','Sectors.sectorsName'.$lang,
                'Subsectors.pkSubsectorsID','Subsectors.subsectorsName'.$lang,
                'Activities.activitiesName'.$lang,
                'Zones.zonesNameEN',
                'Positions.positionsName'.$lang,
                'Provinces.provincesName'.$lang,
                'Districts.districtsName'.$lang,
                'Communes.CommunesName'.$lang,
                'Villages.villagesNameEN',
                'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID',

                'Announcements.pkAnnouncementsID', 'Announcements.fkCompaniesID', 'Announcements.fkPositionsID', 'Announcements.announcementsClosingDate', 'announcementsSalaryType',
                'Announcements.announcementsHiring', 'Announcements.announcementsPublishDate',
                'Announcements.AnnouncementsExperiencePositionN', 'Announcements.AnnouncementsPositionType', 'Announcements.AnnouncementsExperienceSectorN', 'Announcements.AnnouncementsSectorType',
                'Announcements.AnnouncementsGender', 'Announcements.AnnouncementsAgeFrom', 'Announcements.AnnouncementsAgeUntil',
                'Announcements.announcementsCertificate', 'Announcements.announcementsLanguageLevel', 'Announcements.announcementsSalaryFrom', 'Announcements.announcementsSalaryTo',
                'Announcements.AnnouncementsFromHour', 'Announcements.AnnouncementsFromMinute', 'Announcements.AnnouncementsFromType', 'Announcements.AnnouncementsToHour', 'Announcements.AnnouncementsToMinute', 'Announcements.AnnouncementsToType', 'Announcements.AnnouncementsFromDay', 'Announcements.AnnouncementsToDay',

                'Announcements.announcementsBenefit', 'Announcements.announcementsLanguage',
                'Announcements.AnnouncementsSalaryDependsOn', 'Announcements.AnnouncementsExperienceOrAnd',
                'Announcements.AnnouncementsBreakHoursFromHour', 'Announcements.AnnouncementsBreakHoursFromMinute', 'Announcements.AnnouncementsBreakHoursFromType',
                'Announcements.AnnouncementsBreakHoursToHour', 'Announcements.AnnouncementsBreakHoursToMinute', 'Announcements.AnnouncementsBreakHoursToType',
                'Announcements.announcementsStatus', 'Announcements.fkDistrictsID', 'Announcements.fkCommunesID', 'Announcements.fkProvincesID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkActivitiesID', 'Announcements.fkPositionsID',
                'announcementsAllowWithoutExperience', 'announcementsRequiredDocType', 'announcementsSalaryNegotiate', 'announcementsIsFullTime', 'announcementsWorkTime'
            )
            ->first();

        $fkSectorsID = $jobDetail->fkSectorsID;
        $fkSubsectorsID = $jobDetail->fkSubsectorsID;
        $fkActivitiesID = $jobDetail->fkActivitiesID;
        $fkPositionsID = $jobDetail->fkPositionsID;
        $fkProvincesID = $jobDetail->fkProvincesID;
        $fkDistrictsID = $jobDetail->fkDistrictsID;
        $fkCommunesID = $jobDetail->fkCommunesID;

        $districts = [];
        $communes = [];

        $subsectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $fkSectorsID)->orderBy('subsectorsName'.$lang)->pluck('subsectorsName'.$lang, 'pkSubsectorsID');
        $positions = Position::where('positionsStatus', '=', 1)->where('fkSectorsID', '=', $fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName'.$lang, 'pkPositionsID');

        $activities = Activity::where('activitiesStatus', '=', 1)->where('fkSubsectorsID', '=', $fkSubsectorsID)->orderBy('activitiesOrder')->pluck('activitiesName'.$lang, 'pkActivitiesID');

        if( $fkProvincesID != null ){
            $districts = District::where('districtsStatus', '=', 1)->where('fkProvincesID', '=', $fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        }

        if( $fkDistrictsID != null ){
            $communes = Commune::where('communesStatus', '=', 1)->where('fkDistrictsID', '=', $fkDistrictsID)->orderBy('communesName'.$lang)->pluck('communesName'.$lang, 'pkCommunesID');
        }

        $benefits = [];
        if ($jobDetail) {
            $benefits = json_decode($jobDetail->announcementsBenefit);
        }

        $workTime = [];
        if( !empty($jobDetail->announcementsWorkTime) ){
            $workTime = json_decode($jobDetail->announcementsWorkTime);
        }

        //Search Log
        $data['fkProvincesID'] = $fkProvincesID;
        $data['fkDistrictsID'] = $fkDistrictsID;
        $data['fkCommunesID'] = $fkCommunesID;
        $data['fkPositionsID'] = $fkPositionsID;
        $data['fkSectorsID'] = $fkSectorsID;
        $data['fkSubsectorsID'] = $fkSubsectorsID;
        $data['fkActivitiesID'] = $fkActivitiesID;
        $data['fkAnnouncementsID'] = $pkAnnouncementsID;
        fnSearchLog($data);

        $jobLogs['fkAnnouncementsID'] = $pkAnnouncementsID;
        $jobLogs['jobLogsType'] = 'view';
        fnJobLog($jobLogs);

        // check permission who can edit this announcement or this post job?
        $employerId = Auth::id();
        //Get CompanyID by employer
        $userCompanyID = DB::table('CompanyUsers')->where('fkUsersID', $employerId)->value('fkCompaniesID');

        return view('jobAnnouncementText_'.Lang::getLocale(), compact('lang', 'jobDetail', 'sectors', 'subsectors', 'activities', 'positions', 'provinces', 'districts', 'communes', 'benefits', 'userCompanyID', 'fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'fkSectorsID', 'fkSubsectorsID', 'fkActivitiesID', 'fkPositionsID', 'workTime'));
    }

    public function downloadCV($cvFile)
    {
        return response()->download(public_path('files/cv/') . $cvFile);
    }

    public function searchJobLocationAjax(Request $request) {
        $lang = $request->get('lang');
        $location = ["Sectors.sectorsName$lang", "Sectors.sectorsNameEN AS sectorsName_EN", "Positions.positionsName$lang", "Positions.positionsNameEN AS positionsName_EN", "Announcements.fkPositionsID", "Announcements.fkSectorsID"];

        $searchCriteria = [
            'fkProvincesID' => $request->get('fkProvincesID'),
            'fkDistrictsID' => $request->get('fkDistrictsID'),
            'fkCommunesID' => $request->get('fkCommunesID'),
        ];

        $strWhere = 'tblAnnouncements.announcementsClosingDate >= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsPublishDate <= ' . date('"Y-m-d 00:00:00"') . ' AND tblAnnouncements.announcementsStatus = 1';
        foreach ($searchCriteria as $key => $value) {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = $value;
            $strWhere .= ' AND tblAnnouncements.' . $key . " = " . $value;
        }

        $jobs = DB::table('Announcements')
            ->Join('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->whereRaw($strWhere)
            ->select( $location )
            ->groupby('Positions.pkPositionsID')
            ->orderby('Positions.pkPositionsID')
            ->get();

          $jobsArray = json_decode(json_encode($jobs), true);
          return response($jobsArray, 200);
    }


    public function allComp($value='') {
      $lang = strtoupper(Lang::getLocale());

        //Get companies that post job Announcements
        $allComp = DB::table('Announcements')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Sectors', 'pkSectorsID', '=', 'Companies.fkSectorsID')
            ->leftjoin('Subsectors', 'pkSubsectorsID', '=', 'Companies.fkSubsectorsID')
            ->leftjoin('Activities', 'pkActivitiesID', '=', 'Companies.fkActivitiesID')
            ->where('Companies.CompaniesStatus', '<>', 3)
            ->where('Announcements.announcementsPublishDate', '<=', date('Y-m-d 00:00:00'))
            ->where('Announcements.announcementsClosingDate', '>=', date('Y-m-d 00:00:00'))
            ->where('Announcements.announcementsStatus', '=', 1)
            ->where('Provinces.provincesStatus', '=', 1)
            ->where('Districts.districtsStatus', '=', 1)
            ->where('Communes.communesStatus', '=', 1)
            ->where('Sectors.sectorsStatus', '=', 1)
            ->where('Subsectors.subsectorsStatus', '=', 1)
            ->where('Activities.activitiesStatus', '=', 1)
            ->orderby('Announcements.pkAnnouncementsID', 'DESC')
            ->groupby('Companies.pkCompaniesID')
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesNameEN', 'Companies.companiesNameEN AS companiesName_EN', 'Companies.companiesNameKH', 'Companies.companiesNameZH', 'Companies.companiesNameTH', 'Companies.companiesNickName',
                'Announcements.pkAnnouncementsID', 'Announcements.announcementsClosingDate',
                'Provinces.provincesName'.$lang,
                'Districts.districtsName'.$lang,
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Activities.activitiesName'.$lang
            )
            ->paginate(config("constants.PAGINATION_NUM"));
      return view('companies', compact('allComp', 'lang'));
    }

    public function jobDetail(){
        return view('fronts.jobs.detail');
    }
}
