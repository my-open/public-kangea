<?php

namespace App\Http\Controllers;

use App\Activity;
use App\JobApply;
use App\Position;
use App\Postjob;
use Illuminate\Http\Request;
use App\Sector;
use App\Subsector;
use App\Zone;
use App\Company;
use App\Province;
use App\District;
use App\Commune;
use App\Village;

use Mail;
use Auth;
use Flash;
use Illuminate\Support\Facades\Log;
use Lang;
use DB;

use App\Http\Requests;

class PostjobController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:employer');
    }

    public function index()
    {
        $lang =  strtoupper(Lang::getLocale());

        //check user has company or not
        $checkExistsUserID = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', Auth::id() )
            ->where('Companies.companiesStatus', 1)
            ->count();
        if( $checkExistsUserID <= 0 ) {
            return redirect()->to(\Lang::getLocale() . '/account/employer/registerCompany');
        }

        //Get CompanyID by employer
        $Company = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', Auth::id() )
            ->where('Companies.companiesStatus', 1)
            ->first();

        $postjobs = DB::table('Announcements')
            ->join('Subsectors', 'Announcements.fkSubsectorsID', '=', 'Subsectors.pkSubsectorsID')
            ->join('Sectors', 'Announcements.fkSectorsID', '=', 'Sectors.pkSectorsID')
            ->join('Positions', 'Announcements.fkPositionsID', '=', 'Positions.pkPositionsID')
            ->where('Announcements.fkCompaniesID', $Company->pkCompaniesID)
            ->where('Announcements.announcementsStatus', '<>', 3)
            ->orderBy('Announcements.pkAnnouncementsID', 'DESC')
            ->select(
                'Announcements.*',
                'Subsectors.pkSubsectorsID', 'Subsectors.subsectorsName'.$lang,
                'Sectors.pkSectorsID', 'Sectors.sectorsName'.$lang,
                'Positions.pkPositionsID','Positions.positionsName'.$lang
            )
            ->paginate( config("constants.PAGINATION_NUM") );
        return view('accounts.postjobs.index', compact('postjobs', 'lang'));

    }

    public function create()
    {
        $lang =  strtoupper(Lang::getLocale());
        $employerId =  Auth::id();
        $company = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->where('Companies.companiesStatus', 1)
            ->first();

        $sectorName = DB::table('Sectors')->where('pkSectorsID', $company->fkSectorsID)->value('sectorsName'.$lang);
        $subsectorName = DB::table('Subsectors')->where('pkSubsectorsID', $company->fkSubsectorsID)->value('subsectorsName'.$lang);
        $activityName = DB::table('Activities')->where('pkActivitiesID', $company->fkActivitiesID)->value('activitiesName'.$lang);
        $provincesName = DB::table('Provinces')->where('pkProvincesID', $company->fkProvincesID)->value('provincesName'.$lang);
        $districtName = DB::table('Districts')->where('pkDistrictsID', $company->fkDistrictsID)->value('districtsName'.$lang);
        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $company->fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $company->fkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
        $positions = Position::where('positionsStatus', true)->where('fkSectorsID', $company->fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName'.$lang, 'pkPositionsID');
        $activities = Activity::where('activitiesStatus', true)->where('fkSubsectorsID', $company->fkSubsectorsID)->orderBy('activitiesOrder')->pluck('activitiesName'.$lang, 'pkActivitiesID');

        $days = array(''=>trans('text_lang.selectOption'), 'mon'=>trans('text_lang.monday'), 'tue'=>trans('text_lang.tuesday'), 'wed'=>trans('text_lang.wednesday'), 'thu'=>trans('text_lang.thursday'), 'fri'=>trans('text_lang.friday'), 'sat'=>trans('text_lang.saturday'), 'sun'=>trans('text_lang.sunday'));

        return view('accounts.postjobs.add', compact('lang','company', 'positions', 'activities', 'sectorName', 'subsectorName', 'activityName', 'provinces', 'provincesName', 'districtName', 'districtByProvinces', 'communeByDistricts', 'days')  );
    }

    public function store(Request $request)
    {
        $isActivityIdOther = false;
        $isPositionIdOther = false;
        $employerId =  Auth::id();
        $company = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->where('Companies.companiesStatus', 1)
            ->first();

        $required = [
            'fkPositionsID'=>'required',
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'announcementsClosingDate'=> 'required|date',
            'AnnouncementsAgeFrom'=>'required',
            'announcementsSalaryType' => 'required',

            'AnnouncementsFromDay' => 'required',
            'AnnouncementsToDay' => 'required',
            'announcementsRequiredDocType' => 'required',

            'announcementsIsFullTime' => 'required',
            'announcementsWorkTime' => 'required',

            'announcementsIsFullTime' => 'required',
            'announcementsWorkTime' => 'required',
        ];

        if( empty($request['announcementsSalaryNegotiate']) ){
            $required['announcementsSalaryFrom'] = 'required';
//            $required['announcementsSalaryTo'] = 'required';
        }

        if( $request->get('fkPositionsID') == '0' ){
            $required['positionOther'] = 'required';
            $isPositionIdOther = true;
        }

        if( $request->get('fkActivitiesID') == 'other' ){
            $required['activityOther'] = 'required';
            $isActivityIdOther = true;
        }

        // check Experince....
        if( !empty( $request['AnnouncementsExperiencePositionN'] ) ){
            $required['AnnouncementsPositionType'] = 'required';
        }
        if( !empty( $request['AnnouncementsExperienceSectorN'] ) ){
            $required['AnnouncementsSectorType'] = 'required';
        }

        $this->validate($request, $required);

        $langLevel = null;

        if($isPositionIdOther){
            $position = Position::create([
                'fkSectorsID' => $company->fkSectorsID,
                'positionsNameEN' => $request->get('positionOther'),
                'positionsNameKH' => $request->get('positionOther'),
                'positionsNameZH' => $request->get('positionOther'),
                'positionsNameTH' => $request->get('positionOther'),
                'positionsStatus' => 1,
            ]);

            $pkPositionsID = $position->pkPositionsID;
            if (strlen( $pkPositionsID ) == 2 ){
                $positionsCode = 'P00'.$pkPositionsID;
            }elseif( strlen( $pkPositionsID ) > 2 ){
                $positionsCode = 'P0'.$pkPositionsID;
            }else{
                $positionsCode = 'P000'.$pkPositionsID;
            }
            $request['fkPositionsID'] = $pkPositionsID;
            Position::where('pkPositionsID', $pkPositionsID)->update(array('positionsCode' => $positionsCode));

            $emails = config("constants.ADMIN_EMAIL");
            $positionName = $request->get('positionOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Hi, Admin of Bongpheak Job. Now you have new position has been created with '.$domainName.'. The new position name is '.$positionName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New Position created in '.$domainName);
            });
        }

        if($isActivityIdOther){
            $activity = Activity::create([
                'fkSubsectorsID' => $company->fkSubsectorsID,
                'activitiesNameEN' => $request->get('activityOther'),
                'activitiesNameKH' => $request->get('activityOther'),
                'activitiesNameZH' => $request->get('activityOther'),
                'activitiesNameTH' => $request->get('activityOther'),
                'activitiesStatus' => 1,
            ]);

            $pkActivitiesID = $activity->pkActivitiesID;
            $request['fkActivitiesID'] = $pkActivitiesID;

            $emails = config("constants.ADMIN_EMAIL");
            $activityName = $request->get('activityOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Hi, Admin of Bongpheak Job. Now you have new activity has been created with '.$domainName.'. The new activity name is '.$activityName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New Activity created in '.$domainName);
            });
        }

        $input = $request->all();
        if( $request->get('AnnouncementsAgeUntil') == ''){ $input['AnnouncementsAgeUntil'] = $input['AnnouncementsAgeFrom']; }
        $input['announcementsPublishDate'] = date('Y-m-d 00:00:00');

        // check Experience depend on Salary
        if( empty($request['announcementsSalaryNegotiate']) ){
            if( (int)$request['announcementsSalaryTo'] <= (int)$request['announcementsSalaryFrom']  ){
                $input['AnnouncementsSalaryDependsOn'] = '' ;
            }
        }else{
            $input['announcementsSalaryTo']='';
            $input['announcementsSalaryFrom']='';
            $input['AnnouncementsSalaryDependsOn'] = '' ;
        }

        $input['announcementsCertificate'] = json_encode( $request->get('certificate') );
        $input['announcementsBenefit'] = json_encode( $request->get('benefit') );
        $input['announcementsLanguage'] = json_encode( $request->get('language') );
        $input['announcementsWorkTime'] = json_encode( $request->get('announcementsWorkTime') );
        if( !empty($request->get('language')) ){
            foreach($request->get('language') as $key => $value){
                $langLevel[$key] = $request->get('announcementsLanguageLevel')[$key];
            }
        }
        $input['announcementsLanguageLevel'] = json_encode( $langLevel );

        if( $input['AnnouncementsExperiencePositionN'] == '12' && $input['AnnouncementsPositionType'] == 'month' ){
            $input['AnnouncementsExperiencePositionN'] = 1;
            $input['AnnouncementsPositionType'] = 'year';
        }

        if( $input['AnnouncementsExperienceSectorN'] == '12' && $input['AnnouncementsSectorType'] == 'month' ){
            $input['AnnouncementsExperienceSectorN'] = 1;
            $input['AnnouncementsSectorType'] = 'year';
        }

        //get fkPositionsCode from tblPositon
        $GetfkPositionsID = $request['fkPositionsID'];
        $GetfkPositionsCode = Position::where('positionsStatus', true)->where('pkPositionsID', $GetfkPositionsID)->value('positionsCode');
        $input['fkPositionsCode']= $GetfkPositionsCode ;

        $input['fkSectorsID'] = $company->fkSectorsID;
        $input['fkSubsectorsID'] = $company->fkSubsectorsID;
        $input['fkCompaniesID'] = $company->pkCompaniesID;
        $input['fkCountriesID'] = 855;

        $redirectUrl = '/account/postjob';
        if (isset($_REQUEST['previewJob']) ){
            $job = Postjob::create($input);
            $provinceName = DB::table('Provinces')->where('pkProvincesID', $job->fkProvincesID)->value('provincesNameEN');
            $sectorName = DB::table('Sectors')->where('pkSectorsID', $job->fkSectorsID)->value('sectorsNameEN');
            $positionName = DB::table('Positions')->where('pkPositionsID', $job->fkPositionsID)->value('positionsNameEN');
            $province_name = fnConvertSlug($provinceName);
            $sector_name = fnConvertSlug($sectorName);
            $position_name = fnConvertSlug($positionName);
            $redirectUrl = '/account/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $job->pkAnnouncementsID;
        }elseif( isset($_REQUEST['publishJob']) ){
            $input['announcementsStatus']  = 1 ;
            $job = Postjob::create($input);
        }else{
            $job = Postjob::create($input);
        }

        $mJob = new Postjob();
        $mJob->convertConfirmJobApplyMp3( $job );
        $mJob->convertConfirmJobApplyNoCompanyMp3($job);
        $mJob->convertJobMp3( $job );

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().$redirectUrl);
    }

    public function show(Request $request, $id)
    {
    }

    public function edit($id)
    {
        $lang =  strtoupper(Lang::getLocale());
        $postjob = Postjob::findOrFail($id);

        $employerId =  Auth::id();
        $company = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->where('Companies.companiesStatus', 1)
            ->first();

        $sectorName = DB::table('Sectors')->where('pkSectorsID', $company->fkSectorsID)->value('sectorsName'.$lang);
        $subsectorName = DB::table('Subsectors')->where('pkSubsectorsID', $company->fkSubsectorsID)->value('subsectorsName'.$lang);
        $activityName = DB::table('Activities')->where('pkActivitiesID', $company->fkActivitiesID)->value('activitiesName'.$lang);
        $provincesName = DB::table('Provinces')->where('pkProvincesID', $company->fkProvincesID)->value('provincesName'.$lang);
        $districtName = DB::table('Districts')->where('pkDistrictsID', $company->fkDistrictsID)->value('districtsName'.$lang);

        $provinces = Province::where('provincesStatus', true)->orderBy('provincesName'.$lang)->pluck('provincesName'.$lang, 'pkProvincesID');
        $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $postjob->fkProvincesID)->orderBy('districtsName'.$lang)->pluck('districtsName'.$lang, 'pkDistrictsID');
        $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $postjob->fkDistrictsID)->orderBy('CommunesName'.$lang)->pluck('CommunesName'.$lang, 'pkCommunesID');
        $positions = Position::where('positionsStatus', true)->where('fkSectorsID', $company->fkSectorsID)->orderBy('positionsOrder')->pluck('positionsName'.$lang, 'pkPositionsID');
        $activities = Activity::where('activitiesStatus', true)->where('fkSubsectorsID', $company->fkSubsectorsID)->orderBy('activitiesOrder')->pluck('activitiesName'.$lang, 'pkActivitiesID');

        if( !empty($postjob) ){
            $languages = json_decode( $postjob->announcementsLanguage );
            $benefits = json_decode( $postjob->announcementsBenefit );
            $certificates = json_decode( $postjob->announcementsCertificate );
            $announcementsLanguageLevels = json_decode( $postjob->announcementsLanguageLevel );
            $announcementsWorkTime = json_decode( $postjob->announcementsWorkTime );
        }

        $days = array(''=>trans('text_lang.selectOption'), 'mon'=>trans('text_lang.monday'), 'tue'=>trans('text_lang.tuesday'), 'wed'=>trans('text_lang.wednesday'), 'thu'=>trans('text_lang.thursday'), 'fri'=>trans('text_lang.friday'), 'sat'=>trans('text_lang.saturday'), 'sun'=>trans('text_lang.sunday'));

        return view('accounts.postjobs.edit', compact('lang', 'postjob', 'languages', 'announcementsLanguageLevels', 'benefits', 'certificates', 'positions', 'activities', 'provinces', 'districtName', 'districtByProvinces', 'communeByDistricts', 'days', 'company', 'sectorName', 'subsectorName', 'activityName', 'provincesName', 'announcementsWorkTime', 'salaryNegotiate')  );
    }

    public function update(Request $request, $id)
    {
        $isPositionIdOther = false;
        $postjob = Postjob::findOrFail($id);

        $employerId =  Auth::id();
        $company = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.*')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->where('Companies.companiesStatus', 1)
            ->first();

        $required = [
            'announcementsPublishDate' => 'required|date',
            'announcementsClosingDate'=> 'required|date|after:announcementsPublishDate',

            'fkPositionsID'=>'required',
            'fkProvincesID' => 'required|exists:Provinces,pkProvincesID',
            'AnnouncementsAgeFrom'=>'required',
            'announcementsSalaryType' => 'required',

            'AnnouncementsFromDay' => 'required',
            'AnnouncementsToDay' => 'required',
            'announcementsRequiredDocType' => 'required',

            'announcementsIsFullTime' => 'required',
            'announcementsWorkTime' => 'required'
        ];

        if( empty($request['announcementsSalaryNegotiate'])){
            $required['announcementsSalaryFrom'] = 'required';
//            $required['announcementsSalaryTo'] = 'required';
        }

        if( $request->get('fkPositionsID') == '0' ){
            $required['positionOther'] = 'required';
            $isPositionIdOther = true;
        }

        // check Experince....
        if( !empty( $request['AnnouncementsExperiencePositionN'] ) ){
            $required['AnnouncementsPositionType'] = 'required';
        }
        if( !empty( $request['AnnouncementsExperienceSectorN'] ) ){
            $required['AnnouncementsSectorType'] = 'required';
        }

        $this->validate($request, $required);

        if($isPositionIdOther){
            $position = Position::create([
                'fkSectorsID' => $company->fkSectorsID,
                'positionsNameEN' => $request->get('positionOther'),
                'positionsNameKH' => $request->get('positionOther'),
                'positionsNameZH' => $request->get('positionOther'),
                'positionsNameTH' => $request->get('positionOther'),
                'positionsStatus' => 1,
            ]);

            $pkPositionsID = $position->pkPositionsID;
            if (strlen( $pkPositionsID ) == 2 ){
                $positionsCode = 'P00'.$pkPositionsID;
            }elseif( strlen( $pkPositionsID ) > 2 ){
                $positionsCode = 'P0'.$pkPositionsID;
            }else{
                $positionsCode = 'P000'.$pkPositionsID;
            }
            $request['fkPositionsID'] = $pkPositionsID;
            Position::where('pkPositionsID', $pkPositionsID)->update(array('positionsCode' => $positionsCode));

            $emails = config("constants.ADMIN_EMAIL");
            $positionName = $request->get('positionOther');
            $domainName = env('HTTP_HOST');
            Mail::raw('Hi, Admin of Bongpheak Job. Now you have new position has been created with '.$domainName.'. The new position name is '.$positionName.'.' , function ($message) use ($domainName, $emails){
                $message->to($emails)->subject('New Position created in '.$domainName);
            });
        }

        $input = $request->all();

        if( $request->get('AnnouncementsAgeUntil') == ''){ $input['AnnouncementsAgeUntil'] = $input['AnnouncementsAgeFrom']; }
        $input['fkSectorsID'] = $company->fkSectorsID;
        $input['fkSubsectorsID'] = $company->fkSubsectorsID;
        //$input['fkActivitiesID'] = $company->fkActivitiesID;

        if( $input['AnnouncementsExperiencePositionN'] == '12' && $input['AnnouncementsPositionType'] == 'month' ){
            $input['AnnouncementsExperiencePositionN'] = 1;
            $input['AnnouncementsPositionType'] = 'year';
        }

        if( $input['AnnouncementsExperienceSectorN'] == '12' && $input['AnnouncementsSectorType'] == 'month' ){
            $input['AnnouncementsExperienceSectorN'] = 1;
            $input['AnnouncementsSectorType'] = 'year';
        }

        $langLevel = null;

        //get fkPositionsCode from tblPositon
        $GetfkPositionsID = $request['fkPositionsID'];
        $GetfkPositionsCode = Position::where('positionsStatus', true)->where('pkPositionsID', $GetfkPositionsID)->value('positionsCode');
        $input['fkPositionsCode']= $GetfkPositionsCode;

        $input['announcementsCertificate'] = json_encode( $request->get('certificate') );
        $input['announcementsBenefit'] = json_encode( $request->get('benefit') );
        $input['announcementsLanguage'] = json_encode( $request->get('language') );
        $input['announcementsWorkTime'] = json_encode( $request->get('announcementsWorkTime') );
        if( !empty($request->get('language')) ){
            foreach($request->get('language') as $key => $value){
                $langLevel[$key] = $request->get('announcementsLanguageLevel')[$key];
            }
        }
        $input['announcementsLanguageLevel'] = json_encode( $langLevel );
        if( $request->get('announcementsAllowWithoutExperience') == null ){
            $input['announcementsAllowWithoutExperience'] = 0;
        }

        // check Experience depend on Salary
        if( empty($request['announcementsSalaryNegotiate']) ){
            if( (int)$request['announcementsSalaryTo'] <= (int)$request['announcementsSalaryFrom']  ){
                $input['AnnouncementsSalaryDependsOn'] = '' ;
            }
            $input['announcementsSalaryNegotiate']=0;
        }else{
            $input['announcementsSalaryTo']='';
            $input['announcementsSalaryFrom']='';
            $input['AnnouncementsSalaryDependsOn'] = '' ;
        }

        $redirectUrl = '/account/postjob';
        if (isset($_REQUEST['previewJob']) ){
            $postjob->fill($input)->save();
            $postjob = Postjob::findOrFail($id);
            $provinceName = DB::table('Provinces')->where('pkProvincesID', $postjob->fkProvincesID)->value('provincesNameEN');
            $sectorName = DB::table('Sectors')->where('pkSectorsID', $postjob->fkSectorsID)->value('sectorsNameEN');
            $positionName = DB::table('Positions')->where('pkPositionsID', $postjob->fkPositionsID)->value('positionsNameEN');

            $province_name = fnConvertSlug($provinceName);
            $sector_name = fnConvertSlug($sectorName);
            $position_name = fnConvertSlug($positionName);

            $redirectUrl = '/account/jobs/'.$province_name.'/'.$sector_name.'/'.$position_name.'/'. $postjob->pkAnnouncementsID;
        }elseif( isset($_REQUEST['publishJob']) ){
            $input['announcementsStatus']  = 1 ;
            $postjob->fill($input)->save();
        }else{
            $postjob->fill($input)->save();
        }

        $job = Postjob::findOrFail($id);

        $mJob = new Postjob();
        $mJob->convertConfirmJobApplyMp3( $job );
        $mJob->convertConfirmJobApplyNoCompanyMp3($job);
        $mJob->convertJobMp3( $job );

        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect( \Lang::getLocale().$redirectUrl);
    }

    public function destroy($id)
    {
        Postjob::where('pkAnnouncementsID', '=', $id)
            ->update(['announcementsStatus' => '3']);

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/postjob');
    }
    public function jobPublish( $pkAnnouncementsID ){

        DB::table('Announcements')
            ->where('pkAnnouncementsID', $pkAnnouncementsID)
            ->update(['announcementsStatus' => 1]);

        return redirect( \Lang::getLocale().'/account/postjob');
    }

    public function jobUnpublish( $pkAnnouncementsID ){

        DB::table('Announcements')
            ->where('pkAnnouncementsID', $pkAnnouncementsID)
            ->update(['announcementsStatus' => 0]);

        return redirect( \Lang::getLocale().'/account/postjob');
    }

    public function applicationByjob( $pkAnnouncementsID )
    {
        $lang =  strtoupper(Lang::getLocale());
        $employerId =  Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId )
            ->first();

        $jobApplied = DB::table('JobApply')
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->select('pkJobApplyID', 'fkPositionsID', 'fkUsersID', 'jobApplyGender', 'jobApplyName', 'jobApplyEmail', 'jobApplyPhone', 'fkAnnouncementsID', 'created_at', 'jobApplyCvFile')
            ->paginate( config("constants.PAGINATION_NUM") );

        $jobDetail = DB::table('Announcements')
            ->Join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Announcements.fkZonesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName'.$lang, 'Companies.companiesNickName', 'Companies.companiesDescription'.$lang,
                'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesXCoordinate', 'Companies.companiesYCoordinate', 'Companies.companiesDescription'.$lang, 'Companies.companiesPhone', 'Companies.companiesEmail',
                'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Activities.activitiesName'.$lang,
                'Zones.zonesName'.$lang,
                'Positions.positionsName'.$lang, 'Positions.pkPositionsID',
                'Provinces.provincesName'.$lang,
                'Districts.districtsName'.$lang,
                'Communes.CommunesName'.$lang,
                'Villages.villagesName'.$lang,
                'Announcements.fkSectorsID','Announcements.fkSubsectorsID','Announcements.fkPositionsID',

                'Announcements.pkAnnouncementsID', 'Announcements.fkCompaniesID' , 'Announcements.fkPositionsID', 'Announcements.announcementsClosingDate', 'announcementsSalaryType',
                'Announcements.announcementsHiring', 'Announcements.announcementsPublishDate',
                'Announcements.AnnouncementsExperiencePositionN', 'Announcements.AnnouncementsPositionType', 'Announcements.AnnouncementsExperienceSectorN', 'Announcements.AnnouncementsSectorType',
                'Announcements.AnnouncementsGender', 'Announcements.AnnouncementsAgeFrom', 'Announcements.AnnouncementsAgeUntil',
                'Announcements.announcementsCertificate', 'Announcements.announcementsLanguageLevel', 'Announcements.announcementsSalaryFrom', 'Announcements.announcementsSalaryTo',
                'Announcements.AnnouncementsFromHour', 'Announcements.AnnouncementsFromMinute', 'Announcements.AnnouncementsFromType', 'Announcements.AnnouncementsToHour', 'Announcements.AnnouncementsToMinute', 'Announcements.AnnouncementsToType', 'Announcements.AnnouncementsFromDay', 'Announcements.AnnouncementsToDay',
                'Announcements.announcementsBenefit', 'Announcements.announcementsLanguage',
                'Announcements.AnnouncementsSalaryDependsOn', 'Announcements.AnnouncementsExperienceOrAnd',
                'Announcements.AnnouncementsBreakHoursFromHour', 'Announcements.AnnouncementsBreakHoursFromMinute', 'Announcements.AnnouncementsBreakHoursFromType',
                'Announcements.AnnouncementsBreakHoursToHour', 'Announcements.AnnouncementsBreakHoursToMinute', 'Announcements.AnnouncementsBreakHoursToType',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.applyByJob', compact('jobApplied', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name') );
    }

    public function searchCandidate( $pkAnnouncementsID )
    {
        $lang =  strtoupper(Lang::getLocale());
        $postjob = DB::table('Announcements')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            ->select(
                'Sectors.sectorsName'.$lang,
                'Subsectors.subsectorsName'.$lang,
                'Activities.activitiesName'.$lang,
                'Positions.positionsName'.$lang,
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.fkCompaniesID', 'Announcements.announcementsStatus'
            )
            ->first();
        $strWhere = ' tblTargetJobs.fkSectorsID = '.$postjob->fkSectorsID;
        //$strWhere = ' tblTargetJobs.fkSectorsID = '.$postjob->fkSectorsID.' AND tblTargetJobs.fkSubsectorsID = '.$postjob->fkSubsectorsID.' AND tblTargetJobs.fkPositionsID = '.$postjob->fkPositionsID ;

        $seekers = DB::table('users')
            ->leftjoin('TargetJobs', 'TargetJobs.fkUsersID', '=', 'users.id')
            ->whereRaw( $strWhere )
            ->where('users.status', '<>', 3)
            ->select('users.id', 'users.gender', 'users.name', 'users.phone', 'users.email', 'users.fkPositionsID')
            ->paginate( config("constants.PAGINATION_NUM") );

        $countSeekers = DB::table('users')
            ->Join('TargetJobs', 'TargetJobs.fkUsersID', '=', 'users.id')
            ->whereRaw( $strWhere )
            ->count();

        return view('accounts.postjobs.searchCandidate', compact( 'lang', 'seekers', 'countSeekers', 'postjob') );

    }

    public function appAll($pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $employerId = Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId)
            ->first();

        $jobApplies = DB::table('JobApply')
            ->select("pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "jobApplyIsConfirm")
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->where('jobApplyIsConfirm', 1)
            ->orderBy('pkJobApplyID', 'DESC')
            ->paginate(config("constants.PAGINATION_NUM"));

        $jobDetail = DB::table('Announcements')
            ->Join('Companies', 'Companies.pkCompaniesID', '=', 'Announcements.fkCompaniesID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Announcements.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Announcements.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Announcements.fkVillagesID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            //->where('announcementsStatus', 1)
            ->select(
                'Companies.pkCompaniesID', 'Companies.companiesName' . $lang, 'Companies.companiesNickName', 'Companies.companiesDescription' . $lang,
                'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesXCoordinate', 'Companies.companiesYCoordinate', 'Companies.companiesDescription' . $lang, 'Companies.companiesPhone', 'Companies.companiesEmail',
                'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.sectorsName' . $lang,
                'Subsectors.subsectorsName' . $lang,
                'Activities.activitiesName' . $lang,
                'Positions.positionsName' . $lang, 'Positions.pkPositionsID',
                'Provinces.provincesName' . $lang,
                'Districts.districtsName' . $lang,
                'Communes.CommunesName' . $lang,
                'Villages.villagesName' . $lang,
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.application', compact('jobApplies', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name'));
    }

    public function appNew($pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $employerId = Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId)
            ->where('Companies.companiesStatus', 1)
            ->first();


        $jobApplies = DB::table('JobApply')
            ->select(
                "pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "jobApplyIsConfirm"
            )
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->where('jobApplyInteresting', 0)
            ->where('jobApplyCallToInterview', 0)
            ->where('jobApplyRejected', 0)
            ->where('jobApplyHired', 0)
            ->where('jobApplyCallAndReject', 0)
            ->where('jobApplyIsConfirm', 1)
            ->orderBy('pkJobApplyID', 'DESC')
            ->paginate(config("constants.PAGINATION_NUM"));

        $jobDetail = DB::table('Announcements')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            //->where('announcementsStatus', 1)
            ->select(
                'Sectors.sectorsName' . $lang,
                'Subsectors.subsectorsName' . $lang,
                'Activities.activitiesName' . $lang,
                'Positions.positionsName' . $lang, 'Positions.pkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();

        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.application', compact('jobApplies', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name'));
    }

    public function appInteresting($pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $employerId = Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId)
            ->first();

        $jobApplies = DB::table('JobApply')
            ->select("pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "jobApplyIsConfirm")
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->where('jobApplyInteresting', 1)
            ->where('jobApplyIsConfirm', 1)
            ->orderBy('pkJobApplyID', 'DESC')
            ->paginate(config("constants.PAGINATION_NUM"));

        $jobDetail = DB::table('Announcements')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            //->where('announcementsStatus', 1)
            ->select(
                'Sectors.sectorsName' . $lang,
                'Subsectors.subsectorsName' . $lang,
                'Activities.activitiesName' . $lang,
                'Positions.positionsName' . $lang, 'Positions.pkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.application', compact('jobApplies', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name'));
    }

    public function appCallToInterview($pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $employerId = Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId)
            ->first();

        $jobApplies = DB::table('JobApply')
            ->select("pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "jobApplyIsConfirm")
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->where('jobApplyCallToInterview', 1)
            ->where('jobApplyIsConfirm', 1)
            ->orderBy('pkJobApplyID', 'DESC')
            ->paginate(config("constants.PAGINATION_NUM"));

        $jobDetail = DB::table('Announcements')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            //->where('announcementsStatus', 1)
            ->select(
                'Sectors.sectorsName' . $lang,
                'Subsectors.subsectorsName' . $lang,
                'Activities.activitiesName' . $lang,
                'Positions.positionsName' . $lang, 'Positions.pkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.application', compact('jobApplies', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name'));
    }

    public function appCallAndRejected($pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $employerId = Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId)
            ->first();

        $jobApplies = DB::table('JobApply')
            ->select("pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "jobApplyIsConfirm")
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->where('jobApplyCallAndReject', 1)
            ->where('jobApplyIsConfirm', 1)
            ->orderBy('pkJobApplyID', 'DESC')
            ->paginate(config("constants.PAGINATION_NUM"));

        $jobDetail = DB::table('Announcements')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            //->where('announcementsStatus', 1)
            ->select(
                'Sectors.sectorsName' . $lang,
                'Subsectors.subsectorsName' . $lang,
                'Activities.activitiesName' . $lang,
                'Positions.positionsName' . $lang, 'Positions.pkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.application', compact('jobApplies', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name'));
    }

    public function appRejected($pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $employerId = Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId)
            ->first();

        $jobApplies = DB::table('JobApply')
            ->select("pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "jobApplyIsConfirm")
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->where('jobApplyRejected', 1)
            ->where('jobApplyIsConfirm', 1)
            ->orderBy('pkJobApplyID', 'DESC')
            ->paginate(config("constants.PAGINATION_NUM"));

        $jobDetail = DB::table('Announcements')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            //->where('announcementsStatus', 1)
            ->select(
                'Sectors.sectorsName' . $lang,
                'Subsectors.subsectorsName' . $lang,
                'Activities.activitiesName' . $lang,
                'Positions.positionsName' . $lang, 'Positions.pkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.application', compact('jobApplies', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name'));
    }

    public function appHired($pkAnnouncementsID)
    {
        $lang = strtoupper(Lang::getLocale());
        $employerId = Auth::id();
        $companyId = DB::table('Companies')
            ->Join('CompanyUsers', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('Companies.pkCompaniesID')
            ->where('CompanyUsers.fkUsersID', '=', $employerId)
            ->first();

        $jobApplies = DB::table('JobApply')
            ->select("pkJobApplyID", "fkAnnouncementsID", "fkSharesID", "fkCompaniesID", "fkUsersID", "fkPositionsID",
                "jobApplyGender", "jobApplyName", "jobApplyEmail", "jobApplyPhone", "jobApplyDescription", "jobApplyCvFile",
                "jobApplyExperience", "jobApplyInteresting", "jobApplyCallToInterview", "jobApplyCallAndReject", "jobApplyRejected", "jobApplyHired", "jobApplyDuplicate", "jobApplyBy",
                "jobApplyStatus", "jobApplyIsConfirm")
            ->where('fkCompaniesID', $companyId->pkCompaniesID)
            ->where('fkAnnouncementsID', $pkAnnouncementsID)
            ->where('jobApplyHired', 1)
            ->where('jobApplyIsConfirm', 1)
            ->orderBy('pkJobApplyID', 'DESC')
            ->paginate(config("constants.PAGINATION_NUM"));

        $jobDetail = DB::table('Announcements')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Announcements.fkProvincesID')
            ->Join('Sectors', 'Sectors.pkSectorsID', '=', 'Announcements.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Announcements.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Announcements.fkActivitiesID')
            ->leftjoin('Positions', 'Positions.pkPositionsID', '=', 'Announcements.fkPositionsID')
            ->where('Announcements.pkAnnouncementsID', $pkAnnouncementsID)
            //->where('announcementsStatus', 1)
            ->select(
                'Sectors.sectorsName' . $lang,
                'Subsectors.subsectorsName' . $lang,
                'Activities.activitiesName' . $lang,
                'Positions.positionsName' . $lang, 'Positions.pkPositionsID',
                'Announcements.pkAnnouncementsID', 'Announcements.fkSectorsID', 'Announcements.fkSubsectorsID', 'Announcements.fkPositionsID', 'Announcements.fkActivitiesID',
                'Announcements.announcementsStatus',
                'Provinces.provincesNameEN as provincesName_EN',
                'Sectors.sectorsNameEN as sectorsName_EN',
                'Positions.positionsNameEN as positionsName_EN'
            )
            ->first();
        $province_name = fnConvertSlug($jobDetail->provincesName_EN);
        $sector_name = fnConvertSlug($jobDetail->sectorsName_EN);
        $position_name = fnConvertSlug($jobDetail->positionsName_EN);

        return view('accounts.applications.application', compact('jobApplies', 'jobDetail', 'lang', 'province_name', 'sector_name', 'position_name'));
    }

    public function interestingAjax(Request $request)
    {
        if( $request->input('checkboxValue') == 1 ){
            $updateValueCheckbox = 0 ;
        }elseif( $request->input('checkboxValue') == 0 ){
            $updateValueCheckbox = 1 ;
        }
        JobApply::where('pkJobApplyID', '=', $request->input('applyId'))
            ->update(['jobApplyInteresting' => $updateValueCheckbox, 'jobApplyViewedByEmployer' => 1 ]);

        Flash::message(trans('text_lang.updateSuccessful'));
    }

    public function jobApplyCallToInterviewAjax(Request $request)
    {
        $updateValueCheckbox = 0 ;
        if( $request->input('checkboxValue') == 0 ){
            $updateValueCheckbox = 1 ;
        }
        JobApply::where('pkJobApplyID', '=', $request->input('applyId'))
            ->update(['jobApplyCallToInterview' => $updateValueCheckbox, 'jobApplyViewedByEmployer' => 1  ]);

        Flash::message(trans('text_lang.updateSuccessful'));
    }

    public function jobApplyRejectedAjax(Request $request)
    {
        $updateValueCheckbox = 0 ;
        if( $request->input('checkboxValue') == 0 ){
            $updateValueCheckbox = 1 ;
        }
        JobApply::where('pkJobApplyID', '=', $request->input('applyId'))
            ->update(['jobApplyRejected' => $updateValueCheckbox, 'jobApplyViewedByEmployer' => 1  ]);

        Flash::message(trans('text_lang.updateSuccessful'));
    }

    public function jobApplyHiredAjax(Request $request)
    {
        $updateValueCheckbox = 0 ;
        if( $request->input('checkboxValue') == 0 ){
            $updateValueCheckbox = 1 ;
        }
        JobApply::where('pkJobApplyID', '=', $request->input('applyId'))
            ->update(['jobApplyHired' => $updateValueCheckbox, 'jobApplyViewedByEmployer' => 1 ]);

        Flash::message(trans('text_lang.updateSuccessful'));
    }

    public function jobApplyCallAndRejectAjax(Request $request)
    {
        $updateValueCheckbox = 0 ;
        if( $request->input('checkboxValue') == 0 ){
            $updateValueCheckbox = 1 ;
        }
        JobApply::where('pkJobApplyID', '=', $request->input('applyId'))
            ->update(['jobApplyCallAndReject' => $updateValueCheckbox, 'jobApplyViewedByEmployer' => 1 ]);

        Flash::message(trans('text_lang.updateSuccessful'));
    }

}