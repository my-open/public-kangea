<?php

namespace App\Http\Controllers;

use App\Activity;
use App\CompanyUser;
use App\Sector;
use App\Subsector;
use App\Zone;

use App\Province;
use App\District;
use App\Commune;
use App\Village;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Flash;
use Validator;
use Lang;
use DB;
use App\User;
use App\RoleUser;
use App\Company;
use Auth;

class EmployerController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin', ['except' => ['repEmployer']]);
    }
    public function index()
    {
        $fromDate = date('Y-m'.'-01');
        $toDate = date('Y-m-t');
        $create_from = array('0'=> trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.facebook'));

        $lang = strtoupper(Lang::getLocale());
        $name = null;
        $phone = null;
        $email = null;
        $company = null;

        $strWhere = 'tblusers.status <> 3 AND (tblusers.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59")' ;
        $rows = DB::table('users')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->join('Companies', 'Companies.pkCompaniesID', '=', 'CompanyUsers.fkCompaniesID')
            ->select('id', 'fkPositionsID', 'name', 'phone', 'email', 'users.created_at', 'password', 'gender', 'address', 'verified', 'status', 'companiesName'.$lang, 'social_accounts.provider_user_id' )
            ->where('role_user.role_id', '=', config("constants.EMPLOYER") )
            ->orderBy('users.id', 'DESC')
            ->whereRaw($strWhere)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.users.employers.index', compact('rows', 'lang', 'name', 'phone', 'email', 'company', 'fromDate', 'toDate', 'create_from') );
    }

    public function create()
    {
        if( Lang::getLocale() == 'en' ){
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameEN')->pluck('sectorsNameEN', 'pkSectorsID');
//            $zones = Zone::where('zonesStatus', true)->orderBy('zonesNameEN')->pluck('zonesNameEN', 'pkZonesID');
            $companies = Company::orderBy('companiesNameEN')->pluck('companiesNameEN', 'pkCompaniesID');
        }
        else{
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameKH')->pluck('sectorsNameKH', 'pkSectorsID');
//            $zones = Zone::where('zonesStatus', true)->orderBy('zonesNameKH')->pluck('zonesNameKH', 'pkZonesID');
            $companies = Company::orderBy('companiesNameKH')->pluck('companiesNameKH', 'pkCompaniesID');
        }

        return view('accounts.users.employers.add', compact('sectors', 'provinces', 'companies', 'pkCompaniesID')  );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|digits_between:9,10',
            'email' => 'required|email|max:70|unique:users',
            'password' => 'required|confirmed|min:6',
            'companiesNameEN' => 'required',
            'fkSectorsID' => 'required',
            'fkSubsectorsID' => 'required',
            'fkActivitiesID' => 'required',
            'companiesNumberOfWorker' => 'digits_between:1,4',
            'fkProvincesID'=> 'required|exists:Provinces,pkProvincesID',
            'fkDistrictsID' => 'required',
            'companiesPhone' => 'digits_between:9,10',
            'companiesEmail' => 'email|max:70',
            'companiesLogo' => 'max:'.config("constants.IMAGE_SIZE_MAX").'|image|mimes:jpeg,jpg,png',
            'companiesSite' => 'url',
        ]);

        DB::beginTransaction();

        // get data For insert to table user
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'gender' => $request['gender'],
            'password' => bcrypt($request['password']),
            'status' => 1,
        ]);
        //Create role user
        $role_user = DB::table('role_user')->insert(
            ['user_id' => $user->id, 'role_id' => config("constants.EMPLOYER")]
        );

        // get data for table companies
        if( empty( $request['companiesNameKH'] ) ){
            $request['companiesNameKH'] = $request['companiesNameEN'];
        }
        if( empty( $request['companiesDescriptionKH'] ) ){
            $request['companiesDescriptionKH'] = $request['companiesDescriptionEN'];
        }

        //check logo
        if( $request->hasFile('companiesLogo')) {
            if ($request->file('companiesLogo')->isValid()) {
                $file = $request->file('companiesLogo');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if ($file->move('images/companyLogos', $fileName)) {
                    $img = Image::make('images/companyLogos/'.$fileName);
                    $img->resize(config("constants.COMPANY_LOGO_W"), config("constants.COMPANY_LOGO_H"), function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save('images/companyLogos/thumbnails/'.$fileName);
                    $companiesLogoName = $fileName;
                }
            }
        } else {
            $companiesLogoName = 'defaultLogo.png';
        }

        $company = Company::create([
            'companiesNameEN' => $request['companiesNameEN'],
            'companiesNameKH' => $request['companiesNameKH'],
            'companiesNameZH' => $request['companiesNameEN'],
            'companiesNameTH' => $request['companiesNameEN'],
            'fkSectorsID' => $request['fkSectorsID'],
            'fkSubsectorsID' => $request['fkSubsectorsID'],
            'fkActivitiesID' => $request['fkActivitiesID'],
            'companiesPhone' =>$request['companiesPhone'],
            'companiesEmail' =>$request['companiesEmail'],
            'companiesNumberOfWorker' =>$request['companiesNumberOfWorker'],
            'fkProvincesID' =>$request['fkProvincesID'],
            'fkDistrictsID' =>$request['fkDistrictsID'],
            'fkCommunesID' =>$request['fkCommunesID'],
            'companiesAddress' =>$request['companiesAddress'],
            'companiesStatus' => 1,
            'companiesDescriptionEN' =>$request['companiesDescriptionEN'],
            'companiesDescriptionKH' =>$request['companiesDescriptionKH'],
            'companiesLogo' => $companiesLogoName,
        ]);

        $countCompanyID = DB::table('CompanyUsers')->where('fkCompaniesID', '=', $company->pkCompaniesID)->count();
        if( $countCompanyID > 0 ){
            User::where('id', $user->id)->update(array('companyLevel' => 2));
        }else{
            User::where('id', $user->id)->update(array('companyLevel' => 1));
        }

        //Create table tblCompanyUsers
        $companyUser = DB::table('CompanyUsers')->insert(
            ['fkCompaniesID' => $company->pkCompaniesID, 'fkUsersID' => $user->id]
        );

        if( !$user || !$role_user || !$company || !$companyUser )
        {
            DB::rollBack();
        } else {
            DB::commit();
        }

        Flash::message( trans('text_lang.addSuccessful') );
        return redirect( \Lang::getLocale().'/account/employer');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        $company = DB::table('users')
            ->Join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->Join('Companies', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('users.*')
            ->select('CompanyUsers.fkCompaniesID', 'CompanyUsers.fkUsersID')
            ->select('Companies.*')
            ->where('users.id', '=', $id )
            ->first();

        //get Province name
        if( Lang::getLocale() == 'en' ){
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameEN')->pluck('provincesNameEN', 'pkProvincesID');
            $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $company->fkProvincesID)->orderBy('districtsNameEN')->pluck('districtsNameEN', 'pkDistrictsID');
            $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $company->fkDistrictsID)->orderBy('CommunesNameEN')->pluck('CommunesNameEN', 'pkCommunesID');
//            $villageByCommunes = Village::where('villagesStatus', true)->where('fkCommunesID', $company->fkCommunesID)->orderBy('villagesNameEN')->pluck('villagesNameEN', 'pkVillagesID');
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameEN')->pluck('sectorsNameEN', 'pkSectorsID');
            $subsectorBySectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $company->fkSectorsID)->orderBy('subsectorsNameEN')->pluck('subsectorsNameEN', 'pkSubsectorsID');
            $activitiesBySubsector = Activity::where('ActivitiesStatus', true)->where('fkSubsectorsID', $company->fkSubsectorsID)->orderBy('ActivitiesOrder')->pluck('ActivitiesNameEN', 'pkActivitiesID');
//            $zones = Zone::where('zonesStatus', true)->orderBy('zonesNameEN')->pluck('zonesNameEN', 'pkZonesID');
        }
        else{
            $provinces = Province::where('provincesStatus', true)->orderBy('provincesNameKH')->pluck('provincesNameKH', 'pkProvincesID');
            $districtByProvinces = District::where('districtsStatus', true)->where('fkProvincesID', $company->fkProvincesID)->orderBy('districtsNameKH')->pluck('districtsNameKH', 'pkDistrictsID');
            $communeByDistricts = Commune::where('communesStatus', true)->where('fkDistrictsID', $company->fkDistrictsID)->orderBy('CommunesNameKH')->pluck('CommunesNameKH', 'pkCommunesID');
//            $villageByCommunes = Village::where('villagesStatus', true)->where('fkCommunesID', $company->fkCommunesID)->orderBy('villagesNameKH')->pluck('villagesNameKH', 'pkVillagesID');
            $sectors = Sector::where('sectorsStatus', true)->orderBy('sectorsNameKH')->pluck('sectorsNameKH', 'pkSectorsID');
            $subsectorBySectors = Subsector::where('subsectorsStatus', true)->where('fkSectorsID', $company->fkSectorsID)->orderBy('subsectorsNameKH')->pluck('subsectorsNameKH', 'pkSubsectorsID');
            $activitiesBySubsector = Activity::where('ActivitiesStatus', true)->where('fkSubsectorsID', $company->fkSubsectorsID)->orderBy('ActivitiesOrder')->pluck('ActivitiesNameKH', 'pkActivitiesID');
//            $zones = Zone::where('zonesStatus', true)->orderBy('zonesNameKH')->pluck('zonesNameKH', 'pkZonesID');
        }

        return view('accounts.users.employers.edit', compact('company', 'user', 'sectors', 'subsectorBySectors', 'activitiesBySubsector', 'provinces', 'districtByProvinces', 'communeByDistricts')  );
    }

    public function update( Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|digits_between:9,10',
            'email' => 'required|email|max:255|unique:users,email,'.$id.',id',
            'password' => 'confirmed|min:6',

            'companiesNameEN' => 'required',
            'companiesNumberOfWorker' => 'digits_between:1,4',
            'fkSectorsID' => 'required',
            'fkSubsectorsID' => 'required',
            'fkActivitiesID' => 'required',

            'fkProvincesID' => 'required',
            'fkDistrictsID' => 'required',
            'companiesPhone' => 'digits_between:9,10',
            'companiesEmail' => 'email|max:70',
            'companiesLogo' => 'max:'.config("constants.IMAGE_SIZE_MAX").'|image|mimes:jpeg,jpg,png',
            'companiesSite' => 'url',
        ]);

        $user = User::findOrFail($id);

        if( !empty( $request['password']) ){
            $request['password'] = bcrypt($request['password']);
        }else{
            $request['password'] = $user->password;
        }

        // get data for table companies
        if( empty( $request['companiesNameKH'] ) ){
            $request['companiesNameKH'] = $request['companiesNameEN'];
        }

        if( empty( $request['companiesDescriptionKH'] ) ){
            $request['companiesDescriptionKH'] = $request['companiesDescriptionEN'];
        }

        $company = DB::table('users')
            ->Join('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->Join('Companies', 'CompanyUsers.fkCompaniesID', '=', 'Companies.pkCompaniesID')
            ->select('users.*')
            ->select('CompanyUsers.fkCompaniesID', 'CompanyUsers.fkUsersID')
            ->select('Companies.*')
            ->where('users.id', '=', $id )
            ->first();

        if( $request->file('companiesLogo') != '' ) {
            if ($request->file('companiesLogo')->isValid()) {
                $file = $request->file('companiesLogo');
                $extension = $file->getClientOriginalExtension();
                $fileName = time() . '.' . $extension;
                if ($file->move('images/companyLogos', $fileName)) {
                    $img = Image::make('images/companyLogos/'.$fileName);
                    $img->resize(config("constants.COMPANY_LOGO_W"), config("constants.COMPANY_LOGO_H"), function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save('images/companyLogos/thumbnails/'.$fileName);
                    $companiesLogoName = $fileName;
                }
            }
        } else {
            $companiesLogoName = $company->companiesLogo;
        }

        $companyRecord = Company::findOrFail( $company->pkCompaniesID );

        $user->fill( $request->all() )->save();

        $companyRecord->fill( $request->all() )->save();
        DB::table('Companies')
            ->where('pkCompaniesID', $company->pkCompaniesID)
            ->update(['companiesLogo' => $companiesLogoName]);


        Flash::message( trans('text_lang.updateSuccessful') );
        return redirect(\Lang::getLocale().'/account/employer');
    }

    public function destroy($id)
    {
        User::where('id', '=', $id)
            ->update(['status' => '3']);

//        User::findOrFail($id)->delete();
//        DB::table('role_user')->where('user_id', '=', $id)->delete();
//        DB::table('CompanyUsers')->where('fkUsersID', '=', $id)->delete();
//        DB::table('social_accounts')->where('user_id', '=', $id)->delete();

        Flash::message( trans('text_lang.deleteSuccessful') );
        return redirect(\Lang::getLocale().'/account/employer');
    }

    public function repEmployer( Request $request, $pkCompaniesID ){
        $lang =  strtoupper(Lang::getLocale());
        $userCheck = \Auth::user();

        //Get company info
        $company = DB::table('Companies')
            ->leftjoin('Sectors', 'Sectors.pkSectorsID', '=', 'Companies.fkSectorsID')
            ->leftjoin('Subsectors', 'Subsectors.pkSubsectorsID', '=', 'Companies.fkSubsectorsID')
            ->leftjoin('Activities', 'Activities.pkActivitiesID', '=', 'Companies.fkActivitiesID')
            ->leftjoin('Zones', 'Zones.pkZonesID', '=', 'Companies.fkZonesID')
            ->leftjoin('Provinces', 'Provinces.pkProvincesID', '=', 'Companies.fkProvincesID')
            ->leftjoin('Districts', 'Districts.pkDistrictsID', '=', 'Companies.fkDistrictsID')
            ->leftjoin('Communes', 'Communes.pkCommunesID', '=', 'Companies.fkCommunesID')
            ->leftjoin('Villages', 'Villages.pkVillagesID', '=', 'Companies.fkVillagesID')
            ->where('Companies.pkCompaniesID', $pkCompaniesID)
            ->select(
                'Companies.pkCompaniesID', 'Companies.fkSubsectorsID', 'Companies.fkActivitiesID', 'Companies.companiesNameEN', 'Companies.companiesNameKH', 'Companies.companiesNameZH', 'Companies.companiesNameTH',  'Companies.companiesNickName', 'Companies.companiesDescriptionEN', 'Companies.companiesDescriptionKH',
                'Companies.companiesNumberOfWorker', 'Companies.fkZonesID', 'Companies.fkProvincesID', 'Companies.fkDistrictsID', 'Companies.fkCommunesID', 'Companies.fkVillagesID',
                'Companies.companiesAddress', 'Companies.companiesXCoordinate', 'Companies.companiesYCoordinate', 'Companies.companiesDescriptionEN', 'Companies.companiesDescriptionKH', 'Companies.companiesDescriptionZH', 'Companies.companiesDescriptionTH', 'Companies.companiesPhone', 'Companies.companiesEmail',
                'Companies.companiesLogo', 'Companies.companiesSite',
                'Sectors.sectorsNameEN', 'Sectors.sectorsNameKH', 'Sectors.sectorsNameZH', 'Sectors.sectorsNameTH',
                'Subsectors.subsectorsNameEN', 'Subsectors.subsectorsNameKH','Subsectors.subsectorsNameZH', 'Subsectors.subsectorsNameTH',
                'Activities.activitiesNameEN', 'Activities.activitiesNameKH','Activities.activitiesNameZH', 'Activities.activitiesNameTH',
                'Zones.zonesNameEN','Zones.zonesNameKH',
                'Provinces.provincesNameEN', 'Provinces.provincesNameKH','Provinces.provincesNameZH', 'Provinces.provincesNameTH',
                'Districts.districtsNameEN', 'Districts.districtsNameKH','Districts.districtsNameZH', 'Districts.districtsNameTH',
                'Communes.CommunesNameEN', 'Communes.CommunesNameKH','Communes.CommunesNameZH', 'Communes.CommunesNameTH',
                'Villages.villagesNameEN', 'Villages.villagesNameKH'
            )
            ->first();

        if( isset($_POST['btnRepSave']) ){
            $rules = array(
                'name'              => 'required',
                'gender'            => 'required',
                'phone' => 'required|digits_between:9,10|unique:users',
                'email' => 'required|email|max:70|unique:users',
                'password'          => 'required',
                'password_confirmation'  => 'required|same:password'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {

                // get the error messages from the validator
                $messages = $validator->messages();

                // redirect our user back to the form with the errors from the validator
                return Redirect::to('account/repEmployer/'.$pkCompaniesID)
                    ->withErrors($validator);

            }else{
                DB::beginTransaction();

                $user = User::create([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'phone' => $request['phone'],
                    'gender' => $request['gender'],
                    'password' => bcrypt($request['password']),
                    'status' => 1,
                ]);

                //Create role user
                $role_user = DB::table('role_user')->insert(
                    ['user_id' => $user->id, 'role_id' => config("constants.EMPLOYER")]
                );

                $countCompanyID = DB::table('CompanyUsers')->where('fkCompaniesID', '=', $pkCompaniesID)->count();
                if( $countCompanyID > 0 ){
                    User::where('id', $user->id)->update(array('companyLevel' => 2));
                }else{
                    User::where('id', $user->id)->update(array('companyLevel' => 1));
                }

                //Create table tblCompanyUsers
                $companyUser = DB::table('CompanyUsers')->insert(
                    ['fkCompaniesID' => $pkCompaniesID, 'fkUsersID' => $user->id]
                );

                if( !$user || !$role_user || !$companyUser )
                {
                    DB::rollBack();
                } else {
                    DB::commit();
                    Flash::message( trans('text_lang.addSuccessful') );
                }

                if( $userCheck -> hasRole('employer') ){
                    return redirect( \Lang::getLocale().'/account/userProfile');
                }
                return redirect( \Lang::getLocale().'/account/repEmployer/'.$pkCompaniesID);
            }
        }
        return view('accounts.users.employers.addRepEmployer', compact('lang', 'company', 'activity') );
    }

    public function searchEmployer(Request $request)
    {
        $fromDate = ( $request->get('fromDate') ? $request->get('fromDate') : date('Y-m'.'-01'));
        $toDate = ( $request->get('toDate') ? $request->get('toDate') : date('Y-m-t'));
        $create_from  = array('0'=> trans('text_lang.all'), '1'=>trans('text_lang.web'), '2'=>trans('text_lang.facebook'));
        $from = (int)$request->get('create_from');

        $lang = strtoupper(Lang::getLocale());
        $name = trim($request->get('name'));
        $phone = trim($request->get('phone'));
        $email = trim($request->get('email'));
        $company = trim($request->get('companiesName'.$lang));

        $searchCriteria = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
        ];
        $strWhere = 'tblusers.status <> 3 AND (tblusers.created_at between "'. $fromDate .' 00:00:00" AND "'. $toDate .' 23:59:59")' ;
        if($from == 1){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id IS NULL';
        }elseif($from == 2){
            $strWhere .= ' AND tblsocial_accounts.provider_user_id <> "" ';
        }

        if(!empty($company)){
            $strWhere .=" AND tblCompanies.companiesName" . $lang ." like '%".$company."%' ";
        }
        foreach ($searchCriteria as $key => $value)
        {
            if ($value == '') {
                $$key = null;
                continue;
            }
            $$key = trim($value);
            $strWhere .=" AND tblusers." . $key ." like '%".$value."%' ";
        }

        $rows = DB::table('users')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftjoin('social_accounts', 'social_accounts.user_id', '=', 'users.id')
            ->leftjoin('CompanyUsers', 'CompanyUsers.fkUsersID', '=', 'users.id')
            ->leftjoin('Companies', 'Companies.pkCompaniesID', '=', 'CompanyUsers.fkCompaniesID')
            ->select('id', 'fkPositionsID', 'name', 'phone', 'email', 'users.created_at', 'password', 'gender', 'address', 'social_accounts.provider_user_id', 'verified', 'status', 'companiesName'.$lang )
            ->where('role_user.role_id', '=', config("constants.EMPLOYER") )
            ->orderBy('users.id', 'DESC')
            ->where('status', '<>', 3)
            ->whereRaw($strWhere)
            ->paginate( config("constants.PAGINATION_NUM_MAX") );

        return view('accounts.users.employers.index', compact('rows', 'name', 'phone', 'email', 'company', 'lang', 'fromDate', 'toDate', 'create_from', 'from'));
    }

}
