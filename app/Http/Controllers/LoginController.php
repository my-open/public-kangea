<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Lang;
use Auth;

use App\Http\Requests;

class LoginController extends Controller
{
    public function loginEmp(){
        return view('accounts.users.employers.login');
    }

    public function postLoginEmp( Request $request ){
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password'], 'status' => 1], $request->get('remember'))) {
            $user = Auth::user();
            $companyLevel = DB::table('users')->where('id', $user->id)->value('companyLevel');
            if( $companyLevel == 3){
                Auth::logout();
                Flash::error( trans('text_lang.accountNotYetCheckByCompanyAdmin') );
                return redirect(\Lang::getLocale().'/account/employer/login');
            }
            $user = \Auth::user();
            //Add user log
            $comID = DB::table('CompanyUsers')->where('fkUsersID', Auth::id())->value('fkCompaniesID');
            $data['fkUsersID'] = $user->id;
            $data['userLogsActivity'] = 'login';
            $data['userLogsActivityDescription'] = 'login';
            if( $comID ){
                $activityDes['fkUsersID'] = Auth::id();
                $activityDes['fkCompaniesID'] = $comID;
                $data['userLogsActivityDescription'] = json_encode( $activityDes );
            }
            fnUserLog($data);
            if( $user -> hasRole('admin') ){
                if( fnIsSale() ){
                    return redirect( \Lang::getLocale().'/account/company');
                }else{
                    return redirect( \Lang::getLocale().'/account');
                }
            }else{
                return redirect( \Lang::getLocale(). '/account/postjob' );
            }
        }else{
            Flash::error( trans('text_lang.incorrectEmailPassword') );
            return redirect(\Lang::getLocale().'/account/employer/login');
        }
    }

    public function login(){
        return view('accounts.users.jobseekers.login');
    }

    public function postLogin( Request $request ){
        $this->validate($request, [
            'phone' => 'required|digits_between:9,10',
        ]);

        if (Auth::attempt(['phone' => $request['phone'], 'password' => $request['phone'], 'status' => 1], $request->get('remember'))) {
            //Add user log
            $data['fkUsersID'] = Auth::id();
            $data['userLogsActivity'] = 'login';
            $data['userLogsActivityDescription'] = 'login';
            fnUserLog($data);
            return redirect()->intended(\Lang::getLocale(). '/account');
        }else{
            Flash::error( trans('text_lang.incorrectPhoneNumber') );
            return redirect(\Lang::getLocale().'/account/jobseeker/login');
        }
    }
}
