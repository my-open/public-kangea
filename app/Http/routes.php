<?php
/*
Author = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Monday 07, March 2016
Modified Date = Monday 07, March 2016
*/

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        //'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
    ],
    function () {

        Route::group(['middleware' => 'web'], function () {
            Route::auth();
            //Front Page
            Route::get('/home', 'HomeController@home');
            Route::get('/apply', 'HomeController@jobApply');
            Route::get('/job-detail', 'JobController@jobDetail');

            //==============
            Route::get('/', 'HomeController@index');
            Route::get('/error', 'HomeController@noPermission');
            Route::get('/welcome', function(){return view('welcome');});
            Route::get('/companies', 'JobController@allComp');
            Route::get('/media', function(){return view('media');});
            Route::get('/jobs', 'JobController@search');
            Route::post('/jobs', 'JobController@search');
            Route::get('/searchJobLocationAjax', 'JobController@searchJobLocationAjax');
            Route::get('/jobs/{sectorName}/{positionName}/{pkPositionsID}', 'JobController@jobsByPosition');
            Route::get('/company/{companyName}/{pkCompaniesID}', 'JobController@jobsByCompany');
            Route::get('/jobs/{provinceName}/{sectorName}/{positionName}/{pkAnnouncementsID}', 'JobController@jobAnnouncementText');
            Route::get('/jobApply/{pkAnnouncementsID}', 'JobController@jobApplyChoose');

            Route::get('/jobApplyMy/{pkAnnouncementsID}', 'JobController@jobApplyForMyself');
            Route::post('/jobApplyMy/{pkAnnouncementsID}', 'JobController@jobApplyForMyselfPost');

            Route::get('/jobApplySO/{pkAnnouncementsID}', 'JobController@jobApplyForSO');
            Route::post('/jobApplySO/{pkAnnouncementsID}', 'JobController@jobApplyForSOPost');

            Route::get('/aboutUs', 'PageController@aboutUs');
            Route::get('/contactUs', 'PageController@contactUs');
            Route::get('/aboutBongpheak', 'PageController@aboutBongpheak');
            Route::get('/employer', 'RegisterEmployerController@index');

            Route::get('/searchLog', 'LogController@searchLogLocationAjax');
            Route::get('/logShareFB', 'LogController@logShareFBAjax');
            Route::get('/logSharePhone', 'LogController@logShareToPhoneAjax');
            Route::get('/logApplyOwn', 'LogController@logApplyForMyselfAjax');
            Route::get('/logApplyForSO', 'LogController@logApplyForSOAjax');

            //Dashboard
            Route::get('/dashboard', 'Dashboard\DashboardController@accumulative');
            Route::group(['prefix' => 'dashboard'], function () {
                Route::get('/accumulative', 'Dashboard\DashboardController@accumulative');
                Route::post('/accumulative', 'Dashboard\DashboardController@accumulative');
                Route::get('/districts', 'Dashboard\DashboardController@getDistricts');
                Route::get('/communes', 'Dashboard\DashboardController@getCommunes');
                Route::get('/new', 'Dashboard\NewController@newChart');
                Route::post('/new', 'Dashboard\NewController@newChart');
                Route::get('/active', 'Dashboard\ActiveController@activeChart');
                Route::post('/active', 'Dashboard\ActiveController@activeChart');
                Route::get('/accumulativeAllSector', 'Dashboard\AccumulativeAllSectorController@accumulative');
            });

            //Developer
            Route::get('/developer/{param}', 'HomeController@locationInsert');

            //download
            Route::get('/download/cv/{cvFile}', 'JobController@downloadCV');
            Route::get('/download/cv/{cvFile}', 'JobController@downloadCV');

            //Mail
            Route::get('/sendMail', 'MailController@sentMail');

            //End Front Page

            Route::get('/account', 'AccountController@index');

            Route::group(['prefix' => 'account'], function () {
                Route::get('/clearSudo', 'SudoController@clearSudo');
                Route::get('/sudo', 'SudoController@sudo');
                Route::post('/sudoAsEmp', 'SudoController@sudoAsEmpPost');

                Route::get('/jobseeker/export', 'JobseekerController@exportJobseeker');
                Route::post('/jobseeker/export', 'JobseekerController@exportJobseeker');

                Route::get('/exportJobseeker/view', 'JobseekerController@viewJobseekerTargetjob');
                Route::post('/exportJobseeker/view', 'JobseekerController@searchTargetjob');
                Route::post('/exportJobseeker/export', 'JobseekerController@exportTargetjob');

                Route::get('/exportJobseekerExp/view', 'JobseekerController@viewJobseekerExperience');
                Route::post('/exportJobseekerExp/view', 'JobseekerController@searchExperience');
                Route::post('/exportJobseekerExp/export', 'JobseekerController@exportExperience');

                Route::get('/employer/search', 'EmployerController@searchEmployer');
                Route::post('/employer/search', 'EmployerController@searchEmployer');

                Route::resource('/register', 'RegisterController');
                Route::get('/jobseeker/register', 'RegisterController@jobseeker');
                Route::post('/jobseeker/register', 'RegisterController@jobseekerRegisterAjax');
                Route::post('/jobseeker/checkValidateUserInfo', 'RegisterController@checkValidateUserInfoAjax');
                Route::post('/jobseeker/checkValidateExperience', 'RegisterController@checkValidateExperienceAjax');

                Route::get('/jobseeker/login', 'LoginController@login');
                Route::post('/jobseeker/login', 'LoginController@postLogin');

                //Employer
                Route::get('/employer/register', 'RegisterEmployerController@registerRep');
                Route::post('/employer/register', 'RegisterEmployerController@registerRepPost');

                Route::get('/employer/registerCompany', 'CompanyRepController@registerCompany');
                Route::post('/employer/registerCompany', 'CompanyRepController@registerCompanyPost');

                Route::get('/companyProfile/additional/', 'CompanyRepController@additional');
                Route::post('/companyProfile/additional/', 'CompanyRepController@additionalPost');

//                Route::get('/employer/register/success', 'RegisterController@registerSuccess');
//                Route::post('/employer/register', 'RegisterController@employerRegisterAjax');
//                Route::post('/employer/checkValidateCompanyRep', 'RegisterController@checkValidateCompanyRepAjax');
//                Route::post('/employer/checkValidateCompanyPro', 'RegisterController@checkValidateCompanyProAjax');

                Route::get('/employer/login', 'LoginController@loginEmp');
                Route::post('/employer/login', 'LoginController@postLoginEmp');

                Route::get('/searchCandidate', 'TargetjobController@searchCandidate');
                Route::post('/searchCandidate', 'TargetjobController@searchCandidate');

                Route::get('/searchCandidate/{jobseekerID}', 'TargetjobController@viewCandidate');
                Route::get('/postjob/viewCandidate/{jobseekerID}/{jobId}', 'TargetjobController@viewCandidate');

                //User
                Route::resource('/user', 'UserController');
                Route::get('/userProfile', 'UserController@viewProfile');
                Route::get('/userProfile/edit', 'UserController@editUserProfile');

                //JobSeeker
                Route::get('/jobseeker/search', 'JobseekerController@search');
                Route::post('/jobseeker/search', 'JobseekerController@search');
                Route::resource('/jobseeker', 'JobseekerController');
                Route::resource('/experience', 'ExperienceController');

                //Employer ,company
                Route::get('/repEmployer/{pkCompaniesID}', 'EmployerController@repEmployer');
                Route::post('/repEmployer/{pkCompaniesID}', 'EmployerController@repEmployer');
                Route::resource('/employer', 'EmployerController');

                //Roles
                Route::resource('/role', 'RoleController');

                //Sectors
                Route::resource('/sector', 'SectorController');

                //Subsectors
                Route::get('/subsector/search', 'SubsectorController@searchSubsector');
                Route::post('/subsector/search', 'SubsectorController@searchSubsector');
                Route::resource('/subsector', 'SubsectorController');


                //Activity
                Route::get('/getActivities', 'ActivityController@getActivitiesAjax');
                Route::post('/activity/searchActivity', 'ActivityController@searchActivity');
                Route::get('/activity/searchActivity', 'ActivityController@searchActivity');
                Route::resource('/activity', 'ActivityController');

                // Country || Settings
                Route::resource('/country', 'CountryController');
                Route::resource('/province', 'ProvinceController');
                Route::resource('/district', 'DistrictController');
                Route::resource('/commune', 'CommuneController');
                Route::resource('/village', 'VillageController');

                // Position
                Route::get('/position/search', 'PositionController@searchPosition');
                Route::post('/position/search', 'PositionController@searchPosition');
                Route::resource('/position', 'PositionController');

                //Route::resource('/role', 'RoleController', ['except' => ['show']]);

                //Zone
                Route::resource('/zone', 'ZoneController');

                //Languages
                Route::resource('/language', 'LanguageController');

                //ajax get data .........
                Route::get('/getProvinces', 'ProvinceController@getProvincesAjax');
                Route::get('/getDistricts', 'DistrictController@getDistrictsAjax');
                Route::get('/getCountJobByDistrict', 'DistrictController@getCountJobByDistrictAjax');
                Route::get('/getCommunes', 'CommuneController@getCommunesAjax');
                Route::get('/getCountJobByCommune', 'CommuneController@getCountJobByCommuneAjax');
                Route::get('/getVillages', 'VillageController@getVillagesAjax');

                Route::get('/getSectors', 'SectorController@getSectorsAjax');
                Route::get('/getSubsectors', 'SubsectorController@getSubsectorsAjax');
                Route::get('/getPositions', 'PositionController@getPositionsAjax');

                Route::get('/getSubsectorsBySectorsID', 'CompanyRepController@getSubsectorsBySectorsIDAjax');
                Route::get('/getActivitiesBySubsectorsID', 'CompanyRepController@getActivitiesBySubsectorsIDAjax');

                Route::get('/getAutocompleteCompanies', 'CompanyController@getAutocompleteCompaniesAjax');
                Route::get('/getCompanyByID', 'RegisterController@getCompanyByIDAjax');
                Route::get('/getFrmCompany', 'CompanyController@getFrmCompanyAjax');

                //#end ajax get data .........

                //Company
                Route::get('company/search', 'CompanyController@search');
                Route::post('company/search', 'CompanyController@search');
                Route::resource('/company', 'CompanyController');
                Route::get('/companyProfile', 'CompanyController@companyProfile');
                Route::get('/companyProfile/edit', 'CompanyController@editCompanyProfile');

                //Job
                Route::get('/jobs/{provinceName}/{sectorName}/{positionName}/{pkAnnouncementsID}', 'JobController@jobAnnouncementText');


                // Export announcements
                Route::get('/exportAnnouncement/announcement', 'AdminController@viewAnnouncementsExport');
                Route::post('/exportAnnouncement/search', 'AdminController@searchAnnouncement');
                Route::get('/exportAnnouncement/search', 'AdminController@searchAnnouncement');
                Route::get('/exportAnnouncement/export', 'AdminController@exportAnnouncement');
                Route::post('/exportAnnouncement/export', 'AdminController@exportAnnouncement');

                // Export Applicants
                Route::get('/expShareJob', 'AdminController@viewShareJob');
                Route::post('/expShareJob/search', 'AdminController@searchShareJob');
                Route::get('/expShareJob/search', 'AdminController@searchShareJob');
                Route::get('/expShareJob/export', 'AdminController@exportShareJob');
                Route::post('/expShareJob/export', 'AdminController@exportShareJob');

                Route::get('/jobPublish/{pkAnnouncementsID}', 'PostjobController@jobPublish');
                Route::get('/jobUnpublish/{pkAnnouncementsID}', 'PostjobController@jobUnpublish');
                Route::get('/postjob/application/{pkAnnouncementsID}', 'PostjobController@applicationByjob');

                //tab_application
                Route::get('/postjob/application/new/{pkAnnouncementsID}', 'PostjobController@appNew');
                Route::get('/postjob/application/all_/{pkAnnouncementsID}', 'PostjobController@appAll');
                Route::get('/postjob/application/interesting/{pkAnnouncementsID}', 'PostjobController@appInteresting');
                Route::get('/postjob/application/callToInterview/{pkAnnouncementsID}', 'PostjobController@appCallToInterview');
                Route::get('/postjob/application/callAndRejected/{pkAnnouncementsID}', 'PostjobController@appCallAndRejected');
                Route::get('/postjob/application/rejected/{pkAnnouncementsID}', 'PostjobController@appRejected');
                Route::get('/postjob/application/hired/{pkAnnouncementsID}', 'PostjobController@appHired');

                Route::put('/postjob/application/interesting/check', 'PostjobController@interestingAjax');
                Route::put('/postjob/application/jobApplyCallToInterview/check', 'PostjobController@jobApplyCallToInterviewAjax');
                Route::put('/postjob/application/jobApplyRejected/check', 'PostjobController@jobApplyRejectedAjax');
                Route::put('/postjob/application/jobApplyHired/check', 'PostjobController@jobApplyHiredAjax');
                Route::put('/postjob/application/jobApplyCallAndReject/check', 'PostjobController@jobApplyCallAndRejectAjax');
                // end tab_application


                Route::resource('/postjob', 'PostjobController');
                Route::get('/postjob/searchCandidate/{pkAnnouncementsID}', 'PostjobController@searchCandidate');
                Route::post('/postjob/askForApply', 'CompanyRepController@candidatePost');

                Route::get('/targetJob/edit', 'TargetjobController@editTargetJob');
                Route::post('/editTargetJob', 'TargetjobController@updateTargetJob');
                Route::resource('/targetJob', 'TargetjobController');

                //Job Applied
                Route::get('/postjob/userInfo/{pkAnnouncementsID}', 'JobappliedController@show');
                Route::resource('/jobApplied', 'JobappliedController');

                //CompanyRep
                Route::get('/deleteCompanyRep/{userId}', 'CompanyRepController@deleteCompanyRep');
                Route::get('/approveOrUnapproveCompanyRep/{userId}', 'CompanyRepController@approveOrUnapproveCompanyRep');

                // Page
                Route::resource('/page', 'PageController');
                Route::get('/getPagesSubTitles', 'PageController@getPagesSubTitlesAjax');

                //General Seeker Controller
                Route::get('/seekerApplied', 'SeekerController@seekerApplied');
                Route::get('/message', 'SeekerController@message');

                //General Admin Controller
                //Company
                Route::get('/company/job/edit/{pkAnnouncementsID}', 'AdminController@editJob');
                Route::post('/company/job/update/{pkAnnouncementsID}', 'AdminController@updateJob');
                Route::get('/company/job/delete/{pkAnnouncementsID}', 'AdminController@deleteJob');
                Route::get('/company/view/{companyId}', 'AdminController@viewCompany');
                Route::get('/company/postjob/{companyId}', 'AdminController@postJob');
                Route::post('/company/postjob/{companyId}', 'AdminController@postJobPost');
                Route::get('/company/repost/{pkAnnouncementsID}', 'AdminController@repost');


                Route::get('/company/jobPublish/{pkAnnouncementsID}', 'AdminController@jobPublish');
                Route::get('/company/jobUnpublish/{pkAnnouncementsID}', 'AdminController@jobUnpublish');

                //All Applicants
                Route::get('/applicants/export', 'AdminController@exportApplicant');
                Route::post('/applicants/export', 'AdminController@exportApplicant');
                Route::get('/applicants', 'AdminController@applicants');
                Route::get('/applicants/search', 'AdminController@searchApplicants');
                Route::post('/applicants/search', 'AdminController@searchApplicants');

                //All Applicants Register and Not Register
                Route::get('/allApplicants/export', 'AdminController@exportApplicantRegisterAndNot');
                Route::post('/allApplicants/export', 'AdminController@exportApplicantRegisterAndNot');
                Route::get('/allApplicants', 'AdminController@applicantRigisterAndNotRegister');
                Route::get('/allApplicants/search', 'AdminController@searchApplicantRegisterAndNot');
                Route::post('/allApplicants/search', 'AdminController@searchApplicantRegisterAndNot');

                Route::get('/checkmp3/company', 'AdminController@checkMp3Company');
                Route::get('/checkmp3/subsector', 'AdminController@checkMp3Subsector');
                Route::get('/checkmp3/activity', 'AdminController@checkMp3Activity');
                Route::get('/checkmp3/position', 'AdminController@checkMp3Position');

                Route::get('/exportCompany/company', 'AdminController@viewCompaniesExport');
                Route::post('/exportCompany/search', 'AdminController@searchCompany');
                Route::get('/exportCompany/search', 'AdminController@searchCompany');
                Route::get('/exportCompany/export', 'AdminController@exportCompany');
                Route::post('/exportCompany/export', 'AdminController@exportCompany');

            });

            //SocialAuth
            Route::get('/redirect', 'SocialAuthController@redirect');
            Route::get('/callback', 'SocialAuthController@callback');

            Route::get('/redirectEmp', 'SocialAuthController@redirectEmp');

            // Completed
            Route::get(
                'success',
                function () {
                    return view('success');
                }
            );
        });
    });

Route::group(['prefix' => 'api/v1', 'middleware' => ['cors','auth:api']], function () {
    Route::post('/storeSharedRecord', ['as' => 'store-share-record', 'uses' => 'BongPheakAPIController@storeSharedRecord']);
    Route::post('/storeApplyRecord', ['as' => 'store-apply-record', 'uses' => 'BongPheakAPIController@storeApplyRecord']);
    Route::post('/confirmApplyRecord', ['as' => 'confirm-apply-record', 'uses' => 'BongPheakAPIController@confirmApplyRecord']);
});

Route::post('/postDataForm', 'JobController@postData');
