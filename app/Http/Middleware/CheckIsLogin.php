<?php

namespace App\Http\Middleware;

use App\UserLog;
use Closure;
use Illuminate\Support\Facades\Log;
use Auth;

class CheckIsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        UserLog::IsLoggedLogin();
        return $response;
    }
}
