<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Auth;
use SebastianBergmann\Environment\Console;

class CheckIsSale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (Auth::check()) {
            $urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $segments = explode('/', $urlArray);
            $allowRoute = config("constants.ALLOW_SALE");
            Log::info( $allowRoute );
            $user = \Illuminate\Support\Facades\Auth::user();
            if( $user->IsSale == true ){
                if( isset( $segments[3] ) && !in_array( $segments[3], $allowRoute) ){
                    if(!is_numeric($segments[3])){
                        return redirect(\Lang::getLocale().'/error');
                    }
                }
            }
        }
        return $response;
    }
}
