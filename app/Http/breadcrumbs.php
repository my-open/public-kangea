<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push(trans('text_lang.home'), url(\Lang::getLocale() . '/'));
});

// Job
Breadcrumbs::register('jobsList', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('text_lang.jobsList'), url(\Lang::getLocale() . '/jobs'));
});

// Job Detail
Breadcrumbs::register('jobDetail', function($breadcrumbs)
{
    $breadcrumbs->parent('jobsList');
    $breadcrumbs->push(trans('text_lang.jobDetail'));
});

// Company Detail
Breadcrumbs::register('companyDetail', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('text_lang.companyDetail'));
});

// Account
Breadcrumbs::register('account', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('text_lang.account'), url( \Lang::getLocale() . '/account'));
});

// Role
Breadcrumbs::register('role', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.role'), url( \Lang::getLocale() . '/account/role'));
});
// Role Create
Breadcrumbs::register('roleCreate', function($breadcrumbs) {
    $breadcrumbs->parent('role');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Role Update
Breadcrumbs::register('roleUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('role');
    $breadcrumbs->push(trans('text_lang.update'));
});

// User
Breadcrumbs::register('user', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.user'), url( \Lang::getLocale() . '/account/user'));
});
// User Create
Breadcrumbs::register('userCreate', function($breadcrumbs) {
    $breadcrumbs->parent('user');
    $breadcrumbs->push(trans('text_lang.create'));
});
// User Update
Breadcrumbs::register('userUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('user');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Jobseeker
Breadcrumbs::register('jobseeker', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.jobseeker'), url( \Lang::getLocale() . '/account/jobseeker'));
});
// Jobseeker Create
Breadcrumbs::register('jobseekerCreate', function($breadcrumbs) {
    $breadcrumbs->parent('jobseeker');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Jobseeker Update
Breadcrumbs::register('jobseekerUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('jobseeker');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Employer
Breadcrumbs::register('employer', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.employer'), url( \Lang::getLocale() . '/account/employer'));
});
// Employer Create
Breadcrumbs::register('employerCreate', function($breadcrumbs) {
    $breadcrumbs->parent('employer');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Employer Update
Breadcrumbs::register('employerUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('employer');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Company
Breadcrumbs::register('company', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.company'), url( \Lang::getLocale() . '/account/company'));
});
// Company Create
Breadcrumbs::register('companyCreate', function($breadcrumbs) {
    $breadcrumbs->parent('company');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Company Update
Breadcrumbs::register('companyUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('company');
    $breadcrumbs->push(trans('text_lang.update'));
});
// companyProfile
Breadcrumbs::register('companyProfile', function($breadcrumbs) {
    $breadcrumbs->parent('company');
    $breadcrumbs->push(trans('text_lang.profile'));
});

// Sector
Breadcrumbs::register('sector', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.sector'), url( \Lang::getLocale() . '/account/sector'));
});
// Sector Create
Breadcrumbs::register('sectorCreate', function($breadcrumbs) {
    $breadcrumbs->parent('sector');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Sector Update
Breadcrumbs::register('sectorUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('sector');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Subsector
Breadcrumbs::register('subsector', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.subsector'), url( \Lang::getLocale() . '/account/subsector'));
});
// Subsector Create
Breadcrumbs::register('subsectorCreate', function($breadcrumbs) {
    $breadcrumbs->parent('subsector');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Subsector Update
Breadcrumbs::register('subsectorUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('subsector');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Activity
Breadcrumbs::register('activity', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.activity'), url( \Lang::getLocale() . '/account/activity'));
});
// Activity Create
Breadcrumbs::register('activityCreate', function($breadcrumbs) {
    $breadcrumbs->parent('activity');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Activity Update
Breadcrumbs::register('activityUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('activity');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Country || Settings
Breadcrumbs::register('country', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.country'), url( \Lang::getLocale() . '/account/country'));
});
// Country create
Breadcrumbs::register('countryCreate', function($breadcrumbs) {
    $breadcrumbs->parent('country');
    $breadcrumbs->push(trans('text_lang.create'));
});
// Country Update
Breadcrumbs::register('countryUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('country');
    $breadcrumbs->push(trans('text_lang.update'));
});

// province
Breadcrumbs::register('province', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.province'), url( \Lang::getLocale() . '/account/province'));
});

// province create
Breadcrumbs::register('provinceCreate', function($breadcrumbs) {
    $breadcrumbs->parent('province');
    $breadcrumbs->push(trans('text_lang.create'));
});

// province Update
Breadcrumbs::register('provinceUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('province');
    $breadcrumbs->push(trans('text_lang.update'));
});

// district
Breadcrumbs::register('district', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.district'), url( \Lang::getLocale() . '/account/district'));
});

// district create
Breadcrumbs::register('districtCreate', function($breadcrumbs) {
    $breadcrumbs->parent('district');
    $breadcrumbs->push(trans('text_lang.create'));
});

// district Update
Breadcrumbs::register('districtUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('district');
    $breadcrumbs->push(trans('text_lang.update'));
});

// commune
Breadcrumbs::register('commune', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.commune'), url( \Lang::getLocale() . '/account/commune'));
});

// commune create
Breadcrumbs::register('communeCreate', function($breadcrumbs) {
    $breadcrumbs->parent('commune');
    $breadcrumbs->push(trans('text_lang.create'));
});

// commune Update
Breadcrumbs::register('communeUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('commune');
    $breadcrumbs->push(trans('text_lang.update'));
});

// village
Breadcrumbs::register('village', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.village'), url( \Lang::getLocale() . '/account/village'));
});

// village create
Breadcrumbs::register('villageCreate', function($breadcrumbs) {
    $breadcrumbs->parent('village');
    $breadcrumbs->push(trans('text_lang.create'));
});

// village Update
Breadcrumbs::register('villageUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('village');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Position
Breadcrumbs::register('position', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.position'), url( \Lang::getLocale() . '/account/position'));
});

// Position create
Breadcrumbs::register('positionCreate', function($breadcrumbs) {
    $breadcrumbs->parent('position');
    $breadcrumbs->push(trans('text_lang.create'));
});

// Position Update
Breadcrumbs::register('positionUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('position');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Zone
Breadcrumbs::register('zone', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.zone'), url( \Lang::getLocale() . '/account/zone'));
});

// Zone Create
Breadcrumbs::register('zoneCreate', function($breadcrumbs) {
    $breadcrumbs->parent('zone');
    $breadcrumbs->push(trans('text_lang.create'));
});

// Zone Update
Breadcrumbs::register('zoneUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('zone');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Language
Breadcrumbs::register('language', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.language'), url( \Lang::getLocale() . '/account/language'));
});

// Language Create
Breadcrumbs::register('languageCreate', function($breadcrumbs) {
    $breadcrumbs->parent('language');
    $breadcrumbs->push(trans('text_lang.create'));
});

// Language Update
Breadcrumbs::register('languageUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('language');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Postjob Create
Breadcrumbs::register('postjob', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.allMyAnnouncements'), url( \Lang::getLocale() . '/account/postjob'));
});

// Postjob Create
Breadcrumbs::register('postjobCreate', function($breadcrumbs) {
    $breadcrumbs->parent('postjob');
    $breadcrumbs->push(trans('text_lang.create'));
});

// Postjob Update
Breadcrumbs::register('postjobUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('postjob');
    $breadcrumbs->push(trans('text_lang.update'));
});


// Experience
Breadcrumbs::register('experience', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.experience'), url( \Lang::getLocale() . '/account/experience'));
});

// Experience create
Breadcrumbs::register('experienceCreate', function($breadcrumbs) {
    $breadcrumbs->parent('experience');
    $breadcrumbs->push(trans('text_lang.create'));
});

// Experience Update
Breadcrumbs::register('experienceUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('experience');
});

// Search Job or targetJob
Breadcrumbs::register('targetJob', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.targetJob'), url( \Lang::getLocale() . '/account/targetJob'));
});
//  create Search Job
Breadcrumbs::register('targetJobCreate', function($breadcrumbs) {
    $breadcrumbs->parent('targetJob');
    $breadcrumbs->push(trans('text_lang.create'));
});

// Update Search Job
Breadcrumbs::register('targetJobUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('targetJob');
    $breadcrumbs->push(trans('text_lang.update'));
});

// Search Candidate
Breadcrumbs::register('searchCandidate', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.searchForCandidate'), url( \Lang::getLocale() . '/account/searchCandidate'));
});

// View Cadidate
Breadcrumbs::register('viewCandidate', function($breadcrumbs) {
    $breadcrumbs->parent('searchCandidate');
    $breadcrumbs->push(trans('text_lang.viewCandidate'), url( \Lang::getLocale() . '/account/viewCandidate'));
});

// Job Applied
Breadcrumbs::register('jobApplied', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.jobApplied'), url( \Lang::getLocale() . '/account/searchCandidate'));
});

// page
Breadcrumbs::register('page', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.page'), url( \Lang::getLocale() . '/account/page'));
});

// page create
Breadcrumbs::register('pageCreate', function($breadcrumbs) {
    $breadcrumbs->parent('page');
    $breadcrumbs->push(trans('text_lang.create'));
});

// page Update
Breadcrumbs::register('pageUpdate', function($breadcrumbs) {
    $breadcrumbs->parent('page');
    $breadcrumbs->push(trans('text_lang.update'));
});

// applicant
Breadcrumbs::register('applicants', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.applicants'), url( \Lang::getLocale() . '/account/applicants'));
});

// applicant Not register
Breadcrumbs::register('applicantRegisterAndNotRegister', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.applicantRegisterAndNotRegister'), url( \Lang::getLocale() . '/account/all'));
});

// Message
Breadcrumbs::register('message', function($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push(trans('text_lang.allMessages'), url( \Lang::getLocale() . '/account/message'));
});

// Dashboard
Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('text_lang.dashboard'), url( \Lang::getLocale() . '/dashboard'));
});
Breadcrumbs::register('accumulative', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('text_lang.accumulative'), url( \Lang::getLocale() . '/dashboard/accumulative'));
});
Breadcrumbs::register('accumulativeAllSector', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('text_lang.accumulative'), url( \Lang::getLocale() . '/dashboard/accumulativeAllSector'));
});
Breadcrumbs::register('newChart', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('text_lang.new'), url( \Lang::getLocale() . '/dashboard/new'));
});
Breadcrumbs::register('active', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(trans('text_lang.active'), url( \Lang::getLocale() . '/dashboard/activeChart'));
});