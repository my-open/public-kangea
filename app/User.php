<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable
{
    use EntrustUserTrait;

    protected $table = 'users';

    protected $fillable = ['fkPositionsID','name', 'phone', 'email', 'password', 'gender', 'address', 'verified', 'status', 'companyLevel', 'experience','api_token', 'IsSale', 'EmpPhone'];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
