<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'Languages';
    protected $primaryKey = 'pkLanguagesID';
    protected $fillable = ['languagesName', 'languagesStatus'];
}
