<?php
/*
Author = Sorn Sea
Modified = Sorn Sea
email = sorn.sea@gmail.com
Created Date = Thursday 17, March 2016
Modified Date = Thursday 17, March 2016
*/

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['user_id', 'company_code', 'name_en', 'name_kh', 'nickname', 'number_of_workders', 'sector_id', 'subsector_id', 'zone', 'address', 'x_coordinate', 'y_coordinate', 'description_en', 'description_kh', 'job_interest_sector', 'job_interest_subsector', 'job_interest_province', 'job_interest_city', 'phone_number'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
