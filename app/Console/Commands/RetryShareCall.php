<?php

namespace App\Console\Commands;

use App\Shares;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleHttpClient;

class RetryShareCall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retryCall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'retryCall description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->toDateString();
        $time = Carbon::now()->toTimeString();

        $shares = Shares::where('sharesRetryDate', $date)->where('sharesRetryTime', '<=', $time)->where('sharesStatus', 0)->select('*')->get();
        $api_token = config("constants.API_TOKEN");
        if( count($shares) > 0){
            foreach ($shares as $share) {
                $applyRecord = json_decode($share->sharesApplyRecord);
                $soundUrl = "http://". env('HTTP_HOST') ."/files/sounds/kh/jobs/job_".$share->fkAnnouncementsID.".mp3";

                $data = ["sharesGender" => $share->sharesGender, "sharesName" => $share->sharesName,
                    "sharesPhone" => $share->sharesPhone, "sharesWho" => $share->sharesWho,
                    "fkAnnouncementsID" => $share->fkAnnouncementsID, "soundUrl" => $soundUrl,
                    "fkUsersID" => $applyRecord->fkUsersID, "fkCompaniesID" => $applyRecord->fkCompaniesID, "fkPositionsID" => $applyRecord->fkPositionsID, "api_token" => $api_token ];
                $data = json_encode($data, JSON_FORCE_OBJECT);

                // Using laravel php libray GuzzleHttp for execute external API
                $headers = ['Content-Type' => 'application/json'];
                $client = new GuzzleHttpClient();
                $rel = $client->request('POST', config("constants.REQUEST_IVR_API_SHARE"), ['headers'=>$headers,'body' => $data]);
                if( $rel ){
                    Shares::where('pkSharesID', '=', $share->pkSharesID)
                        ->update([ 'sharesStatus' => '1' ]); // 1 = Mean has called
                }
            }
        }
        return true;
    }
}
