<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleHttpClient;
use Symfony\Component\HttpFoundation\Request;
use App\Postjob;

class MergeJobSound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mergeJobSound';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'mergeJobSound description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dir    = public_path('files/sounds/kh/confirmJobs');
        $allFiles = array_diff(scandir($dir), array('..', '.'));

        $jobIDs = [];
        foreach ( $allFiles as $index => $value )
        {
            $subString = substr($value, 11);
            $jobID = str_replace('.mp3','', $subString);
            $jobIDs[] = $jobID;
        }

        $date = Carbon::now()->toDateString().' 00:00:00';
        $jobs = Postjob::where('announcementsClosingDate', '>', $date)
            ->whereNotIn('pkAnnouncementsID', $jobIDs)
            ->get();

        $mJob = new Postjob();
        foreach ( $jobs as $job )
        {
            $mJob->convertConfirmJobApplyMp3( $job );
            $mJob->convertConfirmJobApplyNoCompanyMp3($job);
            $mJob->convertJobMp3( $job );
        }
        return true;
    }
}
