<?php

namespace App\Console\Commands;

use App\JobApply;
use App\Shares;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleHttpClient;

class RetryNoAnswerCall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retryNoAnswerCall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'retryNoAnswerCall description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$data = JobApply::retryCall();
        //Log::info($data);
        return true;
    }
}
