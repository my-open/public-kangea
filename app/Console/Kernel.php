<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\RetryShareCall::class,
        Commands\MergeJobSound::class,
        Commands\RetryNoAnswerCall::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->->daily();
        $schedule->command('mergeJobSound')->everyFiveMinutes();
        $schedule->command('retryCall')->everyMinute();
        $schedule->command('retryNoAnswerCall')->everyMinute();
    }
}
