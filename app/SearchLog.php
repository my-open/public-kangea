<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchLog extends Model
{
    protected $table = 'SearchLogs';
    protected $primaryKey = 'pkSearchLogsID';
    protected $fillable = ['fkProvincesID', 'fkDistrictsID', 'fkCommunesID', 'fkSectorsID', 'fkSubsectorsID', 'fkActivitiesID', 'fkPositionsID', 'fkUsersID', 'fkAnnouncementsID', 'searchLogsIP', 'searchLogsBrowser', 'searchLogsOS', 'searchLogsDeviceType', 'searchLogsDeviceModel', 'searchLogsStatus'];
}
